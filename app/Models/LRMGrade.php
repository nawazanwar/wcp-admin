<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class LRMGrade extends Model implements Permissions
{
    protected $fillable = [
        'name', 'name_ur'
    ];

    protected $table = 'lrmgrades';

    public function types()
    {
        return $this->belongsToMany(Type::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_lrm_grade');
                    break;
                case 'create':
                case 'store':
                    return array('create_lrm_grade');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_lrm_grade');
                    break;
                case 'delete':
                    return array('delete_lrm_grade');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_lrm_grade',
            'create_lrm_grade',
            'edit_lrm_grade',
            'delete_lrm_grade',
        );
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class Message extends Model implements Permissions
{
    protected $fillable = ['name', 'value'];
    protected $table = 'messages';

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_message');
                    break;
                case 'create':
                case 'store':
                    return array('create_message');
                    break;
            }

        }

        return array(
            'read_message',
            'create_message',
        );
    }
}

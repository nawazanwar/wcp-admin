<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class Employer extends Model implements Permissions
{
    protected $fillable = [
        'name', 'name_ur', 'active_salary', 'active', 'type_id'
    ];
    protected $table = 'employers';

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_employer');
                    break;
                case 'create':
                case 'store':
                    return array('create_employer');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_employer');
                    break;
                case 'delete':
                    return array('delete_employer');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_employer',
            'create_employer',
            'edit_employer',
            'delete_employer',
        );
    }
}

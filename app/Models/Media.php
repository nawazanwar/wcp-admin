<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Media extends Model implements Permissions
{
    /**
     * @var string[]
     */

    protected $fillable = [
        'src', 'type'
    ];

    /**
     * @return BelongsToMany
     */
    public function hrm()
    {
        return $this->belongsToMany(HRM::class, 'hrm_media');
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'import':
                    return array('import_media');
                    break;
                case 'export':
                    return array('export_media');
                    break;
                case 'read':
                    return array('read_media');
                    break;
                case 'create':
                case 'store':
                    return array('create_media');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_media');
                    break;
                case 'delete':
                    return array('delete_media');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'import_media',
            'export_media',
            'read_media',
            'create_media',
            'edit_media',
            'delete_media',
        );
    }
}

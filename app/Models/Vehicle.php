<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class Vehicle extends Model implements Permissions
{
    protected $table = 'vehicles';


    //Default one owner (reverse-one-to-many)
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //Many medias (many-to-many relationship)
    public function media()
    {
        return $this->hasMany(Vmedia::class);
    }

    // belongs to some type
    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function getAvatar()
    {
        return asset("uploads/vehicles/avatar/" . $this->media()->where('type', 'avatar')->value('src'));
    }


    public function getDocsImages()
    {
        return $this->media()->where('type', 'vehicle_docs')->get();
    }

    public function getVehicleImages()
    {
        return $this->media()->where('type', 'vehicle_images')->get();
    }

    public function getType()
    {
        return $this->type->label;
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'change_status':
                    return array('change_status_vehicle');
                    break;
                case 'import':
                    return array('import_vehicle');
                    break;
                case 'export':
                    return array('export_vehicle');
                    break;
                case 'read':
                    return array('read_vehicle');
                    break;
                case 'create':
                case 'store':
                    return array('create_vehicle');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_vehicle');
                    break;
                case 'delete':
                    return array('delete_vehicle');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'import_vehicle',
            'export_vehicle',
            'read_vehicle',
            'create_vehicle',
            'edit_vehicle',
            'delete_vehicle',
            'change_status_vehicle'
        );
    }
}

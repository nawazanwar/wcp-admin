<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class LRMApplication extends Model implements Permissions
{
    protected $fillable = [
        'type'
    ];

    protected $table = 'lrmapplications';

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_lrm_application');
                    break;
                case 'create':
                case 'store':
                    return array('create_lrm_application');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_lrm_application');
                    break;
                case 'delete':
                    return array('delete_lrm_application');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_lrm_application',
            'create_lrm_application',
            'edit_lrm_application',
            'delete_lrm_application',
        );
    }
}

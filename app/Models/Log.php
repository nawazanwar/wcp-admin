<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;
use Illuminate\Support\Facades\DB;

class Log extends Model implements Permissions
{
    protected $fillable = ['name'];
    protected $dates = ['created_at', 'updated_at', 'date_time'];
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'import':
                    return array('import_log');
                    break;
                case 'export':
                    return array('export_log');
                    break;
                case 'read':
                    return array('read_log');
                    break;
                case 'create':
                case 'store':
                    return array('create_log');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_log');
                    break;
                case 'delete':
                    return array('delete_log');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'import_log',
            'export_log',
            'read_log',
            'create_log',
            'edit_log',
            'delete_log',
        );
    }
}

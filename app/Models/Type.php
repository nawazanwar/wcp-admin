<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class Type extends Model implements Permissions
{
    protected $fillable = ['name', 'name_ur', 'parent'];
    protected $table = 'types';

    //One user (one-to-one relationship)
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function visitorlogs()
    {
        return $this->hasMany(VisitorLog::class);
    }

    public function departments()
    {
        return $this->hasMany(Department::class);
    }

    //Many Vehicles (one to many relationship)
    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    }

    //Many Family Persons (one to many relationship)
    public function familypersons()
    {
        return $this->hasMany(FamilyPerson::class);
    }

    public function designations()
    {
        return $this->belongsToMany(Designation::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'import':
                    return array('import_type');
                    break;
                case 'export':
                    return array('export_type');
                    break;
                case 'read':
                    return array('read_type');
                    break;
                case 'create':
                case 'store':
                    return array('create_type');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_type');
                    break;
                case 'delete':
                    return array('delete_type');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'import_type',
            'export_type',
            'read_type',
            'create_type',
            'edit_type',
            'delete_type',
        );
    }
}

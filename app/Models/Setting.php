<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Setting extends Model implements Permissions
{
    protected $table = "settings";

    protected $fillable = [
        'name', 'name_ur', 'short_name', 'short_name_ur'
    ];

    public function getMaintenance()
    {
        return DB::table('settings')->value('maintenance');
    }

    public function getSiteName()
    {
        $fieldName = 'name';
        if (session()->has('locale') && session()->get('locale') != 'en') {
            $fieldName = $fieldName . "_" . session()->get('locale');
        }
        return DB::table('settings')->value($fieldName);
    }

    public function getShortSiteName()
    {
        $fieldName = 'short_name';
        if (session()->has('locale') && session()->get('locale') != 'en') {
            $fieldName = $fieldName . "_" . session()->get('locale');
        }
        return DB::table('settings')->value($fieldName);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_setting');
                    break;
                case 'create':
                case 'store':
                    return array('create_setting');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_setting');
                    break;
                case 'delete':
                    return array('delete_setting');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_setting',
            'create_setting',
            'edit_setting',
            'delete_setting',
        );
    }
}

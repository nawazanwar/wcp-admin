<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GateLocation extends Model
{
    protected $table = 'gatelocations';
    protected $fillable = [
        'name', 'name_ur', 'gate_id'
    ];

    public function gate()
    {
        return $this->belongsTo(Gate::class);
    }

    public function devices()
    {
        return $this->hasMany(Device::class);
    }
}

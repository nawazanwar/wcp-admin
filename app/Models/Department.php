<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class Department extends Model implements Permissions
{
    protected $fillable = [
        'name', 'name_ur', 'active_salary', 'active', 'type_id'
    ];
    protected $table = 'departments';

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_department');
                    break;
                case 'create':
                case 'store':
                    return array('create_department');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_department');
                    break;
                case 'delete':
                    return array('delete_department');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_department',
            'create_department',
            'edit_department',
            'delete_department',
        );
    }
}

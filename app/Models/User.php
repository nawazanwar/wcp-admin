<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable implements Permissions
{

    use SoftDeletes;
    use Notifiable;

    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $fillable = ['name', 'email', 'active', 'password'];
    protected $table = 'users';
    protected $dates = ['deleted_at'];

    public function countPermissions()
    {
        $count = 0;
        foreach ($this->roles()->get() as $role) {
            $count += $role->permissions()->count();
        }
        return $count;
    }

    /**
     * @return HasMany
     */
    public function media()
    {
        return $this->hasMany(Umedia::class);
    }

    /**
     * @return BelongsToMany
     */

    public function logs()
    {
        return $this->belongsToMany(Log::class);
    }

    /**
     * @return BelongsToMany
     */

    public function designations()
    {
        return $this->belongsToMany(Designation::class);
    }

    /**
     * @return string
     */

    public function getAvatar()
    {
        return asset("uploads/users/avatar/" . $this->value('avatar'));
    }

    public function hasDesignation($role)
    {
        if (is_string($role)) {
            return $this->designations->contains('slug', $role);
        }

        return !!$role->intersect($this->designations);

    }

    public function ability($permission = null)
    {
        return !is_null($permission) && $this->checkPermission($permission);
    }


    protected function checkPermission($permission)
    {
        $permissions = $this->getAllPermissionsFromAllDesignations();

        if ($permissions === true) {
            return true;
        }

        $permissionArray = is_array($permission) ? $permission : [$permission];
        return count(array_intersect($permissions, $permissionArray));
    }

    protected function getAllPermissionsFromAllDesignations()
    {
        $permissions = array();
        $designations = $this->designations->load('permissions');

        if (!$designations) {
            return true;
        }

        foreach ($designations as $designation) {
            foreach ($designation->permissions as $permission) {
                $permissions[] = $permission->toArray();
            }
        }

        return array_map('strtolower', array_unique($this->array_flatten(array_map(function ($permission) {
            return $permission['name'];
        }, $permissions))));

    }

    function array_flatten($array)
    {
        if (!is_array($array)) {
            return FALSE;
        }
        $result = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result = array_merge($result, array_flatten($value));
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_user');
                    break;
                case 'create':
                case 'store':
                    return array('create_user');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_user');
                    break;
                case 'delete':
                    return array('delete_user');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'export_user',
            'read_user',
            'create_user',
            'edit_user',
            'delete_user',
        );
    }
}

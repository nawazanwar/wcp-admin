<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class LRMDepartment extends Model implements Permissions
{
    protected $fillable = [
        'name', 'name_ur'
    ];

    protected $table = 'lrmdepartments';

    public function types()
    {
        return $this->belongsToMany(Type::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_lrm_department');
                    break;
                case 'create':
                case 'store':
                    return array('create_lrm_department');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_lrm_department');
                    break;
                case 'delete':
                    return array('delete_lrm_department');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_lrm_department',
            'create_lrm_department',
            'edit_lrm_department',
            'delete_lrm_department',
        );
    }
}

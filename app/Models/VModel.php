<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class VModel extends Model implements Permissions
{
    protected $fillable = ['name', 'label', 'vcompany_id'];
    protected $table = 'vmodels';

    public function vcompany()
    {
        return $this->belongsTo(VCompany::class);
    }
    public function VisitorLog()
    {
        return $this->hasMany(VisitorLog::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_company_model');
                    break;
                case 'create':
                case 'store':
                    return array('create_company_model');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_company_model');
                    break;
                case 'delete':
                    return array('delete_company_model');
                    break;
                default:
                    return array();
            }
        }
        return array(
            'read_company_model',
            'create_company_model',
            'edit_company_model',
            'delete_company_model'
        );
    }
}

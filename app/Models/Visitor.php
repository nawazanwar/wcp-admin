<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Visitor extends Authenticatable implements Permissions
{
    protected $fillable = [
        'booth_id',
        'visitor_name',
        'visitor_cnic',
        'visitor_cell_no',
        'purpose_of_visit',
        'type_id',
        'type_id',
        'vcompany_id',
        'vmodel_id',
        'vnature_id',
        'vehicle_color',
        'vehicle_reg_no',
        'vehicle_reg_year',
        'no_of_males',
        'no_of_females',
        'no_of_kids',
        'visitor_biometric_verification',
        'visitor_ratina_verification',
        'visitor_gate_pass_verification',
        'visitor_qrcode_verification',
        'call_visitee',
        'verified',
        'follow',
        'guide',
        'warn',
        'opened_barrier_number',
        'permitted',
        'exit',
        'operator_id',
        'is_enter',
        'is_exit',
        'first_camera_image',
        'second_camera_image',
        'third_camera_image',
        'fourth_camera_image',
        'updated_by'
    ];

    protected $table = 'visitors';

    public function booth()
    {
        return $this->belongsTo(Booth::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function vcompany()
    {
        return $this->belongsTo(VCompany::class);
    }

    public function vmodel()
    {
        return $this->belongsTo(VModel::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'export':
                    return array('export_visitor');
                    break;
                case 'read':
                    return array('read_visitor');
                    break;
                case 'create':
                case 'store':
                    return array('create_visitor');
                default:
                    return array();
            }

        }

        return array(
            'export_visitor',
            'read_visitor',
            'create_visitor',
        );
    }
}

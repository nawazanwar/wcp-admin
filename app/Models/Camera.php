<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Camera extends Model implements Permissions
{
    //Belongs to gate (reverse-one-to-many)
    public function gates()
    {
        return $this->belongsTo(Gates::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_camera');
                    break;
                default:
                    return array();
            }
        }
        return array('read_camera');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class HRM extends Model implements Permissions
{

    use SoftDeletes;

    protected $fillable = [
        'relation_eng',
        'relation_ur',
        'relation_name_eng',
        'relation_name_ur',
        'f_name',
        'f_name_ur',
        'l_name',
        'l_name_ur',
        'cast',
        'cast_ur',
        'first_picture',
        'second_picture',
        'third_picture',
        'fourth_picture',
        'user_id',
        'cnic',
        'cnic_expiry_date',
        'cnic_qr_code',
        'passport',
        'date_of_birth',
        'blood_group',
        'gender',
        'contact_number',
        'secondary_contact_number',
        'secondary_contact_person',
        'temp_city',
        'temp_city_ur',
        'temp_address',
        'temp_address_ur',
        'perm_city',
        'perm_city_ur',
        'perm_address',
        'perm_address_ur',
        'pres_city',
        'pres_city_ur',
        'pres_address',
        'pres_address_ur',
        'card_type',
        'card_issue_date',
        'card_expiry_date',
        'biometric_lh_thumb_verification',
        'biometric_rh_thumb_verification',
        'biometric_lh_index_verification',
        'biometric_rh_index_verification',
        'biometric_lh_middle_verification',
        'biometric_rh_middle_verification',
        'biometric_lh_ring_verification',
        'biometric_rh_ring_verification',
        'biometric_lh_pinky_verification',
        'biometric_rh_pinky_verification',
        'department_id',
        'employer_id',
        'emp_type'
    ];

    protected $table = 'hrms';

    /**
     * @return BelongsToMany
     */
    public function media()
    {
        return $this->belongsToMany(Media::class, 'hrm_media');
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    /**
     * @return BelongsTo
     */
    public function employer()
    {
        return $this->belongsTo(Employer::class);
    }

    /**
     * @return BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    /**
     * @param false $middleware
     * @param null $route
     * @return array|string[]
     */
    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_hrm');
                    break;
                case 'create':
                case 'store':
                    return array('create_hrm');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_hrm');
                    break;
                case 'delete':
                    return array('delete_hrm');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_hrm',
            'create_hrm',
            'edit_hrm',
            'delete_hrm',
        );
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class VRS extends Model implements Permissions
{

    protected $fillable = [

    ];

    protected $table = 'vrs';

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_vrs');
                    break;
                case 'create':
                case 'store':
                    return array('create_vrs');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_vrs');
                    break;
                case 'delete':
                    return array('delete_vrs');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_vrs',
            'create_vrs',
            'edit_vrs',
            'delete_vrs',
        );
    }
}

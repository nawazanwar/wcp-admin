<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\SoftDeletes;

class LRM extends Model implements Permissions
{

    use SoftDeletes;

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_land_record_form');
                    break;
                case 'create':
                case 'store':
                    return array('create_land_record_form');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_land_record_form');
                    break;
                case 'delete':
                    return array('delete_land_record_form');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_land_record_form',
            'create_land_record_form',
            'edit_land_record_form',
            'delete_land_record_form',
        );
    }
}

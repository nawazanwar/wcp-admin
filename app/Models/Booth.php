<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class Booth extends Model implements Permissions
{

    protected $table = 'booths';

    protected $fillable = [
        'gate_id', 'lane', 'type', 'active'
    ];

    public function gate()
    {
        return $this->belongsTo(Gate::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_booth');
                    break;
                case 'create':
                case 'store':
                    return array('create_booth');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_booth');
                    break;
                case 'delete':
                    return array('delete_booth');
                    break;
                case 'operate':
                    return array('operate_booth');
                    break;
                case 'import':
                    return array('import_booth');
                    break;
                case 'export':
                    return array('export_booth');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_booth',
            'create_booth',
            'edit_booth',
            'delete_booth',
            'operate_booth',
            'import_booth',
            'export_booth',
        );
    }
}

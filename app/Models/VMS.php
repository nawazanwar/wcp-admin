<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class VMS extends Model implements Permissions
{

    protected $fillable = [

    ];

    protected $table = 'vms';

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_vms');
                    break;
                case 'create':
                default:
                    return array();
            }

        }

        return array(
            'read_vms'
        );
    }
}

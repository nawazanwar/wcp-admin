<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class Gate extends Model implements Permissions
{
    protected $fillable = [
        'name', 'name_ur', 'gate_id'
    ];
    protected $table = 'gates';

    public function gatelocations()
    {
        return $this->hasMany(GateLocation::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_gate');
                    break;
                case 'create':
                case 'store':
                    return array('create_gate');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_gate');
                    break;
                case 'delete':
                    return array('delete_gate');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_gate',
            'create_gate',
            'edit_gate',
            'delete_gate',
        );
    }
}

<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Location extends Model implements Permissions
{
    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_location');
                    break;
                case 'create':
                case 'store':
                    return array('create_location');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_location');
                    break;
                case 'delete':
                    return array('delete_location');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_location',
            'create_location',
            'edit_location',
            'delete_location',
        );
    }
}

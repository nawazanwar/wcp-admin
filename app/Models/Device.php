<?php

namespace App\Models;

use App\Device\DClass;
use App\Device\DLocation;
use App\Device\DMake;
use App\Device\DModel;
use App\Device\DOperatingSystem;
use App\Device\DType;
use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class Device extends Model implements Permissions
{

    protected $table = 'devices';

    protected $fillable = [
        'dtype_id',
        'dclass_id',
        'dmodel_id',
        'dmake_id',
        'booth_id',
        'doperatingsystem_id',
        'dev_name',
        'dev_name_ur',
        'dev_id',
        'dev_serial_no',
        'dev_ip_address',
        'dev_mac_address',
        'dev_admin_login',
        'dev_admin_pwd',
        'dev_user_login',
        'dev_user_pwd',
        'dev_lot_no',
        'dev_lot_date',
        'dev_other_info',
        'dev_connection_string'
    ];

    public function getAvatar()
    {
        return $this->value('dev_pic');
    }

    public function class()
    {
        return $this->belongsTo(DClass::class);
    }

    public function dtype()
    {
        return $this->belongsTo(DType::class);
    }

    public function dmake()
    {
        return $this->belongsTo(DMake::class);
    }

    public function dmodel()
    {
        return $this->belongsTo(DModel::class);
    }

    public function system()
    {
        return $this->belongsTo(DOperatingSystem::class);
    }

    public function booth()
    {
        return $this->belongsTo(Booth::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_device');
                    break;
                case 'create':
                case 'store':
                    return array('create_device');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_device');
                    break;
                case 'delete':
                    return array('delete_device');
                    break;
                case 'show':
                    return array('show_device');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_device',
            'create_device',
            'edit_device',
            'delete_device',
            'show_device',
        );
    }
}

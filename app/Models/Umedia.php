<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Umedia extends Model
{
    protected $table = 'umedias';
    protected $dates = ['created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

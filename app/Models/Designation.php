<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class Designation extends Model implements Permissions
{
    protected $fillable = [
        'name', 'name_ur', 'shift', 'active'
    ];
    protected $table = 'designations';

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_designation');
                    break;
                case 'create':
                case 'store':
                    return array('create_designation');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_designation');
                    break;
                case 'delete':
                    return array('delete_designation');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_designation',
            'create_designation',
            'edit_designation',
            'delete_designation',
        );
    }
}

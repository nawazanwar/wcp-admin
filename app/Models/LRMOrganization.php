<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class LRMOrganization extends Model implements Permissions
{
    protected $fillable = [
        'name', 'name_ur'
    ];

    protected $table = 'lrmorganizations';

    public function types()
    {
        return $this->belongsToMany(Type::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_lrm_organization');
                    break;
                case 'create':
                case 'store':
                    return array('create_lrm_organization');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_lrm_organization');
                    break;
                case 'delete':
                    return array('delete_lrm_organization');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_lrm_organization',
            'create_lrm_organization',
            'edit_lrm_organization',
            'delete_lrm_organization',
        );
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class LRMWing extends Model implements Permissions
{
    protected $fillable = [
        'name', 'name_ur'
    ];

    protected $table = 'lrmwings';

    public function types()
    {
        return $this->belongsToMany(Type::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_lrm_wing');
                    break;
                case 'create':
                case 'store':
                    return array('create_lrm_wing');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_lrm_wing');
                    break;
                case 'delete':
                    return array('delete_lrm_wing');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_lrm_wing',
            'create_lrm_wing',
            'edit_lrm_wing',
            'delete_lrm_wing',
        );
    }
}

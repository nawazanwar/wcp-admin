<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class Notification extends Model implements Permissions
{
    protected $table = 'notifications';
    protected $fillable = [
        'reader_id', 'type', 'notifiable_type', 'notifiable_id', 'data', 'read_reason', 'read_at'
    ];

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'call_visitee_at_booth_notification':
                    return array('call_visitee_at_booth_notification');
                    break;
                case 'visitor_verified_at_booth_notification':
                    return array('visitor_verified_at_booth_notification');
                    break;
                case 'visitor_follow_at_booth_notification':
                    return array('visitor_follow_at_booth_notification');
                    break;
                case 'visitor_guide_at_booth_notification':
                    return array('visitor_guide_at_booth_notification');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'call_visitee_at_booth_notification',
            'visitor_verified_at_booth_notification',
            'visitor_follow_at_booth_notification',
            'visitor_guide_at_booth_notification'
        );
    }

}

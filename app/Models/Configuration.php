<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Configuration extends Model implements Permissions
{

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_configuration');
                    break;
                case 'create':
                case 'store':
                    return array('create_configuration');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_configuration');
                    break;
                case 'delete':
                    return array('delete_configuration');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_configuration',
            'create_configuration',
            'edit_configuration',
            'delete_configuration',
        );
    }
}

<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class House extends Model implements Permissions
{
    protected $table = 'houses';

    //Default one owner (reverse-one-to-many)
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //Default one type (reverse-one-to-many)
    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    //Many medias (one-to-many relationship)
    public function media()
    {
        return $this->hasMany(Hmedia::class);
    }

    public function families()
    {
        return $this->hasMany(Family::class);
    }

    public function getAvatar()
    {
        return asset("uploads/houses/avatar/" . $this->media()->where('type', 'avatar')->value('src'));
    }

    public function totalFamilies()
    {
        return $this->families()->count();
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'change_status':
                    return array('change_status_house');
                    break;
                case 'import':
                    return array('import_house');
                    break;
                case 'export':
                    return array('export_house');
                    break;
                case 'read':
                    return array('read_house');
                    break;
                case 'create':
                case 'store':
                    return array('create_house');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_house');
                    break;
                case 'delete':
                    return array('delete_house');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'import_house',
            'export_house',
            'read_house',
            'create_house',
            'edit_house',
            'delete_house',
            'change_status_house'
        );
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hmedia extends Model
{
    protected $table = 'hmedias';
    protected $dates = ['created_at', 'updated_at'];

    public function house()
    {
        return $this->belongsTo(House::class);
    }
}

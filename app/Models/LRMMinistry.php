<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class LRMMinistry extends Model implements Permissions
{
    protected $fillable = [
        'name', 'name_ur','type_id'
    ];
    protected $table = 'lrmministries';

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_lrm_ministry');
                    break;
                case 'create':
                case 'store':
                    return array('create_lrm_ministry');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_lrm_ministry');
                    break;
                case 'delete':
                    return array('delete_lrm_ministry');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_lrm_ministry',
            'create_lrm_ministry',
            'edit_lrm_ministry',
            'delete_lrm_ministry',
        );
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LrmSettings extends Model
{
    protected $fillable = ['name', 'parent'];
    protected $table ='lrmsettings';
}

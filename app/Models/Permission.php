<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model implements Permissions
{

    use SoftDeletes;
    protected $fillable = ['name', 'name_ur'];

    public function designations()
    {
        return $this->belongsToMany(Designation::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'import':
                    return array('import_permission');
                    break;
                case 'export':
                    return array('export_permission');
                    break;
                case 'read':
                    return array('read_permission');
                    break;
                case 'create':
                case 'store':
                    return array('create_permission');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_permission');
                    break;
                case 'delete':
                    return array('delete_permission');
                    break;
                case 'restore':
                    return array('restore_permission');
                    break;
                case 'sync':
                    return array('sync_permission');
                    break;
                case 'assign':
                    return array('assign_permission');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'import_permission',
            'export_permission',
            'read_permission',
            'create_permission',
            'edit_permission',
            'delete_permission',
            'sync_permission',
            'assign_permission',
            'restore_permission'
        );
    }

    public static function syncPermissions()
    {

        $models = self::getModels(app_path() . "/Models", '\App\Models\\');
        $permissions = array();
        foreach ($models as $model) {
            if (method_exists($model, 'modulePermissions')) {
                $permissions = array_merge($permissions, $model::modulePermissions());
            }
        }

        $existingPermissions = self::all()->pluck('name')->toArray();

        $permissionsToBeDeleted = array_diff($existingPermissions, $permissions);
        $permissionsToBeAdded = array_diff($permissions, $existingPermissions);

        if (!empty($permissionsToBeDeleted)) {
            Permission::whereIn('name', $permissionsToBeDeleted)->delete();
        }

        foreach ($permissionsToBeAdded as $permission) {
            Permission::create([
                'name' => $permission,
                'name_ur' => $permission,
            ]);
        }

        return true;

    }

    /**
     * Get all model names with namespace from given path
     *
     * @param $path path to models directory
     * @param $namespace namespace for models
     * @return array model names with namespace
     */
    private static function getModels($path, $namespace)
    {
        $models = [];
        $results = scandir($path);

        foreach ($results as $result) {

            if ($result === '.' or $result === '..')
                continue;

            $filename = $path . '/' . $result;
            if (is_dir($filename)) {
                $models = array_merge($models, self::getModels($filename));
            } else {
                $models[] = substr($namespace . $result, 0, -4);
            }
        }

        return $models;
    }
}

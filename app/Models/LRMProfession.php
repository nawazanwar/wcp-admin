<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class LRMProfession extends Model implements Permissions
{
    protected $fillable = [
        'name', 'name_ur'
    ];

    protected $table = 'lrmprofessions';

    public function types()
    {
        return $this->belongsToMany(Type::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_lrm_profession');
                    break;
                case 'create':
                case 'store':
                    return array('create_lrm_profession');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_lrm_profession');
                    break;
                case 'delete':
                    return array('delete_lrm_profession');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_lrm_profession',
            'create_lrm_profession',
            'edit_lrm_profession',
            'delete_lrm_profession',
        );
    }
}

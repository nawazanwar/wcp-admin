<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class LRMRank extends Model implements Permissions
{
    protected $fillable = [
        'name', 'name_ur'
    ];

    protected $table = 'lrmranks';

    public function types()
    {
        return $this->belongsToMany(Type::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_lrm_rank');
                    break;
                case 'create':
                case 'store':
                    return array('create_lrm_rank');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_lrm_rank');
                    break;
                case 'delete':
                    return array('delete_lrm_rank');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_lrm_rank',
            'create_lrm_rank',
            'edit_lrm_rank',
            'delete_lrm_rank',
        );
    }
}

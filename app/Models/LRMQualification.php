<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class LRMQualification extends Model implements Permissions
{
    protected $fillable = [
        'name', 'name_ur'
    ];

    protected $table = 'lrmqualifications';

    public function types()
    {
        return $this->belongsToMany(Type::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_lrm_qualification');
                    break;
                case 'create':
                case 'store':
                    return array('create_lrm_qualification');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_lrm_qualification');
                    break;
                case 'delete':
                    return array('delete_lrm_qualification');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_lrm_qualification',
            'create_lrm_qualification',
            'edit_lrm_qualification',
            'delete_lrm_qualification',
        );
    }
}

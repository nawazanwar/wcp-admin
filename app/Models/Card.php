<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class Card extends Model implements Permissions
{
    protected $fillable = ['name', 'barcode', 'rfid'];

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_card');
                    break;
                case 'create':
                case 'store':
                    return array('create_card');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_card');
                    break;
                case 'delete':
                    return array('delete_card');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_card',
            'create_card',
            'edit_card',
            'delete_card',
        );
    }
}

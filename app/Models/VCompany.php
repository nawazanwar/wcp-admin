<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class VCompany extends Model implements Permissions
{
    protected $fillable = ['name', 'name_ur', 'type_id'];
    protected $table = 'vcompanies';

    public function type()
    {
        return $this->belongsTo(Type::class);
    }
    public function VisitorLog()
    {
        return $this->hasMany(VisitorLog::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_vehicle_company');
                    break;
                case 'create':
                case 'store':
                    return array('create_vehicle_company');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_vehicle_company');
                    break;
                case 'delete':
                    return array('delete_vehicle_company');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_vehicle_company',
            'create_vehicle_company',
            'edit_vehicle_company',
            'delete_vehicle_company',
        );
    }
}

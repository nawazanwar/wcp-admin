<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vmedia extends Model
{
    protected $table = 'vmedias';
    protected $dates = ['created_at', 'updated_at'];

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }
}

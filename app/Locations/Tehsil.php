<?php

namespace App\Location;

use Illuminate\Database\Eloquent\Model;

class Tehsil extends Model
{
    protected $table = 'tehsils';
    protected $fillable = ['name'];
}

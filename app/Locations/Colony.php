<?php

namespace App\Location;

use Illuminate\Database\Eloquent\Model;

class Colony extends Model
{
    protected $table = 'colonies';
    protected $fillable = ['name'];
}

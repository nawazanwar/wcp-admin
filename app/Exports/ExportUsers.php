<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\DB;

class ExportUsers implements FromCollection, WithHeadings
{
    public function collection()
    {
        return DB::table('users')
            ->join('types', 'users.type', '=', 'types.id')
            ->select(
                'users.id as id',
                'users.name as name',
                'users.email as email',
                'types.name as type',
                DB::raw('(CASE
                        WHEN users.active = "1" THEN "Active"
                        ELSE "De Active"
                        END) AS status'),
                'users.created_at as Created Date',
                'users.updated_at as Updated Date')
            ->get();
    }

    public function headings(): array
    {
        return [
            'id',
            'name',
            'email',
            'type',
            'status',
            'Created Date',
            'Updated Date'
        ];
    }
}

<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\DB;

class ExportVisitors implements FromCollection, WithHeadings
{
    public function collection()
    {
        return DB::table('users')
            ->join('types', 'users.type_id', '=', 'types.id')
            ->where('types.name', 'visitor')
            ->select(
                'users.id as id',
                'users.first_name as first_name',
                'users.middle_name as middle_name',
                'users.last_name as last_name',
                'users.parent_first_name as parent_first_name',
                'users.parent_middle_name as parent_middle_name',
                'users.parent_last_name as parent_last_name',
                'users.cnic as cnic',
                'types.name as type',
                'users.phone_number as phone_number',
                'users.cell_number as cell_number',
                'users.reference_phone_number as reference_phone_number',
                'types.name as type',
                'users.created_at as Created Date')
            ->get();
    }

    public function headings(): array
    {
        return [
            'id',
            'first_name',
            'middle_name',
            'last_name',
            'parent_first_name',
            'parent_middle_name',
            'parent_last_name',
            'cnic',
            'type',
            'phone_number',
            'cell_number',
            'reference_phone_number',
            'Created Date'
        ];
    }
}

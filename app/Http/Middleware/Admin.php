<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{

    public function handle($request, Closure $next, $guard = null)
    {
        if(Auth::user() && (Auth::user()->hasRole('super-admin'))) {
            return $next($request);
        }
        return redirect('/');
    }
}

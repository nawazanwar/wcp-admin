<?php

namespace App\Http\Middleware;

use App\Models\Setting;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;

class Maintenance
{

    public function handle($request, Closure $next)
    {
        $setting = new Setting();
        if (Schema::hasTable('settings')) {
            $maintenance = $setting->getMaintenance();
            if ($maintenance == '1') {
                abort(403, __('message.maintenance_mode'));
            }
        }
        return $next($request);
    }
}

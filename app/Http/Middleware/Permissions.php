<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class Permissions
{
    public function handle($request, Closure $next)
    {
        // get route permissions
        $route = Route::getRoutes()->match($request);
        $actions = $route->getAction();
        $permissions = isset($actions['permissions']) ? $actions['permissions'] : array();
        // no permissions? allow access
        if (!$permissions) {
            return $next($request);
        } else {
            $user = Auth::user();
            if ($user && $user->ability($permissions)) {
                return $next($request);
            }
        }
        return redirect()->route('dashboard')
            ->withErrors('No Permission to access this route,so please contact with admin for access');
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class BoothLogin
{

    public function handle($request, Closure $next, $guard = null)
    {
        $mac = strtok(exec('getmac'), ' ');
        $session_booth = session()->get('booth_login');
        if (Auth::guard('operator')->user() && ($mac == $session_booth)) {
            return $next($request);
        }
        return redirect('/boothLogin');
    }
}

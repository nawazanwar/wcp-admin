<?php

namespace App\Http\Controllers\VRS;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Type;
use App\Models\VCompany;
use App\Models\VModel;
use App\Repositories\CurdRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VehicleModelController extends Controller
{

    protected $model;

    public function __construct(VModel $VModel)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($VModel);
    }


    public function index(Request $request)
    {
        $this->authorize('read', VModel::class);
        $pageTitle = __('vrs.list_all_vehicle_models');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'data' => $this->model->all($request->all())->paginate(12)
        ];
        return view('vrs.vehicle-models.index', $viewParams);
    }

    public function create()
    {
        $this->authorize('create', VModel::class);
        $vcompanies = VCompany::all();
        $pageTitle = __('vrs.create_new_vehicle_model');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'vcompanies' => $vcompanies,
        ];
        return view('vrs.vehicle-models.create', $viewParams);
    }

    public function store(Request $request)
    {
        $this->authorize('create', VModel::class);
        $request->validate([
            'name' => 'required|max:255',
            'vehicle_company' => 'required',
        ]);

        $vCompany = VCompany::find($request->input('vehicle_company', null));
        $name = $request->input('name', null);
        $name_ur = $request->input('name_ur', null);
        $this->model->updateOrCreate(
            ['name' => $name, 'vcompany_id' => $vCompany->id],
            ['name' => $name, 'vcompany_id' => $vCompany->id, 'name_ur' => $name_ur]);
        return redirect()
            ->route('vrs-vehicle-models.index')
            ->with('successMessage', "Model created or Updated successfully");
    }

    public function destroy($id)
    {
        $this->authorize('delete', VModel::class);
        $this->model = $this->model->find($id);
        if ($this->model->delete($id)) {
            return redirect()->route('vrs-vehicle-models.index')->with('errorMessage', $this->model->name . " deleted Successfully");
        }
    }
}

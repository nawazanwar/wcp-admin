<?php

namespace App\Http\Controllers\VRS;

use App\Http\Controllers\Controller;
use App\Models\Employer;
use App\Models\Type;
use App\Repositories\CurdRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VehicleTypeController extends Controller
{
    protected $model;

    public function __construct(Type $type)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($type);
    }

    public function index(Request $request)
    {
        $this->authorize('read', Type::class);
        $data = $this->model->all($request->all());
        $data = $data->where('parent', 'vehicle')->orderBy('types.id', 'desc')->get();
        $pageTitle = __('vrs.all_types_of_vehicle');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'data' => $data
        ];
        return view('vrs.vehicle-types.index', $viewParams);
    }

    public function store(Request $request)
    {
        $this->authorize('create', Type::class);
        $input = $request->all();
        $length = count($input['name']);
        for ($i = 0; $i < $length; $i++) {
            $name = $input['name'][$i];
            $name_ur = $input['name_ur'][$i];
            $id = $input['id'][$i];
            if (isset($id) && !empty($id)) {
                $this->model->updateOrCreate(
                    ['id' => $id, 'parent' => 'vehicle'],
                    ['name' => $name, 'name_ur' => $name_ur, 'parent' => 'vehicle']
                );
            }
        }//end of the for loop
        return redirect()->route("vrs-vehicle-types.index")->with('successMessage', "Vehicle types  created/updated successfully");
    }
}

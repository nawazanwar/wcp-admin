<?php

namespace App\Http\Controllers\VRS;

use App\Http\Controllers\Controller;
use App\Models\Vehicle;
use App\Models\VRS;
use App\Repositories\CurdRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $model;

    public function __construct(VRS $VRS)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($VRS);
    }

    public function index(Request $request)
    {
        $this->authorize('read', VRS::class);
        $pageTitle = __('vrs.title');
        $breadcrumbs = [['text' => __('vrs.short_title')]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
        ];
        return view('vrs.index', $viewParams);
    }

}

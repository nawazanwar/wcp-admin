<?php

namespace App\Http\Controllers\VRS;
use App\Http\Controllers\Controller;
use App\Models\Vehicle;
use App\Repositories\CurdRepository;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    protected  $model;
    public function __construct(Vehicle $vehicle)
    {
        $this->middleware('auth');
        $this->model = new CurdRepository($vehicle);
    }

    public function index(Request $request)
    {

    }

    public function create()
    {

        $pageTitle = __('vrs.vehicle.register');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
        ];

        return view('vrs.vehicles.create', $viewParams);
    }

    public function store(Request $request)
    {
        $request->validate([

        ]);
        $vehicle = new Vehicle();
        $vehicle->active = true;

        $this->makeMultipleDirectories('vehicles', ['avatar', 'docs','vehicle_images']);
        $vehicle = new Vehicle();

        /*set the vehicle type*/
       // $type = $request->input('type', null);
        //$vehicle->type()->associate(Type::where('id', $type)->first());

        /*vehicle info*/

        $vehicle->reg_no_series = $request->input('reg_no_series');
        $vehicle->reg_no = $request->input('reg_no');
        $vehicle->reg_no_year = $request->input('reg_no_year');
        $vehicle->reg_on_name = $request->input('reg_on_name');
        $vehicle->model_year = $request->input('model_year');
        $vehicle->model_month = $request->input('model_month');
        $vehicle->vehicle_company = $request->input('vehicle_company');
        $vehicle->vehicle_model = $request->input('vehicle_model');
        $vehicle->vehicle_driven_by = $request->input('vehicle_driven_by');
        $vehicle->chasis_number = $request->input('chasis_number');
        $vehicle->engine_number = $request->input('engine_number');
        $vehicle->color = $request->input('color');
        $vehicle->vehicle_gencode =  Hash::make($request->input('vehicle_gencode'));
        $vehicle->type = $request->input('type');


        $vehicle->added_by = '12';

        if ($vehicle->save()) {

            if ($request->file('avatar')) {
                $this->manageImage($vehicle, $request->file('avatar'), 'avatar', uniqid());
            }
            //save docs images
            if ($request->file('docs')) {
                $counter = 0;
                foreach ($request->file('docs') as $image) {
                    $this->manageImage($vehicle, $image, 'docs', $counter);
                    $counter++;
                }
            }
            //save vehicle images
            if ($request->file('vehicle_images')) {
                $counter = 0;
                foreach ($request->file('vehicle_images') as $image) {
                    $this->manageImage($vehicle, $image, 'vehicle_images', $counter);
                    $counter++;
                }
            }
            return redirect()->route('vehicles')->with('successMessage', __('message.vehicle_created_success_message'));
        } else {
            return redirect()->back()->withErrors(__('message.vehicle_created_failed_message'))->withInput();
        }
    }

    public function manageImage($vehicle, $image, $type, $uniqueId)
    {
        $imageName = sha1($type . "" . $uniqueId . "" . $vehicle->id . time()) . '.' . $image->getClientOriginalExtension();
        $newImage = Image::make($image);
        $newImage->save(public_path("uploads/vehicles/" . $type . "/" . $imageName));
        $media = new Vmedia();
        $media->src = $imageName;
        $media->type = $type;
        $vehicle->media()->save($media);
    }

    public function show(Request $request, $id)
    {
        $model = Vehicle::find($this->decrypt($id));

        $pageTitle = __('system.detail_of') . " " . $model->name;
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle
        ];
        return view('vehicle.show', $viewParams);
    }

    public function edit($id)
    {
        $model = Vehicle::find($this->decrypt($id));
        $pageTitle = __('system.edit') . " " . $model->name;
        $breadcrumbs = [['text' => __('system.edit_vehicle')]];
        $viewParams = [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle
        ];

        return view('vehicle.edit', $viewParams);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|min:4|unique:roles,name,' . $id,
            'label' => 'required|max:255',
        ]);


        if (Role::whereId($id)->update($validatedData)) {
            return redirect()->route('system.roles')->with('successMessage', $validatedData['label'] . " " . __('system.updated_success_message'));
        } else {
            return redirect()->back()
                ->withErrors($validatedData['label'] . " " . __('system.updated_failed_message_message'))
                ->withInput();
        }
    }

    public function destroy($id)
    {
        $model = Vehicle::find($this->decrypt($id));
        if ($model->delete()) {
            return redirect()->route('vehicles')->with('errorMessage', $model->name . " " . __('system.deleted_successfully'));
        }

    }

}

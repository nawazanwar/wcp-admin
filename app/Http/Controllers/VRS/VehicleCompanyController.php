<?php

namespace App\Http\Controllers\VRS;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Type;
use App\Models\VCompany;
use App\Repositories\CurdRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VehicleCompanyController extends Controller
{

    protected $model;

    public function __construct(VCompany $VCompany)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($VCompany);
    }


    public function index(Request $request)
    {
        $this->authorize('read', VCompany::class);
        $pageTitle = __('vrs.list_all_vehicle_companies');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'data' => $this->model->all($request->all())->paginate(12)
        ];
        return view('vrs.vehicle-companies.index', $viewParams);
    }

    public function create()
    {
        $this->authorize('create', VCompany::class);
        $types = Type::where('parent', 'vehicle')->get();
        $pageTitle = __('vrs.create_new_vehicle_company');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'types' => $types,
        ];
        return view('vrs.vehicle-companies.create', $viewParams);
    }

    public function store(Request $request)
    {
        $this->authorize('create', VCompany::class);
        $request->validate([
            'name' => 'required|max:255',
            'vehicle_type' => 'required',
        ]);

        $type = Type::find($request->input('vehicle_type', null));
        $name = $request->input('name', null);
        $name_ur = $request->input('name_ur', null);
        $this->model->updateOrCreate(
            ['name' => $name, 'type_id' => $type->id],
            ['name' => $name, 'type_id' => $type->id, 'name_ur' => $name_ur]);
        return redirect()
            ->route('vrs-vehicle-companies.index')
            ->with('successMessage', "Companies created or Updated successfully");
    }

    public function destroy($id)
    {
        $this->authorize('delete', VCompany::class);
        $this->model = $this->model->find($id);
        if ($this->model->delete($id)) {
            return redirect()->route('vrs-vehicle-companies.index')->with('errorMessage', $this->model->name . " deleted Successfully");
        }
    }
}

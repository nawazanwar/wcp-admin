<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Booth;
use App\Models\Device;
use App\Models\Log;
use App\Models\Notification;
use App\Models\Visitor;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $cLoginUser = Auth::user();
        if ($cLoginUser->hasDesignation('monitoring-cell-operator')) {
            $pageTitle = __('monitoring-cell.dashboard');
            $booths = Booth::join('gates', 'gates.id', 'booths.gate_id');
            if ($request->has('s') && $request->get('s') != '') {
                $keyword = $request->get('s', null);
                $booths = $booths->where(function ($q) use ($keyword) {
                    $q->where('booths.lane', 'like', '%' . $keyword . '%')
                        ->orWhere('booths.type', 'like', '%' . $keyword . '%')
                        ->orWhere('gates.name', 'like', '%' . $keyword . '%')
                        ->orWhere('gates.slug', 'like', '%' . $keyword . '%');
                });
            }
            if ($request->has('booth') && $request->get('booth') != '') {
                $filterBooth = $request->get('booth', null);
                $booths = $booths->where('booths.id', $filterBooth);
            }
            if ($request->has('gate') && $request->get('gate') != '') {
                $filterGate = $request->get('gate', null);
                $booths = $booths->where('gates.id', $filterGate);
            }
            $booths = $booths->select('booths.*', 'gates.name as gate')->get();
            $data = array();
            foreach ($booths as $booth) {
                $unreadNotifications = Notification::whereDate('created_at', Carbon::today())
                    ->where('data->booth_id', $booth->id)
                    ->whereNotifiableId(\auth()->user()->id)
                    ->whereReadAt(null)->count();
                $readNotifications = Notification::whereDate('created_at', Carbon::today())
                    ->where('data->booth_id', $booth->id)
                    ->whereNotifiableId(\auth()->user()->id)
                    ->where('read_at', '!=', null)->count();
                $data[$booth->id]['read'] = $readNotifications;
                $data[$booth->id]['unread'] = $unreadNotifications;
                $data[$booth->id]['visitors'] = Visitor::where('booth_id', $booth->id)->count();
                $data[$booth->id]['cameras'] = Device::where('booth_id', $booth->id)->count();
            }
            return view('monitoring-cell.dashboard', compact('pageTitle', 'data', 'booths'));
        } else {
            $pageTitle = __('dashboard.welcome_back_title');
            return view('dashboard.index', compact('pageTitle'));
        }
    }
}

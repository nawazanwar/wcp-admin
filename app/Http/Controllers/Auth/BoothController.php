<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Booth;
use App\Models\Gate;
use App\Models\Log;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BoothController extends Controller
{
    public function login()
    {
        $pageTitle = __('auth.login_title');
        return view('auth.booth-login', compact('pageTitle'));
    }

    public function customBoothLogin(Request $request)
    {
        $request->validate([
            'unique_id' => 'required|min:5|max:191',
            'password' => 'required|string|min:4|max:255',
            'booth' => 'required'
        ]);
        $credentials = $request->only('unique_id', 'password');
        $credentials['active'] = true;
        if (Auth::guard('operator')->attempt($credentials)) {
            session(['booth_login' => $request->input('mac_address')]);
            $log = Log::updateOrCreate(['name' => 'booth'], ['name' => 'booth']);
            DB::table('booth_logs')->insert(
                [
                    'ip_address' => $request->ip_address,
                    'mac_address' => $request->mac_address,
                    'operator_id' => Auth::guard('operator')->user()->id,
                    'log_id' => $log->id,
                    'type' => 'login',
                    'booth_id' => $request->booth,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]
            );
            return redirect()->route('booth.manage');
        }

    }

    public function logout(Request $request)
    {
        $log = Log::updateOrCreate(['name' => 'booth'], ['name' => 'booth']);
        $mac_address = strtok(exec('getmac'), ' ');
        DB::table('booth_logs')->insert(
            [
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'mac_address' => $mac_address,
                'operator_id' => Auth::guard('operator')->user()->id,
                'log_id' => $log->id,
                'type' => 'logout',
                'booth_id' => $request->booth_id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        );

        Auth::guard('operator')->logout();
        session()->remove('booth_login');
        return redirect()->route('booth.login');

    }

}

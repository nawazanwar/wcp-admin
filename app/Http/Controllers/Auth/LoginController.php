<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        $pageTitle = __('auth.login_title');
        return view('auth.login', compact('pageTitle'));
    }

    public function customLogin(Request $request)
    {
        $request->validate([
            'unique_id' => 'required|min:5|max:191',
            'password' => 'required|string|min:4|max:255'
        ]);

        $credentials = $request->only('unique_id', 'password');
        $credentials['active'] = true;
        if (Auth::attempt($credentials)) {
            return redirect()->route('dashboard');
        } else {
            return redirect()->route('login');
        }
    }

    public function logout(Request $request)
    {

        Auth::logout();
        return redirect()->route('login');
    }
}

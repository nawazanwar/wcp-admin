<?php

namespace App\Http\Controllers\Location;

use App\Http\Controllers\Controller;
use App\Location\State;
use Illuminate\Http\Request;

class StateController extends Controller
{
    public function store(Request $request)
    {
        if ($request->is_ajax) {

            State::updateOrCreate(
                [
                    'name' => $request->name
                ],
                [
                    'name' => $request->name
                ]
            );
        }
    }
}

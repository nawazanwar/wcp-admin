<?php

namespace App\Http\Controllers\Location;

use App\Http\Controllers\Controller;
use App\Location\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public function store(Request $request)
    {
        if ($request->is_ajax) {

            City::updateOrCreate(
                [
                    'name' => $request->name
                ],
                [
                    'name' => $request->name
                ]
            );
        }
    }
}

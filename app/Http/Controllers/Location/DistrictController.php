<?php

namespace App\Http\Controllers\Location;

use App\Http\Controllers\Controller;
use App\Location\District;
use Illuminate\Http\Request;

class DistrictController extends Controller
{
    public function store(Request $request)
    {
        if ($request->is_ajax) {

            District::updateOrCreate(
                [
                    'name' => $request->name
                ],
                [
                    'name' => $request->name
                ]
            );
        }
    }
}

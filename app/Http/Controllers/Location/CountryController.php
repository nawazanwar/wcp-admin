<?php

namespace App\Http\Controllers\Location;

use App\Http\Controllers\Controller;
use App\Location\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function store(Request $request)
    {
        if ($request->is_ajax) {

            Country::updateOrCreate(
                [
                    'name' => $request->name
                ],
                [
                    'name' => $request->name
                ]
            );
        }
    }
}

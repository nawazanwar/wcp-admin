<?php

namespace App\Http\Controllers\Location;

use App\Http\Controllers\Controller;
use App\Location\Colony;
use Illuminate\Http\Request;

class ColonyController extends Controller
{
    public function store(Request $request)
    {
        if ($request->is_ajax) {

            Colony::updateOrCreate(
                [
                    'name' => $request->name
                ],
                [
                    'name' => $request->name
                ]
            );
        }
    }
}

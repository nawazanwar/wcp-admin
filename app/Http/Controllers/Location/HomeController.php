<?php

namespace App\Http\Controllers\Location;

use App\Http\Controllers\Controller;
use App\Location\City;
use App\Location\Colony;
use App\Location\Country;
use App\Location\District;
use App\Location\State;
use App\Location\Tehsil;
use App\Models\Employer;
use App\Models\Location;
use App\Models\LRM;
use App\Models\LRMOrganization;
use App\Models\LrmSettings;
use App\Models\Type;
use App\Repositories\CurdRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $this->authorize('read', Location::class);
        $pageTitle = "Manage Locations";
        $viewParams = [
            'pageTitle' => $pageTitle
        ];
        return view('locations.index', $viewParams);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        foreach ($data['settings'] as $key => $setting) {

            foreach ($setting as $s) {

                switch ($key) {

                    case  "countries":
                        $model = new Country();
                        $model->updateOrCreate(
                            ['name' => $s], ['name' => $s]
                        );
                        break;
                    case  "states":
                        $model = new State();
                        $model->updateOrCreate(
                            ['name' => $s], ['name' => $s]
                        );
                        break;
                    case  "districts":
                        $model = new District();
                        $model->updateOrCreate(
                            ['name' => $s], ['name' => $s]
                        );
                        break;
                    case  "cities":
                        $model = new  City();
                        $model->updateOrCreate(
                            ['name' => $s], ['name' => $s]
                        );
                        break;
                    case  "tehsils":
                        $model = new  Tehsil();
                        $model->updateOrCreate(
                            ['name' => $s], ['name' => $s]
                        );
                        break;
                    case  "colonies":
                        $model = new  Colony();
                        $model->updateOrCreate(
                            ['name' => $s], ['name' => $s]
                        );
                        break;

                }
            }
        }

        return redirect()->route("location.index")->with('successMessage', "Location Saved Successfully");
    }
}

<?php

namespace App\Http\Controllers\Location;

use App\Http\Controllers\Controller;
use App\Location\Tehsil;
use Illuminate\Http\Request;

class TehsilController extends Controller
{
    public function store(Request $request)
    {
        if ($request->is_ajax) {

            Tehsil::updateOrCreate(
                [
                    'name' => $request->name
                ],
                [
                    'name' => $request->name
                ]
            );
        }
    }
}

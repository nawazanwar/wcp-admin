<?php

namespace App\Http\Controllers\LRM;

use App\Http\Controllers\Controller;
use App\Models\Employer;
use App\Models\LRMQualification;
use App\Models\LRMOrganization;
use App\Models\Type;
use App\Repositories\CurdRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class QualificationController extends Controller
{
    protected $model;

    public function __construct(LRMQualification $LRMQualification)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($LRMQualification);
    }

    public function index(Request $request)
    {
        $this->authorize('read', LRMQualification::class);
        $data = $this->model->all($request->all());
        $pageTitle = __('lrm.qualifications.all_types');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'data' => $data->paginate(20)
        ];
        return view('lrm.qualification.index', $viewParams);
    }

    public function create(Request $request)
    {
        $this->authorize('create', LRMQualification::class);
        $types = Type::whereParent('employment')
            ->whereIn('name', ["Government","Private Establishment"])->pluck('name', 'id');
        $pageTitle = __('lrm.qualifications.create');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'types' => $types
        ];
        return view('lrm.qualification.create', $viewParams);
    }

    public function store(Request $request)
    {
        $this->authorize('create', LRMQualification::class);
        $request->validate([
            'name' => 'required|max:255|unique:lrmqualifications,name',
            'name_ur' => 'max:255|unique:lrmqualifications,name_ur',
        ]);
        if ($this->model = $this->model->create($request->only($this->model->getModel()->fillable))) {
            $types = $request->input('types', array());
            if ($types){
                $this->model->types()->sync($types);
            }
            return redirect()->route('lrm-qualification.index')->with('successMessage', __('lrm.qualifications.created_success_message'));
        } else {
            return redirect()->back()
                ->withErrors(__('lrm.qualifications.created_failed'))
                ->withInput();
        }
    }

    public function edit($id)
    {
        $this->authorize('edit', LRMQualifications::class);
        $types = Type::whereParent('employment')
            ->whereIn('name', ["Government","Private Establishment"])->pluck('name', 'id');
        $this->model = $this->model->find($id);
        $pageTitle = __('lrm.edit') ." ". $this->model->name;
        $viewParams = [
            'pageTitle' => $pageTitle,
            'model' => $this->model,
            'types' => $types
        ];
        return view('lrm.qualification.edit', $viewParams);

    }

    public function update(Request $request,$id)
    {
        $this->authorize('edit', LRMQualification::class);
        $request->validate([
            'name' => 'required|max:255|unique:lrmqualifications,name,' . $id,
            'name_ur' => 'unique:lrmqualifications,name,' . $id,
        ]);
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        $this->model = $this->model->find($id);
        if ($this->model) {
            $types = $request->input('types', array());
            if ($types){
                $this->model->types()->sync($types);
            }
            return redirect()->route('lrm-qualification.index')->with('successMessage', $this->model->name . " " . __('lrm.qualifications.updated_success_message'));
        }
    }

    public function destroy($id)
    {
        $this->authorize('delete', LRMQualification::class);
        $this->model = $this->model->find($id);
        $this->model->types()->sync([]);
        if ($this->model->delete()) {
            return redirect()->route('lrm-qualification.index')->with('errorMessage', $this->model->name . " " . __('lrm.deleted_successfully'));
        }
    }
}

<?php

namespace App\Http\Controllers\LRM;

use App\Http\Controllers\Controller;
use App\Models\Employer;
use App\Models\LRMOrganization;
use App\Models\Type;
use App\Repositories\CurdRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    protected $model;

    public function __construct(LRMOrganization $LRMOrganization)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($LRMOrganization);
    }

    public function index(Request $request)
    {
        $this->authorize('read', LRMOrganization::class);
        $data = $this->model->all($request->all());
        $pageTitle = __('lrm.all_types_of_organizations');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'data' => $data->paginate(20)
        ];
        return view('lrm.organization.index', $viewParams);
    }

    public function create(Request $request)
    {
        $this->authorize('create', LRMOrganization::class);
        $types = Type::whereParent('employment')
            ->whereIn('name', ["Government","Private Establishment"])->pluck('name', 'id');
        $pageTitle = __('lrm.create_new_organizations');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'types' => $types
        ];
        return view('lrm.organization.create', $viewParams);
    }

    public function store(Request $request)
    {

        if ($request->is_ajax){

            $this->model = $this->model->updateOrCreate(
                [
                    'name' => $request->name
                ],
                [
                    'name' => $request->name
                ]
            );

            $type_id = Type::whereName($request->type)->first()->id;

        }else{

            $this->authorize('create', LRMOrganization::class);
            $request->validate([
                'name' => 'required|max:255|unique:lrmorganizations,name',
                'name_ur' => 'max:255|unique:lrmorganizations,name_ur',
            ]);
            if ($this->model = $this->model->create($request->only($this->model->getModel()->fillable))) {
                $types = $request->input('types', array());
                if ($types){
                    $this->model->types()->sync($types);
                }
                return redirect()->route('lrm-organization.index')->with('successMessage', __('lrm.organization_created_success_message'));
            } else {
                return redirect()->back()
                    ->withErrors(__('lrm.organization_created_failed'))
                    ->withInput();
            }

        }
    }

    public function edit($id)
    {
        $this->authorize('edit', LRMOrganization::class);
        $types = Type::whereParent('employment')
            ->whereIn('name', ["Government","Private Establishment"])->pluck('name', 'id');
        $this->model = $this->model->find($id);
        $pageTitle = __('lrm.edit') . $this->model->name;
        $viewParams = [
            'pageTitle' => $pageTitle,
            'model' => $this->model,
            'types' => $types
        ];
        return view('lrm.organization.edit', $viewParams);

    }

    public function update(Request $request,$id)
    {
        $this->authorize('edit', LRMOrganization::class);
        $request->validate([
            'name' => 'required|max:255|unique:lrmorganizations,name,' . $id,
            'name_ur' => 'unique:lrmorganizations,name,' . $id,
        ]);
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        $this->model = $this->model->find($id);
        if ($this->model) {
            $types = $request->input('types', array());
            if ($types){
                $this->model->types()->sync($types);
            }
            return redirect()->route('lrm-organization.index')->with('successMessage', $this->model->name . " " . __('lrm.updated_success_message'));
        }
    }

    public function destroy($id)
    {
        $this->authorize('delete', LRMOrganization::class);
        $this->model = $this->model->find($id);
        $this->model->types()->sync([]);
        if ($this->model->delete()) {
            return redirect()->route('lrm-organization.index')->with('errorMessage', $this->model->name . " " . __('lrm.deleted_successfully'));
        }
    }
}

<?php

namespace App\Http\Controllers\LRM;

use App\Http\Controllers\Controller;
use App\Models\Employer;
use App\Models\LRMProfession;
use App\Models\LRMOrganization;
use App\Models\Type;
use App\Repositories\CurdRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProfessionController extends Controller
{
    protected $model;

    public function __construct(LRMProfession $LRMProfession)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($LRMProfession);
    }

    public function index(Request $request)
    {
        $this->authorize('read', LRMProfession::class);
        $data = $this->model->all($request->all());
        $pageTitle = __('lrm.professions.all_types');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'data' => $data->paginate(20)
        ];
        return view('lrm.profession.index', $viewParams);
    }

    public function create(Request $request)
    {
        $this->authorize('create', LRMProfession::class);
        $types = Type::whereParent('employment')
            ->whereIn('name', ["Government","Private Establishment"])->pluck('name', 'id');
        $pageTitle = __('lrm.professions.create');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'types' => $types
        ];
        return view('lrm.profession.create', $viewParams);
    }

    public function store(Request $request)
    {
        $this->authorize('create', LRMProfession::class);
        $request->validate([
            'name' => 'required|max:255|unique:lrmprofessions,name',
            'name_ur' => 'max:255|unique:lrmprofessions,name_ur',
        ]);
        if ($this->model = $this->model->create($request->only($this->model->getModel()->fillable))) {
            $types = $request->input('types', array());
            if ($types){
                $this->model->types()->sync($types);
            }
            return redirect()->route('lrm-profession.index')->with('successMessage', __('lrm.professions.created_success_message'));
        } else {
            return redirect()->back()
                ->withErrors(__('lrm.professions.created_failed'))
                ->withInput();
        }
    }

    public function edit($id)
    {
        $this->authorize('edit', LRMProfession::class);
        $types = Type::whereParent('employment')
            ->whereIn('name', ["Government","Private Establishment"])->pluck('name', 'id');
        $this->model = $this->model->find($id);
        $pageTitle = __('lrm.edit') ." ". $this->model->name;
        $viewParams = [
            'pageTitle' => $pageTitle,
            'model' => $this->model,
            'types' => $types
        ];
        return view('lrm.profession.edit', $viewParams);

    }

    public function update(Request $request,$id)
    {
        $this->authorize('edit', LRMProfession::class);
        $request->validate([
            'name' => 'required|max:255|unique:lrmprofessions,name,' . $id,
            'name_ur' => 'unique:lrmprofessions,name,' . $id,
        ]);
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        $this->model = $this->model->find($id);
        if ($this->model) {
            $types = $request->input('types', array());
            if ($types){
                $this->model->types()->sync($types);
            }
            return redirect()->route('lrm-profession.index')->with('successMessage', $this->model->name . " " . __('lrm.professions.updated_success_message'));
        }
    }

    public function destroy($id)
    {
        $this->authorize('delete', LRMProfession::class);
        $this->model = $this->model->find($id);
        $this->model->types()->sync([]);
        if ($this->model->delete()) {
            return redirect()->route('lrm-profession.index')->with('errorMessage', $this->model->name . " " . __('lrm.deleted_successfully'));
        }
    }
}

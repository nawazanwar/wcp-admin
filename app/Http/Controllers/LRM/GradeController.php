<?php

namespace App\Http\Controllers\LRM;

use App\Http\Controllers\Controller;
use App\Models\Employer;
use App\Models\LRMGrade;
use App\Models\LRMOrganization;
use App\Models\Type;
use App\Repositories\CurdRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GradeController extends Controller
{
    protected $model;

    public function __construct(LRMGrade $LRMGrade)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($LRMGrade);
    }

    public function index(Request $request)
    {
        $this->authorize('read', LRMGrade::class);
        $data = $this->model->all($request->all());
        $pageTitle = __('lrm.grades.all_types');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'data' => $data->paginate(20)
        ];
        return view('lrm.grade.index', $viewParams);
    }

    public function create(Request $request)
    {
        $this->authorize('create', LRMGrade::class);
        $types = Type::whereParent('employment')
            ->whereIn('name', ["Government","Private Establishment"])->pluck('name', 'id');
        $pageTitle = __('lrm.grades.create');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'types' => $types
        ];
        return view('lrm.grade.create', $viewParams);
    }

    public function store(Request $request)
    {
        $this->authorize('create', LRMGrade::class);
        $request->validate([
            'name' => 'required|max:255|unique:lrmgrades,name',
            'name_ur' => 'max:255|unique:lrmgrades,name_ur',
        ]);
        if ($this->model = $this->model->create($request->only($this->model->getModel()->fillable))) {
            $types = $request->input('types', array());
            if ($types){
                $this->model->types()->sync($types);
            }
            return redirect()->route('lrm-grade.index')->with('successMessage', __('lrm.grades.created_success_message'));
        } else {
            return redirect()->back()
                ->withErrors(__('lrm.grades.created_failed'))
                ->withInput();
        }
    }

    public function edit($id)
    {
        $this->authorize('edit', LRMGrade::class);
        $types = Type::whereParent('employment')
            ->whereIn('name', ["Government","Private Establishment"])->pluck('name', 'id');
        $this->model = $this->model->find($id);
        $pageTitle = __('lrm.edit') ." ". $this->model->name;
        $viewParams = [
            'pageTitle' => $pageTitle,
            'model' => $this->model,
            'types' => $types
        ];
        return view('lrm.grade.edit', $viewParams);

    }

    public function update(Request $request,$id)
    {
        $this->authorize('edit', LRMGrade::class);
        $request->validate([
            'name' => 'required|max:255|unique:lrmgrades,name,' . $id,
            'name_ur' => 'unique:lrmgrades,name,' . $id,
        ]);
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        $this->model = $this->model->find($id);
        if ($this->model) {
            $types = $request->input('types', array());
            if ($types){
                $this->model->types()->sync($types);
            }
            return redirect()->route('lrm-grade.index')->with('successMessage', $this->model->name . " " . __('lrm.grades.updated_success_message'));
        }
    }

    public function destroy($id)
    {
        $this->authorize('delete', LRMGrade::class);
        $this->model = $this->model->find($id);
        $this->model->types()->sync([]);
        if ($this->model->delete()) {
            return redirect()->route('lrm-grade.index')->with('errorMessage', $this->model->name . " " . __('lrm.deleted_successfully'));
        }
    }
}

<?php

namespace App\Http\Controllers\LRM;

use App\Http\Controllers\Controller;
use App\Models\Employer;
use App\Models\LRMMinistry;
use App\Models\Type;
use App\Repositories\CurdRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MinistryController extends Controller
{
    protected $model;

    public function __construct(LRMMinistry $ministry)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($ministry);
    }

    public function index(Request $request)
    {
        $this->authorize('read', LRMMinistry::class);
        $data = $this->model->all($request->all());
        $pageTitle = __('lrm.all_types_of_ministries');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'data' => $data->paginate(20)
        ];
        return view('lrm.ministry.index', $viewParams);
    }

    public function create(Request $request)
    {
        $this->authorize('create', LRMMinistry::class);
        $types = Type::whereParent('employment')->whereName('Government')->pluck('name', 'id');
        $pageTitle = __('lrm.create_new_ministry');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'types' => $types
        ];
        return view('lrm.ministry.create', $viewParams);
    }

    public function store(Request $request)
    {

        if ($request->is_ajax) {

            $this->model->updateOrCreate(
                [
                    'name' => $request->name
                ],
                [
                    'name' => $request->name
                ]
            );

            return response()->json([
                'status' => true
            ]);

        } else {

            $this->authorize('create', LRMMinistry::class);
            $request->validate([
                'name' => 'required|max:255|unique:lrmministries,name',
                'name_ur' => 'max:255|unique:lrmministries,name_ur',
                'type_id' => 'required'
            ]);
            if ($this->model->create($request->only($this->model->getModel()->fillable))) {
                return redirect()->route('lrm-ministry.index')->with('successMessage', __('lrm.ministry_created_success_message'));
            }

        }
    }

    public function edit($id)
    {
        $types = Type::whereParent('employment')->whereName('Government')->pluck('name', 'id');
        $this->authorize('edit', LRMMinistry::class);
        $this->model = $this->model->find($id);
        $pageTitle = __('lrm.edit') . $this->model->name;
        $viewParams = [
            'pageTitle' => $pageTitle,
            'model' => $this->model,
            'types' => $types
        ];
        return view('lrm.ministry.edit', $viewParams);

    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255|unique:lrmministries,name,' . $id,
            'name_ur' => 'unique:lrmministries,name,' . $id,
        ]);
        $this->authorize('edit', LRMMinistry::class);
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        $this->model = $this->model->find($id);
        if ($this->model) {
            return redirect()->route('lrm-ministry.index')->with('successMessage', $this->model->name . " " . __('lrm.updated_success_message'));
        }
    }

    public function destroy($id)
    {
        $this->authorize('delete', LRMMinistry::class);
        $this->model = $this->model->find($id);
        if ($this->model->delete($id)) {
            return redirect()->route('lrm-ministry.index')->with('errorMessage', $this->model->name . " " . __('lrm.deleted_successfully'));
        }
    }
}

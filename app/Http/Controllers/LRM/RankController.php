<?php

namespace App\Http\Controllers\LRM;

use App\Http\Controllers\Controller;
use App\Models\Employer;
use App\Models\LRMRank;
use App\Models\Type;
use App\Repositories\CurdRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RankController extends Controller
{
    protected $model;

    public function __construct(LRMRank $LRMRank)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($LRMRank);
    }

    public function index(Request $request)
    {
        $this->authorize('read', LRMRank::class);
        $data = $this->model->all($request->all());
        $pageTitle = __('lrm.all_types_of_ranks');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'data' => $data->paginate(20)
        ];
        return view('lrm.rank.index', $viewParams);
    }

    public function create(Request $request)
    {
        $this->authorize('create', LRMRank::class);
        $types = Type::whereParent('employment')
            ->whereIn('name', ["Government","Private Establishment"])->pluck('name', 'id');
        $pageTitle = __('lrm.create_new_ranks');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'types' => $types
        ];
        return view('lrm.rank.create', $viewParams);
    }

    public function store(Request $request)
    {
        $this->authorize('create', LRMRank::class);
        $request->validate([
            'name' => 'required|max:255|unique:lrmranks,name',
            'name_ur' => 'max:255|unique:lrmranks,name_ur',
        ]);
        if ($this->model = $this->model->create($request->only($this->model->getModel()->fillable))) {
            $types = $request->input('types', array());
            if ($types){
                $this->model->types()->sync($types);
            }
            return redirect()->route('lrm-rank.index')->with('successMessage', __('lrm.rank_created_success_message'));
        } else {
            return redirect()->back()
                ->withErrors(__('lrm.rank_created_failed'))
                ->withInput();
        }
    }

    public function edit($id)
    {
        $this->authorize('edit', LRMRank::class);
        $types = Type::whereParent('employment')
            ->whereIn('name', ["Government","Private Establishment"])->pluck('name', 'id');
        $this->model = $this->model->find($id);
        $pageTitle = __('lrm.edit') . $this->model->name;
        $viewParams = [
            'pageTitle' => $pageTitle,
            'model' => $this->model,
            'types' => $types
        ];
        return view('lrm.rank.edit', $viewParams);

    }

    public function update(Request $request,$id)
    {
        $this->authorize('edit', LRMRank::class);
        $request->validate([
            'name' => 'required|max:255|unique:lrmranks,name,' . $id,
            'name_ur' => 'unique:lrmranks,name,' . $id,
        ]);
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        $this->model = $this->model->find($id);
        if ($this->model) {
            $types = $request->input('types', array());
            if ($types){
                $this->model->types()->sync($types);
            }
            return redirect()->route('lrm-rank.index')->with('successMessage', $this->model->name . " " . __('lrm.updated_success_message'));
        }
    }

    public function destroy($id)
    {
        $this->authorize('delete', LRMRank::class);
        $this->model = $this->model->find($id);
        $this->model->types()->sync([]);
        if ($this->model->delete()) {
            return redirect()->route('lrm-rank.index')->with('errorMessage', $this->model->name . " " . __('lrm.deleted_successfully'));
        }
    }
}

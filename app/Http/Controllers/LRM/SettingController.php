<?php

namespace App\Http\Controllers\LRM;

use App\Http\Controllers\Controller;
use App\Models\Employer;
use App\Models\LRM;
use App\Models\LRMOrganization;
use App\Models\LrmSettings;
use App\Models\Type;
use App\Repositories\CurdRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    protected $model;

    public function __construct(LrmSettings $lrmSettings)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($lrmSettings);
    }

    public function index(Request $request)
    {
        $this->authorize('read', LRM::class);
        $data = $this->model->all($request->all());
        $pageTitle = __('lrm.manage_settings');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'data' => $data->paginate(20)
        ];
        return view('lrm.settings.index', $viewParams);
    }

    public function store(Request $request)
    {

        if ($request->is_ajax == true) {

            $this->model = $this->model->updateOrCreate(
                ['name' => $request->name, 'parent' => $request->parent],
                ['name' => $request->name, 'parent' => $request->parent]
            );
            return response()->json([
                'status' => true
            ]);
        } else {

            $data = $request->all();

            foreach ($data['settings'] as $key => $setting) {

                foreach ($setting as $s) {

                    $this->model = $this->model->updateOrCreate(
                        ['name' => $s, 'parent' => $key],
                        ['name' => $s, 'parent' => $key]
                    );

                }
            }

            return redirect()->route("lrm-settings.index")->with('successMessage', "Settings Saved Successfully");

        }
    }
}

<?php

namespace App\Http\Controllers\LRM;

use App\Http\Controllers\Controller;
use App\Models\LRM;
use App\Models\Visitor;
use App\Repositories\CurdRepository;
use Illuminate\Http\Request;
use App\Models\VMS;

class HomeController extends Controller
{

    protected $model;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $this->authorize('read', LRM::class);
        $pageTitle = __('lrm.title');
        $breadcrumbs = [['text' => __('lrm.short_title')]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
        ];
        return view('lrm.index', $viewParams);
    }
}

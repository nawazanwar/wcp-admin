<?php

namespace App\Http\Controllers\LRM;

use App\Http\Controllers\Controller;
use App\Models\Employer;
use App\Models\LRMDepartment;
use App\Models\LRMOrganization;
use App\Models\Type;
use App\Repositories\CurdRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    protected $model;

    public function __construct(LRMDepartment $LRMDepartment)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($LRMDepartment);
    }

    public function index(Request $request)
    {
        $this->authorize('read', LRMDepartment::class);
        $data = $this->model->all($request->all());
        $pageTitle = __('lrm.departments.all_types');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'data' => $data->paginate(20)
        ];
        return view('lrm.department.index', $viewParams);
    }

    public function create(Request $request)
    {
        $this->authorize('create', LRMDepartment::class);
        $types = Type::whereParent('employment')
            ->whereIn('name', ["Government","Private Establishment"])->pluck('name', 'id');
        $pageTitle = __('lrm.departments.create');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'types' => $types
        ];
        return view('lrm.department.create', $viewParams);
    }

    public function store(Request $request)
    {
        $this->authorize('create', LRMDepartment::class);
        $request->validate([
            'name' => 'required|max:255|unique:lrmdepartments,name',
            'name_ur' => 'max:255|unique:lrmdepartments,name_ur',
        ]);
        if ($this->model = $this->model->create($request->only($this->model->getModel()->fillable))) {
            $types = $request->input('types', array());
            if ($types){
                $this->model->types()->sync($types);
            }
            return redirect()->route('lrm-department.index')->with('successMessage', __('lrm.departments.created_success_message'));
        } else {
            return redirect()->back()
                ->withErrors(__('lrm.departments.created_failed'))
                ->withInput();
        }
    }

    public function edit($id)
    {
        $this->authorize('edit', LRMDepartment::class);
        $types = Type::whereParent('employment')
            ->whereIn('name', ["Government","Private Establishment"])->pluck('name', 'id');
        $this->model = $this->model->find($id);
        $pageTitle = __('lrm.edit') ." ". $this->model->name;
        $viewParams = [
            'pageTitle' => $pageTitle,
            'model' => $this->model,
            'types' => $types
        ];
        return view('lrm.department.edit', $viewParams);

    }

    public function update(Request $request,$id)
    {
        $this->authorize('edit', LRMDepartment::class);
        $request->validate([
            'name' => 'required|max:255|unique:lrmdepartments,name,' . $id,
            'name_ur' => 'unique:lrmdepartments,name,' . $id,
        ]);
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        $this->model = $this->model->find($id);
        if ($this->model) {
            $types = $request->input('types', array());
            if ($types){
                $this->model->types()->sync($types);
            }
            return redirect()->route('lrm-department.index')->with('successMessage', $this->model->name . " " . __('lrm.departments.updated_success_message'));
        }
    }

    public function destroy($id)
    {
        $this->authorize('delete', LRMDepartment::class);
        $this->model = $this->model->find($id);
        $this->model->types()->sync([]);
        if ($this->model->delete()) {
            return redirect()->route('lrm-department.index')->with('errorMessage', $this->model->name . " " . __('lrm.deleted_successfully'));
        }
    }
}

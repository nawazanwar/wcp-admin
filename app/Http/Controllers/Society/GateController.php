<?php

namespace App\Http\Controllers\Society;

use App\Http\Controllers\Controller;
use App\Models\Gate;
use App\Models\Phase;
use App\Repositories\CurdRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GateController extends Controller
{

    protected $model;

    public function __construct(Gate $gate)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($gate);
    }


    public function index(Request $request)
    {
        $data = $this->model->all($request->all())->orderby('name', 'asc')->get();
        if ($request->is_ajax) {
            return view('society.gates.option',['data'=>$data])->render();
        } else {
            $this->authorize('read', Gate::class);
            $pageTitle = __('society.gates.all');
            $viewParams = [
                'pageTitle' => $pageTitle,
                'data' => $data,
            ];
            return view('society.gates.index', $viewParams);
        }

    }

    public function store(Request $request)
    {
        $this->authorize('create', Gate::class);
        $input = $request->all();
        $length = count($input['name']);
        for ($i = 0; $i < $length; $i++) {
            $name = $input['name'][$i];
            $name_ur = $input['name_ur'][$i];
            $id = $input['id'][$i];
            if (isset($id) && !empty($id)) {
                $this->model->updateOrCreate(
                    ['id' => $id],
                    ['name' => $name, 'name_ur' => $name_ur]
                );
            }
        }//end of the for loop

        return redirect()->route("society-gates.index")->with('successMessage', "Gate created/updated successfully");
    }
}

<?php

namespace App\Http\Controllers\Society;

use App\Http\Controllers\Controller;
use App\Models\Booth;
use App\Models\Gate;
use App\Models\Phase;
use App\Repositories\CurdRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BoothController extends Controller
{

    protected $model;

    public function __construct(Booth $booth)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($booth);
    }


    public function index(Request $request)
    {
        $this->authorize('read', Booth::class);
        $data = $this->model->all($request->all())->orderby('lane', 'asc')->get();
        $pageTitle = __('society.booths.all');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'data' => $data,
        ];
        return view('society.booths.index', $viewParams);
    }

    public function create()
    {
        $this->authorize('create', Booth::class);
        $pageTitle = __('society.booths.create');
        $viewParams = [
            'pageTitle' => $pageTitle,
        ];
        return view('society.booths.create', $viewParams);
    }

    public function store(Request $request)
    {
        $this->authorize('create', Gate::class);
        $request->validate([
            'gate_id' => 'required',
            'lane' => 'required',
            'type' => 'required',
        ]);
        $gate_id = $request->input('gate_id', null);
        $lane = $request->input('lane', null);
        $type = $request->input('type', null);
        $active = $request->get('active') ? 1 : 0;
        $this->model->updateOrCreate(
            [
                'gate_id' => $gate_id,
                'lane' => $lane,
                'type' => $type
            ],
            [
                'gate_id' => $gate_id,
                'lane' => $lane,
                'type' => $type,
                'active' => $active
            ]);
        return redirect()
            ->route('society-booths.index')
            ->with('successMessage', "Booth created or Updated successfully");
    }
}

<?php

namespace App\Http\Controllers\Society;

use App\Http\Controllers\Controller;
use App\Models\Gate;
use App\Models\GateLocation;
use App\Models\Phase;
use App\Repositories\CurdRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LocationController extends Controller
{

    protected $model;

    public function __construct(GateLocation $gateLocation)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($gateLocation);
    }


    public function index(Request $request)
    {
        $gate = Gate::find($request->get('gate', null));
        $data = $this->model->all($request->all())->where('gate_id', $gate->id)->orderby('name', 'asc')->get();
        if ($request->is_ajax) {
            return view('society.phases.gates.locations.option', ['data' => $data])->render();
        } else {
            $pageTitle = __('society.all_gates_of') . " " . $gate->name . ' (' . $gate->name_ur . ")";
            $viewParams = [
                'pageTitle' => $pageTitle,
                'data' => $data,
                'gate' => $gate
            ];
            return view('society.phases.gates.locations.index', $viewParams);
        }
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $length = count($input['name']);
        $gate_id = $request->input('gate_id', null);
        for ($i = 0; $i < $length; $i++) {
            $name = $input['name'][$i];
            $name_ur = $input['name_ur'][$i];
            $id = $input['id'][$i];
            if (isset($id) && !empty($id)) {
                $this->model->updateOrCreate(
                    ['id' => $id, 'gate_id' => $gate_id],
                    ['name' => $name, 'name_ur' => $name_ur, 'gate_id' => $gate_id]
                );
            }
        }//end of the for loop

        return redirect()->route("society-phase-gate-locations.index", ['gate' => $gate_id])->with('successMessage', "Gate created/updated successfully");
    }
}

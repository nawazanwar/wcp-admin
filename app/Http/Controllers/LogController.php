<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Traits\General;
use Illuminate\Http\Request;

class LogController extends Controller
{
    use General;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, $type = null)
    {
        if (isset($type) && $type == 'auth') {
            $pageTitle = __('system.log_of_managements');
            $breadcrumbs = [['text' => $pageTitle]];
            $viewParams = [
                'breadcrumbs' => $breadcrumbs,
                'pageTitle' => $pageTitle,
                'userLogs' => $this->get_auth_logs()
            ];
            return view('logs.auth', $viewParams);
        }else if (isset($type) && $type == 'visitor') {
            $pageTitle = __('system.log_of_visitors');
            $breadcrumbs = [['text' => $pageTitle]];
            $viewParams = [
                'breadcrumbs' => $breadcrumbs,
                'pageTitle' => $pageTitle,
                'userLogs' => $this->get_visitor_logs()
            ];
            return view('logs.visitor', $viewParams);
        }
    }

    public function get_auth_logs()
    {
        $logs = DB::table('logs as l')
            ->join('auth_logs as al', 'l.id', '=', 'al.log_id')
            ->join('users as u', 'u.id', '=', 'al.auth_id')
            ->where('l.name', 'auth')
            ->select('u.username as name',
                'al.date_time as date_time',
                'al.ip_address as ip_address',
                'al.mac_address as mac_address',
                'al.type as type',
                'u.id as id')
            ->orderby('al.date_time', 'desc')->get();
        $data = [];
        foreach ($logs as $log) {
            $data[date("d M , Y", strtotime($log->date_time))][] = $log;
        }
        return $data;
    }
    public function get_visitor_logs()
    {
        $logs = DB::table('logs as l')
            ->join('visitor_logs as vl', 'l.id', '=', 'vl.log_id')
            ->join('users as u', 'u.id', '=', 'vl.visitor_id')
            ->where('l.name', 'visitor')
            ->select('u.username as name',
                'vl.date_time as date_time',
                'vl.ip_address as ip_address',
                'vl.mac_address as mac_address',
                'vl.type as type',
                'u.id as id')
            ->orderby('vl.date_time', 'desc')->get();
        $data = [];
        foreach ($logs as $log) {
            $data[date("d M , Y", strtotime($log->date_time))][] = $log;
        }
        return $data;
    }

    public function save_visitor_logs(Request $request)
    {
        $mac_address = exec('getmac');
        $mac_address = strtok($mac_address, ' ');
        $visitor = $request->get('visitor', null);
        $type = $request->get('type', null);
        $log = Log::updateOrCreate(['name' => 'visitor'], ['name' => 'visitor']);
        DB::table('visitor_logs')->insert(
            [
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'mac_address' => $mac_address,
                'visitor_id' => $visitor,
                'log_id' => $log->id,
                'type' => $type
            ]
        );
    }
}

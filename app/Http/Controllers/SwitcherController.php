<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;

class SwitcherController extends Controller
{

    public function lang(Request $request, $locale)
    {
        App::setLocale($locale);
        session()->put('locale', $locale);
        return redirect()->back();
    }
}

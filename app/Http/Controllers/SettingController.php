<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Traits\General;
use Intervention\Image\Facades\Image;

class SettingController extends Controller
{
    use General;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setting = Setting::first();
        $pageTitle = __('system.settings');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'setting' => $setting,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
        ];
        return view('settings.index', $viewParams);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'short_name' => 'required',
            'name' => 'required',
            'short_name_ur' => 'required',
            'name_ur' => 'required'
        ]);
        $setting = Setting::find($id);
        $setting->update($request->all());
        return redirect()
            ->route('settings')
            ->with('successMessage', __('message.setting_updated_successfully'));
    }

}

<?php


namespace App\Http\Controllers\VMS;

use App\Models\Booth;
use App\Models\Card;
use App\Models\Log;
use App\Models\Type;
use App\Models\User;
use App\Models\VCompany;
use App\Models\Visitor;
use App\Models\VisitorLog;
use App\Models\VModel;
use App\Notifications\VisitorWrongPassengerNotification;
use App\Notifications\VisitorWrongVehicleDetailNotification;
use App\Repositories\CurdRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Notifications\VMSNotifications;

class VisitorController extends Controller
{
    protected $model;

    public function __construct(Visitor $visitor)
    {
        $this->model = new CurdRepository($visitor);
    }

    public function exit(Request $request, $vId, $bId)
    {
        $wrong_passenger = $request->input('wrong_passenger', 0);
        $wrong_vehicle_info = $request->input('wrong_vehicle_info', 0);
        $log = Log::updateOrCreate(['name' => 'visitor'], ['name' => 'visitor']);
        $mac_address = exec('getmac');
        $mac_address = strtok($mac_address, ' ');
        $vLog = new VisitorLog();

        $vLog->operator()->associate(Auth::guard('operator')->user());
        $vLog->log()->associate($log);
        $vLog->visitor()->associate(Visitor::find($vId));
        $vLog->booth()->associate(Booth::find($bId));
        $vLog->mac_address = $mac_address;
        $vLog->ip_address = $_SERVER['REMOTE_ADDR'];;
        $vLog->no_of_males = $request->input('no_of_males', null);
        $vLog->no_of_females = $request->input('no_of_females', null);
        $vLog->no_of_kids = $request->input('no_of_kids', null);
        $vLog->visitor_name = $request->input('visitor_name', null);
        $vLog->visitor_cnic = $request->input('visitor_cnic', null);
        $vLog->visitor_cell_no = $request->input('visitor_cell_no', null);
        $vLog->exit = $request->input('exit', 0);
        $vLog->is_exit = true;

        /* Type Company & Models holder*/

        $type_id = $request->input('type_id', null);
        $vLog->type_id = $type_id;
        $vcompany_id = $request->input('vcompany_id', null);
        $vLog->vcompany_id = $vcompany_id;
        $vmodel_id = $request->input('vmodel_id', null);
        $vLog->vmodel_id = $vmodel_id;

        /* Color Reg No & Reg Year*/

        $vLog->vehicle_color = $request->input('vehicle_color', null);
        $vLog->vehicle_reg_no = $request->input('vehicle_reg_no', null);
        $vLog->vehicle_reg_no = $request->input('vehicle_reg_no', null);

        if ($vLog->save()) {
            $users = DB::table('users as u')
                ->join('designation_user as du', 'du.user_id', '=', 'u.id')
                ->join('designation_permission as dp', 'dp.designation_id', '=', 'du.designation_id')
                ->join('permissions as p', 'dp.permission_id', '=', 'p.id')
                ->where('p.name', 'read_notification')->select('u.*');
            if ($wrong_passenger) {
                if ($users->count() > 0) {
                    foreach ($users->get() as $user) {
                        $toUser = User::find($user->id);
                        $toUser->notify(new VisitorWrongPassengerNotification($vLog));
                    }
                }
                $vLog->is_exit = false;
                $vLog->exit = false;
                $vLog->wrong_passenger = true;
                $vLog->save();
            }
            if ($wrong_vehicle_info) {
                if ($users->count() > 0) {
                    foreach ($users->get() as $user) {
                        $toUser = User::find($user->id);
                        $toUser->notify(new VisitorWrongVehicleDetailNotification($vLog));
                    }
                }
                $vLog->exit = false;
                $vLog->is_exit = false;
                $vLog->wrong_vehicle_info = true;
                $vLog->save();
            }

            if ($wrong_passenger || $wrong_vehicle_info) {

                return response()->json([
                    'status' => 'false'
                ]);

            } else {

                return response()->json([
                    'status' => 'true'
                ]);
            }

        }
    }



    public function logs($vId, $type)
    {
        $this->middleware('auth');
        $logs = DB::table('visitor_logs as vl')
            ->join('logs as l', 'l.id', '=', 'vl.log_id')
            ->join('users as u', 'u.id', '=', 'vl.operator_id')
            ->join('visitors as v', 'v.id', '=', 'vl.visitor_id')
            ->join('booths as b', 'b.id', '=', 'vl.booth_id')
            ->join('phases as p', 'p.id', '=', 'b.phase_id')
            ->join('gates as g', 'g.id', '=', 'b.gate_id')
            ->join('types as t', 't.id', '=', 'vl.type_id')
            ->join('vcompanies as vc', 'vc.id', '=', 'vl.vcompany_id')
            ->join('vmodels as vm', 'vm.id', '=', 'vl.vmodel_id');
        if ($type == 'entry') {
            $logs = $logs->where('v.id', $vId)->where('vl.is_enter', true);
        } else {
            $logs = $logs->where('v.id', $vId)->where('vl.is_exit', true);
        }
        $logs = $logs->select(
            'vl.*',
            't.name as vehicle_type_name',
            't.name_ur as vehicle_type_name_ur',
            'vc.name as vehicle_company_name',
            'vc.name_ur as vehicle_company_name_ur',
            'vm.name as vehicle_model_name',
            'vm.name_ur as vehicle_model_name_ur',
            'p.name as phase_name',
            'p.name_ur as phase_name_ur',
            'g.name as gate_name',
            'g.name_ur as gate_name_ur',
            'v.visitor_name as visitor_name',
            'v.visitor_cnic as visitor_cnic')
            ->orderByDesc('vl.id')->paginate(100);
        $pageTitle = __('vms.visitor.logs_of') . " " . Visitor::where('id', $vId)->value('visitor_name') . " for" . " " . ucfirst($type);
        $breadcrumbs = [['text' => __('vms.visitor.logs')]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'logs' => $logs,
            'type' => $type,
            'visitor_id' => $vId
        ];
        return view('vms.visitors.logs.index', $viewParams);
    }
}

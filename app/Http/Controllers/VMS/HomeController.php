<?php

namespace App\Http\Controllers\VMS;

use App\Http\Controllers\Controller;
use App\Models\Visitor;
use App\Repositories\CurdRepository;
use Illuminate\Http\Request;
use App\Models\VMS;

class HomeController extends Controller
{

    protected $model;

    public function __construct(Visitor $visitor)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($visitor);
    }

    public function index(Request $request)
    {
        $this->authorize('read', VMS::class);

        $heading = __('vms.visitor.messages.all_visitors_first_entry');

        $this->model = $this->model->all($request->all());
        $pageTitle = __('vms.title');
        $breadcrumbs = [['text' => __('vms.short_title')]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'data' => $this->model->paginate(50),
            'heading' => $heading
        ];
        return view('vms.index', $viewParams);
    }
}

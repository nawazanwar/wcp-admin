<?php

namespace App\Http\Controllers;

use App\Device\DType;
use App\Models\Booth;
use App\Models\Device;
use App\Models\Message;
use App\Models\Notification;
use App\Models\User;
use App\Models\VCompany;
use App\Models\Visitor;
use App\Models\VModel;
use App\Models\VNature;
use App\Notifications\CallVisiteeNotificationInBooth;
use App\Notifications\FollowVisitorNotificationInBooth;
use App\Notifications\GuideVisitorNotificationInBooth;
use App\Notifications\VerifiedVisitorNotificationInBooth;
use App\Repositories\CurdRepository;
use App\Traits\General;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use WcpHelper;
use Intervention\Image\Facades\Image;

class BoothController extends Controller
{
    protected $model;
    use General;

    public function __construct(Booth $booth)
    {
        $this->model = new CurdRepository($booth);
    }

    /**
     * @param Request $request
     */

    public function checkVisitorAtEnter(Request $request)
    {
        $type = $request->input('type');
        $value = $request->input('value');
        // validate by type
        $model = null;
        if ($type == 'vehicle') {
            $model = Visitor::whereVehicleRegNo($value);
        } else if ($type == 'cnic') {
            $model = Visitor::whereVisitorCnic($value);
        }
        $model = $model->whereIsEnter(true);
        if ($model->exists()) {
            $model = $model->orderBy('id', 'DESC')->limit(1)->first();
            if ($type == 'vehicle') {
                return $this->getLastHistoryByVehicleNumber($model);
            } else if ($type == 'cnic') {
                return $this->getLastHistoryByCNICNumber($model);
            }
        } else {
            return response()->json([
                'status' => 'not_exits'
            ]);
        }
    }

    /**
     * @param object $data
     * @return JsonResponse
     */

    public function getLastHistoryByVehicleNumber(object $data)
    {

        $companyQuery = VCompany::whereTypeId($data->type_id);
        $companyOptions = null;
        if ($companyQuery->exists()) {
            foreach ($companyQuery->get() as $item) {
                $select = null;
                if ($item->id == $data->vcompany_id) {
                    $select = "selected";
                }
                $companyOptions .= '<option ' . $select . ' value=' . $item->id . '>' . $item->name . '</option>';
            }
        } else {
            $companyOptions .= '<option value="" disabled="disabled" selected="selected">Create New Company</option>';
        }
        // Ready to make dropdown for Country Models
        $modelQuery = VModel::where('vcompany_id', $data->vcompany_id);
        $modelOptions = null;
        if ($modelQuery->exists()) {
            foreach ($modelQuery->get() as $item) {
                $select = null;
                if ($item->id == $data->vcompany_id) {
                    $select = "selected";
                }
                $modelOptions .= '<option ' . $select . ' value=' . $item->id . '>' . $item->name . '</option>';
            }
        } else {
            $modelOptions .= '<option value="" disabled="disabled" selected="selected">Create New Model</option>';
        }
        // Ready to make dropdown for nature of the materials
        $vNatureQuery = VNature::whereTypeId($data->type_id);
        $vNatureOptions = null;
        if ($vNatureQuery->exists()) {
            foreach ($vNatureQuery->get() as $item) {
                $select = null;
                if ($item->id == $data->vcompany_id) {
                    $select = "selected";
                }
                $vNatureOptions .= '<option ' . $select . ' value=' . $item->id . '>' . $item->name . '</option>';
            }
        } else {
            $vNatureOptions .= '<option value="" disabled="disabled" selected="selected">Create Nature of Material</option>';
        }
        return response()->json([
            'data' => $data,
            'companyOptions' => $companyOptions,
            'modelOptions' => $modelOptions,
            'vNatureOptions' => $vNatureOptions,
            'status' => 'exist'
        ]);
    }

    /**
     * @param object $data
     * @return JsonResponse
     */

    public function getLastHistoryByCNICNumber(object $data)
    {
        return response()->json([
            'data' => $data,
            'status' => 'exist'
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */

    public function exit(Request $request)
    {

        $this->middleware('boothLogin');
        $boothId = $request->input('booth_id', null);
        $this->model = new Visitor();
        $data = $request->only($this->model->getModel()->fillable);
        $data['booth_id'] = $boothId;
        $data['operator_id'] = \auth()->guard('operator')->user()->id;
        $data['visitor_biometric_verification'] = $request->input('visitor_biometric_verification', 0);
        $data['visitor_ratina_verification'] = $request->input('visitor_ratina_verification', 0);
        $data['visitor_gate_pass_verification'] = $request->input('visitor_gate_pass_verification', 0);
        $data['visitor_qrcode_verification'] = $request->input('visitor_qrcode_verification', 0);
        $data['is_exit'] = true;
        $this->model = $this->model->create($data);
        if ($this->model) {
            $mac_address = exec('getmac');
            $mac_address = strtok($mac_address, ' ');
            $this->mac_address = $mac_address;
            $this->model->save();
            return response()->json([
                'status' => 'true',
                'visitor_name' => $this->model->visitor_name
            ]);
        }
    }

    /**
     * @param Request $request
     */

    public function enter(Request $request)
    {

        $this->middleware('boothLogin');
        $boothId = $request->input('booth_id', null);
        $this->model = new Visitor();
        $data = $request->only($this->model->getModel()->fillable);
        $data['booth_id'] = $boothId;
        $data['operator_id'] = \auth()->guard('operator')->user()->id;
        $data['visitor_biometric_verification'] = $request->input('visitor_biometric_verification', 0);
        $data['visitor_ratina_verification'] = $request->input('visitor_ratina_verification', 0);
        $data['visitor_gate_pass_verification'] = $request->input('visitor_gate_pass_verification', 0);
        $data['visitor_qrcode_verification'] = $request->input('visitor_qrcode_verification', 0);
        $data['call_visitee'] = $request->input('call_visitee', 0);
        $data['verified'] = $request->input('verified', 0);
        $data['follow'] = $request->input('follow', 0);
        $data['guide'] = $request->input('guide', 0);
        $data['warn'] = $request->input('warn', 0);
        $data['permitted'] = $request->input('permitted', 0);
        $data['exit'] = $request->input('exit', 0);
        $data['is_enter'] = true;
        $this->model = $this->model->create($data);
        if ($this->model) {
            $mac_address = exec('getmac');
            $mac_address = strtok($mac_address, ' ');
            $this->mac_address = $mac_address;
            $this->model->save();

            /* Call Visitee*/

            if ($this->model->call_visitee) {
                $notificationReaderUsers = WcpHelper::getNotificationReaderUsers('call_visitee_at_booth_notification');
                if (count($notificationReaderUsers) > 0) {
                    foreach ($notificationReaderUsers as $user) {
                        $toUser = User::find($user->id);
                        $toUser->notify(new CallVisiteeNotificationInBooth($this->model));
                    }
                }
            }

            /* Verified Visitor */

            if ($this->model->verified) {
                $notificationReaderUsers = WcpHelper::getNotificationReaderUsers('visitor_verified_at_booth_notification');
                if (count($notificationReaderUsers) > 0) {
                    foreach ($notificationReaderUsers as $user) {
                        $toUser = User::find($user->id);
                        $toUser->notify(new VerifiedVisitorNotificationInBooth($this->model));
                    }
                }
            }

            /* Follow Visitor */

            if ($this->model->follow) {
                $notificationReaderUsers = WcpHelper::getNotificationReaderUsers('visitor_follow_at_booth_notification');
                if (count($notificationReaderUsers) > 0) {
                    foreach ($notificationReaderUsers as $user) {
                        $toUser = User::find($user->id);
                        $toUser->notify(new FollowVisitorNotificationInBooth($this->model));
                    }
                }
            }

            /* Guide Visitor*/

            if ($this->model->guide) {
                $notificationReaderUsers = WcpHelper::getNotificationReaderUsers('visitor_guide_at_booth_notification');
                if (count($notificationReaderUsers) > 0) {
                    foreach ($notificationReaderUsers as $user) {
                        $toUser = User::find($user->id);
                        $toUser->notify(new GuideVisitorNotificationInBooth($this->model));
                    }
                }
            }

            return response()->json([
                'status' => 'true',
                'visitor_name' => $this->model->visitor_name
            ]);
        }
    }

    public function manage(Request $request)
    {
        $this->middleware('boothLogin');
        $pageTitle = __('vms.manage_booths');
        $last_log = DB::table('booth_logs as bl')
            ->join('booths as b', 'b.id', '=', 'bl.booth_id')
            ->join('gates as g', 'g.id', '=', 'b.gate_id')
            ->join('users as u', 'u.id', '=', 'bl.operator_id')
            ->where('bl.operator_id', Auth::guard('operator')->user()->id)
            ->where('bl.type', 'login')
            ->select(
                'bl.created_at as created_at',
                'b.id as booth_id',
                'bl.ip_address as ip_address',
                'bl.mac_address as mac_address',
                'bl.date_time as date_time',
                'b.lane as booth_lane',
                'b.type as booth_type',
                'g.name as gate_name',
                'g.name_ur as gate_name_ur',
                'u.name as user_name',
                'u.id as user_id',
                'g.id as gate_id'
            )->orderByDesc('bl.id')
            ->limit(1);
        if ($last_log->count() > 0) {
            $last_log = $last_log->get()[0];
            $breadcrumbs = [['text' => $pageTitle]];
            $devices = Device::whereBoothId($last_log->booth_id)->get();
            $viewParams = [
                'breadcrumbs' => $breadcrumbs,
                'pageTitle' => $pageTitle,
                'boothInfo' => $last_log,
                'devices' => $devices
            ];
            if ($last_log->booth_type == 'entry') {
                return view('booths.manage-entry-booth', $viewParams);
            } else {
                return view('booths.manage-exit-booth', $viewParams);
            }
        } else {
            return redirect()->route('booth.login');
        }
    }

    public function operator_logs(Request $request)
    {
        if ($request->has('key')) {
            $key = $request->get('key', null);
            if ($key != 'specific_date' or $key != 'date_range') {
                $date_range = $this->getDateRange($request->get('key', null));
            } else {
                $date_range = array();
            }
        } else {
            $date_range = array();
        }
        $logs = DB::table('booth_logs as bl')
            ->join('booths as b', 'b.id', '=', 'bl.booth_id')
            ->join('phases as p', 'p.id', '=', 'b.phase_id')
            ->join('gates as g', 'g.id', '=', 'b.gate_id')
            ->join('users as u', 'u.id', '=', 'bl.operator_id');
        if ($request->has('key')) {
            if ($key == 'specific_date') {
                $value = $request->get('value', null);
                $logs = $logs->whereDate('bl.created_at', '=', $value);
            } else if ($key == 'date_range' and ($request->has('from') and $request->has('to'))) {
                $from = $request->get('from', null);
                $to = $request->get('to', null);
                $logs = $logs->whereBetween('bl.created_at', [$from, $to]);
            } else {
                if (count($date_range) > 0) {
                    $logs = $logs->whereBetween('bl.created_at', [$date_range['start'], $date_range['end']]);
                } else {
                    $logs = $logs->where('bl.created_at', '>=', Carbon::today());
                }
            }
        }
        $logs = $logs->select(
            'bl.created_at as created_at',
            'bl.type as type',
            'bl.ip_address as ip_address',
            'bl.mac_address as mac_address',
            'bl.date_time as date_time',
            'b.lane as booth_lane',
            'p.name as phase_name',
            'p.name_ur as phase_name_ur',
            'g.name as gate_name',
            'g.name_ur as gate_name_ur',
            'u.name as user_name',
            'u.id as user_id'
        )->orderByDesc('bl.date_time')->paginate(100);
        $data = [];
        foreach ($logs as $log) {
            $data[date("d M , Y", strtotime($log->date_time))][] = $log;
        }
        $pageTitle = __('vms.booth_logs');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'logs' => $data,
            'date_range' => $date_range
        ];
        return view('vms.booths.logs.index', $viewParams);
    }

    public function checkVisitorAtExit(Request $request)
    {
        //$booth = Booth::with('gate')->where('id', $request->booth_id)->first();
        $model = Visitor::where($request->filter_by, $request->value);
        if ($model->exists()) {
            /* IF PERSON ALREADY FOUND THE STATUS IS */
            $model = $model->first();
            $lastEntry = $model
                ->where('visitors.is_enter', true)
                ->orderBy('visitors.id', 'DESC')
                ->limit(1)->first();

            // Ready to make dropdown for companies
            $companyQuery = VCompany::whereTypeId($lastEntry->type_id);
            $companyOptions = null;
            if ($companyQuery->exists()) {
                foreach ($companyQuery->get() as $item) {

                    $select = null;
                    if ($item->id == $lastEntry->vcompany_id) {
                        $select = "selected";
                    }

                    $companyOptions .= '<option ' . $select . ' value=' . $item->id . '>' . $item->name . '</option>';
                }
            } else {
                $companyOptions .= '<option value="" disabled="disabled" selected="selected">Create New Company</option>';
            }

            // Ready to make dropdown for Country Models

            $modelQuery = VModel::where('vcompany_id', $lastEntry->vcompany_id);
            $modelOptions = null;
            if ($modelQuery->exists()) {
                foreach ($modelQuery->get() as $item) {

                    $select = null;
                    if ($item->id == $lastEntry->vcompany_id) {
                        $select = "selected";
                    }

                    $modelOptions .= '<option ' . $select . ' value=' . $item->id . '>' . $item->name . '</option>';
                }
            } else {
                $modelOptions .= '<option value="" disabled="disabled" selected="selected">Create New Model</option>';
            }


            // Ready to make dropdown for nature of the materials

            $vNatureQuery = VNature::whereTypeId($lastEntry->type_id);
            $vNatureOptions = null;
            if ($vNatureQuery->exists()) {
                foreach ($vNatureQuery->get() as $item) {

                    $select = null;
                    if ($item->id == $lastEntry->vcompany_id) {
                        $select = "selected";
                    }

                    $vNatureOptions .= '<option ' . $select . ' value=' . $item->id . '>' . $item->name . '</option>';
                }
            } else {
                $vNatureOptions .= '<option value="" disabled="disabled" selected="selected">Create Nature of Material</option>';
            }

            return response()->json([
                'status' => 'exit',
                'message' => __('vms.visitor.messages.already_register'),
                'data' => $lastEntry,
                'companyOptions' => $companyOptions,
                'modelOptions' => $modelOptions,
                'vNatureOptions' => $vNatureOptions
            ]);
        }
    }


    public function getNotifications($for = null, $notificationType = null, $boothId = null)
    {
        if ($notificationType == 'un-read') {
            return $this->unreadNotifications($for, $boothId);
        } else {
            return $this->readNotifications($for, $boothId);
        }
    }

    public function unreadNotifications($for, $boothId)
    {
        $notifications = auth()->user()->unreadNotifications();
        if ($for == 'visitee') {
            $notifications = $notifications->whereType('App\Notifications\CallVisiteeNotificationInBooth');
        } else if ($for == 'verified') {
            $notifications = $notifications->whereType('App\Notifications\VerifiedVisitorNotificationInBooth');
        } else if ($for == 'follow') {
            $notifications = $notifications->whereType('App\Notifications\FollowVisitorNotificationInBooth');
        } else if ($for == 'guide') {
            $notifications = $notifications->whereType('App\Notifications\GuideVisitorNotificationInBooth');
        }
        $notifications = $notifications->orderby('id', 'desc')->paginate(10);
        $pageTitle = 'Un Read Notifications of <strong class="text-info text-capitalize">' . Booth::find($boothId)->lane . "</strong> at <strong class='text-info text-capitalize'>" . Booth::find($boothId)->type . "</strong>";
        return view('monitoring-cell.notifications.booth.un-read', compact('notifications', 'pageTitle', 'for', 'boothId'));
    }

    public function readNotifications($for, $boothId)
    {
        $notifications = Notification::where('reader_id', '!=', null);
        if ($for == 'visitee') {
            $notifications = $notifications->whereType('App\Notifications\CallVisiteeNotificationInBooth');
        } else if ($for == 'verified') {
            $notifications = $notifications->whereType('App\Notifications\VerifiedVisitorNotificationInBooth');
        } else if ($for == 'follow') {
            $notifications = $notifications->whereType('App\Notifications\FollowVisitorNotificationInBooth');
        } else if ($for == 'guide') {
            $notifications = $notifications->whereType('App\Notifications\GuideVisitorNotificationInBooth');
        }
        $notifications = $notifications->orderby('id', 'desc')->paginate(10);
        $pageTitle = 'All Read Notifications of <strong class="text-info text-capitalize">' . Booth::find($boothId)->lane . "</strong> at <strong class='text-info text-capitalize'>" . Booth::find($boothId)->type . "</strong>";
        return view('monitoring-cell.notifications.booth.read', compact('notifications', 'pageTitle'));
    }

    public function markAsReadNotifications(Request $request)
    {
        $id = $request->input('id', null);
        $notification = auth()->user()->notifications()->find($id);
        if ($notification) {
            $notification->read_reason = $request->input('reason', null);
            $notification->reader_id = \auth()->user()->id;
            $notification->markAsRead();
        }
    }

    public function visitorLog(Request $request, $boothId = null)
    {
        $visitors = Visitor::join('booths', 'booths.id', 'visitors.booth_id')
            ->leftJoin('types', 'types.id', 'visitors.type_id')
            ->leftJoin('vcompanies', 'vcompanies.id', 'visitors.vcompany_id')
            ->leftJoin('vmodels', 'vmodels.id', 'visitors.vmodel_id')
            ->leftJoin('vnatures', 'vnatures.id', 'visitors.vnature_id');

        if ($request->has('s') && $request->get('s') != '') {
            $keyword = $request->get('s', null);
            $visitors = $visitors->where(function ($q) use ($keyword) {
                $q->where('visitors.purpose_of_visit', 'like', '%' . $keyword . '%')
                    ->orWhere('visitors.vehicle_color', 'like', '%' . $keyword . '%')
                    ->orWhere('visitors.vehicle_reg_no', 'like', '%' . $keyword . '%')
                    ->orWhere('visitors.vehicle_reg_year', 'like', '%' . $keyword . '%')
                    ->orWhere('visitors.no_of_males', 'like', '%' . $keyword . '%')
                    ->orWhere('visitors.no_of_females', 'like', '%' . $keyword . '%')
                    ->orWhere('visitors.no_of_kids', 'like', '%' . $keyword . '%')
                    ->orWhere('visitors.visitor_name', 'like', '%' . $keyword . '%')
                    ->orWhere('visitors.visitor_cnic', 'like', '%' . $keyword . '%')
                    ->orWhere('visitors.visitor_cell_no', 'like', '%' . $keyword . '%')
                    ->orWhere('types.name', 'like', '%' . $keyword . '%')
                    ->orWhere('vcompanies.name', 'like', '%' . $keyword . '%')
                    ->orWhere('vnatures.name', 'like', '%' . $keyword . '%')
                    ->orWhere('vmodels.name', 'like', '%' . $keyword . '%');
            });
        }

        // Filter by Date

        if ($request->has('d') && $request->get('d') != '') {
            $keyword = $request->get('d');
            $visitors = $visitors->where('visitors.created_at', 'like', '%' . $keyword . '%');
        }

        // Filter by Id Card

        if ($request->has('id_card') && $request->get('id_card') != '') {
            $keyword = $request->get('id_card');
            $visitors = $visitors->where('visitors.visitor_cnic', 'like', '%' . $keyword . '%');
        }


        // Filter by Vehicle Number

        if ($request->has('vehicle_number') && $request->get('vehicle_number') != '') {
            $keyword = $request->get('vehicle_number');
            $visitors = $visitors->where('visitors.vehicle_reg_no', 'like', '%' . $keyword . '%');
        }

        // Filter by Vehicle Model

        if ($request->has('vehicle_model') && $request->get('vehicle_model') != '') {
            $keyword = $request->get('vehicle_model');
            $visitors = $visitors->where('vmodels.name', 'like', '%' . $keyword . '%');
        }

        // Filter by Vehicle Color

        if ($request->has('vehicle_color') && $request->get('vehicle_color') != '') {
            $keyword = $request->get('vehicle_color');
            $visitors = $visitors->where('visitors.vehicle_color', 'like', '%' . $keyword . '%');
        }

        // Filter by Visitor Name

        if ($request->has('visitor_name') && $request->get('visitor_name') != '') {
            $keyword = $request->get('visitor_name');
            $visitors = $visitors->where('visitors.visitor_name', 'like', '%' . $keyword . '%');
        }

        $visitors = $visitors
            ->where('booths.id', $boothId)
            ->orderby('visitors.id', 'desc')
            ->select('visitors.*')
            ->paginate(10);

        $pageTitle = 'All Visitors of <strong class="text-info text-capitalize">' . Booth::find($boothId)->lane . "</strong> at <strong class='text-info text-capitalize'>" . Booth::find($boothId)->type . "</strong>";
        return view('monitoring-cell.visitors.log', compact('visitors', 'pageTitle', 'boothId'));

    }

    public function alreadyEnterNotExit(Request $request)
    {
        $value = $request->get('value', null);
        $query = Visitor::where('vehicle_reg_no', $value)->where('is_enter', true)->where('is_exit', false);
        if ($query->exists()) {
            $message = WcpHelper::getMessageByName('visitor_already_enter_not_exit');
            $message = str_replace('%vehicle_number%', $value, $message);
            return response()->json([
                'status' => true,
                'message' => $message
            ]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function countNotifications()
    {
        $allBooths = Booth::pluck('id');
        $data = array();
        foreach ($allBooths as $boothId) {
            $unreadNotifications = Notification::whereDate('created_at', Carbon::today())
                ->where('data->booth_id', $boothId)
                ->whereNotifiableId(\auth()->user()->id)
                ->whereReadAt(null)->count();
            $readNotifications = Notification::whereDate('created_at', Carbon::today())
                ->where('data->booth_id', $boothId)
                ->whereNotifiableId(\auth()->user()->id)
                ->where('read_at', '!=', null)->count();
            $data[$boothId]['read'] = $readNotifications;
            $data[$boothId]['unread'] = $unreadNotifications;
            $data[$boothId]['visitors'] = Visitor::where('booth_id', $boothId)->count();
        }
        return response()->json([
            'data' => $data
        ]);
    }

    public function boothCameras(Request $request, $boothId)
    {
        $devices = Device::whereBoothId($boothId)->get();
        $pageTitle = 'All Cameras of <strong class="text-info text-capitalize">' . Booth::find($boothId)->lane . "</strong> at <strong class='text-info text-capitalize'>" . Booth::find($boothId)->type . "</strong>";
        $viewParams = [
            'devices' => $devices,
            'pageTitle' => $pageTitle
        ];
        return view('monitoring-cell.cameras.index', $viewParams);
    }

    /**
     * @param Request $request
     * @param int $boothId
     */

    public function boothImages(Request $request, int $boothId)
    {

        $visitors = DB::table('visitors');


        // Filter by Date

        if ($request->has('d') && $request->get('d') != '') {
            $keyword = $request->get('d');
            $visitors = $visitors->where('visitors.created_at', 'like', '%' . $keyword . '%');
        }

        // Filter by Id Card

        if ($request->has('id_card') && $request->get('id_card') != '') {
            $keyword = $request->get('id_card');
            $visitors = $visitors->where('visitors.visitor_cnic', 'like', '%' . $keyword . '%');
        }


        // Filter by Vehicle Number

        if ($request->has('vehicle_number') && $request->get('vehicle_number') != '') {
            $keyword = $request->get('vehicle_number');
            $visitors = $visitors->where('visitors.vehicle_reg_no', 'like', '%' . $keyword . '%');
        }

        // Filter by Vehicle Color

        if ($request->has('vehicle_color') && $request->get('vehicle_color') != '') {
            $keyword = $request->get('vehicle_color');
            $visitors = $visitors->where('visitors.vehicle_color', 'like', '%' . $keyword . '%');
        }

        // Filter by Visitor Name

        if ($request->has('visitor_name') && $request->get('visitor_name') != '') {
            $keyword = $request->get('visitor_name');
            $visitors = $visitors->where('visitors.visitor_name', 'like', '%' . $keyword . '%');
        }


        $visitors = $visitors->where('visitors.booth_id', $boothId)
            ->orderby('visitors.id', 'desc')
            ->select('visitors.*')
            ->paginate(100);

        $pageTitle = 'All Camera Image of <strong class="text-info text-capitalize">' . Booth::find($boothId)->lane . "</strong> at <strong class='text-info text-capitalize'>" . Booth::find($boothId)->type . "</strong>";
        return view('monitoring-cell.images.index', compact('visitors', 'pageTitle', 'boothId'));

    }

    /**
     * @param Request $request
     * @return string
     */
    public function getTempPics(Request $request)
    {
        $url = $request->input('url', null);
        if ($url) {
            return "data:image/png;base64," . base64_encode(file_get_contents($url));
        }
    }

    /**
     * @param Request $request
     * @return array
     */

    public function getAllTempPics(Request $request)
    {
        $data = $request->all();
        foreach ($request->all() as $key => $value) {
            $data[$key] = "data:image/png;base64," . base64_encode(file_get_contents($value));
        }
        return $data;
    }
}

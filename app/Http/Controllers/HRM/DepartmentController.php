<?php

namespace App\Http\Controllers\HRM;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Type;
use App\Repositories\CurdRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DepartmentController extends Controller
{

    protected $model;

    public function __construct(Department $department)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($department);
    }


    public function index(Request $request)
    {
        $this->authorize('read', Department::class);
        $pageTitle = __('hrm.list_all_departments');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'data' => $this->model->all($request->all())->paginate(12)
        ];
        return view('hrm.departments.index', $viewParams);
    }

    public function create()
    {
        $this->authorize('create', Department::class);
        $types = Type::where('parent', 'department')->get();
        $pageTitle = __('hrm.create_new_department');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'types' => $types,
        ];
        return view('hrm.departments.create', $viewParams);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'name_ur' => 'required|max:255',
            'active' => 'boolean:required'
        ]);
        $this->authorize('create', Department::class);
        $data = $request->only($this->model->getModel()->fillable);
        $data['active'] = $request->get('active') ? 1 : 0;
        $this->model = $this->model->updateOrCreate(
            ['name' => $data['name'], 'name_ur' => $data['name_ur']], $data
        );
        if ($this->model) {
            return redirect()->route('hrm-departments.index')->with('successMessage', __('hrm.department_created_success_message'));
        }
    }

    public function edit($id)
    {
        $this->authorize('edit', Department::class);
        $this->model = $this->model->find($id);
        $types = Type::where('parent', 'department')->get()->pluck('name', 'id');
        $pageTitle = __('hrm.edit') . $this->model->name;
        $viewParams = [
            'pageTitle' => $pageTitle,
            'types' => $types,
            'model' => $this->model
        ];
        return view('hrm.departments.edit', $viewParams);

    }

    public function update(Request $request, $id)
    {
        $this->authorize('edit', Department::class);
        $request->validate([
            'name' => 'required|max:255',
            'name_ur' => 'required|max:255',
            'active' => 'boolean:required'
        ]);
        $data = $request->only($this->model->getModel()->fillable);
        $data['active'] = $request->get('active') ? 1 : 0;
        $this->model->updateOrCreate(
            ['name' => $data['name'], 'name_ur' => $data['name_ur']], $data
        );
        $this->model = $this->model->find($id);
        if ($this->model) {
            return redirect()->route('hrm-departments.index')->with('successMessage', $this->model->name . " " . __('hrm.updated_success_message'));
        }
    }

    public function destroy($id)
    {
        $this->authorize('delete', Department::class);
        $this->model = $this->model->find($id);
        if ($this->model->delete($id)) {
            return redirect()->route('hrm-departments.index')->with('errorMessage', $this->model->name . " " . __('hrm.deleted_successfully'));
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function generate_with_ajax(Request $request)
    {
        $name = $request->input('name', null);
        $this->model = $this->model->updateOrCreate(
            ['name' => $name], ['name' => $name]
        );
        return \response()->json([
            'model' => $this->model,
            'status' => true
        ]);
    }
}

<?php

namespace App\Http\Controllers\HRM;

use App\Http\Controllers\Controller;
use App\Models\Employer;
use App\Models\Type;
use App\Repositories\CurdRepository;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RelationController extends Controller
{
    protected $model;

    public function __construct(Type $type)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($type);
    }

    public function index(Request $request)
    {
        $this->authorize('read', Type::class);
        $data = $this->model->all($request->all());
        $data = $data->where('parent', 'hrm-relation')->get();
        $pageTitle = __('hrm.all_types_of_relations');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'data' => $data
        ];
        return view('hrm.relations.index', $viewParams);
    }

    public function store(Request $request)
    {
        $this->authorize('create', Type::class);
        $input = $request->all();
        $length = count($input['name']);
        for ($i = 0; $i < $length; $i++) {
            $name = $input['name'][$i];
            $name_ur = $input['name_ur'][$i];
            if (isset($input['id'][$i]) && !empty($input['id'][$i])) {
                $this->model->update([
                    'name' => $name,
                    'name_ur' => $name_ur
                ], $input['id'][$i]);
            } else {
                $this->model->create([
                    'name' => $name,
                    'name_ur' => $name_ur,
                    'parent' => 'hrm-relation',
                ]);
            }
        }//end of the for loop
        return redirect()->back()->with('successMessage', "Card types  created/updated successfully");
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function generate_relation_in_english_with_ajax(Request $request)
    {
        $name = $request->input('name', null);
        $this->model = $this->model->updateOrCreate(
            ['name' => $name, 'parent' => 'hrm-relation'],
            ['name' => $name, 'parent' => 'hrm-relation']
        );
        return \response()->json([
            'model' => $this->model,
            'status' => true
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function generate_relation_in_urdu_with_ajax(Request $request)
    {
        $name_ur = $request->input('name', null);
        $this->model = $this->model->updateOrCreate(
            ['name_ur' => $name_ur, 'parent' => 'hrm-relation'],
            ['name_ur' => $name_ur, 'parent' => 'hrm-relation']
        );
        return \response()->json([
            'model' => $this->model,
            'status' => true
        ]);
    }
}

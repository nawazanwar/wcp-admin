<?php

namespace App\Http\Controllers\HRM;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Designation;
use App\Models\Permission;
use App\Models\Type;
use App\Repositories\CurdRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DesignationController extends Controller
{

    protected $model;

    public function __construct(Designation $designation)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($designation);
    }


    public function index(Request $request)
    {
        $this->authorize('read', Designation::class);
        $pageTitle = __('hrm.list_all_designations');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'data' => $this->model->all($request->all())->paginate(12)
        ];
        return view('hrm.designations.index', $viewParams);
    }

    public function create()
    {
        $this->authorize('create', Designation::class);
        $permissions = Permission::all()->pluck('name', 'id');
        $pageTitle = __('hrm.create_new_designation');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'permissions' => $permissions
        ];
        return view('hrm.designations.create', $viewParams);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'name_ur' => 'required|max:255',
            'active' => 'boolean:required',
            'shift' => 'required'
        ]);
        $this->authorize('create', Designation::class);
        $data = $request->only($this->model->getModel()->fillable);
        $data['active'] = $request->get('active') ? 1 : 0;
        $this->model = $this->model->updateOrCreate(
            ['name' => $data['name'], 'name_ur' => $data['name_ur'], 'shift' => $data['shift']], $data
        );
        if ($this->model) {
            $permissions = $request->input('permissions', array());
            if ($permissions){
                $this->model->permissions()->sync($permissions);
            }
            return redirect()->route('hrm-designations.index')->with('successMessage', __('hrm.department_created_success_message'));
        }
    }

    public function edit($id)
    {
        $permissions = Permission::all()->pluck('name', 'id');
        $this->authorize('edit', Designation::class);
        $this->model = $this->model->find($id);
        $pageTitle = __('hrm.edit') . $this->model->name;
        $viewParams = [
            'pageTitle' => $pageTitle,
            'model' => $this->model,
            'permissions' => $permissions
        ];
        return view('hrm.designations.edit', $viewParams);

    }

    public function update(Request $request, $id)
    {
        $this->authorize('edit', Designation::class);
        $request->validate([
            'name' => 'required|max:255',
            'name_ur' => 'required|max:255',
            'active' => 'boolean:required',
            'shift' => 'required'
        ]);
        $data = $request->only($this->model->getModel()->fillable);
        $data['active'] = $request->get('active') ? 1 : 0;
        $this->model->updateOrCreate(
            ['name' => $data['name'], 'name_ur' => $data['name_ur'], 'shift' => $data['shift']], $data
        );
        $this->model = $this->model->find($id);
        if ($this->model) {
            $permissions = $request->input('permissions', array());
            if ($permissions){
                $this->model->permissions()->sync($permissions);
            }
            return redirect()->route('hrm-designations.index')->with('successMessage', $this->model->name . " " . __('hrm.updated_success_message'));
        }
    }

    public function destroy($id)
    {
        $this->authorize('delete', Designation::class);
        $this->model = $this->model->find($id);
        if ($this->model->delete($id)) {
            return redirect()->route('hrm-designations.index')->with('errorMessage', $this->model->name . " " . __('hrm.deleted_successfully'));
        }
    }

    /**
     * @param Request $request
     * @return void
     */
    public function generate_with_ajax(Request $request)
    {
        $name = $request->input('name', null);
        $this->model->updateOrCreate(
            ['name' => $name], ['name' => $name]
        );
    }
}

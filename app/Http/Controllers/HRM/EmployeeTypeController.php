<?php

namespace App\Http\Controllers\HRM;

use App\Http\Controllers\Controller;
use App\Models\Employer;
use App\Models\Type;
use App\Repositories\CurdRepository;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class EmployeeTypeController extends Controller
{
    protected $model;

    public function __construct(Type $type)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($type);
    }

    public function index(Request $request)
    {

        $this->authorize('read', Type::class);
        $data = $this->model->all($request->all());
        $data = $data->where('parent', 'employee')->orderBy('types.id', 'desc')->get();
        $pageTitle = __('hrm.all_types_of_employee');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'data' => $data
        ];
        return view('hrm.emp-types.index', $viewParams);
    }

    public function store(Request $request)
    {
        $this->authorize('create', Type::class);
        $input = $request->all();
        $length = count($input['name']);
        for ($i = 0; $i < $length; $i++) {
            $name = $input['name'][$i];
            $name_ur = $input['name_ur'][$i];
            $id = $input['id'][$i];
            if (isset($id) && !empty($id)) {
                $this->model->updateOrCreate(
                    ['id' => $id, 'parent' => 'employee'],
                    ['name' => $name, 'name_ur' => $name_ur, 'parent' => 'employee']
                );
            }
        }//end of the for loop
        return redirect()->route("hrm-emp-types.index")->with('successMessage', "Employee types  created/updated successfully");
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function generate_with_ajax(Request $request)
    {
        $name = $request->input('name', null);
        $this->model = $this->model->updateOrCreate(
            ['name' => $name, 'parent' => 'employee'],
            ['name' => $name, 'parent' => 'employee']
        );
        return \response()->json([
            'model' => $this->model,
            'status' => true
        ]);
    }

}

<?php

namespace App\Http\Controllers\HRM;

use App\Http\Controllers\Controller;
use App\Models\Employer;
use App\Models\Type;
use App\Repositories\CurdRepository;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CardTypeController extends Controller
{
    protected $model;

    public function __construct(Type $type)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($type);
    }

    public function index(Request $request)
    {
        $this->authorize('read', Type::class);
        $data = $this->model->all($request->all());
        $data = $data->where('parent', 'hrm-card')->get();
        $pageTitle = __('hrm.all_types_of_card');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'data' => $data
        ];
        return view('hrm.card-types.index', $viewParams);
    }

    public function store(Request $request)
    {
        $this->authorize('create', Type::class);
        $input = $request->all();
        $data = array();
        $length = count($input['name']);
        for ($i = 0; $i < $length; $i++) {
            $name = $input['name'][$i];
            if (isset($input['id'][$i]) && !empty($input['id'][$i])) {
                $this->model->update([
                    'name' => $name,
                ], $input['id'][$i]);
            } else {
                $this->model->create([
                    'name' => $name,
                    'parent' => 'hrm-card',
                ]);
            }
        }//end of the for loop
        return redirect()->back()->with('successMessage', "Card types  created/updated successfully");
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function generate_with_ajax(Request $request)
    {
        $name = $request->input('name', null);
        $this->model = $this->model->updateOrCreate(
            ['name' => $name, 'parent' => 'hrm-card'],
            ['name' => $name, 'parent' => 'hrm-card']
        );
        return \response()->json([
            'model' => $this->model,
            'status' => true
        ]);
    }
}

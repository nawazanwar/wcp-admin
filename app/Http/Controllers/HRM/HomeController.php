<?php

namespace App\Http\Controllers\HRM;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Designation;
use App\Models\Employer;
use App\Models\HRM;
use App\Models\Media;
use App\Models\Type;
use App\Models\User;
use App\Repositories\CurdRepository;
use App\Traits\Ed;
use App\Traits\General;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class HomeController extends Controller
{

    /**
     * @var CurdRepository
     */
    protected $model;

    use Ed;
    use General;

    /**
     * HomeController constructor.
     * @param HRM $hrm
     */
    public function __construct(HRM $hrm)
    {
        $this->makeMultipleDirectories('hrm', ['avatar', 'media', 'documents']);
        $this->middleware('auth');
        $this->model = new CurdRepository($hrm);
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     * @throws AuthorizationException
     */

    public function index(Request $request)
    {
        $this->authorize('read', HRM::class);
        $pageTitle = __('hrm.title');
        $breadcrumbs = [['text' => __('hrm.short_title')]];
        $data = $this->model->all($request->all())->orderby('id', 'DESC')->paginate(20);
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'data' => $data
        ];
        return view('hrm.index', $viewParams);
    }

    /**
     * @return Application|Factory|View
     * @throws AuthorizationException
     */

    public function create()
    {
        $this->authorize('create', HRM::class);;
        $pageTitle = __('hrm.create');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle
        ];
        return view('hrm.create', $viewParams);
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(Request $request)
    {

        $this->authorize('create', HRM::class);
        $data = $request->only($this->model->getModel()->fillable);
        $name = $request->input('f_name', null) . " " . $request->input('l_name', null);
        $user = new User();
        $user->name = $name;

        if ($user->save()) {

            $user->unique_id = uniqid();
            $user->password = Hash::make('user1234');
            $active = $request->input('active', false);
            $block = $request->input('block', false);
            $user->active = $active;
            $user->block = $block;
            $user->save();
            // now ready to save the user Designations
            if ($request->has('designations')) {
                $user->designations()->detach();
                $user->designations()->attach($request->input('designations', array()));
            }

            // If User has Upload a Cover Image

            if ($request->file('avatar')) {
                $file = $request->file('avatar');
                $avatar = Image::make($file);
                $avatar->resize(120, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $avatarName = $this->generateFileName($file);
                $avatar->save('uploads/hrm/avatar/' . $avatarName, 100);
                $data['avatar'] = $avatarName;
            }

            // If the user upload the First Image

            if ($request->file('first_picture')) {

                $firstPictureFile = $request->file('first_picture');
                $firstPicture = Image::make($firstPictureFile);
                $firstPictureName = $this->generateFileName($firstPictureFile);
                $firstPicture->save('uploads/hrm/media/' . $firstPictureName, 100);
                $data['first_picture'] = $firstPictureName;

            }

            // If the user upload the First Image

            if ($request->file('first_picture')) {

                $firstPictureFile = $request->file('first_picture');
                $firstPicture = Image::make($firstPictureFile);
                $firstPictureName = $this->generateFileName($firstPictureFile);
                $firstPicture->save('uploads/hrm/media/' . $firstPictureName, 100);
                $data['first_picture'] = $firstPictureName;

            }

            // If the user upload the Second Image

            if ($request->file('second_picture')) {

                $secondPictureFile = $request->file('second_picture');
                $secondPicture = Image::make($secondPictureFile);
                $secondPictureName = $this->generateFileName($secondPictureFile);
                $secondPicture->save('uploads/hrm/media/' . $secondPictureName, 100);
                $data['second_picture'] = $secondPictureName;

            }

            // If the user upload the Third Image

            if ($request->file('third_picture')) {

                $thirdPictureFile = $request->file('third_picture');
                $thirdPicture = Image::make($thirdPictureFile);
                $thirdPictureName = $this->generateFileName($thirdPictureFile);
                $thirdPicture->save('uploads/hrm/media/' . $thirdPictureName, 100);
                $data['third_picture'] = $thirdPictureName;

            }

            // If the user upload the Third Image

            if ($request->file('fourth_picture')) {

                $fourthPictureFile = $request->file('fourth_picture');
                $fourthPicture = Image::make($fourthPictureFile);
                $fourthPictureName = $this->generateFileName($fourthPictureFile);
                $fourthPicture->save('uploads/hrm/media/' . $fourthPictureName, 100);
                $data['fourth_picture'] = $fourthPictureName;

            }

        }

        //Now ready to store the HRM

        $data['user_id'] = $user->id;
        $this->model = $this->model->create($data);
        if ($this->model) {
            //Upload the documents
            if ($request->file('docs')) {
                $documents = $request->file('docs');
                foreach ($documents as $doc) {
                    $document = Image::make($doc);
                    $docName = $this->generateFileName($doc);
                    $document->save('uploads/hrm/documents/' . $docName, 100);
                    $this->model->media()->save(new Media([
                        'src' => $docName,
                        'type' => 'document'
                    ]));
                }
            }
            return redirect()->back()->with('successMessage', 'new User Create Successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param HRM $hrm
     * @return Response
     */
    public function show(HRM $hrm)
    {
        return view('hrm.show', compact('hrm'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param HRM $hrm
     * @return Response
     * @throws Exception
     */
    public function destroy(HRM $hrm)
    {
        $hrm->delete();
        return redirect()->back()->with('successMessage', 'Record Deleted Successfully');
    }

    /**
     * @param HRM $hrm
     * @return Application|Factory|View
     * @throws AuthorizationException
     */

    public function edit(HRM $hrm)
    {
        $this->authorize('edit', HRM::class);
        $pageTitle = __('hrm.edit');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'hrm' => $hrm
        ];
        return view('hrm.edit', $viewParams);
    }

    /**
     * @param Request $request
     * @param HRM $hrm
     * @return RedirectResponse
     * @throws AuthorizationException
     */

    public function update(Request $request, HRM $hrm)
    {
        $this->authorize('edit', HRM::class);
        $data = $request->only($this->model->getModel()->fillable);
        $user = User::find($hrm->user_id);
        $name = $request->input('f_name', null) . " " . $request->input('l_name', null);
        $user->name = $name;
        if ($user->save()) {

            // now ready to save the user Designations
            if ($request->has('designations')) {
                $user->designations()->detach();
                $user->designations()->attach($request->input('designations', array()));
            }

            $user->active = $request->input('active', false);
            $user->block = $request->input('block', false);
            $user->save();

            // If User has Upload a Cover Image

            if ($request->file('avatar')) {
                $oldAvatar = public_path('uploads/hrm/avatar/' . $user->avatar);
                if (file_exists($oldAvatar) && !is_dir($oldAvatar)) {
                    unlink($oldAvatar);
                }
                $file = $request->file('avatar');
                $avatar = Image::make($file);
                $avatar->resize(120, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $avatarName = $this->generateFileName($file);
                $avatar->save('uploads/hrm/avatar/' . $avatarName, 100);
                $user->avatar = $avatarName;
                $user->save();
            }

            // If the user upload the First Image

            if ($request->file('first_picture')) {
                $oldFirstPicture = public_path('uploads/hrm/media/' . $hrm->first_picture);
                if (file_exists($oldFirstPicture) && !is_dir($oldFirstPicture)) {
                    unlink($oldFirstPicture);
                }
                $firstPictureFile = $request->file('first_picture');
                $firstPicture = Image::make($firstPictureFile);
                $firstPictureName = $this->generateFileName($firstPictureFile);
                $firstPicture->save('uploads/hrm/media/' . $firstPictureName, 100);
                $data['first_picture'] = $firstPictureName;

            }

            // If the user upload the Second Image

            if ($request->file('second_picture')) {
                $oldSecondPicture = public_path('uploads/hrm/media/' . $hrm->second_picture);
                if (file_exists($oldSecondPicture) && !is_dir($oldSecondPicture)) {
                    unlink($oldSecondPicture);
                }
                $secondPictureFile = $request->file('second_picture');
                $secondPicture = Image::make($secondPictureFile);
                $secondPictureName = $this->generateFileName($secondPictureFile);
                $secondPicture->save('uploads/hrm/media/' . $secondPictureName, 100);
                $data['second_picture'] = $secondPictureName;

            }

            // If the user upload the Third Image

            if ($request->file('third_picture')) {
                $oldThirdPicture = public_path('uploads/hrm/media/' . $hrm->third_picture);
                if (file_exists($oldThirdPicture) && !is_dir($oldThirdPicture)) {
                    unlink($oldThirdPicture);
                }
                $thirdPictureFile = $request->file('third_picture');
                $thirdPicture = Image::make($thirdPictureFile);
                $thirdPictureName = $this->generateFileName($thirdPictureFile);
                $thirdPicture->save('uploads/hrm/media/' . $thirdPictureName, 100);
                $data['third_picture'] = $thirdPictureName;

            }

            // If the user upload the Third Image

            if ($request->file('fourth_picture')) {
                $oldFourthPicture = public_path('uploads/hrm/media/' . $hrm->fourth_picture);
                if (file_exists($oldFourthPicture) && !is_dir($oldFourthPicture)) {
                    unlink($oldFourthPicture);
                }
                $fourthPictureFile = $request->file('fourth_picture');
                $fourthPicture = Image::make($fourthPictureFile);
                $fourthPictureName = $this->generateFileName($fourthPictureFile);
                $fourthPicture->save('uploads/hrm/media/' . $fourthPictureName, 100);
                $data['fourth_picture'] = $fourthPictureName;

            }

            $this->model->update($data, $hrm->id);
            $this->model = $hrm;

            if ($request->file('docs')) {
                $documents = $request->file('docs');
                foreach ($documents as $doc) {
                    $document = Image::make($doc);
                    $docName = $this->generateFileName($doc);
                    $document->save('uploads/hrm/documents/' . $docName, 100);
                    $this->model->media()->save(new Media([
                        'src' => $docName,
                        'type' => 'document'
                    ]));
                }
            }

            return redirect()->back()->with('successMessage', 'Record Updated Successfully');
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\VCompany;
use App\Models\Visitor;
use App\Models\VModel;
use App\Models\VNature;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VisitorController extends Controller
{

    /**
     * Show the form for editing the specified resource.
     *
     * @param Visitor $visitor
     * @return Response
     */
    public function edit(Visitor $visitor)
    {
        $pageTitle = "Edit " . $visitor->vehicle_reg_no;
        $v_companies = array();
        $v_natures = array();
        if ($visitor->type_id) {
            $v_companies = VCompany::whereTypeId($visitor->type_id)->pluck( 'name','id');
            $v_natures = VNature::whereTypeId($visitor->type_id)->pluck( 'name','id');
        }

        $v_models = array();
        if ($visitor->vcompany_id){
            $v_models = VModel::where('vcompany_id',$visitor->vcompany_id)->pluck('name','id');
        }
        return view('visitors.edit', compact('visitor', 'pageTitle','v_companies','v_models','v_natures'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param Visitor $visitor
     * @return void
     */
    public function update(Request $request, Visitor $visitor)
    {
        $visitor->update($request->all());
        if ($request->has('visitor_biometric_verification')){
            $visitor->visitor_biometric_verification = true;
        }else{
            $visitor->visitor_biometric_verification = false;
        }
        if ($request->has('visitor_ratina_verification')){
            $visitor->visitor_ratina_verification = true;
        }else{
            $visitor->visitor_ratina_verification = false;
        }
        if ($request->has('visitor_gate_pass_verification')){
            $visitor->visitor_gate_pass_verification = true;
        }else{
            $visitor->visitor_gate_pass_verification = false;
        }
        if ($request->has('visitor_qrcode_verification')){
            $visitor->visitor_qrcode_verification = true;
        }else{
            $visitor->visitor_qrcode_verification = false;
        }
        $visitor->save();
        return redirect()->back()->with('success','Visitor updated successfully');
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\Card;
use App\Models\CsvData;
use App\Models\Log;
use App\Models\Media;
use App\Models\Profile;
use App\Models\Role;
use App\Models\Type;
use App\Models\Umedia;
use App\Models\User;
use App\Traits\Ed;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use App\Traits\General;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ExportUsers;
use PDF;

class UserController extends Controller
{
    use General, Ed;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = new User();
        $data = $data->where('type_id', Type::where('name', 'management')->value('id'));
        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        $filters = $request->query('filter', null);
        if ($field && $keyword) {
            $data = $data->where('users.' . $field, 'like', '%' . $keyword . '%');
        }
        if (!empty($filters)) {
            $data = $data->where(function ($q) use ($filters) {
                foreach ($filters as $key => $value) {
                    $q->where($key, '=', $value);
                }
            });
        }
        $data = $data->withTrashed()->where('id', '!=', 1);
        $data = $data->orderby('users.id', 'DESC')->paginate(10);
        $pageTitle = __('system.all_users');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'field' => $field,
            'keyword' => $keyword,
            'filters' => $filters,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'data' => $data,
        ];
        return view('users.index', $viewParams);
    }

    public function create()
    {
        $types = Type::where('name', 'management')->pluck('name', 'id');
        $pageTitle = __('system.create_user');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'types' => $types
        ];
        return view('users.create', $viewParams);
    }

    public function store(Request $request)
    {
        /*apply validations here*/
        $request->validate([

            'email' => 'required|email|max:255|unique:users,email',
            'password' => ['required', 'min:3'],
            'first_name' => 'required|min:2',
            'parent_first_name' => 'required:min:2',
            'type' => 'required'

        ]);

        /*if validation success then ready to save*/

        $first_name = $request->input('first_name', null);
        $middle_name = $request->input('middle_name', null);
        $last_name = $request->input('last_name', null);

        //generate the user name from user first+middle+lastname

        $username = $first_name . " " . $last_name . " " . $last_name;
        $this->makeMultipleDirectories('users', ['avatar', 'cnic']);
        $user = new User();

        /*set the user type*/
        $type = $request->input('type', null);
        $user->type()->associate(Type::where('id', $type)->first());

        /*user info*/

        $user->suffix = $request->input('suffix');
        $user->first_name = $first_name;
        $user->middle_name = $middle_name;
        $user->last_name = $last_name;
        $user->username = $username;
        $user->email = $request->input('email');
        $user->active = $request->input('active', 1);
        $user->password = Hash::make($request->input('password'));

        $user->parent_suffix = $request->input('parent_suffix');
        $user->parent_first_name = $request->input('parent_first_name');
        $user->parent_middle_name = $request->input('parent_middle_name');
        $user->parent_last_name = $request->input('parent_last_name');
        $user->permanent_address = $request->input('permanent_address');
        $user->permanent_city = $request->input('permanent_city');
        $user->permanent_state = $request->input('permanent_state');
        $user->permanent_country = $request->input('permanent_country');
        $user->permanent_postalcode = $request->input('permanent_postalcode');
        $user->temporary_address = $request->input('temporary_address');
        $user->temporary_city = $request->input('temporary_city');
        $user->temporary_state = $request->input('temporary_state');
        $user->temporary_country = $request->input('temporary_country');
        $user->temporary_postalcode = $request->input('temporary_postalcode');
        $user->registered_by = $request->input('registered_by');
        $user->cnic = $request->input('cnic');
        $user->cnic_verify_status_nadra = $request->input('cnic_verify_status_nadra');
        $user->term = $request->input('term');
        $user->designation = $request->input('designation');
        $user->employer = $request->input('employer');
        $user->working_since = $request->input('working_since');
        $user->phone_number = $request->input('phone_number');
        $user->cell_number = $request->input('cell_number');
        $user->reference_phone_number = $request->input('reference_phone_number');
        $user->created_by = Auth::user()->id;

        if ($user->save()) {

            if ($request->file('avatar')) {
                $this->manageImage($user, $request->file('avatar'), 'avatar', uniqid());
            }
            //save CNIC images
            if ($request->file('cnic_media')) {
                $counter = 0;
                foreach ($request->file('cnic_media') as $image) {
                    $this->manageImage($user, $image, 'cnic', $counter);
                    $counter++;
                }
            }
            return redirect()->route('users')->with('successMessage', __('message.user_created_success_message'));
        }

    }

    public function manageImage($user, $image, $type, $uniqueId)
    {
        $imageName = sha1($type . "" . $uniqueId . "" . $user->id . time()) . '.' . $image->getClientOriginalExtension();
        $newImage = Image::make($image);
        $newImage->save(public_path("uploads/users/" . $type . "/" . $imageName));
        $media = new Umedia();
        $media->src = $imageName;
        $media->type = $type;
        $user->media()->save($media);
    }

    public function saveUserCard($user, $array = array())
    {
        $card = new Card();
        $card->name = $array['name'];
        $card->barcode = $array['barcode'];
        $card->rfid = $array['rfid'];
        $user->cards()->save($card);
    }

    public function show(Request $request, $id)
    {
        $model = User::find($this->decrypt($id));
        $pageTitle = __('system.detail_of') . " " . $model->username;
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'logs' => $this->logs()
        ];
        return view('users.show', $viewParams);
    }

    public function edit($id)
    {
        $model = User::find($this->decrypt($id));
        $pageTitle = __('system.edit') . " " . $model->name;
        $breadcrumbs = [['text' => __('system.edit_user')]];
        $viewParams = [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle
        ];

        return view('users.edit', $viewParams);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|min:4|unique:roles,name,' . $id,
            'label' => 'required|max:255',
        ]);

        if (Role::whereId($id)->update($validatedData)) {
            return redirect()->route('system.roles')->with('successMessage', $validatedData['label'] . " " . __('system.updated_success_message'));
        } else {
            return redirect()->back()
                ->withErrors($validatedData['label'] . " " . __('system.updated_failed_message_message'))
                ->withInput();
        }
    }

    public function destroy(Request $request, $id = null)
    {
        if (Auth::user()->ability('delete_user')) {
            $for = $request->get('for');
            if (isset($for) && $for == 'multiple') {
                $ids = explode(",", $request->ids);
                foreach ($ids as $id) {
                    $user = User::find((int)$this->decrypt($id));
                    $user->active = false;
                    if ($user->save) {
                        $user->delete();
                    }
                }
            } else {
                $user = User::find($id);
                $user->active = false;
                if ($user->save) {
                    $user->delete();
                }
            }
            return response()->json(["status" => "success"]);
        } else {
            return response()->json(["status" => "no_permission"]);
        }
    }

    public function restore(Request $request, $id = null)
    {
        if (Auth::user()->ability('restore_user')) {
            User::withTrashed()->where('id', $this->decrypt($id))->first()->restore();
            return response()->json(["status" => "success"]);
        } else {
            return response()->json(["status" => "no_permission"]);
        }
    }

    public function roles($id, Request $request)
    {
        $model = User::find($this->decrypt($id));
        $data = Role::whereActive(true)->where('name', '!=', 'super-admin')->paginate(32);
        $pageTitle = __('system.all_roles_of') . " " . $model->username;
        $breadcrumbs = [['text' => __('system.all_permissions')]];
        $viewParams = [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'data' => $data
        ];
        return view('users.roles', $viewParams);
    }

    public function import()
    {
        $pageTitle = __('system.create_import');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle
        ];
        return view('users.import', $viewParams);
    }

    public function export()
    {
        $filename = "users_excel_" . date('Y-m-d H:i:s:u');
        return Excel::download(new ExportUsers(), $filename . '.xlsx');
    }

    public function previewImport(Request $request)
    {
        $path = $request->file('csv_file')->getRealPath();
        $data = array_map('str_getcsv', file($path));
        $csv_data_file = CsvData::create([
            'csv_filename' => $request->file('csv_file')->getClientOriginalName(),
            'csv_header' => $request->has('header'),
            'csv_data' => json_encode($data)
        ]);
        $csv_data = array_slice($data, 0, 2);
        $pageTitle = __('system.preview_import');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'csv_data' => $csv_data,
            'csv_data_id' => $csv_data_file->id
        ];
        return view('users.import', $viewParams);
    }

    public function syncImport(Request $request)
    {
        $data = CsvData::find($request->csv_data_file_id);
        $csv_data = json_decode($data->csv_data, true);
        foreach ($csv_data as $row) {

            $user = new User();
            foreach (config('importfields.user_fields') as $index => $field) {

                $user->$field = $row[$request->fields[$index]];

            }
            if (!User::where('email', '=', $user->email)->exists()) {
                if ($user->save()) {
                    $user->profile()->save(new Profile());
                }
            }
        }
        return redirect()->route('users')->with('successMessage', __('message.user_sync_successfully'));
    }

    public function exportPDF()
    {
        $data = User::all();
        $pdf = PDF::loadView('users.pdf', compact('data'));
        $filename = "users_pdf_" . date('Y-m-d H:i:s:u');
        return $pdf->download($filename . '.pdf');
    }

    public function autocomplete(Request $request)
    {
        $search = $request->term;
        $data = User::where('name', 'LIKE', "%{$search}%")->where('id', '!=', '1')->orderBy('created_at', 'DESC')->limit(5)->get();
        $response = array();
        if (!$data->isEmpty()) {
            $new_row = array();
            foreach ($data as $d) {
                $new_row['id'] = $d->id;
                $new_row['name'] = $d->name;
                $new_row['image'] = $d->getAvatar();
                $response[] = $new_row;
            }
        }
        echo json_encode($response);
    }

    public function get_logs(Request $request, $id = null)
    {
        $pageTitle = __('system.log_of_users');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'logs' => isset($id)?$this->logs($id):$this->logs()
        ];
        return view('users.logs', $viewParams);
    }

    public function logs($id = null)
    {
        $logs = DB::table('logs as l')
            ->join('auth_logs as al', 'l.id', '=', 'al.log_id')
            ->join('users as u', 'u.id', '=', 'al.auth_id')
            ->where('l.name', 'auth');
        if (isset($id) && $id != '') {
            $logs = $logs->where('u.id', $id);
        }
        $logs = $logs->select('u.username as name',
            'al.date_time as date_time',
            'al.ip_address as ip_address',
            'al.mac_address as mac_address',
            'al.type as type',
            'u.id as id')
            ->orderby('al.date_time', 'desc')->get();
        $data = [];
        foreach ($logs as $log) {
            $data[date("d M , Y", strtotime($log->date_time))][] = $log;
        }
        return $data;
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\User;
use App\Repositories\CurdRepository;
use Illuminate\Http\Request;
use Carbon\Carbon;

class NotificationController extends Controller
{
    protected $model;

    public function __construct(Notification $notification)
    {
        $this->middleware('auth');
        $this->model = new CurdRepository($notification);
    }

    public function index(Request $request)
    {
        if ($request->has('type')){
            echo "show all";
        }else{
            return response()->json([
                'data' => view('notifications.index')->render(),
                'unread_notifications' => auth()->user()->unreadNotifications->count()
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $reason = $request->input('reason', null);
        $this->model = $this->model->update([
            'read_reason' => $reason,
            'read_at' => Carbon::now(),
            'reader_id' => auth()->user()->id
        ], $id);
        $this->model = Notification::find($id);
        if ($this->model) {
            return response()->json([
                'status' => "true",
                'reader' => User::find($this->model->reader_id)->name
            ]);
        }
    }
}

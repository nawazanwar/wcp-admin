<?php

namespace App\Http\Controllers\Device;

use App\Http\Controllers\Controller;
use App\Models\Device;
use App\Models\Employer;
use App\Device\DMake;
use App\Repositories\CurdRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MakeController extends Controller
{
    protected $model;

    public function __construct(DMake $DMake)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($DMake);
    }

    public function index(Request $request)
    {
        $this->authorize('read', Device::class);
        $data = $this->model->all($request->all());
        $pageTitle = __('device.all_makers');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'data' => $data->paginate(20)
        ];
        return view('device.make.index', $viewParams);
    }

    public function create(Request $request)
    {
        $this->authorize('create', Device::class);
        $pageTitle = __('device.create_new_make');
        $viewParams = [
            'pageTitle' => $pageTitle,
        ];
        return view('device.make.create', $viewParams);
    }

    public function store(Request $request)
    {
        $this->authorize('create', Device::class);
        $request->validate([
            'name' => 'required|max:255|unique:dmakes,name',
            'name_ur' => 'max:255|unique:dmakes,name_ur',
        ]);
        if ($this->model->create($request->only($this->model->getModel()->fillable))) {
            return redirect()->route('device-make.index')->with('successMessage', __('device.make_created_success_message'));
        }
    }

    public function edit($id)
    {
        $this->authorize('edit', Device::class);
        $this->model = $this->model->find($id);
        $pageTitle = __('device.edit') . $this->model->name;
        $viewParams = [
            'pageTitle' => $pageTitle,
            'model' => $this->model,
        ];
        return view('device.make.edit', $viewParams);

    }

    public function update(Request $request,$id)
    {
        $request->validate([
            'name' => 'required|max:255|unique:dmakes,name,' . $id,
            'name_ur' => 'unique:dmakes,name,' . $id,
        ]);
        $this->authorize('edit', Device::class);
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        $this->model = $this->model->find($id);
        if ($this->model) {
            return redirect()->route('device-make.index')->with('successMessage', $this->model->name . " " . __('device.updated_success_message'));
        }
    }

    public function destroy($id)
    {
        $this->authorize('delete', Device::class);
        $this->model = $this->model->find($id);
        if ($this->model->delete($id)) {
            return redirect()->route('device-make.index')->with('errorMessage', $this->model->name . " " . __('device.deleted_successfully'));
        }
    }
}

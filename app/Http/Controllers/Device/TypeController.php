<?php

namespace App\Http\Controllers\Device;

use App\Http\Controllers\Controller;
use App\Models\Device;
use App\Models\Employer;
use App\Device\DType;
use App\Repositories\CurdRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TypeController extends Controller
{
    protected $model;

    public function __construct(DType $DType)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($DType);
    }

    public function index(Request $request)
    {
        $this->authorize('read', Device::class);
        $data = $this->model->all($request->all());
        $pageTitle = __('device.all_types');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'data' => $data->paginate(20)
        ];
        return view('device.type.index', $viewParams);
    }

    public function create(Request $request)
    {
        $this->authorize('create', Device::class);
        $pageTitle = __('device.create_new_type');
        $viewParams = [
            'pageTitle' => $pageTitle,
        ];
        return view('device.type.create', $viewParams);
    }

    public function store(Request $request)
    {
        $this->authorize('create', Device::class);
        $request->validate([
            'name' => 'required|max:255|unique:dtypes,name',
            'name_ur' => 'max:255|unique:dtypes,name_ur',
        ]);
        if ($this->model->create($request->only($this->model->getModel()->fillable))) {
            return redirect()->route('device-type.index')->with('successMessage', __('device.type_created_success_message'));
        }
    }

    public function edit($id)
    {
        $this->authorize('edit', Device::class);
        $this->model = $this->model->find($id);
        $pageTitle = __('device.edit') . $this->model->name;
        $viewParams = [
            'pageTitle' => $pageTitle,
            'model' => $this->model,
        ];
        return view('device.type.edit', $viewParams);

    }

    public function update(Request $request,$id)
    {
        $request->validate([
            'name' => 'required|max:255|unique:dtypes,name,' . $id,
            'name_ur' => 'unique:dtypes,name,' . $id,
        ]);
        $this->authorize('edit', Device::class);
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        $this->model = $this->model->find($id);
        if ($this->model) {
            return redirect()->route('device-type.index')->with('successMessage', $this->model->name . " " . __('device.updated_success_message'));
        }
    }

    public function destroy($id)
    {
        $this->authorize('delete', Device::class);
        $this->model = $this->model->find($id);
        if ($this->model->delete($id)) {
            return redirect()->route('device-type.index')->with('errorMessage', $this->model->name . " " . __('device.deleted_successfully'));
        }
    }
}

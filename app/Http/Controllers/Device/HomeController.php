<?php

namespace App\Http\Controllers\Device;

use App\Device\DClass;
use App\Device\DLocation;
use App\Device\DMake;
use App\Device\DModel;
use App\Device\DOperatingSystem;
use App\Device\DType;
use App\Http\Controllers\Controller;
use App\Models\Device;
use App\Models\Type;
use App\Repositories\CurdRepository;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Traits\General;

class HomeController extends Controller
{
    protected $model;

    public function __construct(Device $device)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($device);
    }

    public function index(Request $request)
    {
        $this->authorize('read', Device::class);
        $data = $this->model->all($request->all());
        $pageTitle = __('device.title');
        $breadcrumbs = [['text' => __('device.short_title')]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'data' => $data->paginate(20)
        ];
        return view('device.index', $viewParams);
    }

    public function create(Request $request)
    {
        $this->authorize('create', Device::class);
        $pageTitle = __('device.create_new_device');
        $breadcrumbs = [['text' => __('device.short_title')]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle
        ];
        return view('device.create', $viewParams);


    }

    public function store(Request $request)
    {
        $this->authorize('create', Device::class);
        /*get all field that set in the fill able able array of model*/
        $data = $request->only($this->model->getModel()->fillable);
        $filter_array = [
            'dev_ip_address' => $data['dev_ip_address'],
            'dev_mac_address' => $data['dev_mac_address'],
            'dev_name' => $data['dev_name'],
            'booth_id' => $data['booth_id']
        ];
        $this->model = $this->model->updateOrCreate($filter_array, $data);
        if ($this->model) {
            if ($request->file('dev_pic')) {
                $imageName = sha1('img' . $this->model->id . time()) . '.' . $request->file('dev_pic')->getClientOriginalExtension();
                $image = Image::make($request->file('dev_pic'));
                $image->backup();
                $image->save(public_path('uploads/device/') . $imageName);
                $this->model->dev_pic = $imageName;
                $this->model->save();
            }
        }
        return redirect()->route('device.index')->with('successMessage', "Configuration saved successfully");
    }

    public function show($id)
    {
        $this->authorize('show', Device::class);
        $this->model = $this->model->find($id);
        $dtype = DType::where('id', $this->model->dtype_id)->get()->pluck('name', 'id');
        $dmodel = DModel::where('id', $this->model->dmodel_id)->get()->pluck('name', 'id');
        $dmake = DMake::where('id', $this->model->dmake_id)->get()->pluck('name', 'id');
        $dlocation = DLocation::where('id', $this->model->dlocation_id)->get()->pluck('name', 'id');
        $doperatingsys = DOperatingSystem::where('id', $this->model->doperatingsystem_id)->get()->pluck('name', 'id');
        $dclass = DClass::where('id', $this->model->dclass_id)->get()->pluck('name', 'id');
        $breadcrumbs = [['text' => __('device.short_title')]];
        $pageTitle = __('device.show') . $this->model->name;
        $viewParams = [
            'pageTitle' => $pageTitle,
            'breadcrumbs' => $breadcrumbs,
            'model' => $this->model,
            'dtype' => $dtype,
            'dmodel' => $dmodel,
            'dmake' => $dmake,
            'dlocation' => $dlocation,
            'doperatingsys' => $doperatingsys,
            'dclass' => $dclass,
        ];
        return view('device.show', $viewParams);

    }

    public function edit($id)
    {
        $this->authorize('edit', Device::class);

        $this->model = $this->model->find($id);
        $pageTitle = __('device.edit') . $this->model->name;
        $breadcrumbs = [['text' => __('device.short_title')]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'model' => $this->model,
        ];
        return view('device.edit', $viewParams);

    }

    public function update(Request $request, $id)
    {
        $this->authorize('edit', Device::class);
        $data = $request->only($this->model->getModel()->fillable);
        $this->model->update($data, $id);
        $this->model = Device::find($id);
        if ($this->model) {
            if ($request->file('dev_pic')) {
                $imageName = sha1('img' . $this->model->id . time()) . '.' . $request->file('dev_pic')->getClientOriginalExtension();
                $image = Image::make($request->file('dev_pic'));
                $image->backup();
                $image->save(public_path('uploads/device/') . $imageName);
                $this->model->dev_pic = $imageName;
                $this->model->save();
            }
        }

        if ($this->model) {
            return redirect()->route('device.index')->with('successMessage', $this->model->dev_name . " " . __('device.updated_success_message'));
        }
    }

    public function destroy($id)
    {
        $this->authorize('delete', Device::class);
        $this->model = $this->model->find($id);
        if ($this->model->delete($id)) {
            return redirect()->route('device.index')->with('errorMessage', $this->model->name . " " . __('device.deleted_successfully'));
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Gate;
use App\Models\VCompany;
use App\Models\VModel;
use App\Models\VNature;
use Illuminate\Http\Request;

class DropDownController extends Controller
{
    public function filter(Request $request)
    {
        if ($request->key == 'companyWithNature') {
            $companies = VCompany::where('type_id', $request->parent);
            $natures = VNature::where('type_id', $request->parent);
            $data = array();
            $data['companies'] = $companies->get();
            $data['natures'] = $natures->get();
            return response()->json(['status' => 'true', 'data' => $data]);
        } else if ($request->key == 'models') {
            $modal = VModel::where('vcompany_id', $request->parent);
            if ($modal->count() > 0) {
                $data = $modal->get();
                return response()->json(['status' => 'true', 'data' => $data]);
            } else {
                return response()->json(['status' => 'false']);
            }
        } else if ($request->key == 'phase_gates') {
            $modal = Gate::where('phase_id', $request->parent);
            if ($modal->count() > 0) {
                $data = $modal->get();
                return response()->json(['status' => 'true', 'data' => $data]);
            } else {
                return response()->json(['status' => 'false']);
            }
        }
    }
}

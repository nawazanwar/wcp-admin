<?php

namespace App\Http\Controllers;

use App\Models\Type;
use App\Models\VCompany;
use App\Models\VModel;
use App\Models\VNature;
use Illuminate\Http\Request;

class GenerationController extends Controller
{
    public function vehicleTypes(Request $request)
    {
        $isAjax = $request->input('isAjax', null);
        $name = $request->input('name', null);
        if ($isAjax == 'true') {
            $model = Type::updateOrCreate(
                ['name' => $name, 'parent' => 'vehicle'],
                ['name' => $name, 'name_ur' => null, 'parent' => 'vehicle']
            );
            return response()->json([
                'types' => Type::where('parent', 'vehicle')->get(),
                'model' => $model
            ]);
        }
    }

    public function vehicleCompanies(Request $request)
    {
        $isAjax = $request->input('isAjax', null);
        $name = $request->input('name', null);
        $type_id = $request->input('type_id', null);
        if ($isAjax == 'true') {
            $model = VCompany::updateOrCreate(
                ['name' => $name, 'type_id' => $type_id],
                ['name' => $name, 'type_id' => $type_id, 'name_ur' => null]
            );
            return response()->json([
                'companies' => VCompany::whereTypeId($type_id)->get(),
                'model' => $model
            ]);
        }
    }

    public function vehicleModels(Request $request)
    {
        $isAjax = $request->input('isAjax', null);
        $name = $request->post('name', null);
        $company_id = $request->post('company_id', null);
        if ($isAjax == 'true') {
            $model = VModel::updateOrCreate(
                ['name' => $name, 'vcompany_id' => $company_id],
                ['name' => $name, 'vcompany_id' => $company_id, 'name_ur' => null]
            );
            return response()->json([
                'companies' => VModel::where("vcompany_id", $company_id)->get(),
                'model' => $model
            ]);
        }
    }

    public function vehicleMaterialNature(Request $request)
    {
        $isAjax = $request->input('isAjax', null);
        $name = $request->input('name', null);
        $type_id = $request->input('type_id', null);
        if ($isAjax == 'true') {
            $model = VNature::updateOrCreate(
                ['name' => $name, 'type_id' => $type_id],
                ['name' => $name, 'type_id' => $type_id, 'name_ur' => null]
            );
            return response()->json([
                'natures' => VNature::whereTypeId($type_id)->get(),
                'model' => $model
            ]);
        }
    }
}

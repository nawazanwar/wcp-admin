<?php

namespace App\Http\Controllers;

use App\Models\Employer;
use App\Models\Message;
use App\Models\Type;
use App\Repositories\CurdRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    protected $model;

    public function __construct(Message $message)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($message);
    }

    public function index(Request $request)
    {
        $this->authorize('read', Message::class);
        $data = $this->model->all($request->all());
        $data = $data->orderBy('id', 'desc')->get();
        $pageTitle = __('system.view_all_messages');
        $viewParams = [
            'pageTitle' => $pageTitle,
            'data' => $data
        ];
        return view('messages.index', $viewParams);
    }

    public function store(Request $request)
    {
        $this->authorize('create', Message::class);
        $input = $request->all();
        $data = array();
        $length = count($input['name']);
        for ($i = 0; $i < $length; $i++) {
            $name = $input['name'][$i];
            $value = $input['value'][$i];
            $id = $input['id'][$i];
            if (isset($id) && !empty($id)) {
                $this->model->updateOrCreate(
                    ['id' => $id, 'name' => $name],
                    ['name' => $name, 'value' => $value]
                );
            }
        }//end of the for loop
        return redirect()->route("messages.index")->with('successMessage', "Messages  created/updated successfully");
    }
}

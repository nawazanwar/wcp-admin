<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class NotificationPolicy
{
    use HandlesAuthorization;

    public function call_visitee_notification(User $user)
    {
        return $user->ability('call_visitee_notification');
    }
}

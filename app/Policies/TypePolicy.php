<?php

namespace App\Policies;

use App\Models\Vehicle;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class TypePolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_type');
    }

    public function create(User $user)
    {
        return $user->ability('create_type');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_type');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_type');
    }
}

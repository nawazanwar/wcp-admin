<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class UserPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_user');
    }

    public function create(User $user)
    {
        return $user->ability('create_user');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_user');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_user');
    }

    public function import(User $user)
    {
        return $user->ability('import_user');
    }

    public function export(User $user)
    {
        return $user->ability('export_user');
    }

    public function print(User $user)
    {
        return $user->ability('print_user');
    }
    public function restore(User $user)
    {
        return $user->ability('restore_user');
    }
}

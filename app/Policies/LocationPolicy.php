<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class LocationPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_location');
    }

    public function create(User $user)
    {
        return $user->ability('create_location');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_location');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_location');
    }
    public function import(User $user)
    {
        return $user->ability('import_location');
    }
    public function export(User $user)
    {
        return $user->ability('export_location');
    }
}

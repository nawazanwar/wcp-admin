<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class DepartmentPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_department');
    }

    public function create(User $user)
    {
        return $user->ability('create_department');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_department');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_department');
    }
}

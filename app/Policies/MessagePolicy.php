<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class MessagePolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_message');
    }

    public function create(User $user)
    {
        return $user->ability('create_message');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_message');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_message');
    }
}

<?php

namespace App\Policies;

use App\Models\Vehicle;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class HRMPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_hrm');
    }

    public function create(User $user)
    {
        return $user->ability('create_hrm');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_hrm');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_hrm');
    }

    public function import(User $user)
    {
        return $user->ability('import_hrm');
    }

    public function export(User $user)
    {
        return $user->ability('export_hrm');
    }
}

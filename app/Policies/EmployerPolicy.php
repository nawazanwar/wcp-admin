<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class EmployerPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_employer');
    }

    public function create(User $user)
    {
        return $user->ability('create_employer');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_employer');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_employer');
    }
}

<?php

namespace App\Policies;

use App\Models\Vehicle;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class VRSPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_vrs');
    }

    public function create(User $user)
    {
        return $user->ability('create_vrs');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_vrs');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_vrs');
    }

    public function import(User $user)
    {
        return $user->ability('import_vrs');
    }

    public function export(User $user)
    {
        return $user->ability('export_vrs');
    }
}

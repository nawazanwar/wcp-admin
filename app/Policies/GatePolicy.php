<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class GatePolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_gate');
    }

    public function create(User $user)
    {
        return $user->ability('create_gate');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_gate');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_gate');
    }
}

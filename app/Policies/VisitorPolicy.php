<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class VisitorPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_visitor');
    }

    public function export(User $user)
    {
        return $user->ability('export_visitor');
    }
}

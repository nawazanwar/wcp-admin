<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class VModelPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_company_model');
    }

    public function create(User $user)
    {
        return $user->ability('create_company_model');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_company_model');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_company_model');
    }
}

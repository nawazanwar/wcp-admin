<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class LRMApplicationPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_lrm_application');
    }

    public function create(User $user)
    {
        return $user->ability('create_lrm_application');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_lrm_application');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_lrm_application');
    }
}

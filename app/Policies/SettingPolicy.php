<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class SettingPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_setting');
    }

    public function create(User $user)
    {
        return $user->ability('create_setting');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_setting');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_setting');
    }
}

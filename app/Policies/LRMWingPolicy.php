<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class LRMWingPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_lrm_wing');
    }

    public function create(User $user)
    {
        return $user->ability('create_lrm_wing');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_lrm_wing');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_lrm_wing');
    }
}

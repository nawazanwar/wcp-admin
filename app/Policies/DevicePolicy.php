<?php

namespace App\Policies;

use App\Models\Device;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class DevicePolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_device');
    }

    public function create(User $user)
    {
        return $user->ability('create_device');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_device');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_device');
    }

    public function show(User $user)
    {
        return $user->ability('show_device');
    }

    public function import(User $user)
    {
        return $user->ability('import_device');
    }

    public function export(User $user)
    {
        return $user->ability('export_device');
    }
}

<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class DClassPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_device_class');
    }

    public function create(User $user)
    {
        return $user->ability('create_device_class');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_device_class');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_device_class');
    }
}

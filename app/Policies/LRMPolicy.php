<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class LRMPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_land_record_form');
    }

    public function create(User $user)
    {
        return $user->ability('create_land_record_form');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_land_record_form');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_land_record_form');
    }
}

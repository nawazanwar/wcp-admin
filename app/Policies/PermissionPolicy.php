<?php

namespace App\Policies;

use App\Models\Permission;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class PermissionPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_permission');
    }

    public function assign(User $user)
    {
        return $user->ability('assign_permission');
    }

    public function sync(User $user)
    {
        return $user->ability('sync_permission');
    }

    public function create(User $user)
    {
        return $user->ability('create_permission');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_permission');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_permission');
    }

    public function import(User $user)
    {
        return $user->ability('import_permission');
    }

    public function export(User $user)
    {
        return $user->ability('export_permission');
    }

    public function restore(User $user)
    {
        return $user->ability('restore_permission');
    }

}

<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class VNaturePolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_vehicle_nature');
    }

    public function create(User $user)
    {
        return $user->ability('create_vehicle_nature');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_vehicle_nature');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_vehicle_nature');
    }
}

<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class DesignationPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_designation');
    }

    public function create(User $user)
    {
        return $user->ability('create_designation');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_designation');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_designation');
    }
}

<?php

namespace App\Policies;

use App\Models\Vehicle;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class BoothPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_booth');
    }

    public function create(User $user)
    {
        return $user->ability('create_booth');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_booth');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_booth');
    }

    public function operate(User $user)
    {
        return $user->ability('operate_booth');
    }

    public function import(User $user)
    {
        return $user->ability('import_booth');
    }

    public function export(User $user)
    {
        return $user->ability('export_booth');
    }

}

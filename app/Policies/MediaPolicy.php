<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class MediaPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_media');
    }

    public function create(User $user)
    {
        return $user->ability('create_media');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_media');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_media');
    }
    public function import(User $user)
    {
        return $user->ability('import_media');
    }
    public function export(User $user)
    {
        return $user->ability('export_media');
    }
}

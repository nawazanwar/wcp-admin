<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class EtagPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_etag');
    }


}

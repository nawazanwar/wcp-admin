<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class LogPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_log');
    }

    public function create(User $user)
    {
        return $user->ability('create_log');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_log');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_log');
    }
    public function import(User $user)
    {
        return $user->ability('import_log');
    }
    public function export(User $user)
    {
        return $user->ability('export_log');
    }
}

<?php

namespace App\Traits;

use App\Models\Type;
use Carbon\Carbon;

trait General
{
    /**
     * @param $file
     * @return string
     */

    public function generateFileName($file)
    {
        $avatarNameWithExt = $file->getClientOriginalName();
        $avatarName = pathinfo($avatarNameWithExt, PATHINFO_FILENAME);
        $avatarName = preg_replace("/[^A-Za-z0-9 ]/", '', $avatarName);
        $avatarName = preg_replace("/\s+/", '-', $avatarName);
        $avatarExtension = $file->getClientOriginalExtension();
        return $avatarName . '_' . time() . '.' . $avatarExtension;
    }

    public function getDateRange($key)
    {
        $dates = array();
        switch ($key) {
            case "today":
                $dates = [
                    'start' => Carbon::now()->startOfDay(),
                    'end' => Carbon::now()->endOfDay()
                ];
                break;
            case "yesterday":
                $dates = [
                    'start' => Carbon::yesterday()->startOfDay(),
                    'end' => Carbon::yesterday()->endOfDay()
                ];
                break;
            case "this_week":
                $dates = [
                    'start' => Carbon::parse('this week')->startOfWeek(Carbon::MONDAY),
                    'end' => Carbon::parse('this week')->endOfWeek(Carbon::SUNDAY)
                ];
                break;
            case "last_week":
                $dates = [
                    'start' => Carbon::parse('last week')->startOfWeek(Carbon::MONDAY),
                    'end' => Carbon::parse('last week')->endOfWeek(Carbon::SUNDAY)
                ];
                break;
            case "this_month":
                $dates = [
                    'start' => Carbon::parse('this month')->startOfMonth(),
                    'end' => Carbon::parse('this month')->lastOfMonth()
                ];
                break;
            case "last_month":
                $dates = [
                    'start' => Carbon::parse('last month')->startOfMonth(),
                    'end' => Carbon::parse('last month')->lastOfMonth()
                ];
                break;
            case "this_year":
                $dates = [
                    'start' => Carbon::parse('this year')->startOfYear(),
                    'end' => Carbon::parse('this year')->endOfYear()
                ];
                break;
            case "last_year":
                $dates = [
                    'start' => Carbon::parse('last year')->startOfYear(),
                    'end' => Carbon::parse('last year')->endOfYear()
                ];
                break;
        }
        return $dates;
    }

    public function getVisitorTypeId()
    {
        return Type::whereName('visitor')->whereParent('user')->value('id');
    }

    public function getManagementTypeId()
    {
        return Type::whereName('management')->whereParent('user')->value('id');
    }

    public function makeDirectory($name)
    {
        $dirPath = public_path('uploads/' . $name);
        if (!file_exists($dirPath)) {
            mkdir($dirPath, 0777, true);
        }
    }

    public function makeMultipleDirectories($parent, $childs = array())
    {
        foreach ($childs as $child) {
            $dirPath = public_path('uploads/' . $parent . "/" . $child);
            if (!file_exists($dirPath)) {
                mkdir($dirPath, 0777, true);
            }
        }
    }
}

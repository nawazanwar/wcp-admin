<?php

namespace App\Traits;

trait  Ed
{
    public function encrypt($encrypt_id = null)
    {
        return base64_encode(substr(uniqid(rand(), true), 2, 2) . "<bil>" . $encrypt_id . "<zit>" . config('security.key') . substr(uniqid(rand(), true), 2, 2));
    }

    public function decrypt($decrypt_id = null)
    {
        return preg_match('/<bil>(.*?)<zit>/', base64_decode($decrypt_id), $match) == 1 ? $match[1] : '';
    }

}

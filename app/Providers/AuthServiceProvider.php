<?php

namespace App\Providers;

use App\Models\Block;
use App\Models\Camera;
use App\Models\Configuration;
use App\Models\Department;
use App\Models\Designation;

use App\Models\Employer;
use App\Models\Gate;
use App\Models\Family;
use App\Models\FamilyPerson;
use App\Models\House;
use App\Models\HRM;
use App\Models\LRMApplication;
use App\Models\LRMRank;
use App\Models\Lane;
use App\Models\Location;
use App\Models\Log;
use App\Models\LRM;
use App\Models\LRMDepartment;
use App\Models\LRMProfession;
use App\Models\LRMQualification;
use App\Models\Media;
use App\Models\Message;
use App\Models\LRMMinistry;
use App\Models\LRMGrade;
use App\Models\LRMWing;
use App\Models\Notification;
use App\Models\LRMOrganization;
use App\Models\Device;
use App\Models\DLocation;
use App\Models\DType;
use App\Models\DClass;
use App\Models\DModel;
use App\Models\DMake;
use App\Models\DOperatingSystem;
use App\Models\Phase;
use App\Models\Booth;
use App\Models\Type;
use App\Models\Vehicle;
use App\Models\Permission;
use App\Models\Role;
use App\Models\Setting;
use App\Models\User;
use App\Models\VCompany;
use App\Models\VModel;
use App\Models\Visitor;
use App\Models\VMS;
use App\Models\VNature;
use App\Models\VRS;
use App\Policies\BlockPolicy;
use App\Policies\CameraPolicy;
use App\Policies\ConfigurationPolicy;
use App\Policies\DepartmentPolicy;
use App\Policies\DesignationPolicy;
use App\Policies\DevicePolicy;
use App\Policies\DMakePolicy;
use App\Policies\DModelPolicy;
use App\Policies\DClassPolicy;
use App\Policies\DTypePolicy;
use App\Policies\DOperatingSystemPolicy;
use App\Policies\DLocationPolicy;
use App\Policies\EmployerPolicy;
use App\Policies\FamilyPersonPolicy;
use App\Policies\FamilyPolicy;
use App\Policies\GatePolicy;
use App\Policies\HousePolicy;
use App\Policies\HRMPolicy;
use App\Policies\LanePolicy;
use App\Policies\LocationPolicy;
use App\Policies\LogPolicy;
use App\Policies\LRMApplicationPolicy;
use App\Policies\LRMDepartmentPolicy;
use App\Policies\LRMPolicy;
use App\Policies\LRMProfessionPolicy;
use App\Policies\LRMQualificationPolicy;
use App\Policies\LRMGradePolicy;
use App\Policies\MediaPolicy;
use App\Policies\MessagePolicy;
use App\Policies\LRMMinistryPolicy;
use App\Policies\NotificationPolicy;
use App\Policies\LRMOrganizationPolicy;
use App\Policies\PermissionPolicy;
use App\Policies\PhasePolicy;
use App\Policies\LRMRankPolicy;
use App\Policies\LRMWingPolicy;
use App\Policies\RolePolicy;
use App\Policies\SettingPolicy;
use App\Policies\BoothPolicy;
use App\Policies\TypePolicy;
use App\Policies\UserPolicy;
use App\Policies\VCompanyPolicy;
use App\Policies\VModelPolicy;
use App\Policies\VehiclePolicy;
use App\Policies\VisitorPolicy;
use App\Policies\VMSPolicy;
use App\Policies\VNaturePolicy;
use App\Policies\VRSPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{

    protected $policies = [
        VNature::class => VNaturePolicy::class,
        Device::class => DevicePolicy::class,
        LRMWing::class => LRMWingPolicy::class,
        LRMApplication::class => LRMApplicationPolicy::class,
        LRMProfession::class => LRMProfessionPolicy::class,
        LRMQualification::class => LRMQualificationPolicy::class,
        LRMGrade::class => LRMGradePolicy::class,
        LRMRank::class => LRMRankPolicy::class,
        LRMDepartment::class => LRMDepartmentPolicy::class,
        LRMOrganization::class => LRMOrganizationPolicy::class,
        LRMMinistry::class => LRMMinistryPolicy::class,
        Location::class => LocationPolicy::class,
        Message::class => MessagePolicy::class,
        Notification::class => NotificationPolicy::class,
        LRM::class => LRMPolicy::class,
        HRM::class => HRMPolicy::class,
        VMS::class => VMSPolicy::class,
        VRS::class => VRSPolicy::class,
        Designation::class => DesignationPolicy::class,
        Department::class => DepartmentPolicy::class,
        Gate::class => GatePolicy::class,
        Permission::class => PermissionPolicy::class,
        User::class => UserPolicy::class,
        Setting::class => SettingPolicy::class,
        Log::class => LogPolicy::class,
        Media::class => MediaPolicy::class,
        Vehicle::class => VehiclePolicy::class,
        Type::class => TypePolicy::class,
        VCompany::class => VCompanyPolicy::class,
        VModel::class => VModelPolicy::class,
        Booth::class => BoothPolicy::class,
        Visitor::class => VisitorPolicy::class,
        Employer::class => EmployerPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}

<?php

use App\Models\Employer;
use App\Models\Gate;
use App\Models\Booth;
use App\Models\Type;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Message;
use App\Models\Designation;
use App\Models\Department;

class  WcpHelper
{

    /**
     * @return mixed
     */

    public static function getHrmRelationInEng()
    {
        return Type::whereParent('hrm-relation')->where('name', '!=', '')->pluck('name', 'id');
    }

    /**
     * @return mixed
     */

    public static function getHrmRelationInUrdu()
    {
        return Type::whereParent('hrm-relation')->where('name_ur', '!=', '')->pluck('name_ur', 'id');
    }

    /**
     * @return mixed
     */

    public static function getCardList()
    {
        return Type::whereParent('hrm-card')->pluck('name', 'id');
    }

    /**
     * @return mixed
     */

    public static function getEmployeeTypes()
    {
        return Type::whereParent('employee')->pluck('name', 'id');
    }

    /**
     * @return mixed
     */

    public static function getEmployerList()
    {
        return Employer::whereActive(true)->pluck('name', 'id');
    }

    /**
     * @return mixed
     */

    public static function getDepartmentList()
    {
        return Department::whereActive(true)->pluck('name', 'id');
    }

    /**
     * @return mixed
     */
    public static function getDesignationList()
    {
        return Designation::whereActive(true)->where('id', '!=', 1)->pluck('name', 'id');
    }

    public static function verifications()
    {
        return [
            'visitor_biometric_verification' => 'Biometric Verification',
            'visitor_ratina_verification' => 'Ratina Verification',
            'visitor_gate_pass_verification' => 'Gate Pass Verification',
            'visitor_qrcode_verification' => 'QR code Verification'
        ];
    }

    public static function actions()
    {
        return [
            'call_visitee' => 'Call Visitee',
            'verified' => 'Verified',
            'follow' => 'Follow',
            'guide' => 'Guide',
            'warm' => 'Warm',
            'permitted' => 'Permitted',
            'exit' => 'Exit'
        ];
    }

    public static function getMessageByName($name = null)
    {
        return Message::whereName($name)->value('value');
    }

    public static function getUserNameById($id = null)
    {
        return User::whereId($id)->value('name');
    }

    public static function allBoothLists()
    {
        return Booth::join('gates', 'gates.id', 'booths.gate_id')
            ->whereActive(true)
            ->select(DB::raw('CONCAT(gates.name," ",booths.lane," ",booths.type) as name'), 'booths.id')
            ->pluck('name', 'id');
    }

    public static function allGates()
    {
        return Gate::pluck('name', 'id');
    }

    public static function getNotificationReaderUsers($type = null)
    {
        return DB::table('users as u')
            ->join('designation_user as du', 'du.user_id', '=', 'u.id')
            ->join('designation_permission as dp', 'dp.designation_id', '=', 'du.designation_id')
            ->join('permissions as p', 'dp.permission_id', '=', 'p.id')
            ->where('p.name', $type)->select('u.*')->get();
    }

    /**
     * @param null $permission
     * @return
     */

    public static function getNotificationReaderRole($permission = null)
    {
        return Designation::join('designation_permission as dp', 'designations.id', 'dp.designation_id')
            ->join('permissions as p', 'dp.permission_id', '=', 'p.id')
            ->where('p.name', $permission)
            ->where('designations.slug', 'monitoring-cell-operator')
            ->select('designations.id')->first();
    }

    /**
     * @param $str
     * @return bool
     */

    public static function isBase64Encoded($str)
    {
        try {
            $decoded = base64_decode($str, true);
            if (base64_encode($decoded) === $str) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }

    }

    public static function listOfBloodGroups()
    {
        return [
            'A+' => 'A+',
            'B+' => 'B+',
            'A-' => 'A-',
            'B-' => 'B-',
            'O+' => 'O+',
            'O-' => 'O-',
            'AB+' => 'AB+',
            'AB-' => 'AB-'
        ];
    }
}

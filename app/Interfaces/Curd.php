<?php
namespace App\Interfaces;

interface Curd
{
    public function all(array $data);

    public function create(array $data);

    public function update(array $data, $id);

    public function delete($id);

    public function show($id);
}

<?php

namespace App\Device;

use App\Models\Device;
use Illuminate\Database\Eloquent\Model;

class DLocation extends Model
{
    protected  $table = 'dlocations';
    protected $fillable = ['name','name_ur'];

    /**
     * Get the classes for the device.
     */

    public function devices(){
        return $this->hasMany(Device::class);
    }
}

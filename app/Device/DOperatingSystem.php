<?php

namespace App\Device;

use App\Models\Device;
use Illuminate\Database\Eloquent\Model;

class DOperatingSystem extends Model
{
    protected $table = 'doperatingsystems';
    protected $fillable = ['name', 'name_ur'];

    /**
     * Get the classes for the device.
     */

    public function devices()
    {
        return $this->hasMany(Device::class);
    }
}

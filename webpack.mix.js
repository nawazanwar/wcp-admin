const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css/custom.css')
    .styles([
        'resources/plugins/fonts/css/all.min.css',
        'resources/css/adminlte.min.css',
        'resources/sass/app.scss'
    ], 'public/css/vendor.min.css')
    .scripts([
        'resources/plugins/jquery/jquery.min.js',
        'resources/plugins/bootstrap/js/bootstrap.bundle.min.js',
        'resources/js/adminlte.min.js',
        'resources/js/custom.js'
    ], 'public/js/vendor.min.js')
    .copy([
        'resources/plugins/fonts/webfonts',
    ], 'public/webfonts')
    .copy([
        'resources/plugins/',
    ], 'public/plugins')
    .copy([
        'resources/img',
        'resources/img'
    ], 'public/img');

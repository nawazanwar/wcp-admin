<?php

use Illuminate\Database\Seeder;
use App\Models\Device;

class DeviceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'dev_name' => 'First Camera',
                'dev_ip_address' => '192.168.11.54',
                'dev_connection_string' => 'ISAPI/Streaming/channels/',
                'booth_id' => 1,
                'dev_admin_login' => 'webportal',
                'dev_admin_pwd' => 'web12c45',
                'dev_user_login' => 'webportal',
                'dev_user_pwd' => 'web12c45',
                'dtype_id' => 1,
                'dmake_id' => 1,
                'dmodel_id' => 1,
                'dclass_id' => 1,
                'doperatingsystem_id' => 1,
                'dev_mac_address' => 'D0-37-45-74-D1-D6'
            ],
            [
                'dev_name' => 'Second Camera',
                'dev_ip_address' => '192.168.11.45',
                'dev_connection_string' => 'ISAPI/Streaming/channels/',
                'booth_id' => 1,
                'dev_admin_login' => 'webportal',
                'dev_admin_pwd' => 'web12c45',
                'dev_user_login' => 'webportal',
                'dev_user_pwd' => 'web12c45',
                'dtype_id' => 1,
                'dmake_id' => 1,
                'dmodel_id' => 1,
                'dclass_id' => 1,
                'doperatingsystem_id' => 1,
                'dev_mac_address' => 'D0-37-45-74-D1-D6'
            ],
            [
                'dev_name' => 'Third Camera',
                'dev_ip_address' => '192.168.11.70',
                'dev_connection_string' => 'ISAPI/Streaming/channels/',
                'booth_id' => 1,
                'dev_admin_login' => 'webportal',
                'dev_admin_pwd' => 'web12c45',
                'dev_user_login' => 'webportal',
                'dev_user_pwd' => 'web12c45',
                'dtype_id' => 1,
                'dmake_id' => 1,
                'dmodel_id' => 1,
                'dclass_id' => 1,
                'doperatingsystem_id' => 1,
                'dev_mac_address' => 'D0-37-45-74-D1-D6'
            ],
            [
                'dev_name' => 'Fourth Camera',
                'dev_ip_address' => '192.168.11.129',
                'dev_connection_string' => 'ISAPI/Streaming/channels/',
                'booth_id' => 1,
                'dev_admin_login' => 'webportal',
                'dev_admin_pwd' => 'web12c45',
                'dev_user_login' => 'webportal',
                'dev_user_pwd' => 'web12c45',
                'dtype_id' => 1,
                'dmake_id' => 1,
                'dmodel_id' => 1,
                'dclass_id' => 1,
                'doperatingsystem_id' => 1,
                'dev_mac_address' => 'D0-37-45-74-D1-D6'
            ], [
                'dev_name' => 'First Camera At Exit',
                'dev_ip_address' => '192.168.11.71',
                'dev_connection_string' => 'ISAPI/Streaming/channels/',
                'booth_id' => 2,
                'dev_admin_login' => 'webportal',
                'dev_admin_pwd' => 'web12c45',
                'dev_user_login' => 'webportal',
                'dev_user_pwd' => 'web12c45',
                'dtype_id' => 1,
                'dmake_id' => 1,
                'dmodel_id' => 1,
                'dclass_id' => 1,
                'doperatingsystem_id' => 1,
                'dev_mac_address' => 'D0-37-45-74-D1-D6'
            ],
            [
                'dev_name' => 'Second Camera At Exit',
                'dev_ip_address' => '192.168.11.41',
                'dev_connection_string' => 'ISAPI/Streaming/channels/',
                'booth_id' => 2,
                'dev_admin_login' => 'webportal',
                'dev_admin_pwd' => 'web12c45',
                'dev_user_login' => 'webportal',
                'dev_user_pwd' => 'web12c45',
                'dtype_id' => 1,
                'dmake_id' => 1,
                'dmodel_id' => 1,
                'dclass_id' => 1,
                'doperatingsystem_id' => 1,
                'dev_mac_address' => 'D0-37-45-74-D1-D6'
            ],
            [
                'dev_name' => 'Third Camera At Exit',
                'dev_ip_address' => '192.168.11.72',
                'dev_connection_string' => 'ISAPI/Streaming/channels/',
                'booth_id' => 2,
                'dev_admin_login' => 'webportal',
                'dev_admin_pwd' => 'web12c45',
                'dev_user_login' => 'webportal',
                'dev_user_pwd' => 'web12c45',
                'dtype_id' => 1,
                'dmake_id' => 1,
                'dmodel_id' => 1,
                'dclass_id' => 1,
                'doperatingsystem_id' => 1,
                'dev_mac_address' => 'D0-37-45-74-D1-D6'
            ],
            [
                'dev_name' => 'Fourth Camera At Exit',
                'dev_ip_address' => '192.168.11.130',
                'dev_connection_string' => 'ISAPI/Streaming/channels/',
                'booth_id' => 2,
                'dev_admin_login' => 'webportal',
                'dev_admin_pwd' => 'web12c45',
                'dev_user_login' => 'webportal',
                'dev_user_pwd' => 'web12c45',
                'dtype_id' => 1,
                'dmake_id' => 1,
                'dmodel_id' => 1,
                'dclass_id' => 1,
                'doperatingsystem_id' => 1,
                'dev_mac_address' => 'D0-37-45-74-D1-D6'
            ]


        ];
        foreach ($data as $d) {
            Device::create($d);
        }
    }
}

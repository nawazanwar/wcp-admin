<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GateLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gatelocations')->delete();

        $data = [
            [
                'name' => 'Barrier',
                'name_ur' => 'رکاوٹ',
                'gate_id' => \App\Models\Gate::whereName('Gate 1')->value('id')
            ],
            [
                'name' => 'Cabin',
                'name_ur' => 'کیبن',
                'gate_id' => \App\Models\Gate::whereName('Gate 1')->value('id')
            ],
            [
                'name' => 'Computer',
                'name_ur' => 'کمپیوٹر',
                'gate_id' => \App\Models\Gate::whereName('Gate 1')->value('id')
            ]
        ];
        foreach ($data as $d) {
            \App\Models\GateLocation::create($d);
        }
    }
}

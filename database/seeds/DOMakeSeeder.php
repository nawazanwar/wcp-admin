<?php

use Illuminate\Database\Seeder;

class DOMakeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $model = new \App\Device\DMake();
        $model->name='Hikvision';
        $model->save();
    }
}

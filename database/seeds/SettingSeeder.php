<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Setting;
use Carbon\Carbon;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->delete();
        $data = [
            [
                'name' => 'Wapda City Faisalabad',
                'name_ur' => 'واپڈا سٹی فیصل آباد',
                'short_name' => 'WCF',
                'short_name_ur' => 'ڈبلیو سی ایف',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ];
        DB::table('settings')->insert($data);
    }
}

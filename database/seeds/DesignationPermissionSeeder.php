<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Permission;
use App\Models\Designation;

class DesignationPermissionSeeder extends Seeder
{
    public function run()
    {
        DB::table('designation_permission')->delete();
        $permissions = Permission::all();
        foreach ($permissions as $permission) {
            if ($permission->name == 'operate_till') {
                DB::table('designation_permission')->insert([
                    'permission_id' => $permission->id,
                    'designation_id' => Designation::whereId(2)->value('id'),
                ]);
            }

            if (in_array($permission->name, [
                    'call_visitee_at_booth_notification',
                    'visitor_verified_at_booth_notification',
                    'visitor_follow_at_booth_notification',
                    'visitor_guide_at_booth_notification'
                ])){
                DB::table('designation_permission')->insert([
                    'permission_id' => $permission->id,
                    'designation_id' => Designation::whereSlug('monitoring-cell-operator')->value('id'),
                ]);
            }

            DB::table('designation_permission')->insert([
                'permission_id' => $permission->id,
                'designation_id' => Designation::whereId(1)->value('id'),
            ]);
        }
    }
}

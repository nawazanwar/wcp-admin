<?php

use Illuminate\Database\Seeder;

class DModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $model = new \App\Device\DModel();
        $model->name='IR Network';
        $model->save();
    }
}

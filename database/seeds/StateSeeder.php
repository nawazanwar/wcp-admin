<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Location\State;
use App\Location\Country;

class StateSeeder extends Seeder
{
    public function run()
    {
        DB::table('states')->delete();
        $states = [
            "سندھ" => "Sindh",
            "پنجاب" => "Punjab",
            "خیبر پختونخوا" => "Khyber Pakhtunkhwa",
            "اسلام آباد" => "Islamabad",
            "بلوچستان" => "Balochistan",
            "وفاق کے زیر انتظام قبائلی علاقے" => "Federally Administered Tribal Areas",
            "آزادکشمیر" => "Azad Kashmir"
        ];
        foreach ($states as $key => $value) {
            $state = new State();
            $state->name = $value;
            $state->save();
        }
    }

}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\VCompany;
use App\Models\Type;

class VCompanySeeder extends Seeder
{
    public function run()
    {
        DB::table('vcompanies')->delete();
        $data = [
            [
                'name' => 'BMW',
                'name_ur' => 'بی ایم ڈبلیو',
                'type_id' => Type::whereParent('vehicle')->whereName('jeep')->value('id')
            ],
            [
                'name' => 'Nishan',
                'name_ur' => 'نشان',
                'type_id' => Type::whereParent('vehicle')->whereName('truck')->value('id')
            ],
            [
                'name' => 'Hino',
                'name_ur' => 'ہینو',
                'type_id' => Type::whereParent('vehicle')->whereName('truck')->value('id')
            ],
            [
                'name' => 'Suzuki',
                'name_ur' => 'سوزوکی',
                'type_id' => Type::whereParent('vehicle')->whereName('car')->value('id')
            ],
            [
                'name' => 'Toyota',
                'name_ur' => 'ٹویوٹا',
                'type_id' => Type::whereParent('vehicle')->whereName('car')->value('id')
            ],
            [
                'name' => 'Honda',
                'name_ur' => 'ہونڈا',
                'type_id' => Type::whereParent('vehicle')->whereName('car')->value('id')
            ],
        ];
        foreach ($data as $d) {
            VCompany::create($d);
        }
    }
}

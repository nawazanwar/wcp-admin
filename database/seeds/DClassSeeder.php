<?php

use Illuminate\Database\Seeder;

class DClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   
    public function run()
    {
       $model = new \App\Device\DClass();
       $model->name='A Class';
       $model->save();
    }
}

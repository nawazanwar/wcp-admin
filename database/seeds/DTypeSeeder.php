<?php

use Illuminate\Database\Seeder;

class DTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $model = new \App\Device\DType();
        $model->name = 'Camera';
        $model->save();
    }
}

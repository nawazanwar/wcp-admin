<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Location\Tehsil;

class TehsilSeeder extends Seeder
{
    public function run()
    {
        DB::table('tehsils')->delete();
        $model_array = [
            [
                'name' => 'Chak Jhumra'
            ],
            [
                'name' => 'Faisalabad City.'
            ],
            [
                'name' => 'Faisalabad Sadar'
            ],
            [
                'name' => 'Jaranwala'
            ],
            [
                'name' => 'Samundri'
            ],
            [
                'name' => 'Tandlianwala'
            ]
        ];

        foreach ($model_array as $d) {

            $model = new Tehsil();
            $model->name = $d['name'];
            $model->save();
        }
    }
}

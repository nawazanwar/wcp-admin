<?php

use Illuminate\Database\Seeder;

class DOperatingSystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $model = new \App\Device\DOperatingSystem();
        $model->name='Windows';
        $model->save();
    }
}

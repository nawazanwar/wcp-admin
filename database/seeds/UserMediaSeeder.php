<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\User;
class UserMediaSeeder extends Seeder
{
    public function run()
    {
        DB::table('umedias')->delete();
        $umedias_array = [
            [
                'user_id' => User::where('email','admin@wcp.com')->value('id'),
                'type' => 'avatar',
                'src' => 'default.png',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => User::where('email','user@wcp.com')->value('id'),
                'type' => 'avatar',
                'src' => 'default.png',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => User::where('email','tu1@wcp.com')->value('id'),
                'type' => 'avatar',
                'src' => 'default.png',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => User::where('email','tu2@wcp.com')->value('id'),
                'type' => 'avatar',
                'src' => 'default.png',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ];
        DB::table('umedias')->insert($umedias_array);
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Models\Type;

class UserSeeder extends Seeder
{
    use \App\Traits\General;

    public function run()
    {
        DB::table('users')->delete();
        $users_array = [
            [
                'unique_id' => '5f585f61eb9bf',
                'name' => 'Owner',
                'password' => Hash::make('owner123'),
                'active' => true
            ],
            [
                'unique_id' => '5f585f6213a72',
                'name' => 'Test Till Operator',
                'password' => Hash::make('user1234'),
                'active' => true
            ],[
                'unique_id' => '5f585f622cba2',
                'name' => 'Test Cell Monitor',
                'password' => Hash::make('user1234'),
                'active' => true
            ]
        ];
        foreach ($users_array as $user) {
            User::create($user);
        }
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Gate;
use Illuminate\Support\Str;

class GateSeeder extends Seeder
{
    public function run()
    {
        DB::table('Gates')->delete();

        $data = [
            [
                'name' => 'Gate 01',
                'name_ur' => 'گیٹ 01',
                'slug' => Str::slug('Gate 01', '-')
            ],
            [
                'name' => 'Gate 02',
                'name_ur' => 'گیٹ 02',
                'slug' => Str::slug('Gate 02', '-')
            ],
            [
                'name' => 'Gate 03',
                'name_ur' => 'گیٹ 03',
                'slug' => Str::slug('Gate 03', '-')
            ],
            [
                'name' => 'Gate 04',
                'name_ur' => 'گیٹ 04',
                'slug' => Str::slug('Gate 04', '-')
            ],
            [
                'name' => 'Gate 05',
                'name_ur' => 'گیٹ 05',
                'slug' => Str::slug('Gate 05', '-')
            ],
            [
                'name' => 'Gate 06',
                'name_ur' => 'گیٹ 06',
                'slug' => Str::slug('Gate 06', '-')
            ],
            [
                'name' => 'Gate 07',
                'name_ur' => 'گیٹ 07',
                'slug' => Str::slug('Gate 07', '-')
            ],
            [
                'name' => 'Gate 08',
                'name_ur' => 'گیٹ 08',
                'slug' => Str::slug('Gate 08', '-')
            ]
        ];
        foreach ($data as $d) {
            Gate::create($d);
        }
    }
}

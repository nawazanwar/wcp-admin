<?php

use Illuminate\Database\Seeder;
use App\Models\Type;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->delete();
        $employee_types = [
            [
                'name' => 'Full Time',
                'name_ur' => 'پوراوقت',
                'parent' => 'employee',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Part Time',
                'name_ur' => 'پارٹ ٹائم',
                'parent' => 'employee',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Temporary',
                'name_ur' => 'عارضی',
                'parent' => 'employee',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Weekly',
                'name_ur' => 'ہفتہ وار',
                'parent' => 'employee',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Hourly',
                'name_ur' => 'فی گھنٹہ',
                'parent' => 'employee',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];
        DB::table('types')->insert($employee_types);

        $employment = [
            [
                'name' => 'Government',
                'name_ur' => 'حکومت',
                'parent' => 'employment',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], [
                'name' => 'Private Establishment',
                'name_ur' => 'نجی اسٹیبلشمنٹ',
                'parent' => 'employment',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], [
                'name' => 'Own Business',
                'name_ur' => 'اپنا کاروبار',
                'parent' => 'employment',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];
        DB::table('types')->insert($employment);
        $department_types = [
            [
                'name' => 'Administration',
                'name_ur' => 'انتظامیہ',
                'parent' => 'department',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Production',
                'name_ur' => 'پیداوار',
                'parent' => 'department',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Brand Sales',
                'name_ur' => 'برانڈ سیلز',
                'parent' => 'department',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];
        DB::table('types')->insert($department_types);
        $vehicle_types = [
            [
                'name' => 'Bus',
                'name_ur' => 'بس',
                'parent' => 'vehicle',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Car',
                'name_ur' => 'گاڑی',
                'parent' => 'vehicle',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Jeep',
                'name_ur' => 'جیپ',
                'parent' => 'vehicle',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Loader Rickshaw',
                'name_ur' => 'لوڈر رکشہ',
                'parent' => 'vehicle',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Rickshaw',
                'name_ur' => 'رکشہ',
                'parent' => 'vehicle',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Mini Bus',
                'name_ur' => 'منی بس',
                'parent' => 'vehicle',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Truck',
                'name_ur' => 'ٹرک',
                'parent' => 'vehicle',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Mini Truck',
                'name_ur' => 'منی ٹرک',
                'parent' => 'vehicle',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Motor Cycle',
                'name_ur' => 'موٹرسائیکل',
                'parent' => 'vehicle',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'label' => 'Pickup',
                'name_ur' => 'وین',
                'parent' => 'vehicle',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Van',
                'name_ur' => 'وین',
                'parent' => 'vehicle',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];
        DB::table('types')->insert($vehicle_types);
    }
}

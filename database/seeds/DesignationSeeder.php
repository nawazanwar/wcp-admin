<?php

use Illuminate\Database\Seeder;
use App\Models\Designation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DesignationSeeder extends Seeder
{
    public function run()
    {
        DB::table('designations')->delete();

        $designations = [
            [
                'name' => 'Owner',
                'active' => 1,
                'slug' => Str::slug('Owner', '-')
            ],
            [
                'name' => 'Till Operator',
                'active' => 1,
                'slug' => Str::slug('Till Operator', '-')
            ],
            [
                'name' => 'Monitoring Cell Operator',
                'active' => 1,
                'slug' => Str::slug('Monitoring Cell Operator', '-')
            ]
        ];

        foreach ($designations as $designation) {
            Designation::create($designation);
        }
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\VCompany;
use App\Models\VModel;

class VModelSeeder extends Seeder
{
    public function run()
    {
        DB::table('vmodels')->delete();
        $data = [
            [
                'name' => 'Every',
                'name_ur' => 'ہر کوئی',
                'vcompany_id' => VCompany::whereName('Suzuki')->value('id')
            ],
            [
                'name' => 'GS150',
                'name_ur' => 'جی ایس150',
                'vcompany_id' => VCompany::whereName('Suzuki')->value('id')
            ],
            [
                'name' => 'FX',
                'name_ur' => 'FX',
                'vcompany_id' => VCompany::whereName('Suzuki')->value('id')
            ],
            [
                'name' => 'Lexux',
                'name_ur' => 'لیکسس',
                'vcompany_id' => VCompany::whereName('Toyota')->value('id')
            ],
            [
                'name' => 'Nova',
                'name_ur' => 'نووا',
                'vcompany_id' => VCompany::whereName('Toyota')->value('id')
            ],
        ];
        foreach ($data as $d) {
            VModel::create($d);
        }
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Location\Colony;

class ColonySeeder extends Seeder
{
    public function run()
    {
        DB::table('colonies')->delete();
        $model_array = [
            [
                'name' => 'Lyallpur Town'
            ],
            [
                'name' => 'Madina Town.'
            ],
            [
                'name' => 'Jinnah Town.'
            ],
            [
                'name' => 'Iqbal town.'
            ],
            [
                'name' => 'Chak Jhumra Town.'
            ],
            [
                'name' => 'Tandlianwala Town.'
            ]
        ];

        foreach ($model_array as $d) {

            $model = new Colony();
            $model->name = $d['name'];
            $model->save();
        }
    }
}

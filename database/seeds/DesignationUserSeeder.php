<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Designation;
use App\Models\User;

class DesignationUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('designation_user')->delete();
        DB::table('designation_user')->insert(
            [
                [
                    'designation_id' => Designation::whereId(1)->value('id'),
                    'user_id' => User::whereId(1)->value('id')
                ],
                [
                    'designation_id' => Designation::whereId(2)->value('id'),
                    'user_id' => User::whereId(2)->value('id')
                ],
                [
                    'designation_id' => Designation::whereId(3)->value('id'),
                    'user_id' => User::whereId(3)->value('id')
                ]
            ]
        );
    }
}

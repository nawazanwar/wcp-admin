<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Message;

class MessageSeeder extends Seeder
{
    public function run()
    {
        DB::table('messages')->delete();
        $data = [
            [
                'name' => 'call_visitee_at_booth_notification',
                'value' => 'We let Mr <strong>%visitor_name%</strong> with the vehicle no of <strong>%vehicle_number%</strong> has enter the Society but you have to call the <strong>%visitee%</strong> and confirm the identity',
            ], [
                'name' => 'visitor_verified_at_booth_notification',
                'value' => 'We let Mr <strong>%visitor_name%</strong> with the vehicle no of <strong>%vehicle_number%</strong> has enter the Society and its is already verified',
            ], [
                'name' => 'visitor_follow_at_booth_notification',
                'value' => 'We let Mr <strong>%visitor_name%</strong> with the vehicle no of <strong>%vehicle_number%</strong> has enter the Society but to clear some
doubts you have to follow this visitor',
            ], [
                'name' => 'visitor_guide_at_booth_notification',
                'value' => 'We let Mr <strong>%visitor_name%</strong> with the vehicle no of <strong>%vehicle_number%</strong> has enter the Society but he is unknown to society kindly guide the visitor properly',
            ], [
                'name' => 'visitor_already_enter_not_exit',
                'value' => 'Vehicle with the number of <strong>%vehicle_number%</strong> already Enter In society but not exit at any Exit points so please clear this'
            ]
        ];
        foreach ($data as $d) {
            Message::create($d);
        }
    }
}

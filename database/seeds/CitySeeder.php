<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Location\City;
use App\Location\State;

class CitySeeder extends Seeder
{
    public function run()
    {
        DB::table('cities')->delete();
        $cities = [
            [
                'name' => 'Lahore'
            ],
            [
                'name' => 'Faisalabad'
            ],
            [
                'name' => 'Rawalpindi'
            ],
            [
                'name' => 'Gujranwala'
            ]
        ];

        foreach ($cities as $city) {

            $model = new City();
            $model->name = $city['name'];
            $model->save();
        }
    }
}

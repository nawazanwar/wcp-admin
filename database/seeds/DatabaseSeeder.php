<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {

        /*Some Device Seeder*/
        $this->call(DClassSeeder::class);
        $this->call(DModelSeeder::class);
        $this->call(DOMakeSeeder::class);
        $this->call(DOperatingSystemSeeder::class);
        $this->call(DTypeSeeder::class);

        $this->call(LRMSetting::class);

        $this->call(CountrySeeder::class);
        $this->call(StateSeeder::class);
        $this->call(DistrictSeeder::class);
        $this->call(CitySeeder::class);
        $this->call(TehsilSeeder::class);
        $this->call(ColonySeeder::class);

        $this->call(MessageSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(DesignationSeeder::class);
        $this->call(DesignationPermissionSeeder::class);
        $this->call(DesignationUserSeeder::class);
        $this->call(GateSeeder::class);
        $this->call(BoothSeeder::class);
        $this->call(TypeSeeder::class);
        $this->call(VCompanySeeder::class);
        $this->call(VModelSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(UserMediaSeeder::class);
        $this->call(GateLocationSeeder::class);
        $this->call(DeviceSeeder::class);
        $this->call(TestHRMSeeder::class);
    }
}

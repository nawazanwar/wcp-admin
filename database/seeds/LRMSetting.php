<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class LRMSetting extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lrmsettings')->delete();

        /*Property Type*/
        $property_types = [
            [
                'name' => 'Commercial',
                'parent' => 'property_type',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => '1 Kanal',
                'parent' => 'property_type',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => '2 Kanal',
                'parent' => 'property_type',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ];
        DB::table('lrmsettings')->insert($property_types);

        /* Company types*/

        $company_types = [
            [
                'name' => 'OPC',
                'parent' => 'company_type',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Private',
                'parent' => 'company_type',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Public',
                'parent' => 'company_type',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ];
        DB::table('lrmsettings')->insert($company_types);
        /* title Setting*/

        $title_settings = [
            [
                'name' => 'Mr',
                'parent' => 'title',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Mrs',
                'parent' => 'title',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Ms',
                'parent' => 'title',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Prince',
                'parent' => 'title',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Dr',
                'parent' => 'title',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Prof',
                'parent' => 'title',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ];
        DB::table('lrmsettings')->insert($title_settings);

        /* Prefixes */

        $prefixes_settings = [
            [
                'name' => 'Chaudhary',
                'parent' => 'prefix',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Mehar',
                'parent' => 'prefix',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Mian',
                'parent' => 'prefix',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Malik',
                'parent' => 'prefix',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];
        DB::table('lrmsettings')->insert($prefixes_settings);

        /* Postfix */

        $post_prefix_settings = [
            [
                'name' => 'Mehar',
                'parent' => 'postfix',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Awan',
                'parent' => 'postfix',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];
        DB::table('lrmsettings')->insert($post_prefix_settings);

        /* Nationalities */

        $nationalities_settings = [
            [
                'name' => 'Pakistani',
                'parent' => 'nationality',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Indian',
                'parent' => 'nationality',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'French',
                'parent' => 'nationality',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'German',
                'parent' => 'nationality',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Ghanaian',
                'parent' => 'nationality',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Greek',
                'parent' => 'nationality',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];
        DB::table('lrmsettings')->insert($nationalities_settings);


        /* Relations */

        $relations_settings = [
            [
                'name' => 'S/O',
                'parent' => 'relation',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'F/O',
                'parent' => 'relation',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'M/O',
                'parent' => 'relation',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'D/O',
                'parent' => 'relation',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'B/O',
                'parent' => 'relation',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];
        DB::table('lrmsettings')->insert($relations_settings);


        /* Qualifications */

        $qualification_settings = [
            [
                'name' => 'Metric',
                'parent' => 'qualification',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Middle',
                'parent' => 'qualification',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Primary',
                'parent' => 'qualification',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];

        DB::table('lrmsettings')->insert($qualification_settings);

        /* Profession*/

        $qualification_settings = [
            [
                'name' => 'Accountant',
                'parent' => 'profession',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Audit Manager',
                'parent' => 'profession',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Auditor',
                'parent' => 'profession',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Doctor',
                'parent' => 'profession',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Engineer',
                'parent' => 'profession',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Professor',
                'parent' => 'profession',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];

        DB::table('lrmsettings')->insert($qualification_settings);
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Location\City;
use App\Location\State;

class DistrictSeeder extends Seeder
{
    public function run()
    {
        DB::table('districts')->delete();
        $model_array = [
            [
                'name' => 'Lahore'
            ],
            [
                'name' => 'Faisalabad'
            ],
            [
                'name' => 'Rawalpindi'
            ],
            [
                'name' => 'Gujranwala'
            ]
        ];

        foreach ($model_array as $d) {

            $model = new \App\Location\District();
            $model->name = $d['name'];
            $model->save();
        }
    }
}

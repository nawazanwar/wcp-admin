<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Phase;
use App\Models\Gate;
use App\Models\Booth;

class BoothSeeder extends Seeder
{
    public function run()
    {
        DB::table('booths')->delete();

        $data = [
            [
                'lane' => 'Lane 03',
                'gate_id' => Gate::whereSlug('gate-01')->value('id'),
                'type' => 'entry',
                'active' => true
            ],
            [
                'lane' => 'Lane 03',
                'gate_id' => Gate::whereSlug('gate-01')->value('id'),
                'type' => 'exit',
                'active' => true
            ]
        ];
        foreach ($data as $d) {
            Booth::create($d);
        }
    }
}

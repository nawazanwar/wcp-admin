<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('employers')) {
            return true;
        }

        Schema::create('employers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('type_id')->unsigned()->nullable();
            $table->string('name')->nullable();
            $table->string('name_ur')->nullable();
            $table->string('active_salary')->nullable();
            $table->string('active')->default(true);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('type_id')->references('id')->on('types')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employers');
    }
}

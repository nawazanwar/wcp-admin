<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('vehicles')) {
            return true;
        }
        Schema::create('vehicles', function (Blueprint $table) {

            $table->bigIncrements('id');
            /*here is type,company,model ids of vehicles*/
            $table->bigInteger('type_id')->unsigned()->nullable();//Vehicle-Type
            $table->bigInteger('vcompany_id')->unsigned()->nullable();//Type-Company
            $table->bigInteger('vmodel_id')->unsigned()->nullable();//Company-model

            $table->string('reg_no_series')->nullable();
            $table->string('reg_no')->nullable();
            $table->string('reg_no_year')->nullable();
            $table->string('reg_on_name')->nullable();
            $table->string('model_year')->nullable();
            $table->string('model_month')->nullable();
            $table->string('vehicle_company');
            $table->string('vehicle_model');
            $table->string('vehicle_driven_by'); //multiple user ids
            $table->string('chasis_number');
            $table->string('engine_number');
            $table->string('color');
            $table->string('vehicle_gencode'); //32bit code for etag
            $table->boolean('etag_generated');
            $table->boolean('etag_generated_on'); // day,date,time
            $table->boolean('etag_received_by');
            $table->boolean('etag_received_on');
            $table->boolean('sticker_generated');
            $table->boolean('sticker_generated_on'); // day,date,time
            $table->boolean('sticker_received_by');
            $table->boolean('sticker_received_on');

            $table->string('added_on'); //day, date, time we can make it as a separate too
            $table->string('added_by');  //logged in user id
            $table->string('added_by_ip');
            $table->string('vehicle_verified_by_uid');

            $table->string('last_updated_on');
            $table->string('last_updated_by');
            $table->string('last_updated_by_ip');

            $table->boolean('active')->default(true);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('type_id')
                ->references('id')
                ->on('types')
                ->onDelete('cascade');
            $table->foreign('vcompany_id')
                ->references('id')
                ->on('vcompanies')
                ->onDelete('cascade');
            $table->foreign('vmodel_id')
                ->references('id')
                ->on('vmodels')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}



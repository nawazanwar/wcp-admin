<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrmMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrm_media', function (Blueprint $table) {
            $table->bigInteger('h_r_m_id')->unsigned()->nullable();
            $table->bigInteger('media_id')->unsigned()->nullable();
            $table->foreign('h_r_m_id')
                ->references('id')
                ->on('hrms')
                ->onDelete('cascade');
            $table->foreign('media_id')
                ->references('id')
                ->on('media')
                ->onDelete('cascade');
            $table->primary(['h_r_m_id', 'media_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrm_media');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLRMWingTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('l_r_m_wing_type', function (Blueprint $table) {
            $table->bigInteger('l_r_m_wing_id')->unsigned();
            $table->bigInteger('type_id')->unsigned();

            $table->foreign('l_r_m_wing_id')
                ->references('id')
                ->on('lrmwings')
                ->onDelete('cascade');

            $table->foreign('type_id')
                ->references('id')
                ->on('types')
                ->onDelete('cascade');

            $table->primary(['l_r_m_wing_id', 'type_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('l_r_m_wing_type');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVmodelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vmodels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('vcompany_id')->unsigned()->nullable();
            $table->string('name')->nullable();
            $table->string('name_ur')->nullable();
            $table->timestamps();
            $table->foreign('vcompany_id')
                ->references('id')
                ->on('vcompanies')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vmodels');
    }
}

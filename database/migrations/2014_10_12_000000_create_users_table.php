<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateUsersTable extends Migration
{
    public function up()
    {
        if (Schema::hasTable('users')) {
            return true;
        }
        Schema::create('users', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('email')->unique()->nullable();
            $table->string('unique_id')->unique()->nullable();
            $table->string('name')->nullable();
            $table->string('password')->nullable();
            $table->boolean('active')->default(false);
            $table->boolean('block')->default(false);
            $table->string('avatar')->nullable()->default('default.png');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

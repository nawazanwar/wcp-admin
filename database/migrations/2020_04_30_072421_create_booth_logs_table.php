<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateBoothLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booth_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('log_id')->unsigned();
            $table->bigInteger('booth_id')->unsigned();
            $table->bigInteger('operator_id')->unsigned();
            $table->timestamp('date_time')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('ip_address')->nullable();
            $table->string('mac_address')->nullable();
            $table->enum('type', ['login', 'logout'])->nullable();

            $table->timestamps();

            $table->foreign('log_id')
                ->references('id')
                ->on('logs')
                ->onDelete('cascade');

            $table->foreign('operator_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('booth_id')
                ->references('id')
                ->on('booths')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booth_logs');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('media')) {
            return true;
        }
        Schema::create('media', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('src')->unique();
            $table->enum('type', array('avatar', 'cnic', 'card', 'profile','document'))->default('avatar');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}

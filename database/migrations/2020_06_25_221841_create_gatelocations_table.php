<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGatelocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gatelocations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('gate_id')->unsigned()->nullable();
            $table->string('name')->nullable();
            $table->string('name_ur')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('gate_id')->references('id')->on('gates')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gatelocations');
    }
}

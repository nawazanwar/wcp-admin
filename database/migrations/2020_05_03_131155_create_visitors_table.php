<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitorsTable extends Migration
{

    public function up()
    {
        if (Schema::hasTable('visitors')) {
            return true;
        }

        Schema::create('visitors', function (Blueprint $table) {


            $table->bigIncrements('id');
            $table->bigInteger('operator_id')->unsigned()->nullable();
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->bigInteger('booth_id')->unsigned()->nullable();
            $table->bigInteger('type_id')->unsigned()->nullable();//Vehicle-Type
            $table->bigInteger('vcompany_id')->unsigned()->nullable();//Type-Company
            $table->bigInteger('vmodel_id')->unsigned()->nullable();//Company-model
            $table->bigInteger('vnature_id')->unsigned()->nullable();//Material-Nature-Model
            $table->longText('purpose_of_visit')->nullable();

            $table->string('vehicle_color')->nullable();
            $table->string('vehicle_reg_no')->nullable();
            $table->string('vehicle_reg_year')->nullable();
            $table->string('no_of_males')->nullable();
            $table->string('no_of_females')->nullable();
            $table->string('no_of_kids')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('mac_address')->nullable();

            $table->string('visitor_name')->nullable();
            $table->string('visitor_cnic')->nullable();
            $table->string('visitor_cell_no')->nullable();
            /*verifications*/
            $table->boolean('visitor_biometric_verification')->default(false);
            $table->boolean('visitor_ratina_verification')->default(false);
            $table->boolean('visitor_gate_pass_verification')->default(false);
            $table->boolean('visitor_qrcode_verification')->default(false);
            /*some actions*/
            $table->boolean('call_visitee')->default(false);
            $table->boolean('verified')->default(false);
            $table->boolean('follow')->default(false);
            $table->boolean('guide')->default(false);
            $table->boolean('warn')->default(false);
            /*some extra info*/
            $table->string('opened_barrier_number')->nullable();
            $table->boolean('permitted')->default(false);
            $table->boolean('exit')->default(false);

            $table->longText('first_camera_image')->nullable();
            $table->longText('second_camera_image')->nullable();
            $table->longText('third_camera_image')->nullable();
            $table->longText('fourth_camera_image')->nullable();



            $table->timestamps();

            $table->foreign('updated_by')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('operator_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('booth_id')
                ->references('id')
                ->on('booths')
                ->onDelete('cascade');
            $table->foreign('type_id')
                ->references('id')
                ->on('types')
                ->onDelete('cascade');
            $table->foreign('vcompany_id')
                ->references('id')
                ->on('vcompanies')
                ->onDelete('cascade');
            $table->foreign('vmodel_id')
                ->references('id')
                ->on('vmodels')
                ->onDelete('cascade');
            $table->foreign('vnature_id')
                ->references('id')
                ->on('vnatures')
                ->onDelete('cascade');
            $table->boolean('is_exit')->default(false);
            $table->boolean('is_enter')->default(false);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitors');
    }
}

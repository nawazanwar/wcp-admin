<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
class CreateAuthLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('auth_logs')) {
            return true;
        }
        Schema::create('auth_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('log_id')->unsigned();
            $table->bigInteger('auth_id')->unsigned();

            $table->timestamp('date_time')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('ip_address')->nullable();
            $table->string('mac_address')->nullable();
            $table->enum('type', ['login', 'logout'])->nullable();
            $table->timestamps();
            $table->foreign('log_id')
                ->references('id')
                ->on('logs')
                ->onDelete('cascade');
            $table->foreign('auth_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_logs');
    }
}

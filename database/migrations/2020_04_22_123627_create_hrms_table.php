<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->bigInteger('department_id')->unsigned()->nullable();
            $table->bigInteger('employer_id')->unsigned()->nullable();
            $table->bigInteger('emp_type')->unsigned()->nullable();
            $table->bigInteger('relation_eng')->unsigned()->nullable();
            $table->bigInteger('relation_ur')->unsigned()->nullable();
            $table->string('relation_name_eng')->nullable();
            $table->string('relation_name_ur')->nullable();
            $table->string('f_name')->nullable();
            $table->string('f_name_ur')->nullable();
            $table->string('l_name')->nullable();
            $table->string('l_name_ur')->nullable();
            $table->string('cast')->nullable();
            $table->string('cast_ur')->nullable();
            /*User Pictures*/
            $table->string('first_picture')->nullable();
            $table->string('second_picture')->nullable();
            $table->string('third_picture')->nullable();
            $table->string('fourth_picture')->nullable();
            /*General*/
            $table->string('cnic')->nullable();
            $table->string('cnic_expiry_date')->nullable();
            $table->string('cnic_qr_code')->nullable();
            $table->string('passport')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('blood_group')->nullable();
            $table->enum('gender', ['male', 'female', 'other']);

            /*contact Information*/
            $table->string('contact_number')->nullable();
            $table->string('secondary_contact_number')->nullable();
            $table->string('secondary_contact_person')->nullable();

            /* Address Information*/

            $table->string('temp_city')->nullable();
            $table->string('temp_city_ur')->nullable();
            $table->longText('temp_address')->nullable();
            $table->longText('temp_address_ur')->nullable();

            $table->string('perm_city')->nullable();
            $table->string('perm_city_ur')->nullable();
            $table->longText('perm_address')->nullable();
            $table->longText('perm_address_ur')->nullable();

            $table->string('pres_city')->nullable();
            $table->string('pres_city_ur')->nullable();
            $table->longText('pres_address')->nullable();
            $table->longText('pres_address_ur')->nullable();

            /* Card Details*/
            $table->string('card_type')->nullable();
            $table->date('card_issue_date')->nullable();
            $table->date('card_expiry_date')->nullable();

            /* Biometric verification*/

            $table->boolean('biometric_lh_thumb_verification')->default(false);
            $table->boolean('biometric_lh_index_verification')->default(false);
            $table->boolean('biometric_lh_middle_verification')->default(false);
            $table->boolean('biometric_lh_ring_verification')->default(false);
            $table->boolean('biometric_lh_pinky_verification')->default(false);
            $table->boolean('biometric_rh_thumb_verification')->default(false);
            $table->boolean('biometric_rh_index_verification')->default(false);
            $table->boolean('biometric_rh_middle_verification')->default(false);
            $table->boolean('biometric_rh_ring_verification')->default(false);
            $table->boolean('biometric_rh_pinky_verification')->default(false);

            $table->timestamps();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('department_id')
                ->references('id')
                ->on('departments')
                ->onDelete('cascade');
            $table->foreign('employer_id')
                ->references('id')
                ->on('employers')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrms');
    }
}

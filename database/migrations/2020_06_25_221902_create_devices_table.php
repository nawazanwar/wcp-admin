<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('dev_name')->nullable();
            $table->string('dev_name_ur')->nullable();
            $table->string('dev_id')->nullable();

            /* Some Common Relationships*/

            $table->bigInteger('dtype_id')->unsigned()->nullable();
            $table->bigInteger('dmake_id')->unsigned()->nullable();
            $table->bigInteger('dmodel_id')->unsigned()->nullable();
            $table->bigInteger('dclass_id')->unsigned()->nullable();
            $table->bigInteger('doperatingsystem_id')->unsigned()->nullable();

            $table->bigInteger('booth_id')->unsigned()->nullable();
            $table->string('dpic_id')->nullable();
            $table->string('dev_pic')->nullable()->default('default.png');
            $table->string('dev_serial_no')->nullable();

            $table->string('dev_ip_address')->nullable();
            $table->string('dev_mac_address')->nullable();
            $table->string('dev_admin_login')->nullable();
            $table->string('dev_admin_pwd')->nullable();
            $table->string('dev_user_login')->nullable();
            $table->string('dev_user_pwd')->nullable();
            $table->string('dev_lot_no')->nullable();
            $table->string('dev_lot_date')->nullable();

            $table->string('dev_other_info')->nullable();
            $table->string('dev_connection_string')->nullable();
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('dtype_id')->references('id')->on('dtypes')
                ->onDelete('cascade');
            $table->foreign('dmake_id')->references('id')->on('dmakes')
                ->onDelete('cascade');
            $table->foreign('dmodel_id')->references('id')->on('dmodels')
                ->onDelete('cascade');
            $table->foreign('dclass_id')->references('id')->on('dclasses')
                ->onDelete('cascade');
            $table->foreign('doperatingsystem_id')->references('id')->on('doperatingsystems')
                ->onDelete('cascade');
            $table->foreign('booth_id')->references('id')->on('booths')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}

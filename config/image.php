<?php

return [

    'driver' => 'gd',
    'img_sizes' => [
        'avatar' => 70,
        'profile' => 200
    ]
];

<?php

return [
    'employee_management_system' => 'ملازم مینجمنٹ سسٹم',
    'welcome_back_title' => 'واپس ڈیش بورڈ میں آپ کا استقبال ہے',
    'site_title' => 'واپڈا سٹی',
    'home' => 'گھر',
    'site_complete_name' => 'واپڈا ہاؤسنگ سوسائٹی',

    'user_management_system' => 'صارف کے انتظام کے نظام',
    'house_management_system' => 'ہاؤس مینجمنٹ سسٹم',
    'vehicle_management_system' => 'وہیکل مینجمنٹ سسٹم',
    'family_management_system' => 'فیملی مینجمنٹ سسٹم',
    'network_management_system' => 'نیٹ ورک مینجمنٹ سسٹم',
    'all_tilds' => 'تمام ٹیلڈز'
];

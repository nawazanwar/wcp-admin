<?php

return [
    'logout' => 'Log Out',
    'login_heading' => 'Sign in to start your session',
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'enter_password' => 'Enter Password',
    'enter_unique_id' => 'Enter your Unique Id',
    'login_title' => 'Login to View Dashboard',
    'login' => 'Login',
    'booth' => [
        'heading' => 'Fill your information for login into Booth',
        'login' => 'Login',
        'enter_unique_id' => 'Please Enter Your Unique Id',
        'enter_password' => 'Please Enter Your Password',
    ],
];

<?php

return [
    'created_by' => 'Created By',
    'messages' => 'Messages',
    'already_exists_email' => 'Already exists email please choose new one',
    'you_can_go' => 'You can go',
    'enter_with' => 'Enter with IP address of',
    'exit_with' => 'Exit with IP address of',
    'person_not_belongs_to_our_society' => 'This person not belongs to our society so please verified it by filling a form',
    'access_granted' => 'Access granted',
    'logged_in_with_ip_address_of' => 'logged in with ip address of',
    'logged_out_with_ip_address_of' => 'logged out with ip address of',
    'and_mac_address_is' => 'and mac address is',
    'model_created_failed_message' => 'Failed to create a model',
    'model_created_success_message' => 'Model created Successfully',
    'no_model_found' => 'No Model found',
    'company_created_failed_message' => 'Failed to create new company',
    'company_created_success_message' => 'Company created successfully',
    'no_permissions_found' => 'No permissions found',
    'no_permission' => 'Sorry you have no permission to perform this action',
    'user_created_success_message' => 'New User has been created Successfully',
    'user_created_failed_message' => 'Failed to create a new user',
    'no_user_found' => "No User Found...",
    "no_role_found" => "No Role Found...",
    "successfully_deactivated" => 'Successfully De activated',
    "successfully_activated" => 'Successfully Activated',
    'assigned_successfully' => 'Assigned Successfully',
    'removed_successfully' => 'Removed Successfully',
    'no_people_found' => 'No People Found...',
    'no_vehicle_found' => 'No Vehicle Found...',
    'house_created_success_message' => 'House created successfully',
    'user_sync_successfully' => 'User sync Successfully',
    'maintenance_mode' => 'System is in maintenance Mode.',
    'setting_updated_successfully' => 'Setting Updated Successfully',
    'type_created_success_message' => 'Type Created Successfully',
    'type_created_failed_message' => 'Failed to create new type',
    'no_company_found' => 'No Company found',

    'welcome_back' => 'Welcome Back',
    'vehicle_created_success_message' => 'Vehicle Added',

];

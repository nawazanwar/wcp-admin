<?php

return [
    'readonly_notifications' => 'Readonly Notifications',
    'value' => 'Value',
    'view_all_messages' => 'View All Messages',
    'messages' => 'Messages',
    'company_ur' => 'کمپنی',
    'vehicle_type' => 'Vehicle Type',
    'vehicle_type_ur' => 'گاڑی کی قسم',
    'name_ur' => "نام",
    'logs_of_all_tills' => 'Logs of all Tills',
    'other' => 'Other',
    'admin' => 'Admin',
    'till_operator' => 'Till Operator',
    'entry' => 'Entry',
    'exit' => 'Exit',
    'created_date' => 'Created Date',
    'create_master_table_of' => 'Create Master table of',
    'view_all_tables' => 'View all tables',
    'master_tables' => 'Master Tables',
    'all_tilds' => 'All Tills',
    'log_of_managements' => 'All logs of Management',
    'log_of_users' => 'All logs of Employees',
    'logs_of' => 'All logs of',
    'enterace_time' => 'Enterace time',
    'guardian' => 'Guardian',
    'all_visitors' => 'All Visitors',
    'optional' => 'Optional',
    'visitor_information' => 'Visitor Information',
    'visitor_parent_information' => 'Parent or guardian Information',
    'log_of_visitors' => 'All Visitor logs',
    'log_of_management' => 'All Management logs',
    'in' => 'Enter',
    'out' => 'Exit',
    'auth_logs' => 'Authentication Logs',
    'visitor_logs' => 'Visitor Logs',
    'fill_user_info' => 'Fill out the Visitor Information',
    'open_a_form' => 'Open a Form',
    'caution' => 'Caution',
    'Till' => 'Till Management',
    'last_entrance' => 'Last entrance',
    'click_to_verify' => 'Click to verify',
    'enter_your_cnic' => 'Enter CNIC here',
    'parent' => 'Parent',
    'view_detail' => 'View Detail',
    'authorities' => 'Authorities',
    'assign_new_authority' => 'Assign new authority',
    'username' => 'User Name',
    'suffix' => 'Suffix',
    'visitor_suffix' => 'Visitor Suffix',
    'Mr' => 'Mr',
    'Mrs' => 'Mrs',
    'Miss' => 'Miss',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'middle_name' => 'Middle Name',
    'user_name' => 'User Name',
    'verified' => 'Verified',
    'not_verified' => 'Not Verified',
    'verified_from_nadra' => 'Verified from Nadra?',
    'long_term' => 'Long Term',
    'short_term' => 'Short Term',
    'employement' => 'Employement',
    'employer_name' => 'Employer Name',
    'working_since' => 'Working Since',
    'designation' => 'Designation',
    'verification_information' => 'Verification Information',


    'permanent_address' => 'Permanent Address',
    'permanent_city' => 'Permanent City',
    'permanent_country' => 'Permanent Country',
    'permanent_state' => 'Permanent State',
    'permanent_postal_code' => 'Permanent Postal Code',

    'temporary_address' => 'Temporary Address',
    'temporary_city' => 'Temporary City',
    'temporary_country' => 'Temporary Country',
    'temporary_state' => 'Temporary State',
    'temporary_postal_code' => 'Temporary Postal Code',


    'generate' => 'Generate',
    'year' => 'Year',
    'upload' => 'Upload',
    'vehicle_no' => 'Vehicle Number',
    'model' => 'Model',
    'back_to_models' => 'Back to Models',
    'create_model_for' => 'Create Model for',
    'models' => "Models",
    'back_to_companies' => 'Back to Companies',
    'create_company_for' => 'Create Company for',
    'system_management' => 'System Management',
    'system' => 'System',
    'back_to_types' => 'Back to Types',
    'all_companies_of' => 'All Companies of',
    'companies' => 'Companies',
    'vehicle_information' => 'Vehicle Information',
    'sticker' => 'Sticker',
    'etag' => 'E-tag',
    'personal_information' => 'Personal Information',
    'basic_information' => 'Basic Information',
    'view' => 'View',
    'manage' => 'Manage',
    'apply_authorizations' => 'Apply Authorizations',
    'authorization' => 'Authorizations',
    'configuration' => 'Configuration',
    'maintenance' => 'Maintenance',
    'logo' => 'Logo',
    'create_type' => 'Create Type',
    'favicon' => 'Favicon',
    'short_name' => 'Short Name',
    'cameras' => 'Cameras',
    'gates' => 'Gates',
    'houses' => 'Houses',
    'english' => 'English',
    'urdu' => 'Urdu',
    'gate' => 'Gate',
    'no_cameras_found' => 'No Camera Found',
    'all_cameras' => 'All Cameras',
    'network' => 'Network',
    'assign_cameras' => 'Assign Cameras',
    'all_gates' => 'All Gates',
    'hardware' => 'Hardware',
    'restore' => 'Restore',
    'delete_all' => 'Delete All',
    'perm_delete' => 'Permanent Delete',
    'temp_delete' => 'Temporary Delete',
    'delete_selected' => 'Delete Selected',
    'import_csv' => 'Import csv',
    'sample_csv' => 'Sample csv',
    'print' => 'Print',
    'download_pdf' => 'Download PDF',
    'sync_import_date' => 'Sync Import data',
    'preview_import' => 'Preview Import',
    'create_import' => 'Create Import',
    'import' => 'Import',
    'export' => 'Export',
    'owner_is' => 'Owner is',
    'search_house' => 'Search House',
    'list_all_medias' => 'List of All Medias',
    'docs' => "List of all documents",
    'choose_multiple_files' => "Choose multiple Files",
    'type' => 'Type',
    'choose_type' => 'Choose Type',
    'create_house' => 'Create House',
    'choose_owner' => 'Choose Owner',
    'fms' => 'Family Management',
    'all_families' => 'All Families',
    'vrs' => 'Vehicle Management',
    'ums' => 'User Management',
    'hms' => 'House Management',
    'all_permissions' => 'All Permissions',
    'all_roles' => 'All Roles',
    'show' => 'Show',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'permissions' => 'Permissions',
    'create' => 'Create',
    'search' => 'Search',
    'search_here' => 'Please search here....',
    'save' => 'Save',
    'name' => 'Name',
    'label' => 'Label',
    'placeholder_name' => 'Enter Name..',
    'placeholder_label' => 'Enter Label..',
    'create_permission' => 'Create Permission',
    'create_role' => 'Create Role',
    /* Roles */
    'role_created_success_message' => 'Role Created SuccessFully',
    'role_updated_success_message' => 'Role Updated Successfully',
    'role_created_failed_message_message' => 'Role Created Failed',
    'role_updated_failed_message_message' => 'Role Updated Failed',
    /* Permissions */
    'permission_created_success_message' => 'Permission Created SuccessFully',
    'updated_success_message' => 'Updated Successfully',
    'permission_created_failed_message' => 'Permission Created Failed',
    'updated_failed_message' => 'Updated Failed',
    'edit_role' => 'Edit Role',
    'edit_permission' => 'Edit Permission',
    'update' => 'Update',
    'deleted_successfully' => 'Deleted Successfully',
    'alert' => '!Alert',
    'all_permissions_of' => 'All permissions of',
    'permission_sync' => 'Permissions synced!',
    'failed_permission_sync' => 'Failed to sync permissions!',
    'all_users' => 'All Employees',
    'create_user' => 'Create User',
    'email' => 'Email',
    'placeholder_email' => 'test@domain.com',
    'password' => 'Password',
    'placeholder_password' => 'Enter Password....',
    'is_active' => 'Is Active',
    'all_roles_of' => 'All Roles of',
    'all_users_of' => 'All Users of',
    'heading' => 'System',
    'sync_permissions' => 'Sync Permissions',
    'created_at' => 'Created At',
    'last_modified' => 'Last Modified',
    'action' => 'Action',
    'active' => 'Active',
    'image' => 'Image',
    'choose_file' => 'Choose File',
    'media' => 'Media',
    'info_of' => 'Information of ',
    'settings' => 'Settings',
    'dashboard' => 'Dashboard',
    'assign_roles' => 'Assign Roles',
    'logs' => 'Logs',
    'users' => 'Users',
    'roles' => 'Roles',
    'logged_in_with_ip_address_of' => 'Logged in with Ip address of ',
    'de-active' => 'De Active',
    'load_more' => 'Load More',
    'detail_of' => 'Detail of ',
    'created' => 'Created',
    'last_update' => 'Last Update',
    'latest_logged_in_users' => 'Latest Logged In Users',
    'no_role_found' => 'No Role Found',
    'assign_user' => 'Assign User',
    'assign_role' => 'Assign Role',
    'assign_permission' => 'Assign Permission',
    'peoples' => 'Peoples',
    'all_peoples' => 'All peoples',
    'create_people' => 'Create People',
    'user' => 'User',
    'phone' => 'Phone/Mobile',
    'ptcl' => 'PTCL',
    'emergency_contact' => 'Emergency Contact',
    'cnic_front' => 'CNIC front',
    'cnic_back' => 'CNIC back',
    'dob' => 'Date of Birth',
    'cnic' => 'CNIC',
    'gender' => 'Gender',
    'male' => 'Male',
    'female' => 'Fe Male',
    'unspecified' => 'Unspecified',
    'father_name' => 'Father Name',
    'placeholder_cnic' => 'CNIC',
    'father_cnic' => 'Father CNIC',
    'general_information' => 'General Information',
    'media_information' => 'Media Information',
    'management' => 'Management',
    'residence' => 'Residence',
    'visitor' => 'Visitor',
    'card_detail' => 'Card Detail',
    'barcode' => 'Bar Code',
    'placeholder_barcode' => 'Bar Code',
    'placeholder_rfid' => 'RFID',
    'rfid' => 'RFID',
    'avatar' => 'Avatar',
    'address' => 'Address',
    'temp_address' => 'Temporary Address',
    'perm_address' => 'Permanent Address',
    'vehicles' => 'Vehicles',
    'all_vehicles' => 'All Vehicles',
    'create_vehicle' => 'Create Vehicle',
    'number' => 'Reg Number',
    'terms' => 'Terms',
    'car_type' => 'Type',
    'longterm' => 'Long Term',
    'shortterm' => 'Short Term',
    'vehicle' => 'Vehicle',
    'owner_name' => 'Owner Name',
    'entry_time' => 'Entry Time',
    'exit_time' => 'Exit Time',
    'status' => 'Status',
    'generate_etag' => 'Generate ETag',
    'company' => 'Company',
    'color' => 'Color',
    'profile' => 'Profile',
    'types' => 'Choose Type',
    'all_houses' => 'All Houses',
    'date_of_birth' => 'Date of Birth',
    'all_types' => 'All Types',
    'vehiclecolor' => 'Vehicle Color',
    'vehiclereg_no' => 'Vehicle Registration No.',
    'drivername' => 'Driver Name',
    //families
    'create_family' => 'Create Family',
    'create_family_members' => 'Create Family Members',
    'choose_house' => 'Choose House',
    //houses
    'house_no' => 'House Number',
    'owner' => 'Owner',
    'area' => 'Area',
    'vehicle_companies' => 'Company',
    'vehicle_terms' => 'Terms',
    'vehicle_etag' => 'E-Tag',
    'vehicle_frontdoc' => 'Front Document Picture',
    'vehicle_backdoc' => 'Back Document Picture',
    'vehicle_frontimage' => 'Front Vehicle Picture',
    'vehicle_backimage' => 'Front Vehicle Picture',
    'document_info' => 'Document Information',
    'media_info' => 'Media Information',
    'images' => 'Images',
    'fname' => 'First Name',
    'lname' => 'Last Name',
    'house' => 'House',
    'families' => 'Families',
    'lanes' => 'Lanes',

    'reg_on_name' => 'Registered On (Name)',
    'vehicle_driven_by' => 'Vehicle Driven By',
    'reg_no' => 'Registration Number',
    'reg_no_series' => 'Registration Series',
    'reg_no_year' => 'Registration Year',
    'model_year' => 'Model Year',
    'model_month' => 'Model Month',
    'chasis_number' => 'Chasis Number',
    'engine_number' => 'Engine Number',
    'vehicle_company' => 'Vehicle Company',
    'vehicle_model' => 'Vehicle Model',
    'vehicle_color' => 'Vehicle Color',
    'vehicle_verified_by' => 'Verified By',
    'etag_hashkey' => 'ETag Hash Key',
    'vehicle_images' => 'Vehicle Images',
    'vehicle_info' => 'Vehicle Information',
];

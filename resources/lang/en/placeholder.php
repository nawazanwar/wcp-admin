<?php

return [
    'search_owner'=>'Search Owner',
    'generate_hexadecimal_number' => 'Hexa Decimal Number',
    'enter_number_year' => 'Number allocated year',
    'enter_vehicle_no' => 'Enter vehicle number',
    'search_company' => 'Search Company',
    'search_model' => 'Search Model',
    'enter_color' => 'Enter color',
    'enter_first_name' => 'Enter first name',
    'enter_last_name' => 'Enter last name',
    'enter_cnic_name' => 'XXXXX-XXXXXXX-X',
    'enter_phone'=>'Enter mobile',
    'enter_cell'=>'Enter PTCL contact',

    'reg_on_name'=>'Registeration Name',
    'reg_no'=>'Registerataion Number',
    'reg_no_series'=>'Registeration Series',
    'reg_no_year'=>'Registeration Year',
    'model_year'=>'Model Year',
    'model_month'=>'Model Month',
    'chasis_number'=>'Chasis Number',
    'engine_number'=>'Engine Number',
    'vehicle_verified_by'=>'Vehicle Verified By',
    'enter_cell'=>'Enter',
];

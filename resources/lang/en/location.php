<?php

return [
    /*start form Attributes*/
    'countries' => [
        'title' => 'Countries',
        'all' => 'List of All Countries',
        'create' => 'Create'
    ],
    'states' => [
        'title' => 'States',
        'create' => 'Create',
        'all' => 'List of all States of '
    ],
    'cities' => [
        'title' => 'Cities',
        'create' => 'Create',
        'all' => 'List of all Cities  of '
    ],
    'tehsils' => [
        'title' => 'Tehsils',
        'create' => 'Create',
        'all' => 'List of all Tehsils  of '
    ]
];

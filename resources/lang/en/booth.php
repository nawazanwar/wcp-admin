<?php
return [
    'title' => 'All booths',
    'visitors' => 'All Visitors',
    'cameras' => 'All Cameras',
    'notifications' => 'Notifications',
    'all'=>'List of all Booths',
    'visitor' => [
        'fraud_enter' => 'Fraud Enter',
        'fraud_exit' => 'Fraud Exit',
        're-enter' => 'Re Enter',
        're-exit' => 'Re Exit',
        'wrong_passenger' => 'Exit Wrong Passengers',
        'wrong_vehicle' => 'Exit Wrong Vehicle',
        'logs_of' => 'All Logs of ',
        'logs' => 'Logs',
        'entry_logs' => 'Entry Logs',
        'exit_logs' => 'Exit Logs',
        'name' => 'Visitor Name',
        'name_ur' => 'وزیٹر کا نام',
        "info" => [
            "second" => [
                "vehicle" => 'احتیاط سے گاڑی کی تفصیل کی تصدیق کریں',
                "persons" => "برائے کرم مرد ، خواتین اور بچوں کی تصدیق کریں",
                'old_record_detail' => 'براہ کرم موجودہ ریکارڈ کو چیک کرنے کے لئے ایک فارم کھولیں'
            ],
            "first" => [
                "vehicle" => 'احتیاط سے گاڑی کی معلومات درج کریں',
                "persons" => 'احتیاط سے گاڑی میں افراد کی تعداد درج کریں',
                'driver' => 'ڈرائیور کا فون نمبر درج کرنا نہ بھولیں',
                'visitee' => 'جہاں جانا ہے اس شخص کی معلومات درج کریں'
            ]
        ],

        'view_all_logs' => 'View All Logs',

        'messages' => [
            'fraud_exit_visitor' => 'یہ ملاقاتی کسی بوتھ سے داخل نہیں ہوا ہے لیکن باہر نکلنے کی کوشش کر رہا ہے لہذا نوٹیفکیشن اعلی حکام کو بھیج دیا گیا ہے لہذا براہ کرم جواب کا انتظار کریں',
            'more_vehicle_info' => 'گاڑی کی مزید معلومات کے لئے ایک فارم کھولیں',
            'entry_point' => 'سوسائٹی میں داخلے کا مقام',
            'exit_point' => 'سوسائٹی سے باہر نکلنے کا مقام',
            'existing_info' => 'پچھلی معلومات',
            'all_visitors_first_entry' => 'First time Entry of all Visitors with complete Detail',
            'already_register' => 'یہ ملاقاتی پہلے ہی رجسٹرڈ ہے',
            'fraud_reenter_visitor' => 'یہ وزیٹر کسی بھی بوتھ سے باہر نہیں نکلا تھا اب نوٹیفیکیشن ایڈمن آفس کو تصدیق کے لئے بھیجا گیا ہے لہذا جواب کا انتظار کریں',
            're_exit_message' => 'یہ وزیٹر پہلے ہی سوسائٹی سے باہر نکل گیا ہے اب دوبارہ باہر نکلنے کی کوشش کر رہا ہے لہذا نوٹیفکیشن اعلی حکام کو بھیج دیا گیا ہے لہذا براہ کرم انتظار کریں',
        ],

        'tabs' => [
            'vehicle' => 'Vehicle/Info',
            'verifications' => 'Verifications',
            'actions' => 'Actions'
        ],
        'no_found' => 'No Visitor Found',
        'vehicle' => [
            'type' => 'Type',
            'type_ur' => 'گاڑی کی قسم',
            'company' => 'Company/Make',
            'company_ur' => 'گاڑی کی کمپنی',
            'model' => 'Model',
            'model_ur' => 'گاڑی کا ماڈل',
            'color' => 'Color',
            'color_ur' => 'گاڑی کا رنگ',
            'reg_no' => 'Reg No',
            'reg_no_ur' => 'گاڑی کا نمبر',
            'reg_year' => 'Reg Year',
            'reg_year_ur' => 'رجسٹریشن کا سال',
            'males' => 'Males',
            'females' => 'Females',
            'kids' => 'Kids',
            'males_ur' => 'مردوں کی تعداد',
            'females_ur' => 'خواتین کی تعداد',
            'kids_ur' => 'بچوں کی تعداد'
        ],
        'verifications' => [
            'biometric' => 'biometric',
            'ratina' => 'Ratina',
            'gate_pass' => 'Gate Pass',
            'qrcode' => 'Qr Code',
        ],
        'actions' => [
            'call_visitee' => 'Call Visitee',
            'verified' => 'Verified',
            'follow' => 'Follow',
            'guide' => 'Guide',
            'warm' => 'Warm',
            'permitted' => 'Permitted',
            'exit' => 'Exit'
        ]
    ],
];

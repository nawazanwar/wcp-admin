<?php

return [
    'all_locations_of' => 'All locations of',
    'all_gates_of' => 'All gates of',
    'all_phases' => 'All Phases',
    'total_designations' => 'Total Designations',
    'manage_designations' => 'Manage Designations',
    'designations' => 'Designations',
    'blocks_created_successfully' => 'Blocks created Successfully',
    'blocks' => 'Blocks',
    'block_name' => 'Block Name',
    'block_label' => 'Block Label',
    'create_new_block' => 'Create New Block',
    'create_block' => 'Create New Block',
    'edit_block_of' => 'Edit Block of',
    'block_updated_success_message' => 'Block Updated Successfully',
    'block_deleted_successfully' => 'Block Deleted Successfully',
    'create_designation' => 'Create new Designation',

    'phase_created_successfully' => 'Phase created Successfully',
    'phases' => 'Phases',
    'phase_name' => 'Phase Name',
    'phase_label' => 'Phase Label',
    'create_new_phase' => 'Create New Phase',
    'create_phase' => 'Create New Phase',
    'edit_phase_of' => 'Edit Phase of',
    'phase_updated_success_message' => 'Phase Updated Successfully',
    'phase_deleted_successfully' => 'Phase Deleted Successfully',

    'lane_created_successfully' => 'Lane created Successfully',
    'lane' => 'Lane',
    'lane_name' => 'Lane Name',
    'lane_label' => 'Lane Label',
    'create_new_lane' => 'Create New Lane',
    'create_lane' => 'Create New Lane',
    'edit_lane_of' => 'Edit Lane of',
    'lane_updated_success_message' => 'Lane Updated Successfully',
    'lane_deleted_successfully' => 'Lane Deleted Successfully',

    'gates' => [
        'all' => 'All Gates of Society',
        'updated_success_message' => 'Gate Updated Successfully',
    ],
    'booths' => [
        'all' => 'All Booths of Society',
        'updated_success_message' => 'Booth Updated Successfully',
        'gate' => 'Gate',
        'lane' => 'Lane',
        'type' => 'Type',
        'create' => 'Create New Line/Booth'
    ]
];

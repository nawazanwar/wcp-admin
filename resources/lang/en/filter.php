<?php

return [

    'please_select' => 'Please Select',
    'today' => 'Today',
    'tomorrow' => 'ToMorrow',
    'yesterday' => 'Yesterday',
    'this_week' => 'This week',
    'last_week' => 'Last Week',
    'this_month' => 'This Month',
    'last_month' => 'Last Month',
    'this_year' => 'This Year',
    'last_year' => 'Last Year',
    'specific_date' => 'Specific Date',
    'date_range' => 'Date Range',
    'select_date' => 'Select Date',
    'filter' => 'Filter',
    'date_from' => 'From Date',
    'date_to' => 'To Date',
    'no_logs_found' => 'No Logs Found'
];

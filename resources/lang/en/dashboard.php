<?php

return [
    'employee_management_system' => 'Employee Management System',
    'welcome_back_title' => 'Welcome Back to Dashboard',
    'site_title' => 'WCP',
    'home' => 'Home',
    'site_complete_name' => 'Wapda Housing Society',


    'user_management_system' => 'User Management System',
    'house_management_system' => 'House Management System',
    'vehicle_registration_system' => 'Vehicle Registration System',
    'land_record_management_system' => 'Land Record Management',
    'network_management_system' => 'Network Management System',
    'all_tills' => 'All Tills',
    'visitor_management_system' => 'Visitors Management System',
    'device_management_system' => 'Device Management',
    'vms' => 'VMS',
    'hrm' => 'HRM',
    'vrs' => 'VRS',
];

@php $allCamerasArray = array();  @endphp
@foreach($devices as $dkey=>$device)
    @php
        $dkey = $dkey+1;
        $dtypes = \App\Device\DType::where('id', $device->dtype_id)->get()->pluck('name', 'id');
    @endphp
    <div class="col-md-5 form-group card m-2">
        @if(count($dtypes)>0)
            @foreach($dtypes as $key=>$d)
                <div class="card-header">
                    <div class="card-title text-center">
                        {{$device->dev_name}}
                    </div>
                </div>
                <div class="card-body">
                    @if($d=='CAMERA' OR $d=='Camera' OR $d=='camera')
                        @php $url = 'http://'.$device->dev_admin_login.':'.$device->dev_admin_pwd.'@'.$device->dev_ip_address.'/'.$device->dev_connection_string.'101/picture'; @endphp
                        <iframe
                            src="{{__('http://'.$device->dev_ip_address.'/'.$device->dev_connection_string.'101/httpPreview')}}"
                            frameBorder="0" id="iframVid"
                            style="width: 100%;height:{{ isset($for)?'327px':'200px' }};"></iframe>
                    @endif
                </div>
                <div class="card-footer text-center">
                    @php
                        $camera_name=null;
                        switch ($dkey){
                             case 1:
                             $camera_name='first_camera_image';
                             break;
                             case 2:
                             $camera_name='second_camera_image';
                             break;
                             case 3:
                             $camera_name='third_camera_image';
                             break;
                             case 4:
                             $camera_name='fourth_camera_image';
                             break;
                         }
                        $allCamerasArray[$dkey."_camera_image"] =$url;
                    @endphp
                    <input type="hidden" name="{{ $camera_name }}" id="{{$dkey}}_camera_image">
                    {!! Form::hidden('camera',$url,['id'=>$dkey."_camera"]) !!}
                    {!! Form::button('Capture <i class="fa fa-camera" aria-hidden="true"></i>',['onclick'=>'getCurrentImage("'.$url.'",'.$dkey.');','class'=>'btn btn-sm bg-gradient-success']) !!}
                </div>
            @endforeach
        @endif
    </div>
@endforeach
{!! Form::hidden('all_cameras',json_encode($allCamerasArray),['id'=>'all_cameras']) !!}

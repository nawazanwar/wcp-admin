@extends('layouts.auth')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/colorbox/colorbox.css') }}">
    <link rel='stylesheet' href="{{ asset('plugins/colorpicker/spectrum.css') }}"/>
@endsection
@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop
@section('content')
    <style>
        .bootstrap-select.show-menu-arrow.open > .dropdown-toggle,
        .bootstrap-select.show-menu-arrow.show > .dropdown-toggle {
            z-index: 0;
        }
    </style>
    <section class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card direct-chat direct-chat-primary box-shadow">
                @include('booths.components.header')
                <!-- /.card-header -->`
                    <div class="card-body">
                        <form id="visitor_form" class="text-muted" method="post" action="javascript:void(0)"
                              enctype="multipart/form-data">
                            {!! Form::hidden('booth_id',$boothInfo->booth_id) !!}
                            {!! Form::hidden('visitor_id',null,['id'=>'visitor_id']) !!}
                            @csrf
                            <div class="row ">
                                <div class="col-md-7 px-4 pb-3 border-right">
                                    @include('booths.components.vehicle-number',['for'=>'exit'])
                                    @include('booths.components.visitors',['for'=>'exit'])
                                    @include('booths.components.vehicles',['for'=>'exit'])
                                </div>
                                <div class="col-md-5 px-4 pb-3">
                                    <div class="row">
                                        <div class="col-md-12 info-heading-parent clearfix py-1">
                                            @include('booths.components.clock')
                                        </div>
                                        <div class="col-12 text-center">
                                            <a class="btn bg-gradient-pink btn-sm text-white cursor-pointer" onclick="captureAllImages();return false;">
                                                Capture All <i class="fa fa-camera" aria-hidden="true"></i>
                                            </a>
                                            <button type="submit" class="btn btn-info btn-sm">
                                                Save Record <i class="fa fa-save" aria-hidden="true"></i>
                                            </button>
                                            <button class="btn bg-gradient-success btn-sm" disabled>
                                                Open Barrier <i class="fa fa-arrow-alt-circle-right" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center mt-3">
                                        @if(count($devices))
                                            @include('booths.devices')
                                        @else
                                            <div class="col-12 text-center py-3">
                                                <strong>No Camera's Configure in this Booth</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    @include('booths.components.footer')
                </div>
            </div>
        </div>
    </section>
@stop
@section('scriptInnerFiles')
    <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('plugins/colorbox/jquery.colorbox.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('plugins/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('plugins/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('plugins/colorpicker/spectrum.js') }}"></script>
@endsection
@include('booths.components.scripts')
@section('pageScript')
    <script>
        (function () {

            $("input:not('#vehicle_reg_no')").keypress(function () {
                var _val = $(this).val();
                var _txt = _val.charAt(0).toUpperCase() + _val.slice(1);
                $(this).val(_txt);
            });
            $("textarea").keypress(function () {
                var _val = $(this).val();
                var _txt = _val.charAt(0).toUpperCase() + _val.slice(1);
                $(this).val(_txt);
            });

            $(":input").inputmask();
            $("#input_color_picker").spectrum({
                showPalette: true,
                showInput: true,
                showAlpha: true,
                showInitial: false,
                preferredFormat: "name",
                color: "transparent",
                palette: [
                    ["#000", "#444", "#666", "#999", "#ccc", "#eee", "#f3f3f3", "#fff"],
                    ["#f00", "#f90", "#ff0", "#0f0", "#0ff", "#00f", "#90f", "#f0f"],
                    ["#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d0e0e3", "#cfe2f3", "#d9d2e9", "#ead1dc"],
                    ["#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#9fc5e8", "#b4a7d6", "#d5a6bd"],
                    ["#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6fa8dc", "#8e7cc3", "#c27ba0"],
                    ["#c00", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3d85c6", "#674ea7", "#a64d79"],
                    ["#900", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#0b5394", "#351c75", "#741b47"],
                    ["#600", "#783f04", "#7f6000", "#274e13", "#0c343d", "#073763", "#20124d", "#4c1130"]
                ],
                change: function (color) {
                    $("#vehicle_color").val(color);
                }
            });

            $("#visitor_form").validate({
                rules: {
                    vehicle_reg_no: 'required',
                },
                messages: {
                    vehicle_reg_no: '{{__('vms.errors.vehicle_registration_number')}}'
                }, submitHandler: function (form) {
                    Ajax.setAjaxHeader();
                    $("#overlay").show();
                    Ajax.setAjaxHeader();
                    var formData = new FormData(document.getElementById('visitor_form'));
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('booth-exit-visitor') }}",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: (response) => {
                            $("#overlay").hide();
                            if (response.status == 'true') {
                                resetForm();
                                toastr.success('Welcome you are clear to Go');
                                $("#vehicle_reg_no").val('');
                                $("#visitor_cnic").val('');
                            }
                        }
                    });
                }//end of the submit handler
            });//end of the form

        })(jQuery)

        // Filter by Cnic
        function filterByCNICNumber(cElement) {
            var idCard = $(cElement).val().split("_")[0];
            if (idCard != '' && idCard.length == 15) {
                Ajax.call('{{ route("booth-check-visitor-at-enter") }}', {
                    'type': 'cnic',
                    'value': idCard
                }, 'POST', function (response) {

                    if (response.status == 'exist') {
                        toastr.info('Hi Record has already Found and auto Fill')
                        $("#visitor_cell_no").val(response.data.visitor_cell_no);
                        $("#visitor_name").val(response.data.visitor_name)

                    } else {

                        $("#visitor_cell_no").val('');
                        $("#visitor_name").val('')

                    }

                });
            }
        }

        // Filter by Vehicle Number
        function filterByVehicleOnEnter(cElement) {
            var vehicle_number = $(cElement).val();
            if (vehicle_number != '' && vehicle_number.length > 1) {
                Ajax.call('{{ route("booth-check-visitor-at-enter") }}', {
                    'type': 'vehicle',
                    'value': vehicle_number
                }, 'POST', function (response) {
                    if (response.status == 'exist') {
                        toastr.info('Hi Record has already Found and auto Fill')
                        $("#input_color_picker").spectrum("set", response.data.vehicle_color);
                        $('.v_type_holder option').filter(function () {
                            return ($(this).val() == response.data.type_id);
                        }).prop('selected', true);
                        $(".v_company_holder").empty().append(response.companyOptions);
                        $(".v_mode_holder").empty().append(response.modelOptions);
                        $(".v_vehicle_nature_of_material_holder").empty().append(response.vNatureOptions);
                        $('#vehicle_reg_year option').filter(function () {
                            return ($(this).val() == response.data.vehicle_reg_year);
                        }).prop('selected', true);

                    } else {

                        resetFormOnFilterByVehicleNumber()

                    }
                });
            }
        }

        function resetFormOnFilterByVehicleNumber() {

            $("#input_color_picker").spectrum("set", 'transparent');
            $(".v_type_holder option").first().prop('selected', true);
            $(".v_company_holder").empty().append("<option disabled='disabled' selected='selected'>Create new Company</option>");
            $(".v_mode_holder").empty().append("<option disabled='disabled' selected='selected'>Create new Model</option>");
            $(".v_vehicle_nature_of_material_holder")
                .empty()
                .append("<option disabled='disabled' selected='selected'>Create Nature of Materials</option>");
            $("#vehicle_reg_year option").first().prop('selected', true);
        }

        function resetForm(type = null) {

            $("#no_of_males").val(0);
            $("#no_of_females").val(0);
            $("#no_of_kids").val(0);
            $("#visitor_name").val('');
            $("#visitor_cell_no").val('');
            $("#vehicle_color").val('transparent');
            $("#visitor_id").val('');
            $("#purpose_of_visit").val('');
            $("#input_color_picker").spectrum("set", 'transparent');

            $("#vehicle_reg_year option").first().prop('selected', true);
            $(".v_type_holder option").first().prop('selected', true);
            $(".v_company_holder").empty().append("<option disabled='disabled' selected='selected'>Create new Company</option>");
            $(".v_mode_holder").empty().append("<option disabled='disabled' selected='selected'>Create new Model</option>");
            $(".v_vehicle_nature_of_material_holder")
                .empty()
                .append("<option disabled='disabled' selected='selected'>Create Nature of Materials</option>");

            // Reset Actions

            $('#call_visitee').prop('checked', false).val(0);
            $('#verified').prop('checked', false).val(0);
            $('#follow').prop('checked', false).val(0);
            $('#guide').prop('checked', false).val(0);
            $('#warn').prop('checked', false).val(0);
            $('#permitted').prop('checked', false).val(0);
            $('#exit').prop('checked', false).val(0);

            // Reset Verifications

            $("#visitor_biometric_verification").prop('checked', false).val(0);
            $("#visitor_ratina_verification").prop('checked', false).val(0);
            $("#visitor_gate_pass_verification").prop('checked', false).val(0);
            $("#visitor_qrcode_verification").prop('checked', false).val(0);

            // Rest Images....

            $("#1_camera_image").val('');
            $("#2_camera_image").val('');
            $("#3_camera_image").val('');
            $("#4_camera_image").val('');
        }

        function getCurrentImage(url, key) {
            $("#overlay").show();
            Ajax.call('{{ route("booth.temp.pic") }}', {'url': url}, 'POST', function (response) {
                $("#" + key + "_camera_image").val(response);
                $("#overlay").hide();
            });
        }

        function captureAllImages() {

            var all_cameras = JSON.parse($("#all_cameras").val());
            console.log(all_cameras);
            $("#overlay").show();
            Ajax.call('{{ route("booth.temp.pic.all") }}', all_cameras, 'POST', function (response) {
                $("#overlay").hide();
                $.each(response, function (key, value) {
                    $("#" + key).val(value);
                });
            });
        }

    </script>
@endsection

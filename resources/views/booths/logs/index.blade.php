@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card card-default my-2">
                <div class="card-header">
                    @include('booths.logs.filter')
                </div>
                @if(count($logs)<1)
                    <div class="card-body text-center">
                        <p class="font-weight-bold">{{__('filter.no_logs_found')}}</p>
                    </div>
                @else
                    <div class="card-body">
                        {{-- Start --}}
                        <div class="timeline">
                            <!-- timeline time label -->
                            @foreach($logs as $parent_key=>$parent_logs)
                                <div class="time-label">
                                    <span class="bg-red">{{ $parent_key }}</span>
                                </div>
                                @foreach($parent_logs as $key=>$log)
                                    <div>
                                        <div class="timeline-body" style="padding-left: 60px;">
                                            <div class="direct-chat-msg">
                                                <div class="direct-chat-infos clearfix pl-3">
                                                    <a class="direct-chat-name float-left text-primary">{{ $log->user_name }}</a>
                                                    <a class="direct-chat-timestamp float-right text-danger font-weight-bold">{{ Carbon\Carbon::parse($log->date_time)->format('h:i:s') }}</a>
                                                </div>
                                                <div class="direct-chat-text ml-3 bg-white">
                                                    <table class="table">
                                                        <tr>
                                                            <th class="text-center">{{ __('system.type') }}</th>
                                                            <th>{{__('vms.mac_address')}}</th>
                                                            <th>{{__('vms.ip_address')}}</th>
                                                            <th>{{__('vms.booth')}}</th>
                                                            <th>{{ __('vms.date_time') }}</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center">{{ $log->type }}</td>
                                                            <td>{{ $log->mac_address  }}</td>
                                                            <td>{{ $log->ip_address  }}</td>
                                                            <td>
                                                                <span>{{ strtoupper($log->phase_name) }}({{$log->phase_name_ur}})</span>--
                                                                <span>{{ strtoupper($log->gate_name) }}({{$log->gate_name_ur}})</span>--
                                                                <span
                                                                    class="text-success font-weight-bold">{{ ucfirst($log->booth_lane) }}</span>
                                                            </td>
                                                            <td>{{ Carbon\Carbon::parse($log->created_at)->format('d M Y') }}
                                                                / <span
                                                                    class="text-info">{{ Carbon\Carbon::parse($log->created_at)->format('l') }}</span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                        {{-- End--}}
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
@section('scriptInnerFiles')
    <script type="text/javascript"
            src="{{ asset('plugins/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
@stop
@section('pageScript')
@endsection

<style>
    .card-secondary:not(.card-outline) .card-header,
    .card-secondary:not(.card-outline) .card-header a {
        color: black;
    }
</style>
@php
    $cId = Request::route('id');
    if (isset($cId)){
        $cRoute = route('vms-booths.operator-logs',['id'=>$cId]);
    }else{
        $cRoute = route('vms-booths.operator-logs');
    }
    $cKey = Request::get('key');
@endphp
<form action="{{ $cRoute }}" method="get">
    <div class="row justify-content-center">
        <div class="col-4">
            <select
                onchange='this.form.submit()'
                data-live-search="true"
                data-selected-text-format="count > 3"
                class="form-control form-control-sm selectpicker show-menu-arrow show-tick"
                name="key">
                <option value="" selected disabled>{{__('filter.please_select')}}</option>
                <option value="today"
                        @if(isset($cKey) AND $cKey=='today') selected @endif >{{__('filter.today')}}</option>
                <option value="yesterday"
                        @if(isset($cKey) AND $cKey=='yesterday') selected @endif >{{__('filter.yesterday')}}</option>
                <option value="this_week"
                        @if(isset($cKey) AND $cKey=='this_week') selected @endif >{{__('filter.this_week')}}</option>
                <option value="last_week"
                        @if(isset($cKey) AND $cKey=='last_week') selected @endif >{{__('filter.last_week')}}</option>
                <option value="this_month"
                        @if(isset($cKey) AND $cKey=='this_month') selected @endif >{{__('filter.this_month')}}</option>
                <option value="last_month"
                        @if(isset($cKey) AND $cKey=='last_month') selected @endif >{{__('filter.last_month')}}</option>
                <option value="this_year"
                        @if(isset($cKey) AND $cKey=='this_year') selected @endif >{{__('filter.this_year')}}</option>
                <option value="last_year"
                        @if(isset($cKey) AND $cKey=='last_year') selected @endif >{{__('filter.last_year')}}</option>
                <option value="specific_date"
                        @if(isset($cKey) AND $cKey=='specific_date') selected @endif >{{__('filter.specific_date')}}</option>
                <option value="date_range"
                        @if(isset($cKey) AND $cKey=='date_range') selected @endif >{{__('filter.date_range')}}</option>
            </select>
        </div>
        @if(isset($cKey) AND $cKey=='specific_date')
            <div class="col-md-4">
                <input class="form-control"
                       value="@if(isset($cKey) AND $cKey=='specific_date'){{Request::get('value')}}@endif" name="value"
                       type="date" placeholder="{{__('filter.select_date')}}">
            </div>
        @endif
        @if(isset($cKey) AND $cKey=='date_range')
            <div class="col-md-3">
                <input class="form-control" name="from"
                       value="@if(isset($cKey) AND $cKey=='date_range'){{Request::get('from')}}@endif" type="date"
                       placeholder="{{__('filter.date_from')}}">
            </div>
            <div class="col-md-3">
                <input class="form-control"
                       value="@if(isset($cKey) AND $cKey=='date_range'){{Request::get('to')}}@endif" name="to"
                       type="date" placeholder="{{__('filter.date_to')}}">
            </div>
        @endif
        @if(isset($cKey) AND ($cKey=='specific_date' OR $cKey=='date_range'))
            <div class="col-md-2">
                <input type="submit" class="btn btn-primary" value="{{__('filter.filter')}}">
            </div>
        @endif
    </div>
</form>
<div class="row my-3 border-top border-bottom align-items-center">
    <div class="col-md-1 py-2">
        @can('read',\App\Models\VMS::class)
            {!! HTML::decode(link_to_route('vms.index',' <i class="fa fa-arrow-left"></i>',null,['class'=>'btn bg-gradient-info btn-xs']))!!}
        @endcan
    </div>
    <div class="col-md-11 py-2 text-right">
        @isset($cKey)
            @if($cKey=='specific_date')
                Filter Logs of {{ Request::get('value') }}
            @elseif($cKey=='date_range')
                Filter Logs from <strong class="text-info">{{ Request::get('from') }}</strong> to <strong
                    class="text-info">{{ Request::get('to') }}</strong>
            @else
                Filter Logs from <strong class="text-info">{{ $date_range['start'] }}</strong> to <strong
                    class="text-info">{{ $date_range['end'] }}</strong>
            @endif
        @else
            All Logs of <strong class="text-info">{{__('filter.today')}}</strong>
        @endisset
    </div>
</div>

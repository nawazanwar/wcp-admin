<div class="row m-auto">
    <div class="col-md-8 m-auto">
        <div class="row">
            <div class="col-md-4 form-group mx-auto">
                {!! Form::label('no_of_males','No of Males') !!}
                {!! Form::number('no_of_males',
                   0,
                   [
                       'class'=>'form-control form-control-sm',
                       'id'=>'no_of_males',
                       'min'=>0,
                       'onkeypress'=>'return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57'
                   ])
               !!}
            </div>
            <div class="col-md-4 form-group mx-auto">
                {!! Form::label('no_of_females','No of Females') !!}
                {!! Form::number('no_of_females',
                   0,
                   [
                       'class'=>'form-control form-control-sm',
                       'id'=>'no_of_females',
                       'min'=>0,
                       'onkeypress'=>'return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57'
                   ])
               !!}
            </div>
            <div class="col-md-4 form-group mx-auto">
                {!! Form::label('no_of_kids','No of Kids') !!}
                {!! Form::number('no_of_kids',
                   0,
                   [
                       'class'=>'form-control form-control-sm',
                       'id'=>'no_of_kids',
                       'min'=>0,
                       'onkeypress'=>'return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57'
                   ])
               !!}
            </div>
        </div>
    </div>
</div>

<div class="col-md-3 my-3">
    <div class="icheck-success d-inline">
        <input type="checkbox" id="visitor_biometric_verification" name="visitor_biometric_verification"
               value="0">
        <label for="visitor_biometric_verification">{{__('visitor.visitor_biometric_verification')}}</label>
    </div>
</div>
<div class="col-md-3 my-3">
    <div class="icheck-success d-inline">
        <input type="checkbox" id="visitor_ratina_verification" name="visitor_ratina_verification"
               value="0">
        <label for="visitor_ratina_verification">{{__('visitor.visitor_ratina_verification')}}</label>
    </div>
</div>
<div class="col-md-3 my-3">
    <div class="icheck-success d-inline">
        <input type="checkbox" id="visitor_gate_pass_verification" name="visitor_gate_pass_verification"
               value="0">
        <label for="visitor_gate_pass_verification">{{__('visitor.visitor_gate_pass_verification')}}</label>
    </div>
</div>
<div class="col-md-3 my-3">
    <div class="icheck-success d-inline">
        <input type="checkbox" id="visitor_qrcode_verification" name="visitor_qrcode_verification"
               value="0">
        <label for="visitor_qrcode_verification">{{__('visitor.visitor_qrcode_verification')}}</label>
    </div>
</div>

<div class="row">
    <div class="col-md-12 info-heading-parent clearfix py-1 text-info">
        <p class="info-heading pull-left">{{__('vms.vehicle_information')}}</p>
        <p class="info-heading pull-right">{{__('vms.vehicle_information_ur')}}</p>
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('type_id','Type') !!}
        @php
            $types = \App\Models\Type::whereParent('vehicle')->get();
        @endphp
        <div class="input-group input-group-sm">
            <select
                onchange="getCompaniesANdNatureOfMatierials(this);"
                class="v_type_holder form-control custom-select-box form-control-sm"
                name="type_id" id="type_id">
                <option value="" selected disabled>{{__('visitor.select_type')}}</option>
                @foreach($types as $type)
                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                @endforeach
            </select>
            <span class="input-group-append">
                        <button type="button" class="btn btn-info btn-flat"
                                onclick="gen_vehicle_type('v_type_holder');"><span
                                class="fa fa-plus-circle"></span></button>
                  </span>
        </div>
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('vcompany_id','Make/Company') !!}
        <div class="input-group input-group-sm">
            <select
                onchange="getCompanyModels(this);"
                class="v_company_holder form-control form-control-sm"
                name="vcompany_id" id="vcompany_id">
                <option value="" selected="selected" disabled="disabled">Create New Company</option>
            </select>
            <span class="input-group-append">
                        <button type="button" class="btn btn-info btn-flat"
                                onclick="gen_vehicle_company('v_company_holder');"><span
                                class="fa fa-plus-circle"></span></button>
                  </span>
        </div>
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('vmodel_id','Model') !!}
        <div class="input-group input-group-sm">
            <select
                data-live-search="true"
                class="form-control form-control-sm v_mode_holder"
                name="vmodel_id" id="vmodel_id">
                <option value="" selected="selected" disabled="disabled">Create New Model</option>
            </select>
            <span class="input-group-append">
                        <button type="button" class="btn btn-info btn-flat"
                                onclick="gen_vehicle_model('v_mode_holder');"><span
                                class="fa fa-plus-circle"></span></button>
                  </span>
        </div>
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('vehicle_color','Color') !!}
        {!! Form::hidden('vehicle_color',null,['id'=>'vehicle_color']) !!}
        <input class="form-control form-control-sm" id="input_color_picker" type="color">
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('vnature_id','Nature of Materials') !!}
        <div class="input-group input-group-sm">
            <select
                onchange="getVehicleNatureOfMaterialsModels(this);"
                class="form-control form-control-sm v_vehicle_nature_of_material_holder"
                name="vnature_id" id="vnature_id">
                <option value="" selected="selected" disabled="disabled">Create Nature of Material</option>
            </select>
            <span class="input-group-append">
                        <button type="button" class="btn btn-info btn-flat"
                                onclick="gen_vehicle_nature_of_material('v_vehicle_nature_of_material_holder');"><span
                                class="fa fa-plus-circle"></span></button>
                  </span>
        </div>
    </div>
    <div class="col-md-4 form-group">
        @php
            $years = array_combine(range(date("Y"), 1910), range(date("Y"), 1910));
        @endphp
        {!! Form::label('vehicle_reg_year','Vehicle Registration Year') !!}
        <select
            class="form-control form-control-sm"
            id="vehicle_reg_year"
            name="vehicle_reg_year">
            <option selected="selected" disabled="disabled">Select Vehicle Registration Year</option>
            @foreach($years as $key=>$value)
                <option value="{{ $key }}">{{ $value }}</option>
            @endforeach
        </select>
    </div>
</div>
@include('booths.components.passengers')

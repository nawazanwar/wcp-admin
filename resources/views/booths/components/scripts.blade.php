<script>
    function getCompaniesANdNatureOfMatierials(cElement) {
        var cValue = $(cElement).find('option:selected').val();
        var data = {
            key: 'companyWithNature',
            parent: cValue
        };
        Ajax.call('/dependent/dropdown', data, 'POST', function (response) {
            if (response.data.natures.length > 0) {
                var natureHtml = '<option value="" selected disabled>Select Nature of Material</option>';
                $.each(response.data.natures, function (key, value) {
                    natureHtml += "<option value=" + value.id + ">" + value.name + "</option>";
                });
                $(".v_vehicle_nature_of_material_holder").empty().append(natureHtml);
            } else {
                $(".v_vehicle_nature_of_material_holder")
                    .empty()
                    .append("<option value=\"\" selected disabled>Create Nature of Material</option>");
            }

            if (response.data.companies.length > 0) {
                var companyHtml = '<option value="" selected disabled>Select a Company</option>';
                $.each(response.data.companies, function (key, value) {
                    companyHtml += "<option value=" + value.id + ">" + value.name + "</option>";
                });
                $(".v_company_holder").empty().append(companyHtml);
            } else {
                $(".v_company_holder")
                    .empty()
                    .append("<option value=\"\" selected disabled>Create new Company</option>")
                ;
            }
        });
    }

    function getCompanyModels(cElement) {
        var cValue = $(cElement).find('option:selected').val();
        var data = {
            key: 'models',
            parent: cValue
        };
        Ajax.call('/dependent/dropdown', data, 'POST', function (response) {
            if (response.status == 'true') {
                var html = '<option value="" selected disabled>{{__('visitor.select_model')}}</option>';
                $.each(response.data, function (key, value) {
                    html += "<option value=" + value.id + ">" + value.name + "</option>";
                });
            } else {
                var html = '<option value="" selected disabled>Create a new Model</option>';
            }
            $(".v_mode_holder").empty().append(html);
        });
    }

    function gen_vehicle_type() {
        var name = prompt("Please Enter Vehicle Type", "");
        if (name != null) {
            Ajax.setAjaxHeader();
            $.post("{{ route('gen-vehicle-types') }}", {"isAjax": 'true', 'name': name}, function (result) {
                var html = '<option value="" selected disabled>{{__('visitor.select_type')}}</option>';
                $.each(result.types, function (key, value) {
                    html += "<option value=" + value.id + ">" + value.name + "</option>";
                });
                $(".v_type_holder").empty().append(html).val(result.model.id);
            });
        }
    }

    function gen_vehicle_nature_of_material() {
        var type_id = $(".v_type_holder").find('option:selected').val();
        if (type_id == '') {
            toastr.error('First choose Vehicle Type');
        } else {
            var name = prompt("Enter Nature of Materials", "");
            if (name != null) {
                Ajax.setAjaxHeader();
                $.post("{{ route('gen-vehicle-nature-of-materials') }}", {
                    "isAjax": 'true',
                    'name': name,
                    'type_id': type_id
                }, function (result) {
                    var html = '<option value="" selected disabled>Select or create Nature of Materials</option>';
                    $.each(result.natures, function (key, value) {
                        html += "<option value=" + value.id + ">" + value.name + "</option>";
                    });
                    $(".v_vehicle_nature_of_material_holder").empty().append(html).val(result.model.id);
                });
            }
        }
    }

    function gen_vehicle_company() {
        var type_id = $(".v_type_holder").find('option:selected').val();
        if (type_id == '') {
            toastr.error('First choose Vehicle Type');
        } else {
            var name = prompt("Please Enter Vehicle Company", "");
            if (name != null) {
                Ajax.setAjaxHeader();
                $.post("{{ route('gen-vehicle-companies') }}", {
                    "isAjax": 'true',
                    'name': name,
                    'type_id': type_id
                }, function (result) {
                    var html = '<option value="" selected disabled>{{__('visitor.select_company')}}</option>';
                    $.each(result.companies, function (key, value) {
                        html += "<option value=" + value.id + ">" + value.name + "</option>";
                    });
                    $(".v_company_holder").empty().append(html).val(result.model.id);
                });
            }
        }
    }

    function gen_vehicle_model() {
        var company_id = $(".v_company_holder").find('option:selected').val();
        if (company_id == '') {
            toastr.error('First choose Vehicle Company');
        } else {
            var name = prompt("Please Enter Vehicle Model", "");
            if (name != null) {
                Ajax.setAjaxHeader();
                $.post("{{ route('gen-vehicle-models') }}", {
                    "isAjax": 'true',
                    'name': name,
                    'company_id': company_id
                }, function (result) {
                    var html = '<option value="" selected disabled>{{__('visitor.select_model')}}</option>';
                    $.each(result.companies, function (key, value) {
                        html += "<option value=" + value.id + ">" + value.name + "</option>";
                    });
                    $(".v_mode_holder").empty().append(html).val(result.model.id);
                });
            }
        }
    }
</script>

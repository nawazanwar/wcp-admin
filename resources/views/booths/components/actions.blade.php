<div class="row">
    <div class="col-md-12 info-heading-parent clearfix py-1 text-info">
        <p class="info-heading pull-left">{{__('vms.actions')}}</p>
        <p class="info-heading pull-right">{{__('vms.actions_ur')}}</p>
    </div>
    <div class="col-md-3 my-2">
        <div class="icheck-success d-inline">
            <input type="checkbox" id="call_visitee" name="call_visitee" value="0">
            <label for="call_visitee">{{__('visitor.call_visitee')}}</label>
        </div>
    </div>
    <div class="col-md-3 my-2">
        <div class="icheck-success d-inline">
            <input type="checkbox" id="verified" name="verified" value="0">
            <label for="verified">{{__('visitor.verified')}}</label>
        </div>
    </div>
    <div class="col-md-3 my-2">
        <div class="icheck-success d-inline">
            <input type="checkbox" id="follow" name="follow" value="0">
            <label for="follow">{{__('visitor.follow')}}</label>
        </div>
    </div>
    <div class="col-md-3 my-2">
        <div class="icheck-success d-inline">
            <input type="checkbox" id="guide" name="guide" value="0">
            <label for="guide">{{__('visitor.guide')}}</label>
        </div>
    </div>
    <div class="col-md-3 my-2">
        <div class="icheck-success d-inline">
            <input type="checkbox" id="warn" name="warn" value="0">
            <label for="warn">{{__('visitor.warn')}}</label>
        </div>
    </div>
    <div class="col-md-3 my-2">
        <div class="icheck-success d-inline">
            <input type="checkbox" id="permitted" name="permitted" value="0">
            <label for="permitted">{{__('visitor.permitted')}}</label>
        </div>
    </div>
    <div class="col-md-3 my-2">
        <div class="icheck-success d-inline">
            <input type="checkbox" id="exit" name="exit" value="0">
            <label for="exit">{{__('visitor.exit')}}</label>
        </div>
    </div>
</div>

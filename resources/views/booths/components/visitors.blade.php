<div class="row">
    <div class="col-md-12 info-heading-parent clearfix py-1 text-info">
        <p class="info-heading pull-left">{{__('vms.visitor_information')}}</p>
        <p class="info-heading pull-right">{{__('vms.visitor_information_ur')}}</p>
    </div>
    <div class="col-md-4">
        {!! Form::label('visitor_cnic','Visitor Id/CNIC Card') !!}
        {!! Form::text('visitor_cnic',null,['id'=>'visitor_cnic','class'=>'form-control form-control-sm','data-inputmask'=>"'mask': '99999-9999999-9'",'onkeyup'=>'filterByCNICNumber(this)']) !!}
    </div>
    <div class="col-md-4">
        {!! Form::label('visitor_name','Visitor Name') !!}
        {!! Form::text('visitor_name',null,['id'=>'visitor_name','class'=>'form-control form-control-sm']) !!}
    </div>
    <div class="col-md-4">
        {!! Form::label('visitor_cell_no','Visitor Cell Number') !!}
        {!! Form::text('visitor_cell_no',
           null,
           [
               'class'=>'form-control form-control-sm',
               'id'=>'visitor_cell_no',
                'maxlength'=>15,
                'minlength'=>5,
               'onkeypress'=>'return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57'
           ])
       !!}
    </div>
    <div class="col-12 mt-2">
        {!! Form::label('purpose_of_visit','Purpose of Visit') !!}
        {!! Form::textarea('purpose_of_visit',null,['id'=>'purpose_of_visit','class'=>'form-control form-control-sm','rows'=>'3']) !!}
    </div>
    @include('booths.components.visitor-verifications')
</div>

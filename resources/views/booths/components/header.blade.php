<div class="card-header ui-sortable-handle" style="cursor: move;">
    <h3 class="card-title font-weight-bold">
        WAPDA CITY Phase
        01 {{ ucfirst($boothInfo->gate_name) }} {{ ucfirst($boothInfo->booth_lane) }} {{ ucfirst($boothInfo->booth_type) }}
    </h3>
    <div class="card-tools">
        <button class="btn btn-tool">
            <span class="text-success">Welcome</span> {{ $boothInfo->user_name }}
        </button>
        <form method="post" class="btn btn-tool" action="{{ route('booth.logout') }}"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" value="{{$boothInfo->booth_id}}" name="booth_id">
            <button class="bg-danger border-0 text-white" type="submit"><span>Log Out</span>
            </button>
        </form>
    </div>
</div>

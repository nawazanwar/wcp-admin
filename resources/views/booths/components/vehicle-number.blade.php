<div class="row m-auto">
    <div class="col-md-4 form-group mx-auto my-2 mt-3 text-center">
        {!! Form::text('vehicle_reg_no',null,['id'=>'vehicle_reg_no','class'=>'form-control form-control-sm','onkeyup'=>'filterByVehicleOnEnter(this)','placeholder'=>'Enter the Vehicle Number']) !!}
    </div>
</div>

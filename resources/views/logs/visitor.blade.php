@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card card-primary card-tabs">
                <div class="card-body">
                    {{-- Start --}}
                    <div class="timeline">
                        <!-- timeline time label -->
                        @foreach($userLogs as $o_key=>$o_value)
                            <div class="time-label">
                                <span class="bg-red">{{ $o_key }}</span>
                            </div>
                            @foreach($o_value as $i_key=>$i_value)
                                <div>
                                    @php  $user = \App\Models\User::find($i_value->id); @endphp
                                    <div class="timeline-body" style="padding-left: 60px;">
                                        <div class="direct-chat-msg">
                                            <div class="direct-chat-infos clearfix pl-3">
                                                <span class="direct-chat-name float-left">{{ $i_value->name }}</span>
                                                <span
                                                    class="direct-chat-timestamp float-right">{{ Carbon\Carbon::parse($i_value->date_time)->format('h:i:s') }}</span>
                                            </div>
                                            <!-- /.direct-chat-infos -->
                                            <img class="direct-chat-img img-thumbnail img-circle logs-image" src="{{ $user->getAvatar() }}"
                                                 alt="Message User Image">
                                            <!-- /.direct-chat-img -->
                                            <div class="direct-chat-text ml-3">
                                                @if($i_value->type=='in')
                                                    {{ __('message.enter_with') }}
                                                @else
                                                    {{ __('message.exit_with') }}
                                                @endif
                                                <span class="text-success font-weight-bold">
                                                {{ $i_value->ip_address }}
                                            </span>
                                                {{ __('message.and_mac_address_is') }}
                                                <span class="text-danger font-weight-bold">
                                                {{ $i_value->mac_address }}
                                            </span>
                                            </div>
                                            <!-- /.direct-chat-text -->
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                    {{-- End--}}
                </div>
            </div>
        </div>
    </div>
@stop

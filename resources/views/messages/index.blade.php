@extends('layouts.colorbox')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endsection
@section('content')
    {!! Form::open(['route' => ['messages.store'], 'method' => 'POST','files' => true]) !!}
    {!! csrf_field() !!}
    <div class="card card-solid">
        <div class="card-header text-right">
            <a class="btn btn-success btn-nav-add-row btn-sm text-white cursor-pointer"><i
                    class="fa fa-plus"></i></a>
            {!! Form::submit('Update', array('class' => 'btn btn-primary btn-sm')) !!}
        </div>
        <div class="card-body">
            @include('partials.dashboard.message')
            <table class="table table-bordered" id="cTable">
                <tbody>
                @if(count($data) > 0)
                    @foreach($data as $item)
                        <tr class="my-2">
                            <td style="width: 300px;">
                                <input type="hidden" name="id[]" id="id_{{$item->id}}" value="{{$item->id}}">
                                <input type="text" name="name[]" class="form-control" readonly
                                       placeholder="{{__('system.name')}}"
                                       id="name_{{$item->id}}" value="{{$item->name}}" required>
                            </td>
                            <td>
                                <textarea type="text" name="value[]" class="form-control"
                                          placeholder="{{__('system.value')}}"
                                          id="value_{{$item->id}}" required>{{$item->value}}</textarea>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr class="my-2">
                        <td style="width: 300px;">
                            <input type="hidden" name="id[]" id="id_{{ uniqid() }}" value="{{ uniqid() }}">
                            <input type="text" name="name[]" class="form-control"
                                   placeholder="{{__('system.name')}}" required>
                        </td>
                        <td>
                            <textarea type="text" name="value[]" class="form-control"
                                      placeholder="{{__('system.value')}}" required></textarea>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
    {!! Form::close() !!}
@stop
@section('scriptInnerFiles')
@stop
@section('pageScript')
    <script>
        $(function () {
            $(document).on('click', ".btn-nav-add-row", function (event) {
                var uId = 'id' + (new Date()).getTime();
                var cloned_row = $("#cTable").find('tr:first-child').clone();
                cloned_row.find('input[type=hidden]').attr('id', uId).val(uId);
                cloned_row.find('input[type=text]').val('').removeAttr('readonly').attr('placeholder', 'Start Writing').removeAttr('id');
                cloned_row.find('textarea').val('').attr('placeholder', 'Start Writing').removeAttr('id');
                cloned_row.addClass('cloned_row').append(' <a class="btn btn-danger btn-remove-row  btn-sm text-white"><i class="fa fa-minus"></i></a>');
                $("#cTable").prepend(cloned_row);
            });
            $(document).on('click', ".btn-remove-row", function (event) {
                event.preventDefault();
                var rowCount = $('#cTable tr').length;
                if (rowCount > 1) {
                    var $tr = $(this).closest('tr');
                    $tr.remove();
                }
            });
        });
    </script>
@endsection

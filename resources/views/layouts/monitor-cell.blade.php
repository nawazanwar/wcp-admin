@php
    $dir = (app()->getLocale()!='en')?'rtl':'ltr';
@endphp
    <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
<head>
    <meta charset="utf-8">
    {{--<meta http-equiv="refresh" content="15">--}}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('css/vendor.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    @if(isset($shareSettings['favicon']) && $shareSettings['favicon'])
        <link href="{{ asset('uploads/settings/'.$shareSettings['favicon']) }}" rel="icon" type="image/x-icon">
    @endif
    <link rel="stylesheet" href="{{ asset('plugins/colorbox/colorbox.css') }}">
    @yield('styleInnerFiles')
    <title>@yield('pageTitle')</title>
    @stack('style')
</head>
<body class="sidebar-mini" style="height: auto;">
<div class="wrapper" id="app">
    @include('partials.dashboard.header')
    <div class="content" style="min-height: 470px;">
        <div class="container-fluid" dir="{{ $dir }}">
            @yield('content')
        </div>
    </div>
</div>
<style>
    .main-header {
        margin-left: 0 !important;
    }
    .container-fluid{
        width: 90% !important;
    }
</style>
<script src="{{ asset('js/vendor.min.js') }}"></script>
<script src="{{ asset('js/adminlte.js') }}"></script>
<script src="{{ asset('js/demo.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>
<script src="{{ asset('plugins/colorbox/jquery.colorbox.js') }}"></script>
@yield('scriptInnerFiles')
@yield('pageScript')
<style>
    .toasts-top-right {
        position: absolute;
        right: 0;
        z-index: 1040;
    }
</style>
</body>
</html>

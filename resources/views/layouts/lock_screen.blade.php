<!doctype html>
<html class="js-focus-visible">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('css/vendor.min.css') }}" rel="stylesheet">

    <title>@yield('pageTitle')</title>
</head>
<body class="hold-transition lockscreen" cz-shortcut-listen="true">
@yield('content')
</body>
</html>

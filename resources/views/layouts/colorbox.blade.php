@php
$dir = (app()->getLocale()!='en')?'rtl':'ltr';
@endphp
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('css/vendor.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    @if(isset($shareSettings['favicon']) && $shareSettings['favicon'])
    <link href="{{ asset('uploads/settings/'.$shareSettings['favicon']) }}" rel="icon" type="image/x-icon">
    @endif
    @yield('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
    <title>@yield('pageTitle')</title>
    @stack('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<style>
    .wrapper {
        position: relative;
        padding: 15px 7px;
    }
</style>
<div class="wrapper">
    <h3 class="text-center text-info">@yield('pageTitle')</h3>
    <div class="container-fluid" dir="{{ $dir }}">
        @yield('content')
    </div>
</div>
<script src="{{ asset('js/vendor.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('plugins/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
@yield('scriptInnerFiles')
@yield('pageScript')
</body>
</html>

@php
    $dir = (app()->getLocale()!='en')?'rtl':'ltr';
@endphp
    <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ asset('css/vendor.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    @yield('styleInnerFiles')
    <title>@yield('pageTitle')</title>

    @yield('styles')
</head>
<body class="image-box" style="background: url({{ asset('img/auth/bg.png') }}) repeat;">
@yield('content')
@include('partials.loader')
<script src="{{ asset('js/vendor.min.js') }}"></script>
@yield('scriptInnerFiles')
@yield('pageScript')
@stack('innerScript')
</body>
</html>

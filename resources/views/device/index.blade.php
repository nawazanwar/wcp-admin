@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/colorbox/colorbox.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-header row align-items-center mx-1">
                    <div class="card-title col-xl-10 col-lg-10 col-md-10">
                        @can('read',\App\Models\Device::class)
                            {!! HTML::decode(link_to_route('device-type.index',' <i class="fa fa-eye"></i> '.__('device.types'),null,['class'=>'iframe btn bg-gradient-info btn-xs'])) !!}
                        @endcan
                        @can('read',\App\Models\Device::class)
                            {!! HTML::decode(link_to_route('device-make.index',' <i class="fa fa-eye"></i> '.__('device.makes'),null,['class'=>'iframe btn bg-gradient-info btn-xs'])) !!}
                        @endcan
                        @can('read',\App\Models\Device::class)
                            {!! HTML::decode(link_to_route('device-model.index',' <i class="fa fa-eye"></i> '.__('device.models'),null,['class'=>'iframe btn bg-gradient-info btn-xs'])) !!}
                        @endcan
                        @can('read',\App\Models\Device::class)
                            {!! HTML::decode(link_to_route('device-class.index',' <i class="fa fa-eye"></i> '.__('device.classes'),null,['class'=>'iframe btn bg-gradient-info btn-xs'])) !!}
                        @endcan
                        @can('read',\App\Models\Device::class)
                            {!! HTML::decode(link_to_route('device-operatingsystem.index',' <i class="fa fa-eye"></i> '.__('device.operatingsys'),null,['class'=>'iframe btn bg-gradient-info btn-xs'])) !!}
                        @endcan
                    </div>
                    <div class="card-title col-xl-2 col-lg-2 col-md-2 text-right">
                        @can('create',\App\Models\Device::class)
                            {!! HTML::decode(link_to_route('device.create',' <i class="fa fa-plus"></i> '.__('device.create'),null,['class'=>'btn bg-gradient-primary btn-xs'])) !!}
                        @endcan
                    </div>
                </div>
                <div class="card-body">
                    @include('partials.dashboard.message')
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>{{__('device.name')}}</th>
                                <th>{{__('device.name_ur')}}</th>
                                <th>{{__('device.id')}}</th>
                                <th>{{__('device.serial_no')}}</th>
                                <th class="text-center">{{__('device.create_date')}}</th>
                                <th class="text-center">{{__('device.last_modified')}}</th>
                                <th class="text-center">{{__('device.action')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(count($data)>0)
                            @foreach($data as $d)
                                <tr>
                                    <td>{{$d->dev_name}}</td>
                                    <td>{{$d->dev_name_ur}}</td>
                                    <td>{{$d->dev_id}}</td>
                                    <td>{{$d->dev_serial_no}}</td>
                                    <td class="text-center">{{$d->created_at}}</td>
                                    <td class="text-center">{{$d->updated_at->diffForHumans()}}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default">{{__('device.action')}}</button>
                                            <button type="button" class="btn btn-default dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                @can('show',\App\Models\Device::class)
                                                    <li class="dropdown-item">{!! link_to_route('device.show', __('device.show'), [$d->id]) !!}</li>
                                                @endif
                                                @can('edit',\App\Models\Device::class)
                                                    <li class="dropdown-item">{!! link_to_route('device.edit', __('device.edit'), [$d->id]) !!}</li>
                                                @endif
                                                @can('delete',\App\Models\Device::class)
                                                    <form action="{{ route('device.destroy',$d->id) }}"
                                                          method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <li class="dropdown-item">
                                                            <button type="submit" class="bg-transparent"
                                                                    onclick="return confirm('Do you really wants to delete this?')"
                                                                    style="border: none;padding-left: 0;color: #c13535;">
                                                                {{__('device.delete')}}
                                                            </button>
                                                        </li>
                                                    </form>
                                                @endcan
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="text-center">
                                <td colspan="12" class="p-3">No Device Type Found</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix">
                </div>
            </div>
        </div>
    </div>
@stop
@section('scriptInnerFiles')
    <script src="{{ asset('plugins/colorbox/jquery.colorbox.js') }}"></script>
@endsection

@section('pageScript')
    <script>
        $(document).ready(function () {
            $(".iframe").colorbox({iframe: true, width: "80%", height: "80%"});
        });
    </script>
@endsection

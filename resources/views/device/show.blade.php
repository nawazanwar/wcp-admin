@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop
@inject('edBlade','App\Repositories\EdBlade')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 card-header bg-white my-2 border-0">
                @can('read',\App\Models\Device::class)
                    {!! HTML::decode(link_to_route('device.index',' <i class="fa fa-arrow-left"></i> '.__('device.back_device'),null,['class'=>'btn bg-gradient-info btn-sm']))!!}
                @endcan
                <div class="pull-right">
                    <strong>Location: </strong>
                    @isset($model->booth->gate) <span>{{ $model->booth->gate->name }}</span> <strong
                        class="text-info">></strong>@endisset
                    @isset($model->booth->lane) <span>{{ $model->booth->lane }}</span>@endisset
                </div>
            </div>
            <div class="col-md-12">
                <!-- Profile Image -->
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                @foreach($dtype as $d)

                                    @if($d=='CAMERA' OR $d=='Camera' OR $d=='camera')
                                        <h4>Current Video</h4>
                                        <iframe
                                            src="{{__('http://'.$model->dev_ip_address.'/'.$model->dev_connection_string.'101/httpPreview')}}"
                                            frameBorder="0" id="iframVid"
                                            style="width: 100%;height: 506px;"></iframe>
                                @endif
                                @endforeach
                            </div>
                            <div class="col-md-2">
                                <img class="img-fluid img-thumbnail" src="{{ asset('uploads/device/'.$model->dev_pic) }}"
                                     alt="{{$model->name}}">
                            </div>
                            <div class="col-md-4">

                            </div>
                            <div class="col-md-6 text-right">
                                @foreach($dtype as $d)
                                    @if($d=='CAMERA' OR $d=='Camera' OR $d=='camera')
                                        <a class="btn btn-sm btn-primary"
                                           href="{{__('http://'.$model->dev_ip_address.'/'.$model->dev_connection_string.'101/picture')}}"
                                           target="blank">Take Current Picture</a>
                                        <a class="btn btn-sm btn-warning"
                                           href="{{__('http://'.$model->dev_ip_address.'/'.$model->dev_connection_string.'101/httpPreview')}}"
                                           target="_blank">View Video</a>
                                    @endif
                                @endforeach
                            </div>
                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.name')}}</b>
                                <br>
                                <a class="">{{  $model->dev_name  }}</a>
                            </div>
                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.name_ur')}}</b>
                                <br>
                                <a class="">{{  $model->dev_name_ur  }}</a>
                            </div>
                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.id')}}</b>
                                <br>
                                <a class="">{{ $model->dev_id }}</a>
                            </div>


                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.type')}}</b>
                                <br>
                                <a class="">
                                    @php
                                        foreach($dtype as $d){
                                        print_r($d);}
                                    @endphp
                                </a>
                            </div>

                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.make.title')}}</b>
                                <br>
                                <a class="">
                                    @php
                                        foreach($dmake as $d){
                                        print_r($d);}
                                    @endphp
                                </a>
                            </div>
                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.model.title')}}</b>
                                <br>
                                <a class="">
                                    @php
                                        foreach($dmodel as $d){
                                        print_r($d);}
                                    @endphp
                                </a>
                            </div>

                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.class')}}</b>
                                <br>
                                <a class="">
                                    @php
                                        foreach($dclass as $d){
                                        print_r($d);}
                                    @endphp
                                </a>
                            </div>
                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.operatingsystem')}}</b>
                                <br>
                                <a class="">
                                    @php
                                        foreach($doperatingsys as $d){
                                        print_r($d);}
                                    @endphp
                                </a>
                            </div>
                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.location.title')}}</b>
                                <br>
                                <a class="">
                                    @php
                                        foreach($dlocation as $d){
                                        print_r($d);}
                                    @endphp
                                </a>
                            </div>

                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.serial_no')}}</b>
                                <br>
                                <a class="">{{ $model->dev_serial_no }}</a>
                            </div>
                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.ip_address')}}</b>
                                <br>
                                <a class="">{{ $model->dev_ip_address }}</a>
                            </div>
                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.mac_address')}}</b>
                                <br>
                                <a class="">{{ $model->dev_mac_address }}</a>
                            </div>

                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.admin_login')}}</b>
                                <br>
                                <a class="">{{  $model->dev_admin_login  }}</a>
                            </div>
                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.admin_pwd')}}</b>
                                <br>
                                <a class="">{{  $model->dev_admin_pwd  }}</a>
                            </div>
                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.user_login')}}</b>
                                <br>
                                <a class="">{{  $model->dev_user_login  }}</a>
                            </div>

                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.login_pwd')}}</b>
                                <br>
                                <a class="">{{  $model->dev_user_pwd  }}</a>
                            </div>
                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.lot_no')}}</b>
                                <br>
                                <a class="">{{  $model->dev_lot_no  }}</a>
                            </div>
                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.lot_date')}}</b>
                                <br>
                                <a class="">{{  $model->dev_lot_date  }}</a>
                            </div>

                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.other_info')}}</b>
                                <br>
                                <a class="">{{  $model->dev_other_info  }}</a>
                            </div>
                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.connection_string')}}</b>
                                <br>
                                <a class="">{{  $model->dev_connection_string  }}</a>
                            </div>
                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.create_date')}}</b>
                                <br>
                                <a class="">{{  $model->created_at  }}</a>
                            </div>

                            <div class="col-md-4 mt-3 text-center">
                                <b>{{__('device.update')}}</b>
                                <br>
                                <a class="">{{  $model->updated_at  }}</a>
                            </div>
                        </div>

                    </div>
                </div>
                {{-- Extra Information--}}
                <
            </div>
            <!-- /.col -->

        </div>
    </div>
@stop

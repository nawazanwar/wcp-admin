@extends('layouts.colorbox')
@section('pageTitle', $pageTitle)
@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-header row align-items-center mx-1">
                    <div class="card-title col-xl-6 col-lg-6 col-md-6">
                        @can('create',\App\Models\Device::class)
                            {!! link_to_route('device-operatingsystem.create',__('system.create'),null,['class'=>'btn bg-gradient-primary btn-sm']) !!}
                        @endcan
                    </div>
                </div>
                <div class="card-body">
                    @include('partials.dashboard.message')
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{__('device.name')}}</th>
                            <th>{{__('device.name_ur')}}</th>
                            <th class="text-center">{{__('device.create_date')}}</th>
                            <th class="text-center">{{__('device.last_modified')}}</th>
                            <th class="text-center">{{__('device.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($data)>0)
                            @foreach($data as $d)
                                <tr>
                                    <td>{{$d->name}}</td>
                                    <td>{{$d->name_ur}}</td>
                                    <td class="text-center">{{$d->created_at}}</td>
                                    <td class="text-center">{{$d->updated_at->diffForHumans()}}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default">{{__('device.action')}}</button>
                                            <button type="button" class="btn btn-default dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                @can('edit',\App\Models\Device::class)
                                                    <li class="dropdown-item">{!! link_to_route('device-operatingsystem.edit', __('device.edit'), [$d->id]) !!}</li>
                                                @endif
                                                @can('delete',\App\Models\Device::class)
                                                    <form action="{{ route('device-operatingsystem.destroy',$d->id) }}"
                                                          method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <li class="dropdown-item">
                                                            <button type="submit" class="bg-transparent"
                                                                    onclick="return confirm('Do you really wants to delete this?')"
                                                                    style="border: none;padding-left: 0;color: #c13535;">
                                                                {{__('device.delete')}}
                                                            </button>
                                                        </li>
                                                    </form>
                                                @endcan
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="text-center">
                                <td colspan="12" class="p-3">No Device Operating System Found</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix">
                    {{ $data->links() }}
                </div>
            </div>
        </div>
    </div>
@stop

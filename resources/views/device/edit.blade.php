@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-header row align-items-center mx-1">
                    <div class="card-title col-xl-6 col-lg-6 col-md-6">
                        @can('read',\App\Models\Device::class)
                            {!! HTML::decode(link_to_route('device.index',' <i class="fa fa-arrow-left"></i> '.__('device.back_device'),null,['class'=>'btn bg-gradient-info btn-sm']))!!}
                        @endcan
                    </div>
                </div>
                <div class="card-body">

                    {!! Form::model($model, ['route' => ['device.update',$model->id], 'method' => 'PUT','files'=>'true']) !!}
                    {!! csrf_field() !!}
                    <div class="row">
                        @php  $dTypes = \App\Device\DType::all();  @endphp
                        <div class=" col-md-3 form-group">
                            <label for="dtype_id">{{__('device.type')}}</label>
                            <select
                                data-size='10'
                                data-actions-box='true'
                                data-selected-text-format="count > 3"
                                data-live-search="true"
                                class="form-control  selectpicker show-menu-arrow"
                                name="dtype_id" id="dtype_id">
                                @if(count($dTypes)>0)
                                    @foreach($dTypes as $dt)
                                        <option value="{{$dt->id}}">
                                            <span>{{$dt->name}}</span>
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        @php  $dMakes = \App\Device\DMake::all();  @endphp
                        <div class=" col-md-3 form-group">
                            <label for="dmake_id">{{__('device.makes')}}</label>
                            <select
                                data-size='10'
                                data-actions-box='true'
                                data-selected-text-format="count > 3"
                                data-live-search="true"
                                class="form-control  selectpicker show-menu-arrow"
                                name="dmake_id" id="dmake_id">
                                @if(count($dMakes)>0)
                                    @foreach($dMakes as $dm)
                                        <option value="{{$dm->id}}">
                                            <span>{{$dm->name}}</span>
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        @php  $dModels = \App\Device\DModel::all();  @endphp
                        <div class=" col-md-3 form-group">
                            <label for="dmodel_id">{{__('device.models')}}</label>
                            <select
                                data-size='10'
                                data-actions-box='true'
                                data-selected-text-format="count > 3"
                                data-live-search="true"
                                class="form-control  selectpicker show-menu-arrow"
                                name="dmodel_id" id="dmodel_id">
                                @if(count($dModels)>0)
                                    @foreach($dModels as $dm)
                                        <option value="{{$dm->id}}">
                                            <span>{{$dm->name}}</span>
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        @php  $dClasses = \App\Device\DClass::all();  @endphp
                        <div class=" col-md-3 form-group">
                            <label for="dclass_id">{{__('device.class')}}</label>
                            <select
                                data-size='10'
                                data-actions-box='true'
                                data-selected-text-format="count > 3"
                                data-live-search="true"
                                class="form-control  selectpicker show-menu-arrow"
                                name="dclass_id" id="dclass_id">
                                @if(count($dClasses)>0)
                                    @foreach($dClasses as $dc)
                                        <option value="{{$dc->id}}">
                                            <span>{{$dc->name}}</span>
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        @php  $dOperationSystem = \App\Device\DOperatingSystem::all();  @endphp
                        <div class=" col-md-3 form-group">
                            <label for="doperatingsystem_id">{{__('device.operatingsys')}}</label>
                            <select
                                data-size='10'
                                data-actions-box='true'
                                data-selected-text-format="count > 3"
                                data-live-search="true"
                                class="form-control  selectpicker show-menu-arrow"
                                name="doperatingsystem_id" id="doperatingsystem_id">
                                @if(count($dOperationSystem)>0)
                                    @foreach($dOperationSystem as $do)
                                        <option value="{{$do->id}}">
                                            <span>{{$do->name}}</span>
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class=" col-md-3 form-group">
                            <label for="booth_id">Booth</label>
                            {!! Form::select('booth_id',WcpHelper::allBoothLists(),$model->boot_id,['class'=>'form-control']) !!}
                        </div>
                        {{--general detail with other info--}}
                        <div class="row align-items-center">
                            {{--left Section--}}
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        {!! Form::label('dev_name', __('device.name')) !!}
                                        {!! Form::text('dev_name', old('dev_name'), ['class' => 'form-control ','id' => 'dev_name' ]) !!}
                                        @error('dev_name')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4 text-right">
                                        {!! Form::label('dev_name_ur', __('device.dev_name_ur')) !!}
                                        {!! Form::text('dev_name_ur', old('dev_name_ur'), ['class' => 'form-control ','dir'=>'rtl','id' => 'dev_name_ur' ]) !!}
                                        @error('dev_name_ur')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        {!! Form::label('dev_id', __('device.id')) !!}
                                        {!! Form::text('dev_id', null, ['class' => 'form-control ','id' => 'dev_id' ]) !!}
                                        @error('dev_id')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        {!! Form::label('dev_serial_no', __('device.serial_no')) !!}
                                        {!! Form::text('dev_serial_no', old('dev_serial_no'), ['class' => 'form-control ','id' => 'dev_serial_no' ]) !!}
                                        @error('dev_serial_no')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group col-md-4">
                                        {!! Form::label('dev_ip_address', __('device.ip_address')) !!}
                                        {!! Form::text('dev_ip_address',null, ['class' => 'form-control ','id' => 'dev_ip_address' ]) !!}
                                        @error('dev_ip_address')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group col-md-4">
                                        @php
                                            $mac_address = strtok(exec('getmac'), ' ');
                                        @endphp
                                        {!! Form::label('dev_mac_address', __('device.mac_address')) !!}
                                        {!! Form::text('dev_mac_address',$mac_address, ['class' => 'form-control ','id' => 'dev_mac_address' ]) !!}
                                        @error('dev_mac_address')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group col-md-4">
                                        {!! Form::label('dev_admin_login', __('device.admin_login')) !!}
                                        {!! Form::text('dev_admin_login', old('dev_admin_login'), ['class' => 'form-control ','id' => 'dev_admin_login' ]) !!}
                                        @error('dev_admin_login')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>


                                    <div class="form-group col-md-4">
                                        {!! Form::label('dev_admin_pwd', __('device.admin_pwd')) !!}
                                        {!! Form::text('dev_admin_pwd', old('dev_admin_pwd'), ['class' => 'form-control ','id' => 'dev_admin_pwd' ]) !!}
                                        @error('dev_admin_pwd')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>


                                    <div class="form-group col-md-4">
                                        {!! Form::label('dev_user_login', __('device.user_login')) !!}
                                        {!! Form::text('dev_user_login', old('dev_user_login'), ['class' => 'form-control ','id' => 'dev_user_login' ]) !!}
                                        @error('dev_user_login')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>


                                    <div class="form-group col-md-4">
                                        {!! Form::label('dev_user_pwd', __('device.login_pwd')) !!}
                                        {!! Form::text('dev_user_pwd', old('dev_user_pwd'), ['class' => 'form-control ','id' => 'dev_user_pwd' ]) !!}
                                        @error('dev_user_pwd')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group col-md-4">
                                        {!! Form::label('dev_lot_no', __('device.lot_no')) !!}
                                        {!! Form::text('dev_lot_no', old('dev_lot_no'), ['class' => 'form-control ','id' => 'dev_lot_no' ]) !!}
                                        @error('dev_lot_no')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group col-md-4">
                                        {!! Form::label('dev_lot_date', __('device.lot_date')) !!}
                                        {!! Form::date('dev_lot_date', old('dev_lot_date'), ['class' => 'form-control ','id' => 'dev_lot_date' ]) !!}
                                        @error('dev_lot_date')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group col-md-4">
                                        {!! Form::label('dev_other_info', __('device.other_info')) !!}
                                        {!! Form::text('dev_other_info', old('dev_other_info'), ['class' => 'form-control ','id' => 'dev_other_info' ]) !!}
                                        @error('dev_other_info')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>


                                    <div class="form-group col-md-4">
                                        {!! Form::label('dev_connection_string', __('device.connection_string')) !!}
                                        {!! Form::text('dev_connection_string', old('dev_connection_string'), ['class' => 'form-control ','id' => 'dev_connection_string' ]) !!}
                                        @error('dev_connection_string')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>


                                    <div class="form-group col-md-4">
                                        <label for="picture['dev_pic']">{{ __('device.image') }}</label>
                                        <div class="input-group input-group-sm">
                                            <div class="custom-file">
                                                <input type="file" name="dev_pic"
                                                       class="custom-file-input form-control-sm"
                                                       id="picture['dev_pic']"
                                                       onchange="Custom.previewImage(this,'picture_first')">
                                                <label class="custom-file-label"
                                                       for="picture['back']">{{ __('system.choose_file') }}</label>
                                            </div>
                                        </div>
                                        <div class="picture_first text-center my-3">
                                            <img src="{{ asset('uploads/device/'.$model->dev_pic) }}"
                                                 class="img-thumbnail h-150">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="card-footer clearfix text-right">
                            <button type="submit" class="btn btn-info">{{ __('device.update') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('scriptInnerFiles')
    <script type="text/javascript"
            src="{{ asset('plugins/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
@stop
@section('pageScript')
@endsection

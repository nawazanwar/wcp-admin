@extends('layouts.colorbox')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endsection

@section('content')
    <div class="card card-solid">
        @can('read',\App\Models\Device::class)
            <div class="card-header">
                <div class="card-title">
                    {!! link_to_route('device-make.index',__('device.makes'),null,['class'=>'btn btn-info btn-sm']) !!}
                </div>
            </div>
        @endcan
        <div class="card-body pb-0">
            {!! Form::open(['route' => ['device-make.store'], 'method' => 'POST','files' => true]) !!}
            {!! csrf_field() !!}
            <div class="row">
              <div class="form-group col-md-4">
                    {!! Form::label('inputName', __('device.name')) !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'autofocus', 'id' => 'inputName' ]) !!}
                    @error('name')
                    <div class="error">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-md-4 form-group text-right">
                    {!! Form::label('inputNameUR',__('device.name_ur'),['class'=>'pr-2']) !!}
                    {!! Form::text('name_ur', old('name_ur'), ['class' => 'form-control urduWriter ur','dir'=>'rtl','id' => 'inputNameUR' ]) !!}
                    @error('name_ur')
                    <div class="error">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group text-right col-12">
                    {!! Form::submit(__('lrm.save'), array('class' => 'btn btn-primary btn-sm')) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('scriptInnerFiles')
    <script src="{{ asset('plugins/keybaord/urdu.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('plugins/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
@stop


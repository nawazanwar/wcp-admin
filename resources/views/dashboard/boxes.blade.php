<div class="row">
    {{-- HRM --}}
    <div class="col-md-4">
        <div class="card card-widget widget-user">
            <div class="widget-user-header bg-gradient-success">
                <h4 class="widget-user-username ">{{__('hrm.heading')}}</h4>
            </div>
            <div class="widget-user-image">
                <img class="img-circle elevation-2" src="{{ asset('img/dashboard/hrm.png') }}" alt="HRM">
            </div>
            <div class="card-footer my-2">
                <div class="row ">
                    <div class="col-12 text-right">
                        @can('read',\App\Models\HRM::class)
                            <a href="{{ route('hrm.index') }}"
                               class="btn bg-gradient-success btn-sm">{{ __('system.manage') }} <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- VMS --}}
    <div class="col-md-4">
        <div class="card card-widget widget-user">
            <div class="widget-user-header bg-info">
                <h4 class="widget-user-username ">{{__('dashboard.visitor_management_system')}}</h4>
            </div>
            <div class="widget-user-image">
                <img class="img-circle elevation-2" src="{{ asset('img/dashboard/vms.jpg') }}" alt="VMS">
            </div>
            <div class="card-footer my-2">
                <div class="row ">
                    <div class="col-12 text-right">
                        @can('read',\App\Models\VMS::class)
                            <a href="{{ route('vms.index') }}"
                               class="btn bg-gradient-success btn-sm">{{ __('system.manage') }} <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- VRS --}}
    <div class="col-md-4">
        <div class="card card-widget widget-user">
            <div class="widget-user-header bg-gradient-danger">
                <h4 class="widget-user-username ">{{__('dashboard.vehicle_registration_system')}}</h4>
            </div>
            <div class="widget-user-image">
                <img class="img-circle elevation-2" src="{{ asset('img/dashboard/vehicle.png') }}" alt="VRS">
            </div>
            <div class="card-footer my-2">
                <div class="row ">
                    <div class="col-12 text-right">
                        @can('read',\App\Models\VRS::class)
                            <a href="{{ route('vrs.index') }}"
                               class="btn bg-gradient-success btn-sm">{{ __('system.manage') }} <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--LRM--}}
    <div class="col-md-4">
        <div class="card card-widget widget-user">
            <div class="widget-user-header bg-gradient-info">
                <h4 class="widget-user-username ">{{__('dashboard.land_record_management_system')}}</h4>
            </div>
            <div class="widget-user-image">
                <img class="img-circle elevation-2" src="{{ asset('img/dashboard/lrm.png') }}" alt="LRM">
            </div>
            <div class="card-footer my-2">
                <div class="row ">
                    <div class="col-12 text-right">
                        @can('read',\App\Models\LRM::class)
                            <a href="{{ route('lrm.index') }}"
                               class="btn bg-gradient-success btn-sm">{{ __('system.manage') }} <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--Device--}}
    <div class="col-md-4">
        <div class="card card-widget widget-user">
            <div class="widget-user-header bg-gradient-info">
                <h4 class="widget-user-username ">{{__('dashboard.device_management_system')}}</h4>
            </div>
            <div class="widget-user-image">
                <img class="img-circle elevation-2" src="{{ asset('img/dashboard/device.png') }}" alt="LRM">
            </div>
            <div class="card-footer my-2">
                <div class="row ">
                    <div class="col-12 text-right">
                        @can('read',\App\Models\Device::class)
                            <a href="{{ route('device.index') }}"
                               class="btn bg-gradient-success btn-sm">{{ __('system.manage') }} <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

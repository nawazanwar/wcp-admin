<div class="row m-auto">
    <div class="col-md-4 form-group mx-auto my-2 mt-3 text-center">
        {!! Form::label('vehicle_reg_no','Vehicle Registration Number') !!}
        {!! Form::text('vehicle_reg_no',null,['class'=>'form-control form-control-sm','id'=>'vehicle_reg_no']) !!}
    </div>
</div>
{{-- Vehicle Info--}}
<div class="row">
    <div class="col-md-12 info-heading-parent clearfix py-1 text-info">
        <p class="info-heading pull-left">{{__('vms.vehicle_information')}}</p>
        <p class="info-heading pull-right">{{__('vms.vehicle_information_ur')}}</p>
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('type_id','Type') !!}
        @php
            $types = \App\Models\Type::whereParent('vehicle')->pluck('name','id');
        @endphp
        <div class="input-group input-group-sm">
            {!! Form::select('type_id',$types,null,['class'=>'v_type_holder form-control form-control-sm','onchange'=>'getCompaniesANdNatureOfMatierials(this);']) !!}
            <span class="input-group-append">
                        <button type="button" class="btn btn-info btn-flat"
                                onclick="gen_vehicle_type('v_type_holder');"><span
                                class="fa fa-plus-circle"></span></button>
                  </span>
        </div>
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('vcompany_id','Make/Company') !!}
        <div class="input-group input-group-sm">
            {!! Form::select('vcompany_id',$v_companies,null,['class'=>'v_company_holder form-control form-control-sm','onchange'=>'getCompanyModels(this);']) !!}
            <span class="input-group-append">
                        <button type="button" class="btn btn-info btn-flat"
                                onclick="gen_vehicle_company('v_company_holder');"><span
                                class="fa fa-plus-circle"></span></button>
                  </span>
        </div>
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('vmodel_id','Model') !!}
        <div class="input-group input-group-sm">
            {!! Form::select('vmodel_id',$v_models,null,['class'=>'v_mode_holder form-control form-control-sm','onchange'=>'getCompanyModels(this);']) !!}
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="gen_vehicle_model('v_mode_holder');"><span
                        class="fa fa-plus-circle"></span></button>
            </span>
        </div>
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('vehicle_color','Color') !!}
        {!! Form::hidden('vehicle_color',null,['id'=>'vehicle_color']) !!}
        <input class="form-control form-control-sm" id="input_color_picker" type="color">
    </div>
    <div class="col-md-4 form-group">
        {!! Form::label('vnature_id','Nature of Materials') !!}
        <div class="input-group input-group-sm">
            {!! Form::select('vnature_id',$v_natures,null,['class'=>'v_vehicle_nature_of_material_holder form-control form-control-sm','onchange'=>'getVehicleNatureOfMaterialsModels(this);']) !!}
            <span class="input-group-append">
                        <button type="button" class="btn btn-info btn-flat"
                                onclick="gen_vehicle_nature_of_material('v_vehicle_nature_of_material_holder');"><span
                                class="fa fa-plus-circle"></span></button>
                  </span>
        </div>
    </div>
    <div class="col-md-4 form-group">
        @php
            $years = array_combine(range(date("Y"), 1910), range(date("Y"), 1910));
        @endphp
        {!! Form::label('vehicle_reg_year','Vehicle Registration Year') !!}
        <select
            class="form-control form-control-sm"
            id="vehicle_reg_year"
            name="vehicle_reg_year">
            <option value="" selected disabled>Select Vehicle Registration Year</option>
            @foreach($years as $key=>$value)
                @php
                    $select = ($visitor->vehicle_reg_year==$key)?'selected':'';
                @endphp
                <option {{ $select }} value="{{ $key }}">{{ $value }}</option>
            @endforeach
        </select>
    </div>
</div>
{{-- Visitor Information--}}
<div class="row">
    <div class="col-md-12 info-heading-parent clearfix py-1 text-info">
        <p class="info-heading pull-left">{{__('vms.visitor_information')}}</p>
        <p class="info-heading pull-right">{{__('vms.visitor_information_ur')}}</p>
    </div>
    <div class="col-md-4">
        {!! Form::label('visitor_name','Visitor Name') !!}
        {!! Form::text('visitor_name',null,['class'=>'form-control form-control-sm']) !!}
    </div>
    <div class="col-md-4">
        {!! Form::label('visitor_cnic','Visitor CNIC') !!}

        {!! Form::text('visitor_cnic',null,['class'=>'form-control form-control-sm']) !!}
    </div>
    <div class="col-md-4">
        {!! Form::label('visitor_cell_no','Visitor Cell Number') !!}
        {!! Form::text('visitor_cell_no',
           null,
           [
               'class'=>'form-control form-control-sm',
               'id'=>'visitor_cell_no',
                'maxlength'=>15,
                'minlength'=>5,
               'onkeypress'=>'return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57'
           ])
       !!}
    </div>
    <div class="col-12">
        {!! Form::label('purpose_of_visit','Purpose of Visit') !!}
        {!! Form::textarea('purpose_of_visit',null,['class'=>'form-control form-control-sm']) !!}
    </div>
</div>
<div class="row">
    <div class="col-md-2 my-3">
        <div class="icheck-success d-inline">
            {!! Form::checkbox('visitor_biometric_verification',$visitor->visitor_biometric_verification?1:0,null,['id'=>'visitor_biometric_verification']) !!}
            {!! Form::label('visitor_biometric_verification',__('visitor.visitor_biometric_verification')) !!}
        </div>
    </div>
    <div class="col-md-2 my-3">
        <div class="icheck-success d-inline">
            {!! Form::checkbox('visitor_ratina_verification',$visitor->visitor_ratina_verification?1:0,null,['id'=>'visitor_ratina_verification']) !!}
            {!! Form::label('visitor_ratina_verification',__('visitor.visitor_ratina_verification')) !!}
        </div>
    </div>
    <div class="col-md-2 my-3">
        <div class="icheck-success d-inline">
            {!! Form::checkbox('visitor_gate_pass_verification',$visitor->visitor_gate_pass_verification?1:0,null,['id'=>'visitor_gate_pass_verification']) !!}
            {!! Form::label('visitor_gate_pass_verification',__('visitor.visitor_gate_pass_verification')) !!}
        </div>
    </div>
    <div class="col-md-2 my-3">
        <div class="icheck-success d-inline">
            {!! Form::checkbox('visitor_qrcode_verification',$visitor->visitor_qrcode_verification?1:0,null,['id'=>'visitor_qrcode_verification']) !!}
            {!! Form::label('visitor_qrcode_verification',__('visitor.visitor_qrcode_verification')) !!}
        </div>
    </div>
</div>


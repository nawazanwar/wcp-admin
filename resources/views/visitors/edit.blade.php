@extends('layouts.monitor-cell')
@section('pageTitle', $pageTitle)
@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.css') }}">
    <link rel='stylesheet' href="{{ asset('plugins/colorpicker/spectrum.css') }}"/>
@endsection
@section('content')
    <div class="row my-3">
        <div class="col-md-6">
            <a class="btn btn-xs btn-info text-white cursor-pointer" href="{{ route('dashboard') }}"> <i
                    class="fa fa-arrow-circle-left"></i> Back to Dashboard </a>
        </div>
    </div>
    <div class="row">
        @if ($message = Session::get('success'))
            <div class="col-12">
                <div class="alert alert-success fade in alert-dismissible show">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true" style="font-size:20px">×</span>
                    </button>
                    <strong>Success!</strong> {{ $message }}
                </div>
            </div>
        @endif
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{ $pageTitle }}</h3>
                </div>
                {!! Form::model($visitor, array('route' => array('visitors.update', $visitor->id))) !!}
                @csrf
                @method('PUT')
                {!! Form::hidden('updated_by',auth()->user()->id) !!}
                <div class="card-body">
                    @include('visitors.form')
                </div>
                <div class="card-footer text-right">
                    {!! Form::submit('Update',['class'=>'btn btn-info']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('scriptInnerFiles')
    <script src="{{ asset('plugins/colorpicker/spectrum.js') }}"></script>
@endsection
@include('booths.components.scripts')

@section('pageScript')
    <script>
        $(document).ready(function () {
            $("#input_color_picker").spectrum({
                showPalette: true,
                showInput: true,
                showAlpha: true,
                showInitial: false,
                preferredFormat: "name",
                color: "transparent",
                palette: [
                    ["#000", "#444", "#666", "#999", "#ccc", "#eee", "#f3f3f3", "#fff"],
                    ["#f00", "#f90", "#ff0", "#0f0", "#0ff", "#00f", "#90f", "#f0f"],
                    ["#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d0e0e3", "#cfe2f3", "#d9d2e9", "#ead1dc"],
                    ["#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#9fc5e8", "#b4a7d6", "#d5a6bd"],
                    ["#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6fa8dc", "#8e7cc3", "#c27ba0"],
                    ["#c00", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3d85c6", "#674ea7", "#a64d79"],
                    ["#900", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#0b5394", "#351c75", "#741b47"],
                    ["#600", "#783f04", "#7f6000", "#274e13", "#0c343d", "#073763", "#20124d", "#4c1130"]
                ],
                change: function (color) {
                    $("#vehicle_color").val(color);
                }
            });

            $("#input_color_picker").spectrum("set", "{{ $visitor->vehicle_color }}");
        })
    </script>
@endsection

@extends('layouts.colorbox')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endsection
@section('content')
    <div class="card card-solid">
        <div class="card-body pb-0">
            {!! Form::open(['route' => ['hrm-card-types.store'], 'method' => 'POST','files' => true]) !!}
            {!! csrf_field() !!}
            @include('partials.dashboard.message')
            <table class="table table-bordered" id="cTable">
                <tbody>
                @if(count($data) > 0)
                    <?php $index = 0; ?>
                    @foreach($data as $item)
                        <tr>
                            <td>
                                <input type="hidden" name="id[]" id="id_{{$item->id}}" value="{{$item->id}}">
                                <input type="text" name="name[]" class="form-control"
                                       placeholder="{{__('system.name')}}"
                                       id="name_{{$item->id}}" value="{{$item->name}}" required>
                            </td>
                            <td class="text-center">
                                <a class="btn btn-primary btn-nav-add-row btn-sm text-white"><i class="fa fa-plus"></i></a>
                                <a class="btn btn-danger btn-nav-remove-row  btn-sm text-white"><i
                                        class="fa fa-minus"></i></a>
                            </td>
                        </tr>
                        <?php $index++; ?>
                    @endforeach
                @else
                    <tr>
                        <td>
                            <input type="hidden" name="id[]" id="id_0">
                            <input type="text" name="name[]" class="form-control" placeholder="{{__('system.name')}}"
                                   id="name_0" required>
                        </td>
                        <td class="text-center">
                            <a class="btn btn-primary btn-nav-add-row btn-sm text-white"><i class="fa fa-plus"></i></a>
                            <a class="btn btn-danger btn-nav-remove-row  btn-sm text-white"><i class="fa fa-minus"></i></a>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
            <div class="form-group text-right">
                {!! Form::submit('Update', array('class' => 'btn btn-primary btn-sm')) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('scriptInnerFiles')
@stop
@section('pageScript')
    <script>
        $(function () {
            $(document).on('click', ".btn-nav-add-row", function (event) {
                var uId = 'id' + (new Date()).getTime();
                event.preventDefault();
                var $tr = $(this).closest('tr');
                var $clone = $tr.clone();
                $clone.find('input').val('').attr('id', uId);
                $tr.after($clone);
            });
            $(document).on('click', ".btn-nav-remove-row", function (event) {
                event.preventDefault();
                var rowCount = $('#cTable tr').length;
                if (rowCount > 1) {
                    var $tr = $(this).closest('tr');
                    $tr.remove();
                }
            });
        });
    </script>
@endsection

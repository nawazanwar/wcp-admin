@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/colorbox/colorbox.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-header row align-items-center mx-1">
                    <div class="card-title col-xl-10 col-lg-10 col-md-10">
                        @can('read',\App\Models\Designation::class)
                            {!! HTML::decode(link_to_route('hrm-designations.index',' <i class="fa fa-eye"></i> '.__('hrm.designations'),null,['class'=>'iframe btn bg-gradient-success btn-sm'])) !!}
                        @endcan
                        @can('read',\App\Models\Department::class)
                            {!! HTML::decode(link_to_route('hrm-departments.index',' <i class="fa fa-eye"></i> '.__('hrm.departments'),null,['class'=>'iframe btn bg-gradient-info btn-sm'])) !!}
                        @endcan
                        @can('read',\App\Models\Employer::class)
                            {!! HTML::decode(link_to_route('hrm-employers.index',' <i class="fa fa-eye"></i> '.__('hrm.employers'),null,['class'=>'iframe btn bg-gradient-dark btn-sm'])) !!}
                        @endcan
                        @can('read',\App\Models\Type::class)
                            {!! HTML::decode(link_to_route('hrm-emp-types.index',' <i class="fa fa-eye"></i> '.__('hrm.employee_types'),null,['class'=>'iframe btn bg-gradient-danger btn-sm'])) !!}
                        @endcan
                        @can('read',\App\Models\Type::class)
                            {!! HTML::decode(link_to_route('hrm-card-types.index',' <i class="fa fa-eye"></i> '.__('hrm.cards_types'),null,['class'=>'iframe btn bg-gradient-primary btn-sm'])) !!}
                        @endcan
                        @can('read',\App\Models\Type::class)
                            {!! HTML::decode(link_to_route('hrm-relations.index',' <i class="fa fa-eye"></i> '.__('hrm.relations'),null,['class'=>'iframe btn bg-gradient-success btn-sm'])) !!}
                        @endcan
                    </div>
                    <div class="card-title col-xl-2 col-lg-2 col-md-2 text-right">
                        @can('create',\App\Models\HRM::class)
                            {!! HTML::decode(link_to_route('hrm.create',' <i class="fa fa-plus"></i> '.__('system.create'),null,['class'=>'btn bg-gradient-success btn-sm'])) !!}
                        @endcan
                    </div>
                </div>
                <div class="card-body">
                    <div class="row d-flex align-items-stretch">
                        @forelse ($data as $d)
                            <div class="col-12 col-sm-6 col-md-4">
                                <div class="card bg-light collapsed-card">
                                    <div class="card-header border-top border-bottom row align-items-center">
                                        <div class="col-3 text-left">
                                            @if($d->user->avatar)
                                                <img src="{{ asset('uploads/hrm/avatar/'.$d->user->avatar) }}"
                                                     onerror="this.src='{{asset("img/defaults/user.png")}}'"
                                                     class="avatar avatar-lg">
                                            @else
                                                <img src="{{ asset('img/defaults/user.png') }}"
                                                     class="avatar avatar-lg">
                                            @endif
                                        </div>
                                        <div class="text-right col-9">
                                            <form action="{{ route('hrm.destroy',$d->id) }}" method="POST">
                                                {{--<a class="btn btn-success btn-xs pull-right mx-1" target="_blank"
                                                   href="{{ route('hrm.show',$d->id) }}">
                                                    <i class="fa fa-eye"></i>
                                                </a>--}}
                                                @if($d->user->active)
                                                    <a class="btn btn-xs bg-gradient-success pull-right text-white mx-1">
                                                        Active
                                                    </a>
                                                @else
                                                    <a class="btn btn-xs bg-gradient-danger pull-right text-white mx-1">
                                                        In Active
                                                    </a>
                                                @endif
                                                @if($d->user->block)
                                                    <a class="btn btn-xs bg-gradient-danger pull-right text-white mx-1">
                                                        Block
                                                    </a>
                                                @endif

                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" onclick="return confirm('Are you sure?')"
                                                        class="btn bg-gradient-danger btn-xs pull-right  mx-1">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                                <a class="btn bg-gradient-info btn-xs pull-right mx-1" target="_blank"
                                                   href="{{ route('hrm.edit',$d->id) }}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="card-header text-center bg-white" style="border-bottom: none;">
                                        <p class="text-info small">Below Unique Id For Login to system so initial
                                            password is
                                            @if($d->user->name=='Owner')
                                                <strong class="text-danger">owner123</strong>
                                            @else
                                                <strong class="text-danger">user1234</strong>
                                            @endif
                                        </p>
                                        <p class="text-center text-danger small border-top border-bottom py-2">{{ isset($d->user->unique_id)?$d->user->unique_id:'?' }}</p>
                                        <div class="small font-weight-normal">
                                            {{ Carbon\Carbon::parse($d->created_at)->format('d M y / l') }}
                                            <hr class="bg-light my-1 w-50">
                                            <div>
                                                {{ Carbon\Carbon::parse($d->created_at)->format('h:i:s A') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-header bg-white br-0 pb-1 pt-0">
                                        <ul class="list-group">
                                            <li class="list-group-item py-1">
                                                <span class="fa fa-user text-primary"></span>
                                                <span
                                                    class="pull-right font-weight-normal small">{{ isset($d->user->name)?$d->user->name:'?' }}</span>
                                            </li>
                                            <li class="list-group-item py-1">
                                                <span class="fa fa-id-card text-primary"></span>
                                                <span
                                                    class="pull-right font-weight-normal small">{{ isset($d->cnic)?$d->cnic:'?' }}</span>
                                            </li>
                                            <li class="list-group-item py-1">
                                                <span class="text-primary fa fa-user-friends"></span>
                                                @if($d->user->designations()->count()>0)
                                                    {!! Form::select('designations',$d->user->designations()->pluck('name','id'),null,['class'=>'form-control pull-right form-control-sm','style'=>'width:51%;border: 1px solid #9e9e9e1f;']) !!}
                                                @endif
                                            </li>
                                        </ul>
                                        <div class="text-center">
                                            <button type="button" class="btn btn-tool btn-view-detail"
                                                    data-card-widget="collapse">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="card-body px-0 bg-white py-0">
                                        @include('hrm.listening.tabs')
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="p-3 col-12 text-center font-weight-bold">
                                No User Found....
                            </div>
                        @endforelse
                    </div>
                </div>
                <div class="card-footer clearfix">
                </div>
            </div>
        </div>
    </div>
@stop
@section('scriptInnerFiles')
    <script src="{{ asset('plugins/colorbox/jquery.colorbox.js') }}"></script>
@endsection

@section('pageScript')
    <script>
        $(document).ready(function () {
            $(".iframe").colorbox({iframe: true, width: "80%", height: "80%"});
        });
    </script>
@endsection

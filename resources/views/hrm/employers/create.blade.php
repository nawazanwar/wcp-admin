@extends('layouts.colorbox')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endsection

@section('content')
    <div class="card card-solid">
        @can('read',\App\Models\Employer::class)
            <div class="card-header">
                <div class="card-title">
                    {!! link_to_route('hrm-employers.index',__('hrm.employers'),null,['class'=>'btn btn-info btn-sm']) !!}
                </div>
            </div>
        @endcan
        <div class="card-body pb-0">
            {!! Form::open(['route' => ['hrm-employers.store'], 'method' => 'POST','files' => true]) !!}
            {!! csrf_field() !!}
            <div class="row">
                <div class="form-group col-md-6">
                    {!! Form::label('parent_department',__('hrm.parent_department')) !!}
                    <select
                        data-live-search="true"
                        data-max-options="3"
                        data-selected-text-format="count > 3"
                        class="form-control custom-select-box selectpicker show-menu-arrow"
                        name="parent_department" id="parent_department">
                        @foreach($types as $key=>$value)
                            @if(old('vehicle_type', $value->id) == $value->id )
                                <option value="{{ $value->id }}" selected>{{ $value->name }} - {{ $value->name_ur }}</option>
                            @else
                                <option value="{{ $value->id }}">{{ $value->name }} - {{ $value->name_ur }}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('parent_department')
                    <div class="error">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('inputName', __('hrm.name')) !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'autofocus', 'id' => 'inputName' ]) !!}
                    @error('name')
                    <div class="error">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-md-6 form-group col-md-6 text-right">
                    {!! Form::label('inputNameUR',__('hrm.name_ur'),['class'=>'pr-2']) !!}
                    {!! Form::text('name_ur', old('name_ur'), ['class' => 'form-control urduWriter ur','dir'=>'rtl','id' => 'inputNameUR' ]) !!}
                    @error('name_ur')
                    <div class="error">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('inputActiveSalary', __('hrm.active_salary')) !!}
                    {!! Form::text('active_salary', old('active_salary'), ['class' => 'form-control', 'id' => 'inputLocation' ]) !!}
                    @error('active_salary')
                    <div class="error">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-md-4">
                    <div class="icheck-success d-inline ">
                        {!! Form::checkbox('active',1, true,['id'=>'inputActive']) !!}
                        {!! Form::label('inputActive', __('hrm.is_active')) !!}
                    </div>
                </div>
                <div class="form-group text-right col-12">
                    {!! Form::submit(__('hrm.save'), array('class' => 'btn btn-primary btn-sm')) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('scriptInnerFiles')
    <script src="{{ asset('plugins/keybaord/urdu.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('plugins/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
@stop


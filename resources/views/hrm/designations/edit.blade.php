@extends('layouts.colorbox')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endsection

@section('content')
    <div class="card card-solid">
        @can('read',\App\Models\Designation::class)
            <div class="card-header">
                <div class="card-title">
                    {!! link_to_route('hrm-designations.index',__('hrm.designations'),null,['class'=>'btn btn-info btn-sm']) !!}
                </div>
            </div>
        @endcan
        <div class="card-body pb-0">
            {!! Form::model($model, ['route' => ['hrm-designations.update', $model], 'method' => 'PUT','files' => true] ) !!}
            {!! csrf_field() !!}
            <div class="row">
                <div class="form-group col-md-4">
                    {!! Form::label('permissions', 'Assign Authorizations') !!}
                    {!! Form::select('permissions[]', $permissions, null, array('id' => 'permissions',
                    'class' => 'form-control selectpicker show-menu-arrow', 'data-live-search' => 'true', 'data-selected-text-format' => 'count > 3',
                    'data-size' => '10', 'data-actions-box' => 'true', 'multiple' => 'multiple')) !!}
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('inputName', __('hrm.name')) !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'autofocus', 'id' => 'inputName' ]) !!}
                    @error('name')
                    <div class="error">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-md-4 form-group text-right">
                    {!! Form::label('inputNameUR',__('hrm.name_ur'),['class'=>'pr-2']) !!}
                    {!! Form::text('name_ur', old('name_ur'), ['class' => 'form-control urduWriter ur','dir'=>'rtl','id' => 'inputNameUR' ]) !!}
                    @error('name_ur')
                    <div class="error">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('inputShift', __('hrm.shift')) !!}
                    {!! Form::text('shift', old('shift'), ['class' => 'form-control', 'id' => 'inputShift' ]) !!}
                    @error('shift')
                    <div class="error">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-md-12">
                    <div class="icheck-success d-inline ">
                        {!! Form::checkbox('active', $model->active?1:0, $model->active?true:false,['id'=>'inputActive']) !!}
                        {!! Form::label('inputActive', __('hrm.is_active')) !!}
                    </div>
                </div>
                <div class="form-group text-right col-12">
                    {!! Form::submit(__('hrm.save'), array('class' => 'btn btn-primary btn-sm')) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('scriptInnerFiles')
    <script src="{{ asset('plugins/keybaord/urdu.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('plugins/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
@stop
@section('pageScript')
    <script>
        jQuery('input[type=checkbox]').click(function () {
            if ($(this).is(':checked')) {
                $(this).val(1).attr('checked', 'checked');
            } else {
                $(this).val(0).removeAttr('checked');
            }
        });
    </script>
@endsection

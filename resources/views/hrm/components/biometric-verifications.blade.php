<style>
    .varification_holder {
        height: 300px;
        background-repeat: no-repeat;
        background-size: cover;
        position: relative;
    }

    .left_hand .finger, .right_hand .finger {
        position: absolute;
        font-size: 12px;
    }

    .left_hand .thumb {
        left: 40%;
        top: 33%;
    }

    .left_hand .thumb {
        left: 40%;
        top: 26%;
    }

    .left_hand .index {
        left: 26%;
        top: -8%;
    }

    .left_hand .middle {
        left: 13%;
        top: -11%;
    }

    .left_hand .ring {
        left: 6%;
        top: -2%;
    }

    .left_hand .pinky {
        left: 0%;
        top: 14%;
        /* text-align: center; */
    }


    .right_hand .thumb {
        right: 40%;
        top: 33%;
    }

    .right_hand .thumb {
        right: 42%;
        top: 26%;
    }

    .right_hand .index {
        right: 25%;
        top: -7%;
    }

    .right_hand .middle {
        right: 12%;
        top: -11%;
    }

    .right_hand .ring {
        right: 4%;
        top: -2%;
    }

    .right_hand .pinky {
        right: -1%;
        top: 14%;
    }
</style>
<div class="row">
    <div class="col-md-6 mx-auto text-center varification_holder"
         style='background-image: url("{{ asset('img/hands.png') }}")'>
        <div class="left_hand">
            <div class="thumb finger">
                <div class="icheck-primary d-inline">
                    <input type="checkbox" name="biometric_lh_thumb_verification" id="biometric_lh_thumb_verification">
                    <label for="biometric_lh_thumb_verification">
                    </label>
                </div>
                <p class="my-0">Thumb</p>
            </div>
            <div class="index finger">
                <div class="icheck-primary d-inline">
                    <input type="checkbox" name="biometric_lh_index_verification" id="biometric_lh_index_verification">
                    <label for="biometric_lh_index_verification">
                    </label>
                </div>
                <p class="my-0">Index</p>
            </div>
            <div class="middle finger">
                <div class="icheck-primary d-inline">
                    <input type="checkbox" name="biometric_lh_middle_verification"
                           id="biometric_lh_middle_verification">
                    <label for="biometric_lh_middle_verification">
                    </label>
                </div>
                <p class="my-0">Middle</p>
            </div>
            <div class="ring finger">
                <div class="icheck-primary d-inline">
                    <input type="checkbox" name="biometric_lh_ring_verification" id="biometric_lh_ring_verification">
                    <label for="biometric_lh_ring_verification">
                    </label>
                </div>
                <p class="my-0">Ring</p>
            </div>
            <div class="pinky finger">
                <div class="icheck-primary d-inline">
                    <input type="checkbox" name="biometric_lh_pinky_verification" id="biometric_lh_pinky_verification">
                    <label for="biometric_lh_pinky_verification">
                    </label>
                </div>
                <p class="my-0">Pinky</p>
            </div>
        </div>
        <div class="right_hand">
            <div class="thumb finger">
                <div class="icheck-primary d-inline">
                    <input type="checkbox" name="biometric_rh_thumb_verification" id="biometric_rh_thumb_verification">
                    <label for="biometric_rh_thumb_verification">
                    </label>
                </div>
                <p class="my-0">Thumb</p>
            </div>
            <div class="index finger">
                <div class="icheck-primary d-inline">
                    <input type="checkbox" name="biometric_rh_index_verification" id="biometric_rh_index_verification">
                    <label for="biometric_rh_index_verification">
                    </label>
                </div>
                <p class="my-0">Index</p>
            </div>
            <div class="middle finger">
                <div class="icheck-primary d-inline">
                    <input type="checkbox" name="biometric_rh_middle_verification"
                           id="biometric_rh_middle_verification">
                    <label for="biometric_rh_middle_verification">
                    </label>
                </div>
                <p class="my-0">Middle</p>
            </div>
            <div class="ring finger">
                <div class="icheck-primary d-inline">
                    <input type="checkbox" name="biometric_rh_ring_verification" id="biometric_rh_ring_verification">
                    <label for="biometric_rh_ring_verification">
                    </label>
                </div>
                <p class="my-0">Ring</p>
            </div>
            <div class="pinky finger">
                <div class="icheck-primary d-inline">
                    <input type="checkbox" name="biometric_rh_pinky_verification" id="biometric_rh_pinky_verification">
                    <label for="biometric_rh_pinky_verification">
                    </label>
                </div>
                <p class="my-0">Pinky</p>
            </div>
        </div>
    </div>
</div>

<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" href="#tab-address" data-toggle="pill" role="tab"
           aria-selected="true">{{__('hrm.address')}}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#tab-card" data-toggle="pill" role="tab"
           aria-selected="false">{{__('hrm.card_details')}}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#tab-documents" data-toggle="pill" role="tab"
           aria-selected="false">{{__('hrm.scanned_docx')}}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#tab-action" data-toggle="pill" role="tab"
           aria-selected="false">{{__('hrm.action')}}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#tab-hands-biometric" data-toggle="pill" role="tab"
           aria-selected="false">{{__('hrm.biometric_verification')}}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#tab-qr-code" data-toggle="pill" role="tab"
           aria-selected="false">{{__('hrm.qr_code')}}</a>
    </li>
</ul>
<div class="tab-content p-3">
    <div class="tab-pane fade active show" id="tab-address" role="tabpanel">
        @include('hrm.components.addresses')
    </div>
    <div class="tab-pane fade" id="tab-card" role="tabpanel">
        @include('hrm.components.card-detail')
    </div>
    <div class="tab-pane fade" id="tab-documents" role="tabpanel">
        @include('hrm.components.docs')
    </div>
    <div class="tab-pane fade py-5" id="tab-hands-biometric" role="tabpanel">
        @include('hrm.components.biometric-verifications')
    </div>
    <div class="tab-pane fade" id="tab-qr-code" role="tabpanel">
        @include('hrm.components.qr-code')
    </div>
    <div class="tab-pane fade" id="tab-action" role="tabpanel">
        @php  $cRoute = \Route::currentRouteName(); @endphp
        <div class="form-group clearfix">
            <div class="icheck-success d-inline">
                {!! Form::checkbox('active',($cRoute=='hrm.edit')?$hrm->user->active?1:0:0,($cRoute=='hrm.edit')?$hrm->user->active?true:false:false,['id'=>'active']) !!}
                {!! Form::label('active','Active') !!}
            </div>
        </div>
        <div class="form-group clearfix">
            <div class="icheck-success d-inline">
                {!! Form::checkbox('block',($cRoute=='hrm.edit')?$hrm->user->block?1:0:0,($cRoute=='hrm.edit')?$hrm->user->block?true:false:false,['id'=>'block']) !!}
                {!! Form::label('block','Block') !!}
            </div>
        </div>
    </div>
</div>

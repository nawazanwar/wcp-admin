<div class="row">
    <div class="form-group col-md-3">
        {!! Form::label('f_name', __('hrm.f_name')) !!}
        {!! Form::text('f_name', old('f_name'), ['class' => 'form-control ', 'id' => 'f_name' ]) !!}
        @error('f_name')
        <div class="error">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group col-md-3 text-right">
        {!! Form::label('f_name_ur', __('hrm.f_name_ur')) !!}
        {!! Form::text('f_name_ur', old('f_name_en'), ['class' => 'form-control ','dir'=>'rtl','id' => 'f_name_ur' ]) !!}
        @error('f_name_ur')
        <div class="error">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group col-md-3">
        {!! Form::label('l_name', __('hrm.l_name')) !!}
        {!! Form::text('l_name', old('l_name'), ['class' => 'form-control ', 'id' => 'l_name' ]) !!}
        @error('l_name')
        <div class="error">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group col-md-3 text-right">
        {!! Form::label('l_name_ur', __('hrm.l_name_ur')) !!}
        {!! Form::text('l_name_ur', old('l_name_ur'), ['class' => 'form-control ','dir'=>'rtl','id' => 'l_name_ur' ]) !!}
        @error('l_name_ur')
        <div class="error">{{ $message }}</div>
        @enderror
    </div>
</div>

<div class="row">
    <div class="col-12 form-group">
        <div class="form-group">
            <label for="InputFile">Choose Scanned Documents</label>
            <div class="input-group">
                <div class="custom-file">
                    {!! Form::file('docs[]',['class'=>'custom-file-input','id'=>'docs','accept'=>'image/*','onchange'=>"Custom.prevMultipleImages(this,'images_holder')",'multiple']) !!}
                    {!! Form::label('docs','Press CTRL and select all scanned documents',['class'=>'custom-file-label']) !!}
                </div>
            </div>
        </div>
        <div class="images_holder text-center my-3 row justify-content-center">
            @if(\Route::currentRouteName()=='hrm.edit')
                @if($hrm->media()->where('type','document')->count()>0)
                    <div class="row" style="margin: 0 auto;">
                        @foreach($hrm->media()->where('type','document')->get() as $media)
                            <div class="col-md-2">
                                <img src="{{ asset('uploads/hrm/documents/'.$media->src) }}"
                                     style="height: 170px !important;"
                                     class="img-thumbnail prev_doc_image">
                            </div>
                        @endforeach
                    </div>
                @endif
            @else
                <div class="col-12 text-left my-3">
                    <strong class="text-danger">Caution:</strong>
                    <p class="my-0 pl-4">We have show the 6 default your images can be more than 6</p>
                </div>
                @for ($i =1; $i <= 6; $i++)
                    <div class="col-xl-2 col-lg-2 col-md-2 clearfix">
                        <img src="{{ asset('img/defaults/document.png') }}" style="height: 170px !important;"
                             class="img-thumbnail prev_doc_image">
                    </div>
                @endfor
            @endif
        </div>
    </div>
</div>

<div class="col-md-6 form-group">
    {!! Form::label('relation_eng',__('hrm.relation_eng')) !!}
    <div class="input-group">
        {!! Form::select('relation_eng',WcpHelper::getHrmRelationInEng(),null,['id'=>'relation_eng','class'=>'form-control','placeholder'=>'Choose Relation']) !!}
    </div>
</div>
<div class="col-md-6 form-group text-right">
    {!! Form::label('relation_ur',__('hrm.relation_ur')) !!}
    <div class="input-group">
        {!! Form::select('relation_ur',WcpHelper::getHrmRelationInUrdu(),null,['id'=>'relation_ur','class'=>'form-control','placeholder'=>'تعلق کا انتخاب کریں']) !!}
    </div>
</div>
<div class="form-group col-md-6">
    {!! Form::label('relation_name_eng', __('hrm.relation_name_eng')) !!}
    {!! Form::text('relation_name_eng', null, ['class' => 'form-control ','dir'=>'ltr','id' => 'relation_name_eng' ]) !!}
</div>
<div class="form-group col-md-6 text-right">
    {!! Form::label('relation_name_ur', __('hrm.relation_name_ur')) !!}
    {!! Form::text('relation_name_ur', null, ['class' => 'form-control ','dir'=>'rtl','id' => 'relation_name_ur' ]) !!}
</div>

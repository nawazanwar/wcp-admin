<div class="row justify-content-center">
    <div class="col-md-6 form-group">
        <div class="avatar_preview_holder">
            @php
              $avatar = isset($for)?file_exists(public_path()."/uploads/hrm/avatar/".$hrm->user->avatar)?asset("/uploads/hrm/avatar/".$hrm->user->avatar):asset('img/defaults/user.png'):asset('img/defaults/user.png');
            @endphp
            <img src="{{ $avatar }}" alt="house"
                 style="width: 100px; height: 100px;" class="img-circle img-thumbnail" onerror="this.src='{{ asset('img/defaults/user.png') }}'">
            <div class="upload-btn-wrapper">
                <button class="btn">Upload</button>
                <input type="file" name="avatar" onchange="Custom.previewAvatar(this)"/>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3 form-group">
        {!! Form::label('card_type',__('hrm.card_type')) !!}
        <div class="input-group">
            {!! Form::select('card_type',WcpHelper::getCardList(),null,['id'=>'card_type','class'=>'form-control','placeholder'=>'Choose Card Type']) !!}
        </div>
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('card_issue_date', __('hrm.card_issue_date')) !!}
        {!! Form::date('card_issue_date',null, ['class' => 'form-control ','id' => 'card_issue_date' ]) !!}
    </div>
    <div class="form-group col-md-3">
        {!! Form::label('card_expiry_date', __('hrm.card_expiry_date')) !!}
        {!! Form::date('card_expiry_date',null, ['class' => 'form-control ','id' => 'card_expiry_date' ]) !!}
    </div>
</div>

<style>
    #unique_qr_code_holder img {
        margin: 0 auto;
    }
</style>
<div class="row">
    <div class="col-6">
        <div class="row mx-1">
            <div class="col-md-12 info-heading-parent">
                <p class="info-heading">Unique QR code Generation</p>
            </div>
            <div class="col-12 text-center">
                <a class="btn btn-xs bg-gradient-info text-white my-3 unique_qr_generator_btn"
                   onclick="generateGeneralQRcode();">Generate QR
                    Code</a>
                <div id="unique_qr_code_holder"></div>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="row justify-content-center">
            <div class="col-md-12 info-heading-parent">
                <p class="info-heading">CNIC QR code Generation</p>
            </div>
            <div class="form-group col-md-8">
                {!! Form::label('cnic_qr_code', __('hrm.cnic_qr_code')) !!}
                {!! Form::textarea('cnic_qr_code', old('cnic_qr_code'), ['class' => 'form-control font-control-sm ','rows'=>'1','id' => 'cnic_qr_code' ]) !!}
                <a class="btn btn-xs bg-gradient-info text-white my-3 cnic_qr_generator_btn pull-right"
                   onclick="generateCNICQRcode();">Generate CNIC QR Code</a>
            </div>
            <div id="cnic_qr_code_holder"></div>
        </div>
    </div>
</div>

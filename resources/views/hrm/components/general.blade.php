<div class="row px-3">
    <div class="form-group col-md-6">
        {!! Form::label('cast', __('hrm.cast')) !!}
        {!! Form::text('cast',null, ['class' => 'form-control ','id' => 'cast' ]) !!}
    </div>
    <div class="form-group col-md-6 text-right">
        {!! Form::label('cast_ur', __('hrm.cast_ur')) !!}
        {!! Form::text('cast_ur', null, ['class' => 'form-control ','dir'=>'rtl','id' => 'cast_ur' ]) !!}
    </div>
    @include('hrm.components.relation')
    <div class="form-group col-md-6">
        {!! Form::label('cnic', __('hrm.cnic')) !!}
        {!! Form::text('cnic',null, ['class' => 'form-control ','id' => 'cnic','data-inputmask'=>"'mask': '99999-9999999-9'" ]) !!}
    </div>
    <div class="form-group col-md-6">
        {!! Form::label('cnic_expiry_date', __('hrm.cnic_expiry_date')) !!}
        {!! Form::date('cnic_expiry_date',null, ['class' => 'form-control ','id' => 'cnic_expiry_date' ]) !!}
    </div>
    <div class="form-group col-md-6">
        {!! Form::label('passport', __('hrm.passport')) !!}
        {!! Form::text('passport',null, ['class' => 'form-control ','id' => 'passport' ]) !!}
    </div>
    <div class="form-group col-md-6">
        {!! Form::label('date_of_birth', __('hrm.date_of_birth')) !!}
        {!! Form::date('date_of_birth',null, ['class' => 'form-control ','id' => 'date_of_birth' ]) !!}
    </div>
    <div class="form-group col-md-6">
        {!! Form::label('blood_group', __('hrm.blood_group')) !!}
        {!! Form::select('blood_group',WcpHelper::listOfBloodGroups(),null,['class' => 'form-control ','id' => 'blood_group' ]) !!}
    </div>
    <div class="form-group clearfix text-center" style="margin: 36px auto;">
        <div class="icheck-primary d-inline">
            {!! Form::radio('gender','male',null,['id'=>'male']) !!}
            {!! Form::label('male',__('hrm.male')) !!}
        </div>
        <div class="icheck-primary d-inline">
            {!! Form::radio('gender','female',null,['id'=>'female']) !!}
            {!! Form::label('female',__('hrm.female')) !!}
        </div>
        <div class="icheck-primary d-inline">
            {!! Form::radio('gender','other',null,['id'=>'other']) !!}
            {!! Form::label('other',__('hrm.other')) !!}
        </div>
    </div>
    <div class="form-group col-md-6">
        {!! Form::label('contact_number', __('hrm.contact_number')) !!}
        {!! Form::text('contact_number',null, ['class' => 'form-control ','id' => 'contact_number' ]) !!}
    </div>
    <div class="form-group col-md-6">
        {!! Form::label('secondary_contact_number', __('hrm.secondary_contact_number')) !!}
        {!! Form::text('secondary_contact_number',null, ['class' => 'form-control ','id' => 'secondary_contact_number' ]) !!}
    </div>
    <div class="form-group col-md-12">
        {!! Form::label('secondary_contact_person', __('hrm.secondary_contact_person')) !!}
        {!! Form::text('secondary_contact_person',null, ['class' => 'form-control ','id' => 'secondary_contact_person' ]) !!}
    </div>
</div>

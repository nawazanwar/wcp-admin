<div class="row">
    <div class="col-5 col-sm-3">
        <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
            <a class="nav-link active" id="vert-tabs-home-tab" data-toggle="pill" href="#vert-tabs-temporary-address" role="tab"
               aria-controls="vert-tabs-home" aria-selected="true">Temporary Address</a>
            <a class="nav-link" id="vert-tabs-profile-tab" data-toggle="pill" href="#vert-tabs-permanent-address" role="tab"
               aria-controls="vert-tabs-profile" aria-selected="false">Permanent Address</a>
            <a class="nav-link" id="vert-tabs-messages-tab" data-toggle="pill" href="#vert-tabs-present-address" role="tab"
               aria-controls="vert-tabs-messages" aria-selected="false">Present Address</a>
        </div>
    </div>
    <div class="col-7 col-sm-9">
        <div class="tab-content" id="vert-tabs-tabContent">
            <div class="tab-pane text-left fade show active" id="vert-tabs-temporary-address" role="tabpanel">
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('temp_city', __('hrm.temp_city')) !!}
                        {!! Form::text('temp_city',null, ['class' => 'form-control ', 'id' => 'temp_city' ]) !!}
                    </div>
                    <div class="col-md-6 form-group text-right">
                        {!! Form::label('temp_city_ur', __('hrm.temp_city_ur')) !!}
                        {!! Form::text('temp_city_ur', null, ['class' => 'form-control ', 'id' => 'temp_city_ur','dir'=>'rtl' ]) !!}
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('temp_address', __('hrm.temp_address')) !!}
                        {!! Form::textarea('temp_address',null, ['class' => 'form-control ', 'id' => 'temp_address' ,'rows'=>'2']) !!}
                    </div>
                    <div class="col-md-6 form-group text-right">
                        {!! Form::label('temp_address_ur', __('hrm.temp_address_ur')) !!}
                        {!! Form::textarea('temp_address_ur',null, ['class' => 'form-control ', 'id' => 'temp_address_ur' ,'rows'=>'2','dir'=>'rtl']) !!}
                    </div>
                </div>
            </div>
            <div class="tab-pane text-left fade" id="vert-tabs-permanent-address" role="tabpanel">
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('perm_city', __('hrm.perm_city')) !!}
                        {!! Form::text('perm_city',null, ['class' => 'form-control ', 'id' => 'perm_city' ]) !!}
                    </div>
                    <div class="col-md-6 form-group text-right">
                        {!! Form::label('perm_city_ur', __('hrm.perm_city_ur')) !!}
                        {!! Form::text('perm_city_ur', null, ['class' => 'form-control ', 'id' => 'perm_city_ur','dir'=>'rtl' ]) !!}
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('perm_address', __('hrm.perm_address')) !!}
                        {!! Form::textarea('perm_address',null, ['class' => 'form-control ', 'id' => 'perm_address' ,'rows'=>'2']) !!}
                    </div>
                    <div class="col-md-6 form-group text-right">
                        {!! Form::label('perm_address_ur', __('hrm.perm_address_ur')) !!}
                        {!! Form::textarea('perm_address_ur',null, ['class' => 'form-control ', 'id' => 'perm_address_ur' ,'rows'=>'2','dir'=>'rtl']) !!}
                    </div>
                </div>
            </div>
            <div class="tab-pane text-left fade" id="vert-tabs-present-address" role="tabpanel">
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('pres_city', __('hrm.pres_city')) !!}
                        {!! Form::text('pres_city',null, ['class' => 'form-control ', 'id' => 'pres_city' ]) !!}
                    </div>
                    <div class="col-md-6 form-group text-right">
                        {!! Form::label('pres_city_ur', __('hrm.pres_city_ur')) !!}
                        {!! Form::text('pres_city_ur', null, ['class' => 'form-control ', 'id' => 'pres_city_ur','dir'=>'rtl' ]) !!}
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('pres_address', __('hrm.pres_address')) !!}
                        {!! Form::textarea('pres_address',null, ['class' => 'form-control ', 'id' => 'pres_address' ,'rows'=>'2']) !!}
                    </div>
                    <div class="col-md-6 form-group text-right">
                        {!! Form::label('pres_address_ur', __('hrm.pres_address_ur')) !!}
                        {!! Form::textarea('pres_address_ur',null, ['class' => 'form-control ', 'id' => 'pres_address_ur' ,'rows'=>'2','dir'=>'rtl']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row border-left px-3 align-items-center">
    <div class="col-md-12 info-heading-parent">
        <p class="info-heading">{{__('hrm.media_information')}}</p>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="picture['first']">{{ __('hrm.first_image') }}</label>
            <div class="input-group input-group-sm">
                <div class="custom-file">
                    <input type="file" name="first_picture"
                           class="custom-file-input form-control-sm"
                           id="picture['first']"
                           onchange="Custom.previewImage(this,'picture_first')">
                    <label class="custom-file-label" for="picture['back']">{{ __('system.choose_file') }}</label>
                </div>
            </div>
        </div>
        <div class="picture_first text-center my-3">
            @php
                $first_picture = isset($for)?file_exists(public_path()."/uploads/hrm/media/".$hrm->first_picture)?asset("/uploads/hrm/media/".$hrm->first_picture):asset('img/defaults/user-picture.png'):asset('img/defaults/user-picture.png');
            @endphp
            <img src="{{ $first_picture }}" onerror="this.src='{{ asset('img/defaults/user-picture.png') }}'" class="img-thumbnail h-150">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="picture['second']">{{ __('hrm.second_image') }}</label>
            <div class="input-group input-group-sm">
                <div class="custom-file">
                    <input type="file" name="second_picture"
                           class="custom-file-input form-control-sm"
                           id="picture['second']"
                           onchange="Custom.previewImage(this,'second_image')">
                    <label class="custom-file-label" for="picture['second']">{{ __('system.choose_file') }}</label>
                </div>
            </div>
        </div>
        <div class="second_image text-center my-3">
            @php
                $second_picture = isset($for)?file_exists(public_path()."/uploads/hrm/media/".$hrm->second_picture)?asset("/uploads/hrm/media/".$hrm->second_picture):asset('img/defaults/user-picture.png'):asset('img/defaults/user-picture.png');
            @endphp
            <img src="{{ $second_picture }}" onerror="this.src='{{ asset('img/defaults/user-picture.png') }}'" class="img-thumbnail h-150">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="picture['third']">{{ __('hrm.third_image') }}</label>
            <div class="input-group input-group-sm">
                <div class="custom-file">
                    <input type="file" name="third_picture"
                           class="custom-file-input form-control-sm"
                           id="picture['third']"
                           onchange="Custom.previewImage(this,'third_image')">
                    <label class="custom-file-label" for="picture['third']">{{ __('system.choose_file') }}</label>
                </div>
            </div>
        </div>
        <div class="third_image text-center my-3">
            @php
                $third_picture = isset($for)?file_exists(public_path()."/uploads/hrm/media/".$hrm->third_picture)?asset("/uploads/hrm/media/".$hrm->third_picture):asset('img/defaults/user-picture.png'):asset('img/defaults/user-picture.png');
            @endphp
            <img src="{{ $third_picture }}" onerror="this.src='{{ asset('img/defaults/user-picture.png') }}'" class="img-thumbnail h-150">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="picture['fourth']">{{ __('hrm.fourth_image') }}</label>
            <div class="input-group input-group-sm">
                <div class="custom-file">
                    <input type="file" name="fourth_picture"
                           class="custom-file-input form-control-sm"
                           id="picture['fourth']"
                           onchange="Custom.previewImage(this,'fourth_image')">
                    <label class="custom-file-label" for="picture['fourth']">{{ __('system.choose_file') }}</label>
                </div>
            </div>
        </div>
        <div class="fourth_image text-center my-3">
            @php
                $fourth_picture = isset($for)?file_exists(public_path()."/uploads/hrm/media/".$hrm->fourth_picture)?asset("/uploads/hrm/media/".$hrm->fourth_picture):asset('img/defaults/user-picture.png'):asset('img/defaults/user-picture.png');
            @endphp
            <img src="{{ $fourth_picture }}" onerror="this.src='{{ asset('img/defaults/user-picture.png') }}'" class="img-thumbnail h-150">
        </div>
    </div>
</div>

<style>
    .ratina_holder {
        height: 300px;
        background-repeat: no-repeat;
        background-size: cover;
        position: relative;
    }
</style>
<div class="row">
    <div class="col-md-6 mx-auto text-center ratina_holder"
         style='background-image: url("{{ asset('img/ratina.png') }}")'>
    </div>
</div>

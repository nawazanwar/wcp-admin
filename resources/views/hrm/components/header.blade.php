<div class="col-md-3 form-group">
    {!! Form::label('emp_type',__('hrm.employee_type')) !!}
    <div class="input-group">
        {!! Form::select('emp_type',WcpHelper::getEmployeeTypes(),null,['id'=>'emp_type','class'=>'form-control','placeholder'=>'Choose Employee Type']) !!}
    </div>
</div>
<div class="col-md-3 form-group">
    {!! Form::label('employer_id',__('hrm.employer')) !!}
    <div class="input-group">
        {!! Form::select('employer_id',WcpHelper::getEmployerList(),null,['id'=>'employer_id','class'=>'form-control','placeholder'=>'Choose Employer']) !!}
    </div>
</div>
<div class="col-md-3 form-group">
    {!! Form::label('department_id',__('hrm.department')) !!}
    <div class="input-group">
        {!! Form::select('department_id',WcpHelper::getDepartmentList(),null,['id'=>'department_id','class'=>'form-control','placeholder'=>'Choose Department']) !!}
    </div>
</div>
<div class="col-md-3 form-group">
    {!! Form::label('designations',__('hrm.designations')) !!}
    <div class="input-group">
        {!! Form::select('designations[]',WcpHelper::getDesignationList(),isset($for)?$hrm->user->designations:null,['id'=>'designations','class'=>'form-control selectpicker','data-placeholder'=>'Choose Designation','multiple']) !!}
    </div>
</div>

<dl>
    <dt>{{__('hrm.relation_eng')}}</dt>
    <dd class="ml-2">
        @isset($d->relation_eng)
            {{ ucfirst(\App\Models\Type::find($d->relation_eng)->name)}}
        @else
            --
        @endisset
    </dd>
    <dt>{{__('hrm.relation_ur')}}</dt>
    <dd class="ml-2">
        @isset($d->relation_ur)
            {{ \App\Models\Type::find($d->relation_ur)->name_ur}}
        @else
            --
        @endisset
    </dd>
    <dt>{{__('hrm.relation_name_eng')}}</dt>
    <dd>
        @isset($d->relation_name_eng)
            {{ ucfirst($d->relation_name_eng) }}
        @else
            --
        @endisset
    </dd>
    <dt>{{__('hrm.relation_name_ur')}}</dt>
    <dd>
        @isset($d->relation_name_ur)
            {{ $d->relation_name_ur}}
        @else
            --
        @endisset
    </dd>
</dl>

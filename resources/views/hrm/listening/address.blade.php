<div class="row">
    <div class="col-12">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active fs-13 px-2" data-toggle="pill" href="#temp-address-{{$d->id}}"
                   role="tab">Temporary</a>
            </li>
            <li class="nav-item">
                <a class="nav-link fs-13 px-2" data-toggle="pill" href="#permanent-address-{{$d->id}}"
                   role="tab">Permanent</a>
            </li>
            <li class="nav-item">
                <a class="nav-link fs-13 px-2" data-toggle="pill" href="#present-address-{{$d->id}}"
                   role="tab">Present</a>
            </li>
        </ul>
        <div class="tab-content my-2">
            <div class="tab-pane fade p-2 show active" id="temp-address-{{$d->id}}" role="tabpanel">
                <dl>
                    <dt>{{ __('hrm.temp_city') }}</dt>
                    <dd class="ml-2">
                        @if($d->temp_city)
                            {{ $d->temp_city }}
                        @else
                            --
                        @endif
                    </dd>
                    <dt>{{ __('hrm.temp_city_ur') }}</dt>
                    <dd class="ml-2">
                        @if($d->temp_city_ur)
                            {{ $d->temp_city_ur }}
                        @else
                            --
                        @endif
                    </dd>
                    <dt>{{ __('hrm.temp_address') }}</dt>
                    <dd class="ml-2">
                        @if($d->temp_address)
                            {{ $d->temp_address }}
                        @else
                            --
                        @endif
                    </dd>
                    <dt>{{ __('hrm.temp_address_ur') }}</dt>
                    <dd class="ml-2">
                        @if($d->temp_address_ur)
                            {{ $d->temp_address_ur }}
                        @else
                            --
                        @endif
                    </dd>
                </dl>
            </div>
            <div class="tab-pane fade p-2" id="permanent-address-{{$d->id}}" role="tabpanel">
                <dl>
                    <dt>{{ __('hrm.perm_city') }}</dt>
                    <dd class="ml-2">
                        @if($d->perm_city)
                            {{ $d->perm_city }}
                        @else
                            --
                        @endif
                    </dd>
                    <dt>{{ __('hrm.perm_city_ur') }}</dt>
                    <dd class="ml-2">
                        @if($d->perm_city_ur)
                            {{ $d->perm_city_ur }}
                        @else
                            --
                        @endif
                    </dd>
                    <dt>{{ __('hrm.perm_address') }}</dt>
                    <dd class="ml-2">
                        @if($d->perm_address)
                            {{ $d->perm_address }}
                        @else
                            --
                        @endif
                    </dd>
                    <dt>{{ __('hrm.perm_address_ur') }}</dt>
                    <dd class="ml-2">
                        @if($d->perm_address_ur)
                            {{ $d->perm_address_ur }}
                        @else
                            --
                        @endif
                    </dd>
                </dl>
            </div>
            <div class="tab-pane fade p-2" id="present-address-{{$d->id}}" role="tabpanel">
                <dl>
                    <dt>{{ __('hrm.pres_city') }}</dt>
                    <dd class="ml-2">
                        @if($d->pres_city)
                            {{ $d->pres_city }}
                        @else
                            --
                        @endif
                    </dd>
                    <dt>{{ __('hrm.pres_city_ur') }}</dt>
                    <dd class="ml-2">
                        @if($d->pres_city_ur)
                            {{ $d->pres_city_ur }}
                        @else
                            --
                        @endif
                    </dd>
                    <dt>{{ __('hrm.pres_address') }}</dt>
                    <dd class="ml-2">
                        @if($d->pres_address)
                            {{ $d->pres_address }}
                        @else
                            --
                        @endif
                    </dd>
                    <dt>{{ __('hrm.pres_address_ur') }}</dt>
                    <dd class="ml-2">
                        @if($d->pres_address_ur)
                            {{ $d->pres_address_ur }}
                        @else
                            --
                        @endif
                    </dd>
                </dl>
            </div>
        </div>
    </div>
</div>

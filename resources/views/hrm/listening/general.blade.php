<dl>
    <dt>{{__('hrm.cast')}}</dt>
    <dd class="ml-2">
        @isset($d->cast)
            {{ $d->cast}}
        @else
            --
        @endisset
    </dd>
    <dt>Cast in Urdu</dt>
    <dd class="ml-2">
        @isset($d->cast_ur)
            {{ $d->cast_ur}}
        @else
            --
        @endisset
    </dd>
    <dt>{{__('hrm.cnic_expiry_date')}}</dt>
    <dd>
        @isset($d->cnic_expiry_date)
            {{ $d->cnic_expiry_date}}
        @else
            --
        @endisset
    </dd>
    <dt>{{__('hrm.date_of_birth')}}</dt>
    <dd>
        @isset($d->date_of_birth)
            {{ $d->date_of_birth}}
        @else
            --
        @endisset
    </dd>
    <dt>{{__('hrm.blood_group')}}</dt>
    <dd>
        @isset($d->blood_group)
            {{ $d->blood_group}}
        @else
            --
        @endisset
    </dd>
    <dt>Gender</dt>
    <dd>
        @isset($d->gender)
            {{ $d->gender}}
        @else
            --
        @endisset
    </dd>
    <dt>{{__('hrm.contact_number')}}</dt>
    <dd>
        @isset($d->contact_number)
            {{ $d->contact_number}}
        @else
            --
        @endisset
    </dd>
    <dt>{{__('hrm.secondary_contact_number')}}</dt>
    <dd>
        @isset($d->secondary_contact_number)
            {{ $d->secondary_contact_number}}
        @else
            --
        @endisset
    </dd>
    <dt>{{__('hrm.secondary_contact_person')}}</dt>
    <dd>
        @isset($d->secondary_contact_person)
            {{ $d->secondary_contact_person}}
        @else
            --
        @endisset
    </dd>
</dl>

<div class="row">
    @if(count($d->media))
        @foreach($d->media as $key=>$media)
            <div class="col-12 mb-2">
                <img src="{{ asset('uploads/hrm/documents/'.$media->src) }}" class="img-fluid img-thumbnail">
            </div>
        @endforeach
    @else
        <div class="col-12 mb-2">
            <p>No Documents Found</p>
        </div>
    @endif
</div>

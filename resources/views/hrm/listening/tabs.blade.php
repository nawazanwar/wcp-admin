<ul class="nav nav-tabs nav-pills mx-3 mt-3" role="tablist">
    <li class="nav-item">
        <a class="nav-link active fs-13 px-2" data-toggle="pill" href="#media-tab-{{$d->id}}"
           role="tab">Media</a>
    </li>
    <li class="nav-item">
        <a class="nav-link fs-13 px-2" data-toggle="pill" href="#general-tab-{{$d->id}}"
           role="tab">General</a>
    </li>
    <li class="nav-item">
        <a class="nav-link fs-13 px-2" data-toggle="pill" href="#relation-tab-{{$d->id}}"
           role="tab">Relation</a>
    </li>
    <li class="nav-item">
        <a class="nav-link fs-13 px-2" data-toggle="pill" href="#address-tab-{{$d->id}}"
           role="tab">Address</a>
    </li>
    <li class="nav-item">
        <a class="nav-link fs-13 px-2" data-toggle="pill" href="#documents-tab-{{$d->id}}"
           role="tab">Documents</a>
    </li>
</ul>
<div class="tab-content mx-3 mb-3" style="border: 1px solid #dfdfdf;border-top: 0;">
    <div class="tab-pane fade p-2 active show" id="media-tab-{{$d->id}}" role="tabpanel">
        @include('hrm.listening.media')
    </div>
    <div class="tab-pane fade p-2 " id="general-tab-{{$d->id}}" role="tabpanel" style="height: 237px;overflow-y: auto;">
        @include('hrm.listening.general')
    </div>
    <div class="tab-pane fade p-2 " id="relation-tab-{{$d->id}}" role="tabpanel" style="height: 237px;overflow-y: auto;">
        @include('hrm.listening.relation')
    </div>
    <div class="tab-pane fade p-2 " id="address-tab-{{$d->id}}" role="tabpanel" style="height: 237px;overflow-y: auto;">
        @include('hrm.listening.address')
    </div>
    <div class="tab-pane fade p-2 " id="documents-tab-{{$d->id}}" role="tabpanel" style="height: 237px;overflow-y: auto;">
        @include('hrm.listening.documents')
    </div>
</div>

<div class="row">
    <div class="col-12 text-center">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active fs-13 px-2" data-toggle="pill" href="#media-1-{{$d->id}}"
                   role="tab">First</a>
            </li>
            <li class="nav-item">
                <a class="nav-link fs-13 px-2" data-toggle="pill" href="#media-2-{{$d->id}}"
                   role="tab">Second</a>
            </li>
            <li class="nav-item">
                <a class="nav-link fs-13 px-2" data-toggle="pill" href="#media-3-{{$d->id}}"
                   role="tab">Third</a>
            </li>
            <li class="nav-item">
                <a class="nav-link fs-13 px-2" data-toggle="pill" href="#media-4-{{$d->id}}"
                   role="tab">Fourth</a>
            </li>
        </ul>
        <div class="tab-content my-2">
            <div class="tab-pane fade p-2 show active" id="media-1-{{$d->id}}" role="tabpanel">
                @if($d->first_picture)
                    <img src="{{ asset('uploads/hrm/media/'.$d->first_picture) }}"
                         onerror="this.src='{{ asset('img/defaults/user.png') }}'"
                         class="img-thumbnail h-240px img-fluid">
                @else
                    <img src="{{ asset('img/defaults/user.png') }}"
                         class="img-thumbnail h-240px">
                @endif
            </div>
            <div class="tab-pane fade p-2" id="media-3-{{$d->id}}" role="tabpanel">
                @if($d->second_picture)
                    <img src="{{ asset('uploads/hrm/media/'.$d->second_picture) }}"
                         onerror="this.src='{{ asset('img/defaults/user.png') }}'"
                         class="img-thumbnail h-240px img-fluid">
                @else
                    <img src="{{ asset('img/defaults/user.png') }}"
                         class="img-thumbnail h-240px">
                @endif
            </div>
            <div class="tab-pane fade p-2" id="media-4-{{$d->id}}" role="tabpanel">
                @if($d->third_picture)
                    <img src="{{ asset('uploads/hrm/media/'.$d->third_picture) }}"
                         onerror="this.src='{{ asset('img/defaults/user.png') }}'"
                         class="img-thumbnail h-240px img-fluid">
                @else
                    <img src="{{ asset('img/defaults/user.png') }}"
                         class="img-thumbnail h-240px">
                @endif
            </div>
            <div class="tab-pane fade p-2" id="media-2-{{$d->id}}" role="tabpanel">
                @if($d->fourth_picture)
                    <img src="{{ asset('uploads/hrm/media/'.$d->fourth_picture) }}"
                         onerror="this.src='{{ asset('img/defaults/user.png') }}'"
                         class="img-thumbnail h-240px img-fluid">
                @else
                    <img src="{{ asset('img/defaults/user.png') }}"
                         class="img-thumbnail h-240px">
                @endif
            </div>
        </div>
    </div>
</div>

@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-header row align-items-center mx-1">
                    <div class="card-title col-xl-6 col-lg-6 col-md-6">
                        @can('read',\App\Models\HRM::class)
                            {!! HTML::decode(link_to_route('hrm.index',' <i class="fa fa-arrow-left"></i> '.__('hrm.back_hrm'),null,['class'=>'btn bg-gradient-info btn-sm']))!!}
                        @endcan
                    </div>
                </div>
                <div class="card-body">

                    @include('partials.dashboard.toaster')

                    {!! Form::model($hrm,['route' => ['hrm.update',[$hrm]], 'method' => 'PUT','files' => true,'id'=>'CreationForm','onSubmit'=>'return false;']) !!}
                    {!! csrf_field() !!}
                    @include('hrm.components.user',['for'=>'edit'])
                    <div class="row">
                        @include('hrm.components.header',['for'=>'edit'])
                    </div>
                    <!-- User Information -->
                    @include('hrm.components.user-info')
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            @include('hrm.components.general')
                        </div>
                        <div class="col-md-6">
                            @include('hrm.components.media',['for'=>'edit'])
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            @include('hrm.components.tabs')
                        </div>
                    </div>
                </div>
                <div class="card-footer clearfix text-right">
                    <button type="submit" class="btn btn-info">{{ __('system.save') }}</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scriptInnerFiles')
    <script type="text/javascript"
            src="{{ asset('plugins/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('plugins/QReader/qrcode.js') }}"></script>
    <script src="{{ asset('plugins/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('plugins/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>
@stop
@section('pageScript')
    <script>

        (function ($) {

            $(":input").inputmask();

            $("#CreationForm").validate({
                rules: {
                    "f_name": "required",
                    "l_name": "required",
                },
                messages: {
                    "f_name": "First name is required",
                    "l_name": "Last name is required",
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });

        })(jQuery);

        function generateCNICQRcode() {
            $("#cnic_qr_code_holder").empty().removeAttr('title');
            var cnic_qr_code_reader = new QRCode(document.getElementById("cnic_qr_code_holder"), {
                width: 200,
                height: 200
            });
            cnic_qr_code_reader.makeCode($("#cnic_qr_code").val());
            $("#cnic_qr_code").attr('disabled', 'disabled');
            $(".cnic_qr_generator_btn").remove();
            console.log($("#cnic_qr_code_holder").find('img'));
        }

        function generateGeneralQRcode() {
            $("#unique_qr_code_holder").empty().removeAttr('title');
            var unique_qr_code_holder = new QRCode(document.getElementById("unique_qr_code_holder"), {
                width: 200,
                height: 200
            });
            unique_qr_code_holder.makeCode(new Date().valueOf());
            $(".unique_qr_generator_btn").addClass('disable');
        }
    </script>
@endsection

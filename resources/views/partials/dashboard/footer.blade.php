<footer class="main-footer py-2">
    <p class="text-center mb-0">Copyright © {{ date('Y') }} All rights reserved.</p>
</footer>

@php
    $cRouteName = \Request::route()->getName();
@endphp
<style>.nav_link p{font-size:15px !Important;}</style>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('dashboard') }}" class="brand-link text-center">
        <span class="brand-text font-weight-bold">
            {{ $settingModel->getShortSiteName() }}
        </span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar  mt-1">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            @can('create',\App\Models\LRM::class)
                <li class="nav-item">
                    <a href="{{ route('lrm-application.create') }}" class="nav-link">
                        <p>{{ __('lrm.application.long_title') }}</p>
                    </a>
                </li>
            @endcan
            @can('read',\App\Models\LRM::class)
                <li class="nav-item">
                    <a href="{{ route('hrm.index') }}" class="nav-link">
                        <p>{{ __('lrm.plot.long_title') }}</p>
                    </a>
                </li>
            @endcan
            @can('read',\App\Models\LRM::class)
                <li class="nav-item ">
                    <a href="{{ route('hrm.index') }}" class="nav-link">
                        <p>{{ __('lrm.title_transfer.long_title') }}</p>
                    </a>
                </li>
            @endcan
            @can('read',\App\Models\LRM::class)
                <li class="nav-item ">
                    <a href="{{ route('hrm.index') }}" class="nav-link">
                        <p>{{ __('lrm.tenat.long_title') }}</p>
                    </a>
                </li>
            @endcan
            @can('read',\App\Models\HRM::class)
                <li class="nav-item ">
                    <a href="{{ route('hrm.index') }}" class="nav-link">
                        <p>{{__('dashboard.hrm')}}</p>
                    </a>
                </li>
            @endcan
            @can('read',\App\Models\VRS::class)
                <li class="nav-item">
                    <a href="{{ route('vrs.index') }}" class="nav-link">
                        <p>{{__('dashboard.vrs')}}</p>
                    </a>
                </li>
            @endcan
            @can('read',\App\Models\Device::class)
                <li class="nav-item ">
                    <a href="{{ route('device.index') }}" class="nav-link bg-blue">
                        <p>{{__('dashboard.device_management_system')}}</p>
                    </a>
                </li>
            @endcan
            <li class="nav-item ">
                <a href="#" class="nav-link bg-white">
                    <p>Outsources</p>
                </a>
            </li>
            <li class="nav-item ">
                <a href="#" class="nav-link bg-lime">
                    <p>Accounts</p>
                </a>
            </li>
            @can('read',\App\Models\Setting::class)
                <li class="nav-item ">
                    <a href="{{ route('settings') }}" class="nav-link bg-teal">
                        <p>{{__('system.settings')}}</p>
                    </a>
                </li>
            @endcan
        </ul>
    </div>
</aside>

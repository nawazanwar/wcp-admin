<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link font-weight-bolder text-info">
                {{ $settingModel->getSiteName() }}
            </a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown mx-2">
            <a class="btn btn-info dropdown-toggle dropdown-icon btn-sm" data-toggle="dropdown" href="#"
               aria-expanded="false">
                <img src="{{ Auth::user()->getAvatar() }}" alt="{{ Auth::user()->name }}"
                     class="avatar avatar-sm mr-1 img-circle"> {{ Auth::user()->name }}
            </a>
            <div class="dropdown-menu dropdown-menu-sm-right" role="menu">
                <a class="dropdown-item" href="{{ route('auth.logout') }}">{{__('auth.logout')}}</a>
            </div>
        </li>
    </ul>
</nav>



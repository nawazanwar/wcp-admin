@if($errors->any())
    <div id="snackbar" style="visibility: visible; background-color: darkred;">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if ($successMessage = \Illuminate\Support\Facades\Session::get('successMessage'))

    <div id="snackbar" style="visibility: visible; background-color:green;">
        <ul class="list-group">
            <li class="list-group-item bg-gradient-success">
                <i class="icon fas fa-check"></i> {{$successMessage}}
            </li>
        </ul>
    </div>

@endif

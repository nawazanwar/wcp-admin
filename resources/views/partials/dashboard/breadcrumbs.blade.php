<div class="content-header">
    <div class="container-fluid">
        <div class="row my-0 align-items-center">
            <div class="col-6">
                @if(isset($pageTitle) && $pageTitle != '')
                    <h5 class="m-0 text-dark font-weight-bold"> {{$pageTitle}} </h5>
                @endif
            </div><!-- /.col -->
            <div class="col-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href=" {{ route('dashboard') }}">{{__('dashboard.home')}}</a></li>
                    @foreach($breadcrumbs as $leaf)
                        @if(!empty($leaf['url']))
                            <li class="breadcrumb-item"><a href="{{$leaf['url']}}">{{$leaf['text']}}</a></li>
                        @else
                            <li class="breadcrumb-item active">{{$leaf['text']}}</li>
                        @endif
                    @endforeach
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

@php
    $cRouteName = \Request::route()->getName();
@endphp
<style>.nav_link p{font-size:15px !Important;}</style>
<aside class="main-sidebar sidebar-dark-primary elevation-4" style="width: 196px;">
    <a href="{{ route('dashboard') }}" class="brand-link text-center">
        <span class="brand-text font-weight-bold">
            {{ $settingModel->getShortSiteName() }}
        </span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar  mt-1">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

        </ul>
    </div>
</aside>

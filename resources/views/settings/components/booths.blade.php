@can('read',\App\Models\Booth::class)
    <div class="small-box bg-gradient-info">
        <div class="inner">
            <h5>{{__('society.booths.all')}}</h5>
        </div>
        <div class="icon">
            <i class="fa fa-building" style="font-size: 22px;top: 14px;"></i>
        </div>
        <a href="{{ route('society-booths.index') }}"
           class="iframe small-box-footer btn btn-sm">
            {{ __('system.manage') }} <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
@endcan

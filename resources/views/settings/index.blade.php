@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-body">
                    @include('partials.dashboard.message')

                    <div class="row">
                        <div class="col-4">
                            @include('settings.components.gates')
                        </div>
                        <div class="col-4">
                            @include('settings.components.booths')
                        </div>
                        <div class="col-4">
                            @can('read',\App\Models\Message::class)
                                <div class="col-12">
                                    <div class="small-box bg-gradient-info">
                                        <div class="inner">
                                            <h5>{{__('system.messages')}}</h5>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-cog" style="font-size: 22px;top: 14px;"></i>
                                        </div>
                                        <a href="{{ route('messages.index') }}"
                                           class="iframe small-box-footer btn btn-sm">
                                            {{ __('system.manage') }} <i class="fa fa-arrow-circle-right"></i>
                                        </a>
                                    </div>
                                </div>
                            @endcan
                        </div>
                        <div class="col-4"></div>
                    </div>

                    <form action="{{ route('settings.update',$setting->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="card card-primary card-tabs">
                            <div class="card-header p-0 pt-1">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="pill" href="#eng-translator" role="tab"
                                           aria-selected="true">{{__('system.english')}}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="pill" href="#urdu-translator" role="tab"
                                           aria-selected="false">{{__('system.urdu')}}</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade active show" id="eng-translator" role="tabpanel">
                                        <div class="form-group">
                                            <label for="short_name">{{__('system.short_name')}}</label>
                                            <input type="text" name="short_name" class="form-control" id="short_name"
                                                   value="{{ $setting->short_name }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="name">{{__('system.name')}}</label>
                                            <input type="text" name="name" class="form-control" id="name"
                                                   value="{{ $setting->name }}">
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="urdu-translator" role="tabpanel">
                                        <div class="form-group">
                                            <label for="short_name_ur">{{__('system.short_name')}}</label>
                                            <input type="text" name="short_name_ur" dir="rtl"
                                                   class="form-control urduWriter ur"
                                                   id="short_name_ur"
                                                   value="{{ $setting->short_name_ur }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="name_ur">{{__('system.name')}}</label>
                                            <input type="text" name="name_ur" dir="rtl"
                                                   class="form-control urduWriter ur"
                                                   id="name_ur"
                                                   value="{{ $setting->name_ur }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="icheck-success d-inline" dir="ltr">
                                <input type="checkbox" name="maintenance" id="maintenance">
                                <label for="maintenance">{{__('system.maintenance')}}</label>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary btn-sm">{{ __('system.update') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-header row align-items-center mx-1">
                    <div class="card-title col-10"><h3>LRM</h3></div>
                    <div class="card-title col-10">
                        @can('read',\App\Models\LRM::class)
                            {!! HTML::decode(link_to_route('lrm-ministry.index',' <i class="fa fa-eye"></i> '.__('lrm.ministries'),null,['class'=>'iframe btn bg-gradient-info btn-xs'])) !!}
                        @endcan
                        @can('read',\App\Models\LRM::class)
                            {!! HTML::decode(link_to_route('lrm-organization.index',' <i class="fa fa-eye"></i> '.__('lrm.organizations'),null,['class'=>'iframe btn bg-gradient-info btn-xs'])) !!}
                        @endcan
                        @can('read',\App\Models\LRM::class)
                            {!! HTML::decode(link_to_route('lrm-department.index',' <i class="fa fa-eye"></i> '.__('lrm.departments.title'),null,['class'=>'iframe btn bg-gradient-info btn-xs'])) !!}
                        @endcan
                        @can('read',\App\Models\LRM::class)
                            {!! HTML::decode(link_to_route('lrm-rank.index',' <i class="fa fa-eye"></i> '.__('lrm.ranks.title'),null,['class'=>'iframe btn bg-gradient-info btn-xs'])) !!}
                        @endcan
                        @can('read',\App\Models\LRM::class)
                            {!! HTML::decode(link_to_route('lrm-grade.index',' <i class="fa fa-eye"></i> '.__('lrm.grades.title'),null,['class'=>'iframe btn bg-gradient-info btn-xs'])) !!}
                        @endcan
                        @can('read',\App\Models\LRM::class)
                            {!! HTML::decode(link_to_route('lrm-wing.index',' <i class="fa fa-eye"></i> '.__('lrm.wings.title'),null,['class'=>'iframe btn bg-gradient-info btn-xs'])) !!}
                        @endcan
                    </div>
                    <div class="card-title col-2 text-right">
                        @can('read',\App\Models\LRM::class)
                            {!! HTML::decode(link_to_route('lrm-settings.index',' <i class="fa fa-plus"></i> '.__('lrm.settings'),null,['class'=>'iframe btn bg-gradient-info btn-xs'])) !!}
                        @endcan
                        @can('read',\App\Models\LRM::class)
                            {!! HTML::decode(link_to_route('location.index',' <i class="fa fa-plus"></i> '.__('lrm.locations.title'),null,['class'=>'iframe btn bg-gradient-warning btn-xs'])) !!}
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scriptInnerFiles')
    <script src="{{ asset('plugins/keybaord/urdu.js') }}"></script>
@endsection

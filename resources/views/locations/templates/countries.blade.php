<div class="row">
    <div class="col-12 text-right p-3">
        <a class="btn btn-success btn-xs text-white cursor-pointer" onclick="add('country');">
            Add
        </a>
    </div>
</div>
<table class="table table-bordered" id="countryTable">
    @php
        $countries = \App\Location\Country::orderby('id','desc')->get();
    @endphp
    @foreach($countries as $c)
        <tr class="my-2 bg-white">
            <td>
                <input type="text" name="settings[countries][]" class="form-control" value="{{$c->name}}"
                       placeholder="{{__('system.name')}}">
            </td>
        </tr>
    @endforeach
</table>

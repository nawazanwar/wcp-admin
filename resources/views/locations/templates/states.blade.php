<div class="row">
    <div class="col-12 text-right p-3">
        <a class="btn btn-success btn-xs text-white cursor-pointer" onclick="add('state');">
            Add
        </a>
    </div>
</div>
<table class="table table-bordered" id="stateTable">
    @php
        $states = \App\Location\State::orderby('id','desc')->get();
    @endphp
    @foreach($states as $s)
        <tr class="my-2 bg-white">
            <td>
                <input type="text" name="settings[states][]" class="form-control" value="{{$s->name}}"
                       placeholder="{{__('system.name')}}">
            </td>
        </tr>
    @endforeach
</table>

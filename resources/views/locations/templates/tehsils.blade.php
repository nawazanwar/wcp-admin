<div class="row">
    <div class="col-12 text-right p-3">
        <a class="btn btn-success btn-xs text-white cursor-pointer" onclick="add('tehsil');">
            Add
        </a>
    </div>
</div>
<table class="table table-bordered" id="tehsilTable">
    @php
        $tehsils = \App\Location\Tehsil::orderby('id','desc')->get();
    @endphp
    @foreach($tehsils as $t)
        <tr class="my-2 bg-white">
            <td>
                <input type="text" name="settings[tehsils][]" class="form-control" value="{{$t->name}}"
                       placeholder="{{__('system.name')}}">
            </td>
        </tr>
    @endforeach
</table>

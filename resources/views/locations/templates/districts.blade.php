<div class="row">
    <div class="col-12 text-right p-3">
        <a class="btn btn-success btn-xs text-white cursor-pointer" onclick="add('district');">
            Add
        </a>
    </div>
</div>
<table class="table table-bordered" id="districtTable">
    @php
        $districts = \App\Location\District::orderby('id','desc')->get();
    @endphp
    @foreach($districts as $d)
        <tr class="my-2 bg-white">
            <td>
                <input type="text" name="settings[districts][]" class="form-control" value="{{$d->name}}"
                       placeholder="{{__('system.name')}}">
            </td>
        </tr>
    @endforeach
</table>

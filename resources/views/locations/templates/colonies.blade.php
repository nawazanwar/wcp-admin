<div class="row">
    <div class="col-12 text-right p-3">
        <a class="btn btn-success btn-xs text-white cursor-pointer" onclick="add('colony');">
            Add
        </a>
    </div>
</div>
<table class="table table-bordered" id="colonyTable">
    @php
        $colonies = \App\Location\Colony::orderby('id','desc')->get();
    @endphp
    @foreach($colonies as $c)
        <tr class="my-2 bg-white">
            <td>
                <input type="text" name="settings[colonies][]" class="form-control" value="{{$c->name}}"
                       placeholder="{{__('system.name')}}">
            </td>
        </tr>
    @endforeach
</table>

@extends('layouts.colorbox')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endsection

@section('content')
    @include('partials.dashboard.toaster')
    <div class="row">
        <div class="col-md-2">
            <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                <a class="nav-link active" id="vert-tabs-home-tab" data-toggle="pill" href="#vert-tabs-1" role="tab"
                   aria-controls="vert-tabs-1" aria-selected="true">Countries</a>
                <a class="nav-link" id="vert-tabs-profile-tab" data-toggle="pill" href="#vert-tabs-2" role="tab"
                   aria-controls="vert-tabs-profile" aria-selected="false">States/Provinces</a>
                <a class="nav-link" id="vert-tabs-messages-tab" data-toggle="pill" href="#vert-tabs-3" role="tab"
                   aria-controls="vert-tabs-messages" aria-selected="false">Districts</a>
                <a class="nav-link" id="vert-tabs-messages-tab" data-toggle="pill" href="#vert-tabs-4"
                   role="tab"
                   aria-controls="vert-tabs-messages" aria-selected="false">Cities</a>
                <a class="nav-link" id="vert-tabs-messages-tab" data-toggle="pill" href="#vert-tabs-5"
                   role="tab"
                   aria-controls="vert-tabs-messages" aria-selected="false">Tehsils</a>
                <a class="nav-link" id="vert-tabs-messages-tab" data-toggle="pill" href="#vert-tabs-6"
                   role="tab"
                   aria-controls="vert-tabs-messages" aria-selected="false">Villages/Colonies</a>
            </div>
        </div>
        <div class="col-md-10">
            {!! Form::open(['route' => ['location.store'], 'method' => 'POST','files'=>'true']) !!}
            {!! csrf_field() !!}
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-12 text-right p-1">
                    <input type="submit" class="btn btn-xs btn-info" value="Update Settings">
                </div>
            </div>
            <div class="tab-content" id="vert-tabs-tabContent">
                <div class="tab-pane text-left fade active show" id="vert-tabs-1" role="tabpanel"
                     aria-labelledby="vert-tabs-home-tab">
                    @include('locations.templates.countries')
                </div>
                <div class="tab-pane fade" id="vert-tabs-2" role="tabpanel"
                     aria-labelledby="vert-tabs-profile-tab">
                    @include('locations.templates.states')
                </div>
                <div class="tab-pane fade" id="vert-tabs-3" role="tabpanel"
                     aria-labelledby="vert-tabs-messages-tab">
                    @include('locations.templates.districts')
                </div>
                <div class="tab-pane fade" id="vert-tabs-4" role="tabpanel"
                     aria-labelledby="vert-tabs-messages-tab">
                    @include('locations.templates.cities')
                </div>
                <div class="tab-pane fade" id="vert-tabs-5" role="tabpanel"
                     aria-labelledby="vert-tabs-messages-tab">
                    @include('locations.templates.tehsils')
                </div>
                <div class="tab-pane fade" id="vert-tabs-6" role="tabpanel"
                     aria-labelledby="vert-tabs-messages-tab">
                    @include('locations.templates.colonies')
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('pageScript')
    <script>
        function add(type) {
            switch (type) {
                case "country":
                    addCountry();
                    break;
                case "state":
                    addState();
                    break;
                case "district":
                    addDistrict();
                    break;
                case "city":
                    addCity();
                    break;
                case "tehsil":
                    addTehsil();
                    break;
                case "colony":
                    addColony();
                    break;
            }
        }

        /* addCountry */

        function addCountry() {
            var uId = 'id' + (new Date()).getTime();
            var cloned_row = $("#countryTable").find('tr:first-child').clone();
            cloned_row.find('input[type=hidden]').attr('id', uId).val(uId);
            cloned_row.find('input[type=text]').val('').attr('placeholder', 'Start Writing').removeAttr('id');
            $("#countryTable").prepend(cloned_row);
        }

        /* addState */

        function addState() {
            var uId = 'id' + (new Date()).getTime();
            var cloned_row = $("#stateTable").find('tr:first-child').clone();
            cloned_row.find('input[type=hidden]').attr('id', uId).val(uId);
            cloned_row.find('input[type=text]').val('').attr('placeholder', 'Start Writing').removeAttr('id');
            $("#stateTable").prepend(cloned_row);
        }

        /* addDistrict */

        function addDistrict() {
            var uId = 'id' + (new Date()).getTime();
            var cloned_row = $("#districtTable").find('tr:first-child').clone();
            cloned_row.find('input[type=hidden]').attr('id', uId).val(uId);
            cloned_row.find('input[type=text]').val('').attr('placeholder', 'Start Writing').removeAttr('id');
            $("#districtTable").prepend(cloned_row);
        }

        /* addCity */

        function addCity() {
            var uId = 'id' + (new Date()).getTime();
            var cloned_row = $("#cityTable").find('tr:first-child').clone();
            cloned_row.find('input[type=hidden]').attr('id', uId).val(uId);
            cloned_row.find('input[type=text]').val('').attr('placeholder', 'Start Writing').removeAttr('id');
            $("#cityTable").prepend(cloned_row);
        }

        /* addTehsil */

        function addTehsil() {
            var uId = 'id' + (new Date()).getTime();
            var cloned_row = $("#tehsilTable").find('tr:first-child').clone();
            cloned_row.find('input[type=hidden]').attr('id', uId).val(uId);
            cloned_row.find('input[type=text]').val('').attr('placeholder', 'Start Writing').removeAttr('id');
            $("#tehsilTable").prepend(cloned_row);
        }

        /* addColony */

        function addColony() {
            var uId = 'id' + (new Date()).getTime();
            var cloned_row = $("#colonyTable").find('tr:first-child').clone();
            cloned_row.find('input[type=hidden]').attr('id', uId).val(uId);
            cloned_row.find('input[type=text]').val('').attr('placeholder', 'Start Writing').removeAttr('id');
            $("#colonyTable").prepend(cloned_row);
        }
    </script>
@endsection

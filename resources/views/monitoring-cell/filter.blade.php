<section>
    <article class="card-group-item">
        <header class="card-header">
            <h6 class="title">Filter BY all Fields</h6>
        </header>
        <div class="filter-content">
            <div class="card-body">
                {!! Form::open(array('url' =>route('dashboard'),'method' => 'GET')) !!}
                <div class="form-group">
                    {!! Form::label('filter_by_all_fields','Filter BY all Fields') !!}
                    <div class="input-group input-group-sm">
                        {!! Form::text('s',request()->has('s')?request()->get('s'):null,['class'=>'form-control form-control-sm','placeholder'=>'']) !!}
                        <div class="input-group-append">
                            <div class="btn btn-primary">
                                <i class="fas fa-search"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('filter_by_gate','Filter BY Gate') !!}
                    <div class="input-group input-group-sm">
                        {!! Form::select('gate',WcpHelper::allGates(),request()->has('gate')?request()->get('gate'):null,['class'=>'form-control form-control-sm','placeholder'=>'Please choose a Gate']) !!}
                        <div class="input-group-append">
                            <div class="btn btn-primary">
                                <i class="fas fa-search"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('filter_by_booth','Filter BY Booth') !!}
                    <div class="input-group input-group-sm">
                        {!! Form::select('booth',WcpHelper::allBoothLists(),request()->has('booth')?request()->get('booth'):null,['class'=>'form-control form-control-sm','placeholder'=>'Please choose a Booth']) !!}
                        <div class="input-group-append">
                            <div class="btn btn-primary">
                                <i class="fas fa-search"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-right">
                    {!! Form::submit('Filter',['class'=>'btn btn-primary btn-sm']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </article>
</section>

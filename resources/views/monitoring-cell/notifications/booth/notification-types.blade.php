<div class="card">
    <div class="card-header">
        <h3 class="card-title">Types</h3>
    </div>
    <div class="card-body p-0">
        <ul class="nav nav-pills flex-column">
            <li class="nav-item  @if(request()->for=='all') active-notification @endif">
                <a href="{{ route('booth-notifications',['all',request()->type,request()->boothId]) }}"
                   class="nav-link">
                    All <i class="fas fa-chevron-circle-right float-right"></i>
                </a>
            </li>
            <li class="nav-item  @if(request()->for=='visitee') active-notification @endif">
                <a href="{{ route('booth-notifications',['visitee',request()->type,request()->boothId]) }}"
                   class="nav-link">
                    Visitee <i class="fas fa-chevron-circle-right float-right"></i>
                </a>
            </li>
            <li class="nav-item  @if(request()->for=='verified') active-notification @endif">
                <a href="{{ route('booth-notifications',['verified',request()->type,request()->boothId]) }}"
                   class="nav-link">
                    Verified <i class="fas fa-chevron-circle-right float-right"></i>
                </a>
            </li>
            <li class="nav-item  @if(request()->for=='follow') active-notification @endif">
                <a href="{{ route('booth-notifications',['follow',request()->type,request()->boothId]) }}"
                   class="nav-link">
                    Follow <i class="fas fa-chevron-circle-right float-right"></i>
                </a>
            </li>
            <li class="nav-item  @if(request()->for=='guide') active-notification @endif">
                <a href="{{ route('booth-notifications',['guide',request()->type,request()->boothId]) }}"
                   class="nav-link">
                    Guide <i class="fas fa-chevron-circle-right float-right"></i>
                </a>
            </li>
        </ul>
    </div>
</div>

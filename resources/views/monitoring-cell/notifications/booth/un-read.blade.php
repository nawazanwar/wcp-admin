@extends('layouts.monitor-cell')
@section('pageTitle', $pageTitle)
@section('content')

    <div class="row my-2">
        <div class="col-12">
            <a class="btn btn-xs btn-info text-white cursor-pointer" href="{{ route('dashboard') }}"> <i
                    class="fa fa-arrow-circle-left"></i> Back to Dashboard </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 my-3">
            @include('monitoring-cell.notifications.booth.notification-types')
        </div>
        <div class="col-md-9 my-3">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title">{!! $pageTitle !!}</h3>
                    <div class="card-tools">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" placeholder="Search Notification">
                            <div class="input-group-append">
                                <div class="btn btn-primary">
                                    <i class="fas fa-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive mailbox-messages">
                        <table class="table table-bordered">
                            <tbody>
                            @if(count($notifications)>0)
                                @foreach($notifications as $notification)

                                    <tr class="py-3">
                                        <td class="mailbox-subject">
                                            @php
                                                $data = $notification->data;
                                            @endphp
                                            @php
                                                $bellHeading =null;
                                                switch ($notification->type){
                                                    case "App\Notifications\GuideVisitorNotificationInBooth":
                                                        $bellHeading='Guide';
                                                    break;
                                                    case "App\Notifications\FollowVisitorNotificationInBooth":
                                                        $bellHeading='Follow';
                                                    break;
                                                    case "App\Notifications\CallVisiteeNotificationInBooth":
                                                        $bellHeading='Call Visitee';
                                                    break;
                                                    case "App\Notifications\VerifiedVisitorNotificationInBooth":
                                                        $bellHeading='Verified';
                                                    break;
                                                }

                                            @endphp


                                            <a class="text-success fs-15 font-weight-bold">
                                                {{ WcpHelper::getUserNameById($data['operator_id']) }}</a>
                                            Says: <span
                                                class="btn btn-xs btn-success pull-right">{{ $bellHeading }} <i
                                                    class="fa fa-bell"></i>
                                            </span>
                                            <span
                                                class="btn btn-xs bg-info mx-2">{{ date('d-m-Y h:i A', strtotime($data['created_at'])) }} <i
                                                    class="fa fa-clock"></i>
                                               </span>
                                            <p class="my-2">
                                                @php
                                                    $messageType = null;
                                                    switch ($notification->type){
                                                        case "App\Notifications\GuideVisitorNotificationInBooth":
                                                            $messageType='visitor_guide_at_booth_notification';
                                                        break;
                                                        case "App\Notifications\FollowVisitorNotificationInBooth":
                                                            $messageType='visitor_follow_at_booth_notification';
                                                        break;
                                                        case "App\Notifications\CallVisiteeNotificationInBooth":
                                                            $messageType='call_visitee_at_booth_notification';
                                                        break;
                                                        case "App\Notifications\VerifiedVisitorNotificationInBooth":
                                                            $messageType='visitor_verified_at_booth_notification';
                                                        break;
                                                    }
                                                    $message = WcpHelper::getMessageByName($messageType);
                                                    $message = str_replace("%visitor_name%", $data['visitor_name'], $message);
                                                    $message = str_replace("%vehicle_number%", $data['vehicle_reg_no'], $message);
                                                @endphp
                                                {!! $message !!}
                                            </p>
                                        </td>
                                        <td class="text-center position-relative" style="width: 118px;">
                                            <a class="btn btn-xs btn-info text-white cursor-pointer"
                                               data-toggle="modal"
                                               data-target="#mark-as-paid-{{$notification->id}}-holder">
                                                Mark as Read <i class="fa fa-plus-circle"></i>
                                            </a>
                                            <div class="modal" id="mark-as-paid-{{$notification->id}}-holder">
                                                {!! Form::open(array('url' => route('booth-read-notifications'),'onsubmit'=>'readNotifications(this);return false;'))!!}
                                                @csrf
                                                <div class="modal-dialog">
                                                    {!! Form::hidden('id',$notification->id) !!}
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Mark as Read</h4>
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">&times;
                                                            </button>
                                                        </div>
                                                        <div class="modal-body text-left">
                                                            <div class="form-group">
                                                                {!! Form::label('enter-reason','Reason for Reading') !!}
                                                                {!! Form::textarea('reason',null,['class'=>'form-control','rows'=>3]) !!}
                                                            </div>
                                                            <div class="form-group text-right">
                                                                {!! Form::submit('Save',['class'=>'btn btn-primary btn-sm']) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="12">No Notification to Read</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        <!-- /.table -->
                    </div>
                </div>
                @if(method_exists($notifications, 'links'))
                    <div class="card-footer text-right">
                        {{ $notifications->links() }}
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('pageScript')
    <script>
        function readNotifications(cForm = null) {
            var formData = $(cForm).serializeArray();
            var route = $(cForm).attr('action');
            Ajax.call(route, formData, 'post', function () {
                location.reload();
            });
        }

        $(document).ready(function () {

            setInterval(function () {
                Ajax.call('{{ route('booth-notifications',[$for,'un-read',$boothId]) }}', {'isAjax': true}, 'GET', function (response) {

                    console.log(response);

                });

            }, 5000);
        })
    </script>
@endsection

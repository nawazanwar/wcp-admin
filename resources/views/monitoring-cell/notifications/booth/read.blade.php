@extends('layouts.monitor-cell')
@section('pageTitle', $pageTitle)
@section('content')
    <div class="row my-2">
        <div class="col-12">
            <a class="btn btn-xs btn-info text-white cursor-pointer" href="{{ route('dashboard') }}"> <i class="fa fa-arrow-circle-left"></i> Back to Dashboard </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 my-3">
            @include('monitoring-cell.notifications.booth.notification-types')
        </div>
        <div class="col-md-9 my-3">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title">{!! $pageTitle !!}</h3>
                    <div class="card-tools">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" placeholder="Search Notification">
                            <div class="input-group-append">
                                <div class="btn btn-primary">
                                    <i class="fas fa-search"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive mailbox-messages">
                        <table class="table table-bordered">
                            <tbody>
                            @if(count($notifications)>0)
                                @foreach($notifications as $notification)

                                    <tr class="py-3">
                                        <td class="mailbox-subject">
                                            @php
                                                $data = json_decode($notification->data,TRUE);
                                            @endphp
                                            @php
                                                $bellHeading =null;
                                                switch ($notification->type){
                                                    case "App\Notifications\GuideVisitorNotificationInBooth":
                                                        $bellHeading='Guide';
                                                    break;
                                                    case "App\Notifications\FollowVisitorNotificationInBooth":
                                                        $bellHeading='Follow';
                                                    break;
                                                    case "App\Notifications\CallVisiteeNotificationInBooth":
                                                        $bellHeading='Call Visitee';
                                                    break;
                                                    case "App\Notifications\VerifiedVisitorNotificationInBooth":
                                                        $bellHeading='Verified';
                                                    break;
                                                }
                                            @endphp
                                            <a class="text-success fs-15 font-weight-bold">
                                                {{ WcpHelper::getUserNameById($data['operator_id']) }}</a>
                                            Says:   <span
                                                class="btn btn-xs btn-success pull-right">{{ $bellHeading }} <i
                                                    class="fa fa-bell"></i></span>
                                            <span
                                                class="btn btn-xs bg-info mx-2">{{ date('d-m-Y h:i A', strtotime($data['created_at'])) }} <i
                                                    class="fa fa-clock"></i>
                                               </span>
                                            <p class="my-2">
                                                @php
                                                    $messageType = null;
                                                    switch ($notification->type){
                                                        case "App\Notifications\GuideVisitorNotificationInBooth":
                                                            $messageType='visitor_guide_at_booth_notification';
                                                        break;
                                                        case "App\Notifications\FollowVisitorNotificationInBooth":
                                                            $messageType='visitor_follow_at_booth_notification';
                                                        break;
                                                        case "App\Notifications\CallVisiteeNotificationInBooth":
                                                            $messageType='call_visitee_at_booth_notification';
                                                        break;
                                                        case "App\Notifications\VerifiedVisitorNotificationInBooth":
                                                            $messageType='visitor_verified_at_booth_notification';
                                                        break;
                                                    }
                                                    $message = WcpHelper::getMessageByName($messageType);
                                                    $message = str_replace("%visitor_name%", $data['visitor_name'], $message);
                                                    $message = str_replace("%vehicle_number%", $data['vehicle_reg_no'], $message);
                                                @endphp
                                                {!! $message !!}
                                            </p>
                                            <p>
                                                <strong>Read By :</strong> {{ \App\Models\User::find($notification->reader_id)->name }}
                                                <br>
                                                <strong>Read Date :</strong> {{ $notification->read_at }}
                                                <br>
                                                <strong>Read Reason :</strong> {!! $notification->read_reason !!}
                                            </p>
                                        </td>
                                    </tr>

                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="12">Still no notification Read</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                @if(method_exists($notifications, 'links'))
                    <div class="card-footer text-right">
                        {{ $notifications->links() }}
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

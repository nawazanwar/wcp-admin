@extends('layouts.monitor-cell')
@section('pageTitle', $pageTitle)
@section('content')
    <div class="row my-2">
        <div class="col-12">
            <a class="btn btn-xs btn-info text-white cursor-pointer" href="{{ route('dashboard') }}"> <i class="fa fa-arrow-circle-left"></i> Back to Dashboard </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title">{!! $pageTitle !!}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        @include('booths.devices',['for'=>'monitoring-cell'])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

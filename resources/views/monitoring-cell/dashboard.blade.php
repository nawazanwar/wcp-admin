@extends('layouts.monitor-cell')
@section('pageTitle', $pageTitle)
@section('styleInnerFiles')
@endsection
@section('content')
    <div class="row my-4">
        <div class="col-md-3">
            <div class="card card-primary card-outline">
                @include('monitoring-cell.filter')
            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{ __('booth.all') }}</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{__('booth.title')}}</th>
                            <th class="text-center">{{__('system.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($booths as $booth)
                            <tr>
                                <td>
                                    {{ $booth->gate }} {{ ucfirst($booth->lane) }}  {{ ucfirst($booth->type) }}
                                </td>
                                <td class="text-center pt-3">
                                    <a class="btn btn-app"
                                       href="{{ route('booth-visitor-logs',[$booth->id]) }}">
                                        <span class="badge bg-success"
                                              id="visitors_count_{{$booth->id}}">{{ $data[$booth->id]['visitors'] }}</span>
                                        <i class="fas fa-user"></i>All Visitors
                                    </a>
                                    <a class="btn btn-app"
                                       href="{{ route('booth-cameras',[$booth->id]) }}">
                                        <span class="badge bg-gradient-pink">{{ $data[$booth->id]['cameras'] }}</span>
                                        <i class="fas fa-camera"></i>{{__('booth.cameras')}}
                                    </a>
                                    <a class="btn btn-app"
                                       href="{{ route('booth-notifications',['all','un-read',$booth->id]) }}">
                                        <span class="badge bg-warning"
                                              id="un_read_notifications_count_{{$booth->id}}">{{ $data[$booth->id]['unread'] }}</span>
                                        <i class="fas fa-bullhorn"></i>Unread Notifications
                                    </a>
                                    <a class="btn btn-app"
                                       href="{{ route('booth-notifications',['all','read',$booth->id]) }}">
                                        <span class="badge bg-success"
                                              id="read_notifications_count_{{$booth->id}}">{{ $data[$booth->id]['read'] }}</span>
                                        <i class="fas fa-bullhorn"></i>Marked/Read Notifications
                                    </a>
                                    <a class="btn btn-app"
                                       href="{{ route('booth-images',[$booth->id]) }}">
                                        <i class="fas fa-eye"></i> All Camera Images
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr class="text-center">
                                <td colspan="12" class="p-3">{{__('vms.no_booth_found')}}</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection
@section('scriptInnerFiles')
@endsection
@section('pageScript')
    <script>
        window.setInterval(function () {
            Ajax.call('{{ route('booth-notifications-count') }}', null, 'GET', function (response) {
                for (d in response.data) {
                    $("#un_read_notifications_count_" + d).empty().text(response.data[d].unread);
                    $("#read_notifications_count_" + d).empty().text(response.data[d].read);
                    $("#visitors_count_" + d).empty().text(response.data[d].visitors);
                }
            });
        }, 5000);
    </script>
@endsection

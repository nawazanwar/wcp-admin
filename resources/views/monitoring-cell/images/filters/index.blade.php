<section>
    <article class="card-group-item">
        <header class="card-header text-center">
            <h6 class="title">Apply Filtration's <i class="fa fa-filter text-success"></i></h6>
        </header>
        <div class="filter-content">
            <div class="card-body">
                {{-- Filter By Vehcile Number--}}
                @include('monitoring-cell.images.filters.vehicle-number')
                {{-- Fiter By Visitor Id Card--}}
                @include('monitoring-cell.images.filters.visitor-id-card')
                {{-- Filter By Vehcile Color--}}
                @include('monitoring-cell.images.filters.vehicle-color')
                {{-- Filter By Visitor Name--}}
                @include('monitoring-cell.images.filters.visitor-name')
            </div>
        </div>
    </article>
</section>

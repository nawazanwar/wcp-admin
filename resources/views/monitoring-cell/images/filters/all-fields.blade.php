<div class="form-group">
    {!! Form::open(array('url' =>route('booth-visitor-logs',[$boothId]),'method' => 'GET')) !!}
    {!! Form::label('filter_by_all_fields','Filter BY all Fields') !!}
    <div class="input-group input-group-sm">
        {!! Form::text('s',request()->has('s')?request()->get('s'):null,['class'=>'form-control form-control-sm','placeholder'=>'Enter any Field']) !!}
        <div class="input-group-append">
            <button class="btn bg-gradient-success">
                <i class="fas fa-search"></i>
            </button>
        </div>
    </div>
    {!! Form::close() !!}
</div>

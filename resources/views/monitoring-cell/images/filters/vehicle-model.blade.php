<div class="form-group">
    {!! Form::open(array('url' =>route('booth-images',[$boothId]),'method' => 'GET')) !!}
    {!! Form::label('filter_by_vehicle_model','Filter BY Vehicle Model') !!}
    <div class="input-group input-group-sm">
        {!! Form::text('vehicle_model',request()->has('vehicle_model')?request()->get('vehicle_model'):null,['class'=>'form-control form-control-sm','placeholder'=>'Any letter of Vehicle Model']) !!}
        <div class="input-group-append">
            <button class="btn bg-gradient-success">
                <i class="fas fa-search"></i>
            </button>
        </div>
    </div>
    {!! Form::close() !!}
</div>

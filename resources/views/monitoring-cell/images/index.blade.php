@extends('layouts.monitor-cell')
@section('pageTitle', $pageTitle)
@section('content')
    <div class="row my-2">
        <div class="col-12">
            <a class="btn btn-xs btn-info text-white cursor-pointer" href="{{ route('dashboard') }}"> <i
                    class="fa fa-arrow-circle-left"></i> Back to Dashboard </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="card card-primary card-outline">
                @include('monitoring-cell.images.filters.index')
            </div>
        </div>
        <div class="col-md-9">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title">{!! $pageTitle !!}</h3>
                    <div class="card-tools">
                        {!! Form::open(array('url' =>route('booth-images',[$boothId]),'method' => 'GET')) !!}
                        <div class="input-group input-group-sm">
                            {!! Form::date('d',request()->has('d')?request()->get('d'):null,['class'=>'form-control form-control-sm','placeholder'=>'Filter by Date']) !!}
                            <div class="input-group-append">
                                <button type="submit" class="btn bg-gradient-success cursor-pointer">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        @forelse($visitors as $v)
                            <div class="col-md-6">
                                <div class="card ">
                                    <div class="card-header text-center bg-white" style="border-bottom: none;">
                                        <strong class="text-info text-left"> Visitor Name: </strong> {{ ($v->visitor_name)?$v->visitor_name:'?' }}
                                        <hr class="bg-light my-1 w-50">
                                        <strong class="text-info text-left"> Visitor Id Card: </strong> {{ ($v->visitor_cnic)?$v->visitor_cnic:'?' }}
                                        <hr class="bg-light my-1 w-50">
                                        <div class="small font-weight-normal">
                                            {{ Carbon\Carbon::parse($v->created_at)->format('d M y / l') }}
                                            <hr class="bg-light my-1 w-50">
                                            <div>
                                                {{ Carbon\Carbon::parse($v->created_at)->format('h:i:s A') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-6 my-2">
                                                @if($v->first_camera_image)
                                                    <img src="{{ $v->first_camera_image }}" class="img-thumbnail h-240px">
                                                @else
                                                    <img src="{{ asset('img/booth/no-capture.jpg') }}"
                                                         class="img-thumbnail h-240px">
                                                @endif
                                            </div>
                                            <div class="col-6 my-2">
                                                @if($v->second_camera_image)
                                                    <img src="{{ $v->second_camera_image }}" class="img-thumbnail h-240px">
                                                @else
                                                    <img src="{{ asset('img/booth/no-capture.jpg') }}"
                                                         class="img-thumbnail h-240px">
                                                @endif
                                            </div>
                                            <div class="col-6 my-2">
                                                @if($v->third_camera_image)
                                                    <img src="{{ $v->third_camera_image }}" class="img-thumbnail h-240px">
                                                @else
                                                    <img src="{{ asset('img/booth/no-capture.jpg') }}"
                                                         class="img-thumbnail h-240px">
                                                @endif
                                            </div>
                                            <div class="col-6 my-2">
                                                @if($v->fourth_camera_image)
                                                    <img src="{{ $v->fourth_camera_image }}" class="img-thumbnail h-240px">
                                                @else
                                                    <img src="{{ asset('img/booth/no-capture.jpg') }}"
                                                         class="img-thumbnail h-240px">
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-header border-top border-bottom">
                                        <h6 class="pull-left"><strong class="text-info"> Vehicle No: </strong>{{ $v->vehicle_reg_no }}</h6>
                                        <a class="pull-right btn btn-xs btn-info cursor-pointer"
                                           href="{{ route('visitors.edit',[$v->id]) }}" target="_blank">
                                            <i class="fa fa-edit text-white"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="col-12 text-center p-4">
                                No Images Found
                            </div>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

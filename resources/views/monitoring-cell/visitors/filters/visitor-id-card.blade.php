<div class="form-group">
    {!! Form::open(array('url' =>route('booth-visitor-logs',[$boothId]),'method' => 'GET')) !!}
    {!! Form::label('filter_by_id_card','Filter BY Visitor Id Card') !!}
    <div class="input-group input-group-sm">
        {!! Form::text('id_card',request()->has('id_card')?request()->get('id_card'):null,['class'=>'form-control form-control-sm','data-inputmask'=>"'mask': '99999-9999999-9'"]) !!}
        <div class="input-group-append">
            <button class="btn bg-gradient-success">
                <i class="fas fa-search"></i>
            </button>
        </div>
    </div>
    {!! Form::close() !!}
</div>

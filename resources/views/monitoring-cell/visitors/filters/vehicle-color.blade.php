<div class="form-group">
    {!! Form::open(array('url' =>route('booth-visitor-logs',[$boothId]),'method' => 'GET')) !!}
    {!! Form::label('vehicle_color','Filter BY Vehicle Color') !!}
    <div class="input-group input-group-sm">
        {!! Form::text('vehicle_color',request()->has('vehicle_color')?request()->get('vehicle_color'):null,['id'=>'vehicle_color','class'=>'form-control form-control-sm','placeholder'=>'Any letter of Vehicle color']) !!}
        <div class="input-group-append">
            <button class="btn bg-gradient-success">
                <i class="fas fa-search"></i>
            </button>
        </div>
    </div>
    {!! Form::close() !!}
</div>

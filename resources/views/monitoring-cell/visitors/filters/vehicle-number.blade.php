<div class="form-group">
    {!! Form::open(array('url' =>route('booth-visitor-logs',[$boothId]),'method' => 'GET')) !!}
    {!! Form::label('filter_by_vehicle_number','Filter BY Vehicle Number') !!}
    <div class="input-group input-group-sm">
        {!! Form::text('vehicle_number',request()->has('vehicle_number')?request()->get('vehicle_number'):null,['class'=>'form-control form-control-sm','placeholder'=>'Enter any letter of number']) !!}
        <div class="input-group-append">
            <button class="btn bg-gradient-success">
                <i class="fas fa-search"></i>
            </button>
        </div>
    </div>
    {!! Form::close() !!}
</div>

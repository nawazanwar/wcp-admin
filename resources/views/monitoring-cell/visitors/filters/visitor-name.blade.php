<div class="form-group">
    {!! Form::open(array('url' =>route('booth-visitor-logs',[$boothId]),'method' => 'GET')) !!}
    {!! Form::label('filter_by_visitor_name','Filter BY Visitor Name') !!}
    <div class="input-group input-group-sm">
        {!! Form::text('visitor_name',request()->has('visitor_name')?request()->get('visitor_name'):null,['class'=>'form-control form-control-sm','placeholder'=>'Any Letter of Visitor Name']) !!}
        <div class="input-group-append">
            <button class="btn bg-gradient-success">
                <i class="fas fa-search"></i>
            </button>
        </div>
    </div>
    {!! Form::close() !!}
</div>

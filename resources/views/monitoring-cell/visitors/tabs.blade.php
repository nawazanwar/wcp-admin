<ul class="nav nav-tabs mx-3 mt-3" role="tablist">
    <li class="nav-item">
        <a class="nav-link active fs-13 px-2" data-toggle="pill" href="#vehicle-tab-{{$v->id}}"
           role="tab">{{__('booth.visitor.tabs.vehicle')}}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link fs-13 px-2" data-toggle="pill" href="#verifications-tab-{{$v->id}}"
           role="tab">{{__('booth.visitor.tabs.verifications')}}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link fs-13 px-2" data-toggle="pill" href="#actions-tab-{{$v->id}}"
           role="tab">{{__('booth.visitor.tabs.actions')}}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link fs-13 px-2" data-toggle="pill" href="#image-tab-{{$v->id}}"
           role="tab">Image</a>
    </li>
</ul>

<div class="tab-content mx-3 mb-3" style="border: 1px solid #dfdfdf;border-top: 0;">
    <div class="tab-pane fade p-2" id="image-tab-{{$v->id}}" role="tabpanel">
        @include('monitoring-cell.visitors.components.images')
    </div>
    <div class="tab-pane fade active show p-2" id="vehicle-tab-{{$v->id}}" role="tabpanel">
        @include('monitoring-cell.visitors.components.vehicle')
    </div>
    <div class="tab-pane fade p-2" id="verifications-tab-{{$v->id}}" role="tabpanel">
        @include('monitoring-cell.visitors.components.verifications')
    </div>
    <div class="tab-pane fade p-2" id="actions-tab-{{$v->id}}" role="tabpanel">
        @include('monitoring-cell.visitors.components.actions')
    </div>
</div>

<ul>
    <li>
        <strong class="fs-13 text-info">{{__('booth.visitor.verifications.biometric')}}</strong>
        @isset($v->visitor_biometric_verification)
            @if($v->visitor_biometric_verification)
                <span class="badge badge-success pull-right"><i class="fa fa-check"></i></span>
            @else
                <span class="badge badge-danger pull-right"><i class="fa fa-times"></i></span>
            @endif
        @else
            <span class="badge pull-right">--</span>
        @endif
    </li>
    <li>
        <strong class="fs-13 text-info">{{__('booth.visitor.verifications.ratina')}}</strong>
        @isset($v->visitor_ratina_verification)
            @if($v->visitor_ratina_verification)
                <span class="badge badge-success pull-right"><i class="fa fa-check"></i></span>
            @else
                <span class="badge badge-danger pull-right"><i class="fa fa-times"></i></span>
            @endif
        @else
            <span class="badge pull-right">--</span>
        @endif
    </li>
    <li>
        <strong class="fs-13 text-info">{{__('booth.visitor.verifications.gate_pass')}}</strong>
        @isset($v->visitor_gate_pass_verification)
            @if($v->visitor_gate_pass_verification)
                <span class="badge badge-success pull-right"><i class="fa fa-check"></i></span>
            @else
                <span class="badge badge-danger pull-right"><i class="fa fa-times"></i></span>
            @endif
        @else
            <span class="badge pull-right">--</span>
        @endif
    </li>
    <li>
        <strong class="fs-13 text-info">{{__('booth.visitor.verifications.qrcode')}}</strong>
        @isset($v->visitor_qrcode_verification)
            @if($v->visitor_qrcode_verification)
                <span class="badge badge-success pull-right"><i class="fa fa-check"></i></span>
            @else
                <span class="badge badge-danger pull-right"><i class="fa fa-times"></i></span>
            @endif
        @else
            <span class="badge pull-right">--</span>
        @endif
    </li>
</ul>

<div class="row">
    <div class="col-12 text-center">
        <h6>List of all Camera Images</h6>
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active fs-13 px-2" data-toggle="pill" href="#image-camera-1-{{$v->id}}"
                   role="tab">First</a>
            </li>
            <li class="nav-item">
                <a class="nav-link fs-13 px-2" data-toggle="pill" href="#image-camera-2-{{$v->id}}"
                   role="tab">Second</a>
            </li>
            <li class="nav-item">
                <a class="nav-link fs-13 px-2" data-toggle="pill" href="#image-camera-3-{{$v->id}}"
                   role="tab">Third</a>
            </li>
            <li class="nav-item">
                <a class="nav-link fs-13 px-2" data-toggle="pill" href="#image-camera-4-{{$v->id}}"
                   role="tab">Fourth</a>
            </li>
        </ul>
        <div class="tab-content my-2">
            <div class="tab-pane fade p-2 show active" id="image-camera-1-{{$v->id}}" role="tabpanel">
                @if($v->first_camera_image)
                    <img src="{{ $v->first_camera_image }}" class="img-thumbnail h-240px img-fluid">
                @else
                    <img src="{{ asset('img/booth/no-capture.jpg') }}"
                         class="img-thumbnail h-240px">
                @endif
            </div>
            <div class="tab-pane fade p-2" id="image-camera-3-{{$v->id}}" role="tabpanel">
                @if($v->second_camera_image)
                    <img src="{{ $v->second_camera_image }}" class="img-thumbnail h-240px img-fluid">
                @else
                    <img src="{{ asset('img/booth/no-capture.jpg') }}"
                         class="img-thumbnail h-240px">
                @endif
            </div>
            <div class="tab-pane fade p-2" id="image-camera-4-{{$v->id}}" role="tabpanel">
                @if($v->third_camera_image)
                    <img src="{{ $v->third_camera_image }}" class="img-thumbnail h-240px img-fluid">
                @else
                    <img src="{{ asset('img/booth/no-capture.jpg') }}"
                         class="img-thumbnail h-240px">
                @endif
            </div>
            <div class="tab-pane fade p-2" id="image-camera-2-{{$v->id}}" role="tabpanel">
                @if($v->fourth_camera_image)
                    <img src="{{ $v->fourth_camera_image }}" class="img-thumbnail h-240px img-fluid">
                @else
                    <img src="{{ asset('img/booth/no-capture.jpg') }}"
                         class="img-thumbnail h-240px">
                @endif
            </div>
        </div>
    </div>
</div>

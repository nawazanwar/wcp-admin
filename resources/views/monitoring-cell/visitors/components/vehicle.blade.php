<ul>
    <li>
        <strong class="fs-13 text-info">{{__('booth.visitor.vehicle.type')}}</strong>
        <span class="badge pull-right text-muted text-center">
                    @isset($v->type_id)
                {{ $v->type->name}}
            @else
                --
            @endisset
                </span>
    </li>
    <li>
        <strong class="fs-13 text-info">{{__('booth.visitor.vehicle.company')}}</strong>
        <span class="badge pull-right text-muted text-center">
                    @isset($v->vcompany_id)
                {{ $v->vcompany->name}}
            @else
                --
            @endisset
                </span>
    </li>
    <li>
        <strong class="fs-13 text-info">{{__('booth.visitor.vehicle.model')}}</strong>
        <span class="badge pull-right text-muted text-center">
                @isset($v->vmodel_id)
                {{ $v->vmodel->name}}
            @else
                --
            @endisset
                </span>
    </li>
    <li>
        <strong class="fs-13 text-info">{{__('booth.visitor.vehicle.color')}}</strong>
        <span class="badge pull-right text-muted text-center">
                @isset($v->vehicle_color)
                {{ $v->vehicle_color }}
            @else
                --
            @endisset
                </span>
    </li>
    <li>
        <strong class="fs-13 text-info">{{__('booth.visitor.vehicle.reg_no')}}</strong>
        <span class="badge pull-right text-muted text-center">
                @isset($v->vehicle_reg_no)
                {{ $v->vehicle_reg_no }}
            @else
                --
            @endisset
                </span>
    </li>
    <li>
        <strong class="fs-13 text-info">{{__('booth.visitor.vehicle.reg_year')}}</strong>
        <span class="badge pull-right text-muted text-center">
                @isset($v->vehicle_reg_year)
                {{ $v->vehicle_reg_year }}
            @else
                --
            @endisset
                </span>
    </li>
    <li>
        <strong class="fs-13 text-info">{{__('booth.visitor.vehicle.males')}}</strong>
        <span class="badge pull-right text-muted text-center">
                @isset($v->no_of_males)
                {{ $v->no_of_males }}
            @else
                --
            @endisset
                </span>
    </li>
    <li>
        <strong class="fs-13 text-info">{{__('booth.visitor.vehicle.females')}}</strong>
        <span class="badge pull-right text-muted text-center">
                @isset($v->no_of_females)
                {{ $v->no_of_females }}
            @else
                --
            @endisset
                </span>
    </li>
    <li>
        <strong class="fs-13 text-info">{{__('booth.visitor.vehicle.kids')}}</strong>
        <span class="badge pull-right text-muted text-center">
                @isset($v->no_of_kids)
                {{ $v->no_of_kids }}
            @else
                --
            @endisset
                </span>
    </li>
</ul>

<ul>
    <li>
        <strong class="fs-13 text-info">{{__('booth.visitor.actions.call_visitee')}}</strong>
        @isset($v->call_visitee)
            @if($v->call_visitee)
                <span class="badge badge-success pull-right"><i class="fa fa-check"></i></span>
            @else
                <span class="badge badge-danger pull-right"><i class="fa fa-times"></i></span>
            @endif
        @else
            <span class="badge pull-right">--</span>
        @endif
    </li>
    <li>
        <strong class="fs-13 text-info">{{__('booth.visitor.actions.verified')}}</strong>
        @isset($v->verified)
            @if($v->verified)
                <span class="badge badge-success pull-right"><i class="fa fa-check"></i></span>
            @else
                <span class="badge badge-danger pull-right"><i class="fa fa-times"></i></span>
            @endif
        @else
            <span class="badge pull-right">--</span>
        @endif
    </li>
    <li>
        <strong class="fs-13 text-info">{{__('booth.visitor.actions.follow')}}</strong>
        @isset($v->follow)
            @if($v->follow)
                <span class="badge badge-success pull-right"><i class="fa fa-check"></i></span>
            @else
                <span class="badge badge-danger pull-right"><i class="fa fa-times"></i></span>
            @endif
        @else
            <span class="badge pull-right">--</span>
        @endif
    </li>
    <li>
        <strong class="fs-13 text-info">{{__('booth.visitor.actions.guide')}}</strong>
        @isset($v->guide)
            @if($v->guide)
                <span class="badge badge-success pull-right"><i class="fa fa-check"></i></span>
            @else
                <span class="badge badge-danger pull-right"><i class="fa fa-times"></i></span>
            @endif
        @else
            <span class="badge pull-right">--</span>
        @endif
    </li>
    <li>
        <strong class="fs-13 text-info">{{__('booth.visitor.actions.warm')}}</strong>
        @isset($v->warn)
            @if($v->warn)
                <span class="badge badge-success pull-right"><i class="fa fa-check"></i></span>
            @else
                <span class="badge badge-danger pull-right"><i class="fa fa-times"></i></span>
            @endif
        @else
            <span class="badge pull-right">--</span>
        @endif
    </li>
    <li>
        <strong class="fs-13 text-info">{{__('booth.visitor.actions.permitted')}}</strong>
        @isset($v->permitted)
            @if($v->permitted)
                <span class="badge badge-success pull-right"><i class="fa fa-check"></i></span>
            @else
                <span class="badge badge-danger pull-right"><i class="fa fa-times"></i></span>
            @endif
        @else
            <span class="badge pull-right">--</span>
        @endif
    </li>
    <li>
        <strong class="fs-13 text-info">{{__('booth.visitor.actions.exit')}}</strong>
        @isset($v->exit)
            @if($v->exit)
                <span class="badge badge-success pull-right"><i class="fa fa-check"></i></span>
            @else
                <span class="badge badge-danger pull-right"><i class="fa fa-times"></i></span>
            @endif
        @else
            <span class="badge pull-right">--</span>
        @endif
    </li>
</ul>

@extends('layouts.monitor-cell')
@section('pageTitle', $pageTitle)
@section('styleInnerFiles')
    <link rel='stylesheet' href="{{ asset('plugins/colorpicker/spectrum.css') }}"/>
@endsection
@section('content')
    <div class="row my-3">
        <div class="col-md-6">
            <a class="btn btn-xs btn-info text-white cursor-pointer" href="{{ route('dashboard') }}"> <i
                    class="fa fa-arrow-circle-left"></i> Back to Dashboard </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="card card-primary card-outline">
                @include('monitoring-cell.visitors.filters.index')
            </div>
        </div>
        <div class="col-md-9">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title">{!! $pageTitle !!}</h3>
                    <div class="card-tools">
                        {!! Form::open(array('url' =>route('booth-visitor-logs',[$boothId]),'method' => 'GET')) !!}
                        <div class="input-group input-group-sm">
                            {!! Form::date('d',request()->has('d')?request()->get('d'):null,['class'=>'form-control form-control-sm','placeholder'=>'Filter by Date']) !!}
                            <div class="input-group-append">
                                <button type="submit" class="btn bg-gradient-success cursor-pointer">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="card-body">
                    <div class="row d-flex align-items-stretch">
                        @forelse($visitors as $v)
                            <div class="col-12 col-sm-6 col-md-6">
                                <div class="card bg-light collapsed-card">
                                    <div class="card-header border-top border-bottom">
                                        <h6 class="pull-left">{{ $v->vehicle_reg_no }}</h6>
                                        <a class="pull-right btn btn-xs btn-info cursor-pointer"
                                           href="{{ route('visitors.edit',[$v->id]) }}" target="_blank">
                                            <i class="fa fa-edit text-white"></i>
                                        </a>
                                    </div>
                                    <div class="card-header text-center bg-white" style="border-bottom: none;">
                                        <div class="small font-weight-normal">
                                            {{ Carbon\Carbon::parse($v->created_at)->format('d M y / l') }}
                                            <hr class="bg-light my-1 w-50">
                                            <div>
                                                {{ Carbon\Carbon::parse($v->created_at)->format('h:i:s A') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-header bg-white br-0 pb-1 pt-0">
                                        <ul class="list-group">
                                            <li class="list-group-item py-1">
                                                <span class="fa fa-user text-primary"></span>
                                                <span
                                                    class="pull-right font-weight-normal small">{{ isset($v->visitor_name)?$v->visitor_name:'?' }}</span>
                                            </li>
                                            <li class="list-group-item py-1">
                                                <span class="fa fa-id-card text-primary"></span>
                                                <span
                                                    class="pull-right font-weight-normal small">{{ isset($v->visitor_cnic)?$v->visitor_cnic:'?' }}</span>
                                            </li>
                                            <li class="list-group-item py-1">
                                                <span class="fa fa-phone text-primary"></span>
                                                <span
                                                    class="pull-right font-weight-normal small">{{ isset($v->visitor_cell_no)?$v->visitor_cell_no:'?' }}</span>
                                            </li>
                                        </ul>
                                        <div class="text-center">
                                            <button type="button" class="btn btn-tool btn-view-detail"
                                                    data-card-widget="collapse">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="card-body px-0 bg-white py-0">
                                        @include('monitoring-cell.visitors.tabs')
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="col-12 text-center p-4">
                                No Logs Found
                            </div>
                        @endforelse
                        @if(method_exists($visitors, 'links'))
                            <div class="col-12 text-right bg-gray-light p-3">
                                {!! $visitors->links() !!}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scriptInnerFiles')
    <script src="{{ asset('plugins/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('plugins/colorpicker/spectrum.js') }}"></script>

@endsection
@section('pageScript')
    <script>
        $(document).ready(function () {
            $(":input").inputmask();
        });
    </script>
@endsection


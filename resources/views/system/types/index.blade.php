@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop
@inject('edBlade','App\Repositories\EdBlade')
@section('content')
    <div class="timeline">
        @php $counter=1;  @endphp
        @foreach($data as $parent=>$value)
            <div>
                <i class="fa bg-gradient-success">{{ $counter++ }}</i>
                <div class="timeline-item align-items-center">
                    <a class="time badge badge-info text-white" style="padding: 6px;margin: 7px;"
                       href="{{ route('system.types.create',[$parent]) }}">
                        {{ __('system.create')." ".$parent }}
                    </a>
                    <h1 class="timeline-header font-weight-bold"> {{ ucfirst(str_replace('-',' ',$parent)) }}</h1>
                    <div class="timeline-body">
                        @foreach($value as $inner_key=>$type)
                            <div class="btn btn-app">
                                {{ $type->label }}
                                @if($parent=='vehicle')
                                    {!! link_to_route('vcompanies.index',__('system.companies'),["type_id"=>$type->id],['class'=>'time badge badge-success text-white']) !!} ({{\App\Models\VCompany::where('type_id',$type->id)->count()}})
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@stop

@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    <div class="card card-solid">
        @can('read',\App\Models\Type::class)
            <div class="card-header">
                <div class="card-title">
                    {!! link_to_route('system.types',__('system.master_tables'),[$parent],['class'=>'btn btn-info btn-sm']) !!}
                </div>
            </div>
        @endcan
        <div class="card-body pb-0">
            <!-- form start -->
            {!! Form::open(['route' => ['system.types.store',$parent], 'method' => 'POST']) !!}
            {!! csrf_field() !!}
            @include('partials.dashboard.message')
            <div class="form-group">
                {!! Form::label('name', __('system.name')) !!}
                {!! Form::text('name', null, ['class' => 'form-control', 'required', 'autofocus','placeholder'=>__('system.placeholder_name'), 'id' => 'name','onkeyup'=>'Custom.replaceSpaceWithHypen(this)' ]) !!}
            </div>
            <div class="form-group">
                {!! Form::label('label', __('system.label')) !!}
                {!! Form::text('label', null, ['class' => 'form-control', 'required', 'id' => 'label','placeholder'=>__('system.placeholder_label'),'onkeyup'=>'Custom.upperCaseEveryLetter(this)' ]) !!}
            </div>
            <div class="form-group text-right">
                {!! Form::submit(__('system.save'), array('class' => 'btn btn-primary btn-sm')) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

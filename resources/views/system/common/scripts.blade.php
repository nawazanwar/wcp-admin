@section('pageScript')
    <script>
        function applyStatus(cELement, id) {
            var value = $(cELement).attr('data-value');
            var cRoute = '/dashboard/system/apply/status/{{$for}}' + "/" + id + "/" + value;
            Ajax.call(cRoute, null, "Get", function (response) {
                if (response.status == 'no_permission') {
                    toastr.error("{{__('message.no_permission')}}", 'Error');
                } else if (response.status == 'success') {
                    if (value == 'active') {
                        //Ready to de activate
                        $("#c-row-" + id)
                            .removeClass('active-row')
                            .addClass('de-active-row')
                            .find('.active_holder')
                            .find('.btn')
                            .removeClass('btn-success')
                            .addClass('btn-danger')
                            .attr('data-value', 'd-active')
                            .find('i')
                            .removeAttr('class')
                            .attr('class', 'fa fa-times');
                        toastr.warning("{{__('message.successfully_deactivated')}}", 'Warning');
                    } else {
                        //Ready to Activate
                        $("#c-row-" + id)
                            .addClass('active-row')
                            .removeClass('de-active-row')
                            .find('.active_holder')
                            .find('.btn')
                            .removeClass('btn-danger')
                            .addClass('btn-success')
                            .attr('data-value', 'active')
                            .find('i')
                            .removeAttr('class')
                            .attr('class', 'fa fa-check');
                        toastr.success("{{__('message.successfully_activated')}}", 'Success');
                    }
                    $("#c-row-" + id).find('.updated_at').empty().text(response.modified);
                }
            })
        }

        function assign(cElement, parent_id, assigned_id) {
            var is_assigned;
            if ($(cElement).is(':checked')) {
                is_assigned = true;
            } else {
                is_assigned = false;
            }
            var data = {
                'for': '{{isset($for)?$for:""}}',
                'type': '{{isset($type)?$type:""}}',
                'parent_id': parent_id,
                'assigned_id': assigned_id,
                'is_assigned': is_assigned
            };
            Ajax.call('/dashboard/system/assign', data, 'post', function (response) {
                if (is_assigned == true) {
                    toastr.success(response.title + " " + "{{__('message.assigned_successfully')}}", 'Success');
                } else {
                    toastr.warning(response.title + " " + "{{__('message.removed_successfully')}}", 'Warning');
                }
            })
        }

    </script>
@endsection

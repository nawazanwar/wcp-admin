@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop
@inject('edBlade','App\Repositories\EdBlade')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-header">
                    @include('users.components.filter')
                </div>
                <div class="card-body">
                    @include('partials.dashboard.message')
                    <div class="card-header text-right">
                        <div class="row mb-2" dir="ltr">
                            <div class="col-sm-4 text-left">
                                @can('delete',\App\Models\User::class)
                                    <a class="btn bg-gradient-danger btn-xs text-white cursor-pointer delete_all hide"
                                       onclick="Custom.deleteAll('users');">{{ __('system.delete_all') }}</a>
                                @endcan
                            </div>
                            <div class="col-sm-8">
                                @can('create',\App\Models\User::class)
                                    {!! link_to_route('users.create',__('system.create'),null,['class'=>'btn bg-gradient-primary btn-xs']) !!}
                                @endcan
                                {{--@can('import',\App\Models\User::class)
                                    {!! link_to_route('users.import',__('system.import'),null,['class'=>'btn bg-gradient-info btn-xs']) !!}
                                @endcan--}}
                                @can('export',\App\Models\User::class)
                                    {!! link_to_route('users.export',__('system.export'),null,['class'=>'btn bg-gradient-success btn-xs']) !!}
                                @endcan
                                @can('export',\App\Models\User::class)
                                    {!! link_to_route('users.pdf',__('system.download_pdf'),null,['class'=>'btn bg-danger btn-xs']) !!}
                                @endcan
                                {{-- @can('print',\App\Models\User::class)
                                     <a class="btn bg-secondary btn-xs cursor-pointer"
                                        onclick="Custom.printMe('printTable')">{{ __('system.print') }}</a>
                                 @endcan--}}
                                @can('read',\App\Models\Log::class)
                                    {!! link_to_route('users.logs',__('system.logs'),null,['class'=>'btn bg-gradient-info btn-xs']) !!}
                                @endcan
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered" id="printTable">
                        <thead>
                        <tr>
                            <th width="50px text-center">
                                <div class="icheck-success d-inline" dir="ltr">
                                    <input type="checkbox" id="master" onclick="Custom.showHideDeleteAll(this);">
                                    <label for="master"></label>
                                </div>
                            </th>
                            <th class="text-center">{{__('system.media')}}</th>
                            <th>{{__('system.username')}}</th>
                            <th class="text-center">{{__('system.type')}}</th>
                            <th>{{__('system.email')}}</th>
                            <th class="text-center">{{ __('system.authorities') }}</th>
                            <th class="text-center">{{__('system.active')}}</th>
                            <th class="text-center action">{{__('system.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($data)>0)
                            @foreach($data as $d)
                                @php
                                    $isDeletedRow = ($d->deleted_at !== null)?"deleted-row":"";
                                    $isDeleted = ($d->deleted_at !== null)?"yes":"no";
                                @endphp
                                <tr class="{{$isDeletedRow}}"
                                    id="c-row-{{$d->id}}" data-deleted="{{ $isDeleted }}">
                                    <td class="icheck-holder">
                                        <div class="icheck-success d-inline" dir="ltr">
                                            <input class="sub_chk del_checkbox" type="checkbox"
                                                   id="sub_chk_{{$d->id}}"
                                                   onclick="Custom.showHideDeleteAll(this);"
                                                   data-id="{{$edBlade->encrypt($d->id)}}">
                                            <label for="sub_chk_{{$d->id}}"></label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <img src="{{ $d->getAvatar() }}" alt="{{ $d->visitor_firstname }}"
                                             class="avatar avatar-lg">
                                    </td>
                                    <td>{{$d->username}}</td>
                                    <td class="text-center">{{$d->getType()}}</td>
                                    <td>{{$d->email}}</td>
                                    <th class="text-center">
                                        <div class="btn-group btn-xs" dir="ltr">
                                            <button type="button"
                                                    class="btn btn-info"><i class="fa fa-eye-slash"></i></button>
                                            <button type="button" class="btn btn-success dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu p-0" role="menu">
                                                @can('read',\App\Models\User::class)
                                                    <li class="dropdown-item text-center py-3 bg-gradient-info">
                                                        {!! link_to_route('users.show',__('system.view_detail'),[$edBlade->encrypt($d->id)],['class'=>'btn btn-default btn-xs']) !!}
                                                        {!! link_to_route('users.roles',__('system.assign_new_authority'),[$edBlade->encrypt($d->id)],['class'=>'btn btn-default btn-xs']) !!}
                                                    </li>
                                                @endcan
                                                <li class="dropdown-item">
                                                    {{__('system.roles')}} <span
                                                        class="float-right badge bg-primary">{{ $d->roles()->count() }}</span>
                                                </li>
                                                <li class="dropdown-item">
                                                    {{__('system.permissions')}} <span
                                                        class="float-right badge bg-primary">{{ $d->countPermissions() }}</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </th>
                                    <td class="text-center active_holder">
                                        @if($d->active)
                                            <i class="fa fa-check text-success"></i>
                                        @else
                                            <i class="fa fa-times"></i>
                                        @endif
                                    </td>
                                    <td class="text-center action">
                                        @can('restore',\App\Models\User::class)
                                            {!! link_to_route('users.restore',__('system.restore'),[$edBlade->encrypt($d->id)],['class'=>'btn bg-gradient-success btn-xs restore_btn hide','onclick'=>'Custom.restoreMe(this); return false']) !!}
                                        @endcan
                                        {{--@can('edit',\App\Models\User::class)
                                            {!! link_to_route('users.edit', __('system.edit'), [$edBlade->encrypt($d->id)],['class'=>'btn bg-gradient-primary btn-xs action_btn']) !!}
                                        @endif--}}
                                        @can('delete',\App\Models\User::class)
                                            {!! link_to_route('users.delete',__('system.delete'),[$edBlade->encrypt($d->id)],['class'=>'btn bg-gradient-danger btn-xs action_btn','onclick'=>'Custom.deleteMe(this); return false']) !!}
                                        @endcan
                                        @can('read',\App\Models\Log::class)
                                            {!! link_to_route('users.logs',__('system.logs'),$d->id,['class'=>'btn bg-gradient-info btn-xs']) !!}
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="10" class="text-center py-5">
                                    <strong>{{ __('message.no_user_found') }}</strong>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                @if ($data->hasMorePages())
                    <div class="card-footer clearfix">
                        {{ $data->links() }}
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
@section('scriptInnerFiles')
    <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
@endsection
@include('system.common.scripts',['for'=>'user'])

@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)
@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header font-weight-bold">{{__('system.import_csv')}}</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-gray-dark collapsed-card">
                                <div class="card-header">
                                    <div class="card-title font-weight-bold">
                                        {{ __('system.sample_csv') }}
                                    </div>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                class="fas fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="card-body bg-gradient-light">
                                    @include('users.components.sampleImport')
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <form class="form-horizontal" method="POST" action="{{ route('users.import.preview') }}"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('csv_file') ? ' has-error' : '' }}">
                                    <label for="csv_file" class="col-md-4 control-label">CSV file to import</label>
                                    <div class="custom-file">
                                        <input id="csv_file" type="file" class="form-control form-control-sm"
                                               name="csv_file" onchange="this.form.submit()">
                                        <label class="custom-file-label" for="csv_file">Choose file</label>
                                    </div>
                                    @if ($errors->has('csv_file'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('csv_file') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            @isset($csv_data)
                                <div class="card-body pt-0">
                                    <form class="form-horizontal" method="POST"
                                          action="{{ route('users.import.sync') }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="csv_data_file_id" value="{{ $csv_data_id }}"/>
                                        <table class="table table-condensed table-hover">
                                            <tr>
                                                @foreach ($csv_data[0] as $key => $value)
                                                    <td>
                                                        <select name="fields[{{ $key }}]"
                                                                class="form-control form-control-sm">
                                                            @foreach (config('importfields.user_fields') as $db_field)
                                                                <option
                                                                    value="{{ $loop->index }}">{{ $db_field }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                @endforeach
                                            </tr>
                                            @foreach ($csv_data as $row)
                                                <tr>
                                                    @foreach ($row as $key => $value)
                                                        <td>{{ $value }}</td>
                                                    @endforeach
                                                </tr>
                                            @endforeach
                                        </table>
                                        <div class="card-footer text-right">
                                            <button type="submit" class="btn btn-primary btn-sm">
                                                {{ __('system.sync_import_date') }}
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            @endisset
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

<!DOCTYPE html>
<meta charset="utf-8"/>
<html>
<head>
    <style>
        table, td, th {
            border: 1px solid #ddd;
            text-align: left;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            padding: 15px;
        }
        .avatar.avatar-lg {
            font-size: 1.2rem;
            height: 2.4rem;
            width: 2.4rem;
        }
        .avatar {
            background: transparent!important;
            border-radius: 50%;
            color: hsla(0,0%,100%,.85);
            display: inline-block;
            font-size: .8rem;
            font-weight: 300;
            height: 1.6rem;
            line-height: 1.25;
            margin: 0;
            position: relative;
            vertical-align: middle;
            width: 1.6rem;
        }
        .text-center{
            text-align: center;
        }
    </style>
</head>
<body>
    <table class="table">
        <thead>
        <tr>
            <th class="text-center">{{__('system.media')}}</th>
            <th>{{__('system.name')}}</th>
            <th>{{__('system.email')}}</th>
            <th class="text-center">{{__('system.active')}}</th>
            <th>{{ __("system.type") }}</th>
        </tr>
        </thead>
        <tbody>
        @if(count($data)>0)
            @foreach($data as $d)
                <tr>
                    <td class="text-center">
                        <img src="{{ $d->getAvatar() }}" alt="{{ $d->name }}"
                             class="avatar avatar-lg">
                    </td>
                    <td>{{$d->name}}</td>
                    <td>{{$d->email}}</td>
                    <td class="text-center">
                        @if($d->active)
                            yes
                        @else
                            No
                        @endif
                    </td>
                    <td>{{$d->getType()}}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10" class="text-center py-5">
                    <strong>{{ __('message.no_user_found') }}</strong>
                </td>
            </tr>
        @endif
        </tbody>
    </table>
</body>
</html>

@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
@endsection

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop


@section('content')
    <div class="card card-solid">
        <div class="card-header">
            @can('read',\App\Models\User::class)
                <a class="btn bg-gradient-info btn-xs text-white cursor-pointer"
                   href="{{ route('users') }}"><i
                        class="fa fa-arrow-left"></i> {{__('system.users')}}</a>
            @endcan
        </div>
        <div class="card-body pb-0">
            @include('partials.dashboard.message')
            <div class="form-group">
                <div class="row">
                    @php  $sModels = new \App\Models\User();  @endphp
                    @foreach($data as $d)
                        @php $alreadyExists=$sModels->hasAlreadyRole($d->id,$model->id);@endphp
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 my-2">
                            <div class="icheck-success d-inline">
                                <input type="checkbox"
                                       onclick="assign(this,'{{$model->id}}','{{$d->id}}')"
                                       {{ $alreadyExists==true?'checked':'' }} id="checkboxSuccess{{$d->id}}">
                                <label for="checkboxSuccess{{$d->id}}" class="font-weight-light">
                                    {!! link_to_route('roles.permissions',$d->label, [$d->id]) !!}
                                </label>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop
@section('scriptInnerFiles')
    <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
@endsection
@include('system.common.scripts',['for'=>'user','type'=>'role'])

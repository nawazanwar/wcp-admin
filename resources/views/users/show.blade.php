@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop
@inject('edBlade','App\Repositories\EdBlade')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 card-header bg-white my-2 border-0">
                @can('read',\App\Models\User::class)
                    <a class="btn bg-gradient-info btn-xs text-white cursor-pointer"
                       href="{{ route('users') }}"><i
                            class="fa fa-arrow-left"></i> {{__('system.users')}}</a>
                @endcan
            </div>
            <div class="col-md-3">
                <!-- Profile Image -->
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="img-fluid img-thumbnail" src="{{ $model->getAvatar() }}"
                                 alt="{{$model->name}}">
                        </div>
                        <h3 class="profile-username text-center">{{ $model->username }}</h3>
                        <p class="text-muted text-center">{{ $model->email }}</p>
                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>{{__('system.roles')}}</b> <a class="float-right">{{ $model->roles->count() }}</a>
                            </li>
                            <li class="list-group-item">
                                <b>{{__('system.created')}}</b> <a
                                    class="float-right">{{ $model->created_at->diffForHumans() }}</a>
                            </li>
                            <li class="list-group-item">
                                <b>{{__('system.last_update')}}</b> <a
                                    class="float-right">{{ $model->updated_at->diffForHumans() }}</a>
                            </li>
                        </ul>
                        <div class="text-center">
                            @can('edit',\App\Models\User::class)
                                {!! link_to_route('users.edit', __('system.edit'), [$edBlade->encrypt($model->id)],['class'=>'btn btn-info btn-xs']) !!}
                            @endif
                            @can('delete',\App\Models\User::class)
                                {!! link_to_route('users.delete', __('system.delete'), [$edBlade->encrypt($model->id)],['class'=>'btn btn-info btn-xs',"onclick"=>"return confirm('Do you really wants to delete this?')"]) !!}
                            @endcan
                            @can('read',\App\Models\Role::class)
                                {!! link_to_route('users.roles', __('system.assign_roles'), [$edBlade->encrypt($model->id)],['class'=>'btn btn-info btn-xs']) !!}
                            @endcan
                        </div>
                    </div>
                </div>
                {{-- Extra Information--}}
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('system.profile') }}</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <strong>{{ __('system.cnic') }}</strong>
                        <p class="text-muted mb-1">
                            {{  $model->cnic }}
                        </p>
                        <hr class="my-1">
                        <strong>{{ __('system.phone') }}</strong>
                        <p class="text-muted mb-1"> {{  $model->phone_number }}</p>
                        <hr class="my-1">
                        <strong>{{ __('system.ptcl') }}</strong>
                        <p class="text-muted mb-1"> {{  $model->cell_number }}</p>
                        <hr class="my-1">
                        <strong>{{ __('system.emergency_contact') }}</strong>
                        <p class="text-muted mb-1"> {{  $model->reference_phone_number}}</p>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header p-2">
                        <ul class="nav nav-pills">
                            @can('read',\App\Models\Media::class)
                                <li class="nav-item">
                                    <a class="nav-link btn-sm active" href="#medias"
                                       data-toggle="tab">{{ __('system.media') }}</a>
                                </li>
                            @endcan
                            @can('read',\App\Models\Role::class)
                                <li class="nav-item">
                                    <a class="nav-link btn-sm" href="#roles"
                                       data-toggle="tab">{{ __('system.roles') }}</a>
                                </li>
                            @endcan
                            @can('read',\App\Models\Log::class)
                                <li class="nav-item">
                                    <a class="nav-link btn-sm" href="#logs"
                                       data-toggle="tab">{{ __('system.logs') }}</a>
                                </li>
                            @endcan
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane active show" id="medias">
                                @include('users.components.media')
                            </div>
                            <div class="tab-pane" id="roles">
                                @include('users.components.roles')
                            </div>
                            <div class="tab-pane" id="logs">
                                @if(count($logs)>0)
                                    @include('users.components.logs')
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('pageScript')
    <script>
        function loadMoreUserRolesPermissions(user, role, last) {
            var cRoute = '{{ route('system.load.more',['user_role_permissions']) }}';
            Ajax.call(cRoute, {
                'user': user,
                'role': role,
                'last': last
            }, 'get', function (response) {
                $("#role_permission_load_more_holder_" + user + "_" + role).remove();
                $("#permission_holder_" + user + "_" + role).append(response);
            });
        }
    </script>
@endsection

@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    <div class="card card-solid">
        <div class="card-header">
            @can('read',\App\Models\User::class)
                <a class="btn bg-gradient-info btn-xs text-white cursor-pointer" href="{{ route('users') }}"><i
                        class="fa fa-arrow-left"></i> {{__('system.users')}}</a>
            @endcan
        </div>
        <div class="card-body">
            {!! Form::open(['route' => ['users.store'], 'method' => 'POST','files'=>'true']) !!}
            {!! csrf_field() !!}
            {{-- first row--}}
            <div class="row align-items-center m-auto">
                <div class="col-md-6 form-group ">
                    <div class="text-center">
                        @foreach($types as $key=>$value)
                            <div class="icheck-success d-inline text-center">
                                <input type="radio" id="{{ $key }}" name="type" value="{{ $key }}"
                                       @if ($loop->first) checked @endif>
                                <label for="{{ $key }}">
                                    {{ ucfirst($value) }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <strong class="font-weight-bold text-danger">{{__('system.caution')}}</strong>
                    <p class="my-0 text-muted text-sm"><i class="fa fa-hand-o-right text-danger"></i> User first name is
                        necessary
                        to add</p>
                    <p class="my-0 text-muted text-sm"><i class="fa fa-hand-o-right text-danger"></i> Guardian first
                        name is
                        necessary to add</p>
                    <p class="my-0 text-muted text-sm"><i class="fa fa-hand-o-right text-danger"></i> One Phone number
                        and one
                        reference number must be added</p>
                </div>
                <div class="col-md-6 form-group text-center">
                    <div class="avatar_preview_holder">
                        <img src="{{ asset('img/defaults/user.png') }}" alt="house"
                             style="width: 100px; height: 100px;" class="img-circle img-thumbnail">
                        <div class="upload-btn-wrapper">
                            <button class="btn">Upload</button>
                            <input type="file" name="avatar" onchange="Custom.previewAvatar(this)"/>
                        </div>
                    </div>
                </div>
            </div>
            {{-- General Information--}}
            <div class="row">
                <div class="col-12">
                    <h5 class="text-left text-white font-weight-bold bg-primary p-2">{{__('system.general_information')}}</h5>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <label>{{__('system.user_name')}} </label>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="general['suffix']">{{__('system.suffix')}}</label>
                                <select class="form-control " name="suffix" id="general['suffix']">
                                    {{ __('system.suffix') }}
                                    <option value="male">{{ __('system.Mr') }}</option>
                                    <option value="female">{{ __('system.Mrs') }}</option>
                                    <option value="female">{{ __('system.Miss') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="general['first_name']">{{__('system.first_name')}} <span
                                    class="text-danger">*</span></label>
                            <input class="form-control @error('first_name') is-invalid @enderror"
                                   id="general['first_name']"
                                   name="first_name" type="text" value="{{old('first_name')}}">
                            @error('first_name')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="general['middle_name']">{{__('system.middle_name')}}</label>
                            <input class="form-control" id="general['middle_name']" name="last_name" type="text"
                                   value="{{old('middle_name')}}">
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="general['last_name']">{{__('system.last_name')}}</label>
                            <input class="form-control" id="general['last_name']" name="last_name" type="text"
                                   value="{{old('last_name')}}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <label>{{__('system.parent')}} </label>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="general['parent_suffix']">{{__('system.suffix')}}</label>
                                <select class="form-control " name="parent_suffix" id="general['parent_suffix']">
                                    {{ __('system.suffix') }}
                                    <option value="male">{{ __('system.Mr') }}</option>
                                    <option value="female">{{ __('system.Mrs') }}</option>
                                    <option value="female">{{ __('system.Miss') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="general['parent_first_name']">{{__('system.first_name')}} <span
                                    class="text-danger">*</span></label>
                            <input class="form-control @error('parent_first_name') is-invalid @enderror"
                                   id="general['parent_first_name']"
                                   name="parent_first_name" type="text" value="{{old('parent_first_name')}}">
                            @error('parent_first_name')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="general['parent_middle_name']">{{__('system.middle_name')}}</label>
                            <input class="form-control" id="general['parent_middle_name']" name="parent_middle_name"
                                   type="text" value="{{old('parent_middle_name')}}">
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="general['parent_last_name']">{{__('system.last_name')}}</label>
                            <input class="form-control" id="general['parent_last_name']" name="parent_last_name"
                                   type="text" value="{{old('parent_last_name')}}">
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="general['email']">{{__('system.email')}} <span
                                    class="text-danger">*</span></label>
                            <input class="form-control @error('email') is-invalid @enderror"
                                   id="general['email']"
                                   name="email" type="text" value="{{old('email')}}">
                            @error('email')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="general['password']">{{__('system.password')}} <span
                                    class="text-danger">*</span></label>
                            <input class="form-control @error('password') is-invalid @enderror"
                                   id="general['password']"
                                   name="password" type="password" value="{{old('password')}}">
                            @error('password')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-12 clearfix text-right form-group">
                    <div class="form-group icheck-success d-inline mb-4">
                        <input id="general['active']" checked="checked" name="active" type="checkbox" value="1">
                        <label for="general['active']">{{__('system.active')}}</label>
                    </div>
                </div>
            </div>
            {{--Personal Information--}}
            <div class="row">
                <div class="col-12">
                    <h5 class="text-left text-white font-weight-bold bg-primary p-2">{{__('system.personal_information')}}</h5>
                </div>
                <div class="col-12 text-right form-group">
                    <div class="form-group icheck-success d-inline">
                        <input id="general['cnic_verify_status_nadra']" checked="checked" name="active" type="checkbox"
                               value="1">
                        <label for="general['cnic_verify_status_nadra']">{{__('system.verified_from_nadra')}}</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="profile['cnic']">{{ __('system.cnic') }}</label>
                        <input class="form-control" placeholder="xxxx-xxxxxxx-x" id="profile['cnic']"
                               name="profile[cnic]" type="text" onkeydown="Custom.validateCNIC(event);">
                    </div>
                    <div class="form-group">
                        <label for="profile['gender']">Gender</label>
                        <select class="form-control " name="profile[gender]" id="profile['gender']">
                            {{ __('system.gender') }}
                            <option value="male">{{ __('system.male') }}</option>
                            <option value="female">{{ __('system.female') }}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="profile['date_of_birth']">{{ __('system.dob') }}</label>
                        <input type="date" name="profile[date_of_birth]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="profile['terms']">{{ __('system.terms') }}</label>
                        <select class="form-control " name="profile[terms]" id="profile['terms']">
                            {{ __('system.terms') }}
                            <option value="long_term">{{ __('system.long_term') }}</option>
                            <option value="short_term">{{ __('system.short_term') }}</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="profile['phone']">{{ __('system.phone') }}</label>
                        <input type="text" placeholder="xxxx-xxxxxxx" name="profile[cell_number]" class="form-control"
                               onkeydown="Custom.validatePhone(event);">
                    </div>
                    <div class="form-group">
                        <label for="profile['ptcl']">{{ __('system.ptcl') }}</label>
                        <input type="text" placeholder="xxxx-xxxxxxx" name="profile[phone_number]" class="form-control"
                               onkeydown="Custom.validatePhone(event);">
                    </div>
                    <div class="form-group">
                        <label for="profile['emergency']">{{ __('system.emergency_contact') }}</label>
                        <input type="text" placeholder="xxxx-xxxxxxx" name="profile[reference_phone_number]"
                               class="form-control"
                               onkeydown="Custom.validatePhone(event);">
                    </div>
                </div>
            </div>
            {{--Employment--}}
            <div class="row">
                <div class="col-12">
                    <h5 class="text-left text-white font-weight-bold bg-primary p-2">{{__('system.employement')}}</h5>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="general['last_name']">{{__('system.employer_name')}} <span
                                        class="text-danger">*</span></label>
                                <input class="form-control @error('employer_name') is-invalid @enderror"
                                       id="general['employer_name']" name="employer_name"
                                       type="text" value="{{old('employer_name')}}">
                                @error('employer_name')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="general['last_name']">{{__('system.designation')}} <span
                                        class="text-danger">*</span></label>
                                <input class="form-control @error('designation') is-invalid @enderror"
                                       id="general['designation']" name="designation"
                                       type="text" value="{{old('designation')}}">
                                @error('designation')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="profile['working_since']">{{ __('system.working_since') }}</label>
                                <input type="date" name="profile[working_since]" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Media Information--}}
            <div class="row">
                <div class="col-12">
                    <h5 class="text-left text-white font-weight-bold bg-primary p-2">{{__('system.media_information')}}</h5>
                </div>
                <div class="col-12">
                    <div class="row justify-content-center m-auto">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="cnic_media['front']">{{ __('system.cnic_front') }}</label>
                                <div class="input-group input-group-sm">
                                    <div class="custom-file">
                                        <input type="file" name="cnic_media['front']"
                                               class="custom-file-input form-group-sm"
                                               id="cnic_media['front']"
                                               onchange="Custom.previewImage(this,'cnic_front_holder')">
                                        <label class="custom-file-label"
                                               for="media['cnic_front']">{{ __('system.choose_file') }}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="cnic_front_holder text-center my-3">
                                <img src="{{ asset('img/defaults/nadra.jpg') }}" class="img-thumbnail h-200">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="cnic_media['back']">{{ __('system.cnic_back') }}</label>
                                <div class="input-group input-group-sm">
                                    <div class="custom-file">
                                        <input type="file" name="cnic_media['back']" class="custom-file-input"
                                               id="cnic_media['back']"
                                               onchange="Custom.previewImage(this,'cnic_back_holder')">
                                        <label class="custom-file-label"
                                               for="cnic_media['back']">{{ __('system.choose_file') }}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="cnic_back_holder text-center my-3">
                                <img src="{{ asset('img/defaults/nadra.jpg') }}" class="img-thumbnail h-200">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Address Information--}}
            <div class="row">
                <div class="col-12">
                    <h5 class="text-left text-white font-weight-bold bg-primary p-2">{{__('system.address')}}</h5>
                </div>
                <div class="form-group col-md-6">
                    <label for="profile['permanent_address']">{{ __('system.permanent_address') }}</label>
                    <textarea type="text" name="profile['permanent_address']" class="form-control"></textarea>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="profile['permanent_city']">{{ __('system.permanent_city') }}</label>
                                <input type="text" name="profile['permanent_city]" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="profile['permanent_country']">{{ __('system.permanent_country') }}</label>
                                <input type="text" name="profile['permanent_country']" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="profile['permanent_state']">{{ __('system.permanent_state') }}</label>
                                <input type="text" name="profile['permanent_state']" class="form-control">
                            </div>
                            <div class="form-group">
                                <label
                                    for="profile['permanent_postal_code']">{{ __('system.permanent_postal_code') }}</label>
                                <input type="text" name="profile['permanent_postal_code']" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="profile['temporary_address']">{{ __('system.temporary_address') }}</label>
                    <textarea type="text" name="profile['temporary_address']" class="form-control"></textarea>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="profile['temporary_city']">{{ __('system.temporary_city') }}</label>
                                <input type="text" name="profile['temporary_city]" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="profile['temporary_country']">{{ __('system.temporary_country') }}</label>
                                <input type="text" name="profile['temporary_country']" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="profile['temporary_state']">{{ __('system.temporary_state') }}</label>
                                <input type="text" name="profile['temporary_state']" class="form-control">
                            </div>
                            <div class="form-group">
                                <label
                                    for="profile['permanent_postal_code']">{{ __('system.temporary_postal_code') }}</label>
                                <input type="text" name="profile['temporary_postal_code']" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-primary btn-sm">{{ __('system.save') }}</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

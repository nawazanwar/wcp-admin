<div class="card card-primary card-outline">
    <div class="card-header d-flex p-0">
        <h3 class="card-title p-3">{{ __('system.list_all_medias') }}</h3>
        <ul class="nav nav-pills ml-auto p-2">
            <li class="nav-item">
                <a class="nav-link btn btn-sm active" href="#avatar" data-toggle="tab">{{ __('system.avatar') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn btn-sm" href="#cnic" data-toggle="tab">{{ __('system.cnic') }}</a>
            </li>
        </ul>
    </div><!-- /.card-header -->
    <div class="card-body">
        <div class="tab-content">
            <div class="tab-pane active text-center py-3" id="avatar">
                <img src="{{ $model->getAvatar() }}" class="avatar avatar-xl">
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane text-center py-3" id="cnic">
                <div class="row justify-content-center m-auto">
                    @php  $cnic_images =$model->getCnicImages();  @endphp
                    @foreach($cnic_images as $cnic_image)
                        <div class="col-md-4 text-center">
                            <img src="{{ asset('uploads/users/cnic/'.$cnic_image->src) }}" class="img-thumbnail h-200 img-fluid">
                        </div>
                    @endforeach
                </div>
            </div>
            <!-- /.tab-content -->
        </div><!-- /.card-body -->
    </div>
</div>

<div class="row">
    <div class="col-5 col-sm-3">
        <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
             aria-orientation="vertical">
            @foreach($model->roles as $key=>$ur)
                <a class="nav-link @if ($loop->first) active @endif" data-toggle="pill"
                   href="#vert-{{$model->id}}-{{$ur->id}}-tab" role="tab"
                   aria-selected="true">{{ $ur->label }}</a>
            @endforeach
        </div>
    </div>
    <div class="col-7 col-sm-9">
        <div class="tab-content">
            @foreach($model->roles as $key=>$role)
                <div
                    class="tab-pane text-left fade @if ($loop->first) show active @endif"
                    id="vert-{{$model->id}}-{{$role->id}}-tab"
                    role="tabpanel">
                    <div class="row align-items-center permission_holder">
                        @if($role->permissions->count()>0)
                            @include('users.components.serach',["pId"=>"permission_holder_".$model->id."_".$role->id])
                            <div class="col-12">
                                <div id="permission_holder_{{$model->id}}_{{$role->id}}"
                                     class="row p-3">
                                    @include('users.components.permissions',["permissions"=>$role->permissions,"user_id"=>$model->id,"role_id"=>$role->id])
                                </div>
                            </div>
                        @else
                            <div class="col-12 text-center py-3">
                                <strong>{{ __('message.no_role_found') }}</strong>
                            </div>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

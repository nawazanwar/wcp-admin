@foreach($permissions->take(15) as $innerKey=>$rp)
    <div
        class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-6 my-2 text-left">
        <strong><i
                class="fa fa-unlock text-success"></i></strong> {{ $rp->label }}
    </div>
    @if($loop->last)
        <div class="col-12 text-center py-2" id="role_permission_load_more_holder_{{$user_id}}_{{$role_id}}">
            <a class="cursor-pointer btn btn-info btn-xs text-white"
               onclick="loadMoreUserRolesPermissions('{{$user_id}}','{{$role_id}}','{{$rp->id}}')">
                {{__('system.load_more')}}
            </a>
        </div>
    @endif
@endforeach

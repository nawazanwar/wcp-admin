<div class="col-12 text-right">
    <div class="row justify-content-end">
        <div
            class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12 col-12">
            <div class="input-group input-group-sm">
                <input onkeyup="Custom.searchDiv(this,'{{$pId}}')"
                    class="form-control form-control-navbar"
                    type="search" placeholder="{{__('system.search')}}"
                    aria-label="Search">
                <div class="input-group-append bg-primary">
                    <button class="btn btn-navbar">
                        <i class="fas fa-search text-white"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .table-wrapper {
        max-width: 100%;
        overflow-x: auto;
    }

    table {
        position: relative;
        border: 1px solid #ddd;
        border-collapse: collapse;
    }

    td, th {
        white-space: nowrap;
        border: 1px solid #ddd;
        padding: 20px;
    }

    th {
        background-color: #eee;
    }

    tbody tr td:first-of-type {
        background-color: #eee;
    }
</style>
<div class="table-wrapper">
    <table class="table table-hover">
        <thead>
            <tr>
                @foreach (config('importfields.user_fields') as $db_field)
                    <th>{{ $db_field }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>test</td>
                <td>test</td>
                <td>test@gmail.com</td>
                <td>1</td>
            </tr>
            <tr>
                <td>test</td>
                <td>test</td>
                <td>test@gmail.com</td>
                <td>1</td>
            </tr>
            <tr>
                <td>test</td>
                <td>test</td>
                <td>test@gmail.com</td>
                <td>1</td>
            </tr>
            <tr>
                <td>test</td>
                <td>test</td>
                <td>test@gmail.com</td>
                <td>1</td>
            </tr>
        </tbody>
    </table>
</div>

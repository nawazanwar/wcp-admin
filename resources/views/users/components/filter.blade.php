<form action="{{ route('users') }}" method="get" dir="ltr">
    <div class="row justify-content-center align-items-center">
        <div
            class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 col-12 form-group form-group-sm mb-xl-0 mb-lg-0 mb-xl-0 text-center">
            <div class="icheck-success d-inline">
                <input type="radio" id="active" name="filter[active]" value="1" @if(isset($filters['active']) && $filters['active'] == 1) checked @endif>
                <label for="active">
                   {{ __('system.active') }}
                </label>
            </div>
            <div class="icheck-danger d-inline">
                <input type="radio" id="de-active" name="filter[active]"  value="0" @if(isset($filters['active']) && $filters['active'] == 0) checked @endif>
                <label for="de-active">
                    {{ __('system.de-active') }}
                </label>
            </div>
        </div>
        <div
            class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 col-12 form-group form-group-sm mb-xl-0 mb-lg-0 mb-xl-0">
            <select class="custom-select custom-select-sm" name="field">
                <option value="name" @if(isset($field) and $field == 'name') selected @endif>
                    {{ __('system.name') }}
                </option>
                <option value="email"
                        @if(isset($field) and $field == 'email') selected @endif>
                    {{ __('system.email') }}
                </option>
                <option value="cnic"
                        @if(isset($field) and $field == 'cnic') selected @endif>
                    {{ __('system.cnic') }}
                </option>
            </select>
        </div>
        <div
            class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 col-12 form-group form-group-sm mb-xl-0 mb-lg-0 mb-xl-0">
            <input type="search" name="keyword"
                   value="@if(isset($keyword) and $keyword != '') {{ $keyword }} @endif"
                   class="form-control form-control-sm"
                   placeholder="{{__('system.search_here')}}">
        </div>
        <div
            class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12 col-12 form-group form-group-sm mb-xl-0 mb-lg-0 mb-xl-0">
            <input type="submit" class="btn btn-success btn-sm" value="{{ __('system.search') }}">
        </div>
    </div>
</form>

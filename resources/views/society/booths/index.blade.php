@extends('layouts.colorbox')
@section('pageTitle', $pageTitle)
@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-header row align-items-center mx-1">
                    <div class="card-title col-xl-6 col-lg-6 col-md-6">
                        @can('create',\App\Models\Booth::class)
                            {!! HTML::decode(link_to_route('society-booths.create',' <i class="fa fa-plus"></i> ',null,['class'=>'btn btn-xs bg-gradient-success text-center'])) !!}
                        @endcan
                    </div>
                </div>
                <div class="card-body">
                    @include('partials.dashboard.message')
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>{{__('society.booths.gate')}}</th>
                                <th class="text-center">{{__('society.booths.lane')}}</th>
                                <th class="text-center">{{__('society.booths.type')}}</th>
                                <th class="text-center">{{__('system.created_date')}}</th>
                                <th class="text-center">{{__('system.last_modified')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(count($data)>0)
                            @foreach($data as $d)
                                <tr>
                                    <td>{{$d->gate->name}}</td>
                                    <td class="text-center">{{ $d->lane }}</td>
                                    <td class="text-center">{{ ucfirst($d->type) }}</td>
                                    <td class="text-center">{{$d->created_at}}</td>
                                    <td class="text-center">{{$d->updated_at->diffForHumans()}}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="text-center">
                                <td colspan="12" class="p-3">{{__('society.booths.no_booth_found')}}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@extends('layouts.colorbox')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endsection

@section('content')
    <div class="card card-solid">
        <div class="card-header">
            <div class="card-title">
                @can('read',\App\Models\Booth::class)
                    {!! HTML::decode(link_to_route('society-booths.index',' <i class="fa fa-arrow-left"></i> ',null,['class'=>'btn btn-xs bg-gradient-success text-center phase_selector'])) !!}
                @endcan
            </div>
        </div>
        <div class="card-body pb-0">
            {!! Form::open(['route' => ['society-booths.store'], 'method' => 'POST','files' => true]) !!}
            {!! csrf_field() !!}
            <div class="row">
                <div class="form-group col-md-3">
                    <select
                        data-live-search="true"
                        data-max-options="3"
                        data-selected-text-format="count > 3"
                        class="form-control custom-select-box selectpicker show-menu-arrow"
                        name="gate_id" id="gate_id">
                        @foreach(WcpHelper::allGates() as $key=>$value)
                            <option value="{{ $value->id }}">{{ $value->name }} - {{ $value->name_ur }}</option>
                        @endforeach
                    </select>
                    @error('gate')
                    <div class="error">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-md-3">
                    <input type="text" name="lane"
                        class="form-control" id="lane" placeholder="Enter Lane like 01,02,etc">
                    @error('lane')
                    <div class="error">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-md-3">
                    <select
                        data-live-search="true"
                        class="lane_selector form-control"
                        name="type" id="type">
                        <option value="entry">{{__('vms.entry')}}</option>
                        <option value="exit">{{__('vms.exit')}}</option>
                    </select>
                    @error('type')
                    <div class="error">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-md-3">
                    <div class="icheck-success d-inline ">
                        {!! Form::checkbox('active',1, true,['id'=>'inputActive','onclick'=>'Custom.checkBoxStatus(this);']) !!}
                        {!! Form::label('inputActive', __('hrm.is_active')) !!}
                    </div>
                </div>
                <div class="form-group text-right col-12">
                    {!! Form::submit(__('hrm.save'), array('class' => 'btn btn-primary btn-sm')) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('scriptInnerFiles')
    <script src="{{ asset('plugins/keybaord/urdu.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('plugins/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
@stop
@section('pageScript')
@endsection

@if(count($data)>0)
    <option disabled="disabled" selected>Choose Location</option>
    @foreach($data as $d)
        <option value="{{ $d->id }}">{{ $d->name }} {{ $d->name_ur }}</option>
    @endforeach
@else
    <option disabled="disabled" selected>No Location Found</option>
@endif

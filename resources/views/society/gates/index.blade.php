@extends('layouts.colorbox')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endsection
@section('content')
    {!! Form::open(['route' => ['society-gates.store'], 'method' => 'POST','files' => true]) !!}
    {!! csrf_field() !!}
    <div class="card card-solid">
        <div class="card-header">
            <div class="pull-right">
                <a class="btn btn-success btn-nav-add-row btn-sm text-white cursor-pointer"><i
                        class="fa fa-plus"></i></a>
                {!! Form::submit('Update', array('class' => 'btn btn-primary btn-sm')) !!}
            </div>
        </div>
        <div class="card-body">
            @include('partials.dashboard.message')
            <table class="table table-bordered" id="cTable">
                <tbody>
                @if(count($data) > 0)
                    @foreach($data as $item)
                        <tr class="my-2">
                            <td>
                                <input type="hidden" name="id[]" id="id_{{$item->id}}" value="{{$item->id}}">
                                <input type="text" name="name[]" class="form-control eng"
                                       placeholder="{{__('system.name')}}"
                                       id="name_{{$item->id}}" value="{{$item->name}}" required>
                            </td>
                            <td>
                                <input type="text" name="name_ur[]" class="form-control ur" dir="rtl"
                                       placeholder="{{__('system.name_ur')}}"
                                       id="name_ur_{{$item->id}}" value="{{$item->name_ur}}">
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr class="my-2">
                        <td>
                            <input type="hidden" name="id[]" id="id_{{ uniqid() }}" value="{{ uniqid() }}">
                            <input type="text" name="name[]" class="form-control eng"
                                   placeholder="{{__('system.name')}}" required>
                        </td>
                        <td>
                            <input type="text" name="name_ur[]" class="form-control ur" dir="rtl"
                                   placeholder="{{__('system.name_ur')}}">
                        </td>
                        <td class="text-center">
                            <a class="btn btn-danger remove-row  btn-sm text-white cursor-pointer">Remove <i
                                    class="fa fa-minus-circle"></i></a>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
    {!! Form::close() !!}
@stop
@section('pageScript')
    <script>
        $(function () {
            $(document).on('click', ".btn-nav-add-row", function (event) {
                var uId = 'id' + (new Date()).getTime();
                var cloned_row = $("#cTable").find('tr:first-child').clone();
                cloned_row.find('input[type=hidden]').attr('id', uId).val(uId);
                cloned_row.find('#action_holder').empty().append('<a class="btn btn-danger remove-row  btn-sm text-white cursor-pointer">Remove <i class="fa fa-minus-circle"></i></a>');
                cloned_row.find('.eng').val('').attr('placeholder', 'Start Writing').removeAttr('id');
                cloned_row.find('.ur').val('').attr('placeholder', 'لکھنا شروع کریں').attr('dir', 'rtl').removeAttr('id');
                $("#cTable").prepend(cloned_row);
            });
            $(document).on('click', ".remove-row", function (event) {
                event.preventDefault();
                var rowCount = $('#cTable tr').length;
                if (rowCount > 1) {
                    var $tr = $(this).closest('tr');
                    $tr.remove();
                }
            });
        });
    </script>
@endsection

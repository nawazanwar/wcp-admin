@if(count($data)>0)
    <option disabled="disabled" selected>Choose Gate</option>
    @foreach($data as $d)
        <option value="{{ $d->id }}">{{ $d->name }} {{ $d->name_ur }}</option>
    @endforeach
@else
    <option disabled="disabled" selected>No gate Found</option>
@endif

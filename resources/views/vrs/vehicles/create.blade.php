@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-header row align-items-center mx-1">
                    <div class="card-title col-xl-6 col-lg-6 col-md-6">
                        @can('read',\App\Models\VRS::class)
                            {!! HTML::decode(link_to_route('vrs.index',' <i class="fa fa-arrow-left"></i>',null,['class'=>'btn bg-gradient-info btn-sm']))!!}
                        @endcan
                    </div>
                </div>
                <div class="card-body">
                    {!! Form::open(['route' => ['vrs-vehicles.store'], 'method' => 'POST','files'=>'true']) !!}
                    {!! csrf_field() !!}
                    {{--User Profile Image--}}
                    <div class="row justify-content-center">
                        <div class="col-md-6 form-group">
                            <div class="avatar_preview_holder">
                                <img src="{{ asset('uploads/vehicles/default.png') }}" alt="house"
                                     style="width: 100px; height: 100px;" class="img-circle img-thumbnail">
                                <div class="upload-btn-wrapper">
                                    <button class="btn">Upload</button>
                                    <input type="file" name="avatar" onchange="Custom.previewAvatar(this)"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--Vehicle Imformation--}}
                    <div class="row">
                        <div class="col-md-4">
                            @php
                                $types = \App\Models\Type::whereParent('vehicle')->get();
                            @endphp
                            <select
                                onchange="getTypeCompanies(this);"
                                data-live-search="true"
                                data-max-options="3"
                                data-selected-text-format="count > 3"
                                class="form-control form-control-sm form-control form-control-sm-sm custom-select-box selectpicker show-menu-arrow"
                                name="type_id" id="type_id">
                                <option value="" selected disabled>{{__('vrs.vehicle.type')}}</option>
                                @foreach($types as $type)
                                    <option value="{{ $type->id }}">{{ $type->name }} ( {{ $type->name_ur }} )</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <select
                                onchange="getCompanyModels(this);"
                                data-live-search="true"
                                class="vcompanies_holder form-control form-control-sm form-control form-control-sm-sm"
                                name="vcompany_id" id="vcompany_id">
                                <option value="" selected disabled>{{__('vrs.vehicle.company')}}</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <select
                                data-live-search="true"
                                class="form-control form-control-sm form-control form-control-sm-sm cmodels_holder"
                                name="vmodel_id" id="vmodel_id">
                                <option value="" selected disabled>{{__('vrs.vehicle.model')}}</option>
                            </select>
                        </div>
                    </div>
                    {{--here start Developer Abdullah--}}
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="">Registration Form Date</label>
                            <input class="form-control form-control-sm " id="" name="l_name" type="text">
                        </div>
                        <div class="form-group col-md-9">

                        </div>

                        <div class="form-group col-md-3">
                            <label for="">Registration CHR</label>
                            <input class="form-control form-control-sm " id="" name="regCHR" type="text" placeholder="LES">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">Registration No.</label>
                            <input class="form-control form-control-sm " id="" name="regNo" type="text" placeholder="421">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">Registration Year</label>
                            <input class="form-control form-control-sm " id="" name="regYear" type="text" placeholder="20">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">Registration CC</label>
                            <input class="form-control form-control-sm " id="" name="regCC" type="text" placeholder="962">
                        </div>


                        <div class="form-group col-md-4">
                            <label for="">Registration Color</label>
                            <input class="form-control form-control-sm " id="" name="regColor" type="color" placeholder="Color">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Registration Terms</label>
                            <select name="regTerms" class="form-control form-control-sm">
                                <option>Short Term</option>
                                <option>Long Term</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Registration Status</label>
                            <select name="regStatus" class="form-control form-control-sm">
                                <option>Active</option>
                                <option>Deactive</option>
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="">Unregistered?</label>
                            <select name="unRegistered" class="form-control form-control-sm">
                                <option>Yes</option>
                                <option>No</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Registration Date</label>
                            <input class="form-control form-control-sm " id="" name="unRegDate" type="date" placeholder="Registration Date">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Registration Remarks</label>
                            <input class="form-control form-control-sm " id="" name="unRegRemarks" type="text" placeholder="Registration Remarks">
                        </div>

                        {{--Registerant Details--}}
                        <div class="row align-items-center">
                            {{--left Section--}}
                            <div class="col-md-6">
                                <div class="row px-3">
                                    <div class="form-group col-md-6">
                                        {!! Form::label('regName', __('vrs.regName')) !!}
                                        {!! Form::text('regName', old('regName'), ['class' => 'form-control form-control-sm ','id' => 'regName' ]) !!}
                                        @error('regname')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('regRelation', __('vrs.regRelation')) !!}
                                        {!! Form::text('regRelation', old('regRelation'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'regRelation' ]) !!}
                                        @error('cast_ur')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('regRelationName', __('vrs.regRelationName')) !!}
                                        {!! Form::text('regRelationName', old('regRelationName'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'regRelationName' ]) !!}
                                        @error('cast_ur')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('cnic', __('vrs.cnic')) !!}
                                        {!! Form::text('cnic', old('cnic'), ['class' => 'form-control form-control-sm ','id' => 'cnic' ]) !!}
                                        @error('cnic')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group clearfix col-md-6">
                                        {!! Form::label('Gender', __('vrs.Gender')) !!}<br>
                                        <div class="icheck-primary d-inline">
                                            <input type="radio" id="male" name="gender" value="male" checked="">
                                            <label for="male">{{__('vrs.male')}}</label>
                                        </div>
                                        <div class="icheck-primary d-inline">
                                            <input type="radio" id="female" name="gender" value="female">
                                            <label for="female">{{__('vrs.female')}}</label>
                                        </div>
                                        <div class="icheck-primary d-inline">
                                            <input type="radio" id="other" name="gender" value="other">
                                            <label for="other">{{__('vrs.other')}}</label>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        {!! Form::label('regAddress', __('vrs.regAddress')) !!}
                                        {!! Form::text('regAddress', old('regAddress'), ['class' => 'form-control form-control-sm ','id' => 'regAddress' ]) !!}
                                        @error('regAddress')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group col-md-6">
                                        {!! Form::label('regCity', __('vrs.regCity')) !!}
                                        {!! Form::text('regCity', old('regCity'), ['class' => 'form-control form-control-sm ','id' => 'regCity' ]) !!}
                                        @error('regCity')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>


                                </div>
                            </div>
                            {{--Right Section--}}
                            <div class="col-md-6">
                                <div class="row border-left px-3 align-items-center">
                                    <div class="col-md-12 info-heading-parent">
                                        <p class="info-heading">{{__('Media Information')}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="picture['first']">{{ __('Registration Pic 1') }}</label>
                                            <div class="input-group input-group-sm">
                                                <div class="custom-file">
                                                    <input type="file" name="first_picture"
                                                           class="custom-file-input form-control form-control-sm-sm"
                                                           id="picture['first']"
                                                           onchange="Custom.previewImage(this,'picture_first')">
                                                    <label class="custom-file-label" for="picture['back']">{{ __('system.choose_file') }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="picture_first text-center my-3">
                                            <img src="{{ asset('img/defaults/user-picture.png') }}" class="img-thumbnail h-150">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="picture['second']">{{ __('Registration Pic 2') }}</label>
                                            <div class="input-group input-group-sm">
                                                <div class="custom-file">
                                                    <input type="file" name="second_picture"
                                                           class="custom-file-input form-control form-control-sm-sm"
                                                           id="picture['second']"
                                                           onchange="Custom.previewImage(this,'second_image')">
                                                    <label class="custom-file-label" for="picture['second']">{{ __('system.choose_file') }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="second_image text-center my-3">
                                            <img src="{{ asset('img/defaults/user-picture.png') }}" class="img-thumbnail h-150">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="picture['third']">{{ __('Scanned Docs') }}</label>
                                            <div class="input-group input-group-sm">
                                                <div class="custom-file">
                                                    <input type="file" name="third_picture"
                                                           class="custom-file-input form-control form-control-sm-sm"
                                                           id="picture['third']"
                                                           onchange="Custom.previewImage(this,'third_image')">
                                                    <label class="custom-file-label" for="picture['third']">{{ __('system.choose_file') }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="third_image text-center my-3">
                                            <img src="{{ asset('img/defaults/user-picture.png') }}" class="img-thumbnail h-150">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="row px-3">
                                    <div class="col-md-12">
                                        <div class="col-md-12 info-heading-parent">
                                            <p class="info-heading">Vehicle Used By</p>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        {!! Form::label('vehicleUsedBy', __('vrs.vehicleUsedBy')) !!}
                                        {!! Form::text('vehicleUsedBy', old('vehicleUsedBy'), ['class' => 'form-control form-control-sm ','id' => 'vehicleUsedBy' ]) !!}
                                        @error('vehicleUsedBy')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('vehicleUserName', __('vrs.vehicleUserName')) !!}
                                        {!! Form::text('vehicleUserName', old('vehicleUserName'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'vehicleUserName' ]) !!}
                                        @error('vehicleUserName')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group clearfix col-md-6">
                                        {!! Form::label('vehicleUserGender', __('vrs.vehicleUserGender')) !!}<br>
                                        <div class="icheck-primary d-inline">
                                            <input type="radio" id="male" name="vehicleUserGender" value="male" checked="">
                                            <label for="male">{{__('vrs.male')}}</label>
                                        </div>
                                        <div class="icheck-primary d-inline">
                                            <input type="radio" id="female" name="vehicleUserGender" value="female">
                                            <label for="female">{{__('vrs.female')}}</label>
                                        </div>
                                        <div class="icheck-primary d-inline">
                                            <input type="radio" id="other" name="vehicleUserGender" value="other">
                                            <label for="other">{{__('vrs.other')}}</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('vehicleUserRelation', __('vrs.vehicleUserRelation')) !!}
                                        {!! Form::text('vehicleUserRelation', old('vehicleUserRelation'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'vehicleUserRelation' ]) !!}
                                        @error('vehicleUserRelation')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('vehicleUserRelationName', __('vrs.vehicleUserRelationName')) !!}
                                        {!! Form::text('vehicleUserRelationName', old('vehicleUserRelationName'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'vehicleUserRelationName' ]) !!}
                                        @error('vehicleUserRelationName')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('vehicleUserCNIC', __('vrs.vehicleUserCNIC')) !!}
                                        {!! Form::text('vehicleUserCNIC', old('vehicleUserCNIC'), ['class' => 'form-control form-control-sm ','id' => 'vehicleUserCNIC' ]) !!}
                                        @error('vehicleUserCNIC')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('vehicleUserCNICQR', __('vrs.vehicleUserCNICQR')) !!}
                                        {!! Form::text('vehicleUserCNICQR', old('vehicleUserCNICQR'), ['class' => 'form-control form-control-sm ','id' => 'vehicleUserCNICQR' ]) !!}
                                        @error('vehicleUserCNICQR')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('vehicleUserCNICExpiry', __('vrs.vehicleUserCNICExpiry')) !!}
                                        {!! Form::text('vehicleUserCNICExpiry', old('vehicleUserCNICExpiry'), ['class' => 'form-control form-control-sm ','id' => 'vehicleUserCNICExpiry' ]) !!}
                                        @error('vehicleUserCNICExpiry')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('vehicleUserLicense', __('vrs.vehicleUserLicense')) !!}
                                        {!! Form::text('vehicleUserLicense', old('vehicleUserLicense'), ['class' => 'form-control form-control-sm ','id' => 'vehicleUserLicense' ]) !!}
                                        @error('vehicleUserLicense')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('vehicleUserLicenseExpiry', __('vrs.vehicleUserLicenseExpiry')) !!}
                                        {!! Form::text('vehicleUserLicenseExpiry', old('vehicleUserLicenseExpiry'), ['class' => 'form-control form-control-sm ','id' => 'vehicleUserLicenseExpiry' ]) !!}
                                        @error('vehicleUserLicenseExpiry')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('Contact1', __('vrs.Contact1')) !!}
                                        {!! Form::text('Contact1', old('Contact1'), ['class' => 'form-control form-control-sm ','id' => 'Contact1' ]) !!}
                                        @error('Contact1')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('Contact2', __('vrs.Contact2')) !!}
                                        {!! Form::text('Contact2', old('Contact2'), ['class' => 'form-control form-control-sm ','id' => 'Contact2' ]) !!}
                                        @error('Contact2')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>

                                </div>
                            </div>
                        </div>

                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" href="#tab-property" data-toggle="pill" role="tab"
                                   aria-selected="true">{{__('Property')}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tab-primarycontact" data-toggle="pill" role="tab"
                                   aria-selected="false">{{__('Primary Contact')}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tab-secondarycontact" data-toggle="pill" role="tab"
                                   aria-selected="false">{{__('Secondary Contact')}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tab-payment" data-toggle="pill" role="tab"
                                   aria-selected="false">{{__('Payment')}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tab-sticker" data-toggle="pill" role="tab"
                                   aria-selected="false">{{__('Sticker')}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tab-etag" data-toggle="pill" role="tab"
                                   aria-selected="false">{{__('E-Tag')}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tab-sc" data-toggle="pill" role="tab"
                                   aria-selected="false">{{__('SC')}}</a>
                            </li>
                        </ul>
                        <div class="tab-content p-3">
                            <div class="tab-pane fade active show" id="tab-property" role="tabpanel">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        {!! Form::label('propertyType', __('Property Type')) !!}
                                        {!! Form::text('propertyType', old('propertyType'), ['class' => 'form-control form-control-sm ','id' => 'propertyType' ]) !!}
                                        @error('propertyType')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        {!! Form::label('propertyCategory', __('Property Category')) !!}
                                        {!! Form::text('propertyCategory', old('propertyCategory'), ['class' => 'form-control form-control-sm ','id' => 'propertyCategory' ]) !!}
                                        @error('propertyCategory')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        {!! Form::label('propertyBlock', __('Property Block')) !!}
                                        {!! Form::text('propertyBlock', old('propertyBlock'), ['class' => 'form-control form-control-sm ','id' => 'propertyBlock' ]) !!}
                                        @error('propertyBlock')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        {!! Form::label('plotNo', __('Plot Number')) !!}
                                        {!! Form::text('plotNo', old('plotNo'), ['class' => 'form-control form-control-sm ','id' => 'plotNo' ]) !!}
                                        @error('plotNo')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        {!! Form::label('FloorNo', __('Floor Number')) !!}
                                        {!! Form::text('FloorNo', old('FloorNo'), ['class' => 'form-control form-control-sm ','id' => 'FloorNo' ]) !!}
                                        @error('FloorNo')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        {!! Form::label('ShopNo', __('Shop Number')) !!}
                                        {!! Form::text('ShopNo', old('ShopNo'), ['class' => 'form-control form-control-sm ','id' => 'ShopNo' ]) !!}
                                        @error('ShopNo')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab-primarycontact" role="tabpanel">
                                <div class="row">
                                <div class="form-group col-md-6">
                                    {!! Form::label('PrimaryName', __('vrs.PrimaryName')) !!}
                                    {!! Form::text('PrimaryName', old('PrimaryName'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'PrimaryName' ]) !!}
                                    @error('PrimaryName')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    {!! Form::label('PrimaryRelation', __('vrs.PrimaryRelation')) !!}
                                    {!! Form::text('PrimaryRelation', old('PrimaryRelation'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'PrimaryRelation' ]) !!}
                                    @error('PrimaryRelation')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    {!! Form::label('PrimaryRelationName', __('vrs.PrimaryRelationName')) !!}
                                    {!! Form::text('PrimaryRelationName', old('PrimaryRelationName'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'PrimaryRelationName' ]) !!}
                                    @error('PrimaryRelationName')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    {!! Form::label('PrimaryCNIC', __('vrs.PrimaryCNIC')) !!}
                                    {!! Form::text('PrimaryCNIC', old('PrimaryCNIC'), ['class' => 'form-control form-control-sm ','id' => 'PrimaryCNIC' ]) !!}
                                    @error('PrimaryCNIC')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    {!! Form::label('PrimaryCNICQR', __('vrs.PrimaryCNICQR')) !!}
                                    {!! Form::text('PrimaryCNICQR', old('PrimaryCNICQR'), ['class' => 'form-control form-control-sm ','id' => 'PrimaryCNICQR' ]) !!}
                                    @error('PrimaryCNICQR')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    {!! Form::label('PrimaryCNICExpiry', __('vrs.PrimaryCNICExpiry')) !!}
                                    {!! Form::text('PrimaryCNICExpiry', old('PrimaryCNICExpiry'), ['class' => 'form-control form-control-sm ','id' => 'PrimaryCNICExpiry' ]) !!}
                                    @error('PrimaryCNICExpiry')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    {!! Form::label('PrimaryContact1', __('vrs.PrimaryContact1')) !!}
                                    {!! Form::text('PrimaryContact1', old('PrimaryContact1'), ['class' => 'form-control form-control-sm ','id' => 'PrimaryContact1' ]) !!}
                                    @error('PrimaryContact1')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    {!! Form::label('PrimaryContact2', __('vrs.PrimaryContact2')) !!}
                                    {!! Form::text('PrimaryContact2', old('PrimaryContact2'), ['class' => 'form-control form-control-sm ','id' => 'PrimaryContact2' ]) !!}
                                    @error('PrimaryContact24t')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab-secondarycontact" role="tabpanel">
                                <div class="row">
                                <div class="form-group col-md-6">
                                    {!! Form::label('SecondaryName', __('vrs.SecondaryName')) !!}
                                    {!! Form::text('SecondaryName', old('SecondaryName'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'SecondaryName' ]) !!}
                                    @error('SecondaryName')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    {!! Form::label('SecondaryRelation', __('vrs.SecondaryRelation')) !!}
                                    {!! Form::text('SecondaryRelation', old('SecondaryRelation'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'SecondaryRelation' ]) !!}
                                    @error('SecondaryRelation')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    {!! Form::label('SecondaryRelationName', __('vrs.SecondaryRelationName')) !!}
                                    {!! Form::text('SecondaryRelationName', old('SecondaryRelationName'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'SecondaryRelationName' ]) !!}
                                    @error('SecondaryRelationName')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    {!! Form::label('SecondaryCNIC', __('vrs.SecondaryCNIC')) !!}
                                    {!! Form::text('SecondaryCNIC', old('SecondaryCNIC'), ['class' => 'form-control form-control-sm ','id' => 'SecondaryCNIC' ]) !!}
                                    @error('SecondaryCNIC')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    {!! Form::label('SecondaryCNICQR', __('vrs.SecondaryCNICQR')) !!}
                                    {!! Form::text('SecondaryCNICQR', old('SecondaryCNICQR'), ['class' => 'form-control form-control-sm ','id' => 'SecondaryCNICQR' ]) !!}
                                    @error('SecondaryCNICQR')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    {!! Form::label('SecondaryCNICExpiry', __('vrs.SecondaryCNICExpiry')) !!}
                                    {!! Form::text('SecondaryCNICExpiry', old('SecondaryCNICExpiry'), ['class' => 'form-control form-control-sm ','id' => 'SecondaryCNICExpiry' ]) !!}
                                    @error('SecondaryCNICExpiry')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    {!! Form::label('SecondaryContact1', __('vrs.SecondaryContact1')) !!}
                                    {!! Form::text('SecondaryContact1', old('SecondaryContact1'), ['class' => 'form-control form-control-sm ','id' => 'SecondaryContact1' ]) !!}
                                    @error('SecondaryContact1')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    {!! Form::label('SecondaryContact2', __('vrs.SecondaryContact2')) !!}
                                    {!! Form::text('SecondaryContact2', old('SecondaryContact2'), ['class' => 'form-control form-control-sm ','id' => 'SecondaryContact2' ]) !!}
                                    @error('SecondaryContact2')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab-payment" role="tabpanel">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        {!! Form::label('pmtReceiptNo', __('vrs.pmtReceiptNo')) !!}
                                        {!! Form::text('pmtReceiptNo', old('pmtReceiptNo'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'pmtReceiptNo' ]) !!}
                                        @error('pmtReceiptNo')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        {!! Form::label('pmtReceiptDate', __('vrs.pmtReceiptDate')) !!}
                                        {!! Form::text('pmtReceiptDate', old('pmtReceiptDate'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'pmtReceiptDate' ]) !!}
                                        @error('pmtReceiptDate')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4">
                                        {!! Form::label('pmtReceiptAmount', __('vrs.pmtReceiptAmount')) !!}
                                        {!! Form::text('pmtReceiptAmount', old('pmtReceiptAmount'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'pmtReceiptAmount' ]) !!}
                                        @error('pmtReceiptAmount')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab-sticker" role="tabpanel">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        {!! Form::label('stickerApplied', __('vrs.stickerApplied')) !!}
                                        {!! Form::text('stickerApplied', old('stickerApplied'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'stickerApplied' ]) !!}
                                        @error('stickerApplied')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('AppliedBy', __('vrs.AppliedBy')) !!}
                                        {!! Form::text('AppliedBy', old('AppliedBy'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'AppliedBy' ]) !!}
                                        @error('AppliedBy')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('AppliedDate', __('vrs.AppliedDate')) !!}
                                        {!! Form::text('AppliedDate', old('AppliedDate'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'AppliedDate' ]) !!}
                                        @error('AppliedDate')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('ReceivedBy', __('vrs.ReceivedBy')) !!}
                                        {!! Form::text('ReceivedBy', old('ReceivedBy'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'ReceivedBy' ]) !!}
                                        @error('ReceivedBy')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                        </div>

                            <div class="tab-pane fade" id="tab-etag" role="tabpanel">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        {!! Form::label('etagType', __('vrs.etagType')) !!}
                                        {!! Form::text('etagType', old('etagType'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'etagType' ]) !!}
                                        @error('etagType')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('etagNo', __('vrs.etagNo')) !!}
                                        {!! Form::text('etagNo', old('etagNo'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'etagNo' ]) !!}
                                        @error('etagNo')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('etagIssueDate', __('vrs.etagIssueDate')) !!}
                                        {!! Form::text('etagIssueDate', old('etagIssueDate'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'etagIssueDate' ]) !!}
                                        @error('etagIssueDate')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('etagStatus', __('vrs.etagStatus')) !!}
                                        {!! Form::text('etagStatus', old('etagStatus'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'etagStatus' ]) !!}
                                        @error('etagStatus')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('etagIssueRequest', __('vrs.etagIssueRequest')) !!}
                                        {!! Form::text('etagIssueRequest', old('etagIssueRequest'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'etagIssueRequest' ]) !!}
                                        @error('etagIssueRequest')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('etagIssueRequestDate', __('vrs.etagIssueRequestDate')) !!}
                                        {!! Form::text('etagIssueRequestDate', old('etagIssueRequestDate'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'etagIssueRequestDate' ]) !!}
                                        @error('etagIssueRequestDate')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('etagBlocked', __('vrs.etagBlocked')) !!}
                                        {!! Form::text('etagBlocked', old('etagBlocked'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'etagBlocked' ]) !!}
                                        @error('etagBlocked')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('etagBlockDate', __('vrs.etagBlockDate')) !!}
                                        {!! Form::text('etagBlockDate', old('etagBlockDate'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'etagBlockDate' ]) !!}
                                        @error('etagBlockDate')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('etagBlockRemarks', __('vrs.etagBlockRemarks')) !!}
                                        {!! Form::text('etagBlockRemarks', old('etagBlockRemarks'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'etagBlockRemarks' ]) !!}
                                        @error('etagBlockRemarks')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="tab-sc" role="tabpanel">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        {!! Form::label('scCardType', __('vrs.scCardType')) !!}
                                        {!! Form::text('scCardType', old('scCardType'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'scCardType' ]) !!}
                                        @error('scCardType')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('scCardNo', __('vrs.scCardNo')) !!}
                                        {!! Form::text('scCardNo', old('scCardNo'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'scCardNo' ]) !!}
                                        @error('scCardNo')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('scCardDate', __('vrs.scCardDate')) !!}
                                        {!! Form::text('scCardDate', old('scCardDate'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'scCardDate' ]) !!}
                                        @error('scCardDate')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('scCardPrep', __('vrs.scCardPrep')) !!}
                                        {!! Form::text('scCardPrep', old('scCardPrep'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'scCardPrep' ]) !!}
                                        @error('scCardPrep')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('scCardPrepDate', __('vrs.scCardPrepDate')) !!}
                                        {!! Form::text('scCardPrepDate', old('scCardPrepDate'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'scCardPrepDate' ]) !!}
                                        @error('scCardPrepDate')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('scCardIssue', __('vrs.scCardIssue')) !!}
                                        {!! Form::text('scCardIssue', old('scCardIssue'), ['class' => 'form-control form-control-sm','dir'=>'rtl','id' => 'scCardIssue' ]) !!}
                                        @error('scCardIssue')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('scCardIssueDate', __('vrs.scCardIssueDate')) !!}
                                        {!! Form::text('scCardIssueDate', old('scCardIssueDate'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'scCardIssueDate' ]) !!}
                                        @error('scCardIssueDate')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('scCardStatus', __('vrs.scCardStatus')) !!}
                                        {!! Form::text('scCardStatus', old('scCardStatus'), ['class' => 'form-control form-control-sm ','dir'=>'rtl','id' => 'scCardStatus' ]) !!}
                                        @error('scCardStatus')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>


                    </div>
                </div>
                <div class="card-footer clearfix text-right">
                    <button type="submit" class="btn btn-info">{{ __('vrs.vehicle.save') }}</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scriptInnerFiles')
    <script type="text/javascript"
            src="{{ asset('plugins/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('plugins/QReader/qrcode.js') }}"></script>
@stop
@section('pageScript')
    <script>
        function getTypeCompanies(cElement) {

            $('.vcompanies_holder').selectpicker('destroy');
            var cValue = $(cElement).find('option:selected').val();
            var data = {
                key: 'companies',
                parent: cValue
            }
            Ajax.call('/dependent/dropdown', data, 'POST', function (response) {
                if (response.status == 'true') {
                    var html = '<option value="" selected disabled>{{__('vrs.vehicle.company')}}</option>';
                    $.each(response.data, function (key, value) {
                        html += "<option value=" + value.id + ">" + value.name + " ( " + value.name_ur + " )</option>";
                    });
                } else {
                    var html = '<option value="" selected disabled>{{__('vrs.vehicle.no_company')}}</option>';
                }
                $(".vcompanies_holder").empty().append(html).selectpicker('refresh');
            });
        }

        function getCompanyModels(cElement) {
            $('.cmodels_holder').selectpicker('destroy');
            var cValue = $(cElement).find('option:selected').val();
            var data = {
                key: 'models',
                parent: cValue
            }
            Ajax.call('/dependent/dropdown', data, 'POST', function (response) {
                if (response.status == 'true') {
                    var html = '<option value="" selected disabled>{{__('vrs.vehicle.model')}}</option>';
                    $.each(response.data, function (key, value) {
                        html += "<option value=" + value.id + ">" + value.name + " ( " + value.name_ur + " )</option>";
                    });
                } else {
                    var html = '<option value="" selected disabled>{{__('vrs.vehicle.no_model')}}</option>';
                }
                $(".cmodels_holder").empty().append(html).selectpicker('refresh');
            });
        }
    </script>
@endsection

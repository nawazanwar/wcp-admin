@extends('layouts.colorbox')
@section('pageTitle', $pageTitle)
@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-header row align-items-center mx-1">
                    <div class="card-title col-xl-6 col-lg-6 col-md-6">
                        @can('create',\App\Models\VNature::class)
                            {!! HTML::decode(link_to_route('vrs-vehicle-nature.create',' <i class="fa fa-plus"></i> ',null,['class'=>'btn btn-xs bg-gradient-success text-center'])) !!}
                        @endcan
                    </div>
                </div>
                <div class="card-body">
                    @include('partials.dashboard.message')
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{__('system.name')}}</th>
                            <th class="text-right">{{__('system.name_ur')}}</th>
                            <th>{{__('system.vehicle_type')}}</th>
                            <th class="text-right">{{__('system.vehicle_type_ur')}}</th>
                            <th class="text-center">{{__('system.created_date')}}</th>
                            <th class="text-center">{{__('system.last_modified')}}</th>
                            <th class="text-center">{{__('system.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($data)>0)
                            @foreach($data as $d)
                                <tr>
                                    <td>{{$d->name}}</td>
                                    <td class="text-right">{{$d->name_ur}}</td>
                                    <td>{{$d->type->name}}</td>
                                    <td class="text-right">{{$d->type->name_ur}}</td>
                                    <td class="text-center">{{$d->created_at}}</td>
                                    <td class="text-center">{{$d->updated_at->diffForHumans()}}</td>
                                    <td class="text-center">
                                        @can('delete',\App\Models\VNature::class)
                                            <form action="{{ route('vrs-vehicle-nature.destroy',$d->id) }}"
                                                  method="POST" style="display: contents;">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-xs bg-gradient-danger"
                                                        onclick="return confirm('Do you really wants to delete this?')">
                                                    <i class="fa fa-trash-o"></i>
                                                </button>
                                            </form>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="text-center">
                                <td colspan="12" class="p-3">No Department Found</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix">
                    {{ $data->links() }}
                </div>
            </div>
        </div>
    </div>
@stop

@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/colorbox/colorbox.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-header row align-items-center mx-1">
                    <div class="card-title col-xl-10 col-lg-10 col-md-10">
                        @can('read',\App\Models\Type::class)
                            {!! HTML::decode(link_to_route('vrs-vehicle-types.index',' <i class="fa fa-eye"></i> '.__('vrs.vehicle_types'),null,['class'=>'iframe btn bg-gradient-success btn-sm'])) !!}
                        @endcan
                        @can('read',\App\Models\VCompany::class)
                            {!! HTML::decode(link_to_route('vrs-vehicle-companies.index',' <i class="fa fa-eye"></i> '.__('vrs.vehicle_companies'),null,['class'=>'iframe btn bg-gradient-info btn-sm'])) !!}
                        @endcan
                        @can('read',\App\Models\VModel::class)
                            {!! HTML::decode(link_to_route('vrs-vehicle-models.index',' <i class="fa fa-eye"></i> '.__('vrs.vehicle_models'),null,['class'=>'iframe btn bg-gradient-danger btn-sm'])) !!}
                        @endcan
                        @can('read',\App\Models\VNature::class)
                            {!! HTML::decode(link_to_route('vrs-vehicle-nature.index',' <i class="fa fa-eye"></i> '.__('vrs.vehicle_nature'),null,['class'=>'iframe btn bg-gradient-primary btn-sm'])) !!}
                        @endcan
                    </div>
                    <div class="card-title col-xl-2 col-lg-2 col-md-2 text-right">
                        @can('create',\App\Models\Vehicle::class)
                            {!! HTML::decode(link_to_route('vrs-vehicles.create',' <i class="fa fa-plus"></i> '.__('system.create'),null,['class'=>'btn bg-gradient-success btn-sm'])) !!}
                        @endcan
                    </div>
                </div>
                <div class="card-body">
                </div>
                <div class="card-footer clearfix">
                </div>
            </div>
        </div>
    </div>
@stop
@section('scriptInnerFiles')
    <script src="{{ asset('plugins/colorbox/jquery.colorbox.js') }}"></script>
@endsection

@section('pageScript')
    <script>
        $(document).ready(function () {
            $(".iframe").colorbox({iframe: true, width: "80%", height: "80%"});
        });
    </script>
@endsection

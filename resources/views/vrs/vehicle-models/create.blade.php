@extends('layouts.colorbox')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endsection

@section('content')
    <div class="card card-solid">
        <div class="card-header">
            <div class="card-title">
                @can('create',\App\Models\VModel::class)
                    {!! HTML::decode(link_to_route('vrs-vehicle-models.index',' <i class="fa fa-arrow-left"></i> ',null,['class'=>'btn btn-xs bg-gradient-success text-center'])) !!}
                @endcan
            </div>
        </div>
        <div class="card-body pb-0">
            {!! Form::open(['route' => ['vrs-vehicle-models.store'], 'method' => 'POST','files' => true]) !!}
            {!! csrf_field() !!}
            <div class="row">
                <div class="form-group col-md-4">
                    {!! Form::label('vehicle_company',__('vrs.vehicle_company')) !!}
                    <select
                        data-live-search="true"
                        data-max-options="3"
                        data-selected-text-format="count > 3"
                        class="form-control custom-select-box selectpicker show-menu-arrow"
                        name="vehicle_company" id="vehicle_company">
                        @foreach($vcompanies as $key=>$value)
                            @if(old('vehicle_company', $value->id) == $value->id )
                                <option value="{{ $value->id }}" selected>{{ $value->name }} - {{ $value->name_ur }}</option>
                            @else
                                <option value="{{ $value->id }}">{{ $value->name }} - {{ $value->name_ur }}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('vehicle_company')
                     <div class="error">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-md-4">
                    {!! Form::label('inputName', __('system.name')) !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'autofocus', 'id' => 'inputName' ]) !!}
                    @error('name')
                    <div class="error">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-md-4 form-group text-right">
                    {!! Form::label('inputNameUR',__('system.name_ur'),['class'=>'pr-2']) !!}
                    {!! Form::text('name_ur', old('name_ur'), ['class' => 'form-control urduWriter ur','dir'=>'rtl','id' => 'inputNameUR' ]) !!}
                    @error('name_ur')
                    <div class="error">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group text-right col-12">
                    {!! Form::submit(__('hrm.save'), array('class' => 'btn btn-primary btn-sm')) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('scriptInnerFiles')
    <script src="{{ asset('plugins/keybaord/urdu.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('plugins/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
@stop


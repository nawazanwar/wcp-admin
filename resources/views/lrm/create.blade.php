@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/multiform/css/style.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/multiform/css/arrows.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/multiform/css/dots.min.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12 mx-auto">
                            <div id="dotwizard">
                                <ul>
                                    <li><a href="#application-form">1<br/><small>Application Form</small></a></li>
                                </ul>
                                <div class="mt-4">
                                    <div id="application-form">
                                        @include('lrm.form.application.create')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scriptInnerFiles')
    <script src="{{ asset('plugins/multiform/js/style.min.js') }}"></script>
@endsection

@section('pageScript')
    <script>
        $(document).ready(function () {
            $('#dotwizard').smartWizard({
                selected: 0,
                theme: 'dots',
                autoAdjustHeight: true,
                transitionEffect: 'fade',
                showStepURLhash: false,
            });
            $("#arrowwizard").smartWizard({
                selected: 0,
                theme: 'arrows',
                autoAdjustHeight: true,
                transitionEffect: 'fade',
                showStepURLhash: false,
            });
        });
    </script>
@endsection

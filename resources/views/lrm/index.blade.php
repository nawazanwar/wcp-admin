@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/colorbox/colorbox.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-header row align-items-center mx-1">
                    <div class="card-title col-10">
                        @can('read',\App\Models\LRM::class)
                            {!! HTML::decode(link_to_route('lrm-ministry.index',' <i class="fa fa-eye"></i> '.__('lrm.ministries'),null,['class'=>'iframe btn bg-gradient-info btn-xs'])) !!}
                        @endcan
                        @can('read',\App\Models\LRM::class)
                            {!! HTML::decode(link_to_route('lrm-organization.index',' <i class="fa fa-eye"></i> '.__('lrm.organizations'),null,['class'=>'iframe btn bg-gradient-info btn-xs'])) !!}
                        @endcan
                        @can('read',\App\Models\LRM::class)
                            {!! HTML::decode(link_to_route('lrm-department.index',' <i class="fa fa-eye"></i> '.__('lrm.departments.title'),null,['class'=>'iframe btn bg-gradient-info btn-xs'])) !!}
                        @endcan
                        @can('read',\App\Models\LRM::class)
                            {!! HTML::decode(link_to_route('lrm-rank.index',' <i class="fa fa-eye"></i> '.__('lrm.ranks.title'),null,['class'=>'iframe btn bg-gradient-info btn-xs'])) !!}
                        @endcan
                        @can('read',\App\Models\LRM::class)
                            {!! HTML::decode(link_to_route('lrm-grade.index',' <i class="fa fa-eye"></i> '.__('lrm.grades.title'),null,['class'=>'iframe btn bg-gradient-info btn-xs'])) !!}
                        @endcan
                        @can('read',\App\Models\LRM::class)
                            {!! HTML::decode(link_to_route('lrm-wing.index',' <i class="fa fa-eye"></i> '.__('lrm.wings.title'),null,['class'=>'iframe btn bg-gradient-info btn-xs'])) !!}
                        @endcan
                    </div>
                    <div class="card-title col-2 text-right">
                        @can('read',\App\Models\LRM::class)
                            {!! HTML::decode(link_to_route('lrm-settings.index',' <i class="fa fa-plus"></i> '.__('lrm.settings'),null,['class'=>'iframe btn bg-gradient-info btn-xs'])) !!}
                        @endcan
                        @can('read',\App\Models\LRM::class)
                            {!! HTML::decode(link_to_route('location.index',' <i class="fa fa-plus"></i> '.__('lrm.locations.title'),null,['class'=>'iframe btn bg-gradient-warning btn-xs'])) !!}
                        @endcan
                    </div>
                </div>
                <div class="card-body">
                    @include('lrm.boxes')
                </div>
                <div class="card-footer clearfix">
                </div>
            </div>
        </div>
    </div>
@stop
@section('scriptInnerFiles')
    <script src="{{ asset('plugins/colorbox/jquery.colorbox.js') }}"></script>
@endsection

@section('pageScript')
    <script>
        $(document).ready(function () {
            $(".iframe").colorbox({iframe: true, width: "80%", height: "80%"});
        });
    </script>
@endsection

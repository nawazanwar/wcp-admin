@extends('layouts.colorbox')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endsection

@section('content')
    @include('partials.dashboard.toaster')
    <div class="row">
        <div class="col-md-2">
            <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                <a class="nav-link active" id="vert-tabs-home-tab" data-toggle="pill" href="#vert-tabs-title" role="tab"
                   aria-controls="vert-tabs-home" aria-selected="true">Titles</a>
                <a class="nav-link" id="vert-tabs-profile-tab" data-toggle="pill" href="#vert-tabs-postfix" role="tab"
                   aria-controls="vert-tabs-profile" aria-selected="false">Postfixes</a>
                <a class="nav-link" id="vert-tabs-messages-tab" data-toggle="pill" href="#vert-tabs-prefix" role="tab"
                   aria-controls="vert-tabs-messages" aria-selected="false">Prefixes</a>
                <a class="nav-link" id="vert-tabs-messages-tab" data-toggle="pill" href="#vert-tabs-nationalities"
                   role="tab"
                   aria-controls="vert-tabs-messages" aria-selected="false">Nationalities</a>
                <a class="nav-link" id="vert-tabs-messages-tab" data-toggle="pill" href="#vert-tabs-relations"
                   role="tab"
                   aria-controls="vert-tabs-messages" aria-selected="false">Relations</a>
                <a class="nav-link" id="vert-tabs-messages-tab" data-toggle="pill" href="#vert-tabs-qualifications"
                   role="tab"
                   aria-controls="vert-tabs-messages" aria-selected="false">Qualifications</a>
                <a class="nav-link" id="vert-tabs-messages-tab" data-toggle="pill" href="#vert-tabs-professions"
                   role="tab"
                   aria-controls="vert-tabs-messages" aria-selected="false">Professions</a>
                <a class="nav-link" id="vert-tabs-messages-tab" data-toggle="pill" href="#vert-tabs-company-types"
                   role="tab"
                   aria-controls="vert-tabs-messages" aria-selected="false">Company Types</a>
                <a class="nav-link" id="vert-tabs-messages-tab" data-toggle="pill" href="#vert-tabs-property-types"
                   role="tab"
                   aria-controls="vert-tabs-messages" aria-selected="false">Property Types</a>
            </div>
        </div>
        <div class="col-md-10">
            {!! Form::open(['route' => ['lrm-settings.store'], 'method' => 'POST','files'=>'true']) !!}
            {!! csrf_field() !!}
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-12 text-right p-1">
                    <input type="submit" class="btn btn-xs btn-info" value="Update Settings">
                </div>
            </div>
            <div class="tab-content" id="vert-tabs-tabContent">
                <div class="tab-pane text-left fade active show" id="vert-tabs-title" role="tabpanel"
                     aria-labelledby="vert-tabs-home-tab">
                    @include('lrm.settings.templates.titles')
                </div>
                <div class="tab-pane fade" id="vert-tabs-postfix" role="tabpanel"
                     aria-labelledby="vert-tabs-profile-tab">
                    @include('lrm.settings.templates.postfixes')
                </div>
                <div class="tab-pane fade" id="vert-tabs-prefix" role="tabpanel"
                     aria-labelledby="vert-tabs-messages-tab">
                    @include('lrm.settings.templates.prefixes')
                </div>
                <div class="tab-pane fade" id="vert-tabs-nationalities" role="tabpanel"
                     aria-labelledby="vert-tabs-messages-tab">
                    @include('lrm.settings.templates.nationalities')
                </div>
                <div class="tab-pane fade" id="vert-tabs-relations" role="tabpanel"
                     aria-labelledby="vert-tabs-messages-tab">
                    @include('lrm.settings.templates.relations')
                </div>
                <div class="tab-pane fade" id="vert-tabs-qualifications" role="tabpanel"
                     aria-labelledby="vert-tabs-messages-tab">
                    @include('lrm.settings.templates.qualifications')
                </div>
                <div class="tab-pane fade" id="vert-tabs-professions" role="tabpanel"
                     aria-labelledby="vert-tabs-messages-tab">
                    @include('lrm.settings.templates.professions')
                </div>
                <div class="tab-pane fade" id="vert-tabs-company-types" role="tabpanel"
                     aria-labelledby="vert-tabs-company-types">
                    @include('lrm.settings.templates.company-types')
                </div>
                <div class="tab-pane fade" id="vert-tabs-property-types" role="tabpanel"
                     aria-labelledby="vert-tabs-company-types">
                    @include('lrm.settings.templates.property-types')
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('pageScript')
    <script>
        function add(type) {
            switch (type) {
                case "title":
                    addTitle();
                    break;
                case "prefix":
                    addPreFix();
                    break;
                case "postfix":
                    addPostFix();
                    break;
                case "nationality":
                    addNationality();
                    break;
                case "relation":
                    addRelation();
                    break;
                case "qualification":
                    addQualification();
                    break;
                case "profession":
                    addProfession();
                    break;
                case "company_type":
                    addCompanyType();
                    break;
                case "property_type":
                    addPropertyType();
                    break;
            }
        }

        /* Title Functions*/

        function addTitle() {
            var uId = 'id' + (new Date()).getTime();
            var cloned_row = $("#titleTable").find('tr:first-child').clone();
            cloned_row.find('input[type=hidden]').attr('id', uId).val(uId);
            cloned_row.find('input[type=text]').val('').attr('placeholder', 'Start Writing').removeAttr('id');
            $("#titleTable").prepend(cloned_row);
        }

        /* Prefix */

        function addPreFix() {
            var uId = 'id' + (new Date()).getTime();
            var cloned_row = $("#prefixTable").find('tr:first-child').clone();
            cloned_row.find('input[type=hidden]').attr('id', uId).val(uId);
            cloned_row.find('input[type=text]').val('').attr('placeholder', 'Start Writing').removeAttr('id');
            $("#prefixTable").prepend(cloned_row);
        }

        /* Postfix */

        function addPostFix() {
            var uId = 'id' + (new Date()).getTime();
            var cloned_row = $("#postfixTable").find('tr:first-child').clone();
            cloned_row.find('input[type=hidden]').attr('id', uId).val(uId);
            cloned_row.find('input[type=text]').val('').attr('placeholder', 'Start Writing').removeAttr('id');
            $("#postfixTable").prepend(cloned_row);
        }

        /* Nationality */

        function addNationality() {
            var uId = 'id' + (new Date()).getTime();
            var cloned_row = $("#nationalityTable").find('tr:first-child').clone();
            cloned_row.find('input[type=hidden]').attr('id', uId).val(uId);
            cloned_row.find('input[type=text]').val('').attr('placeholder', 'Start Writing').removeAttr('id');
            $("#nationalityTable").prepend(cloned_row);
        }

        /* Relation */

        function addRelation() {
            var uId = 'id' + (new Date()).getTime();
            var cloned_row = $("#relationTable").find('tr:first-child').clone();
            cloned_row.find('input[type=hidden]').attr('id', uId).val(uId);
            cloned_row.find('input[type=text]').val('').attr('placeholder', 'Start Writing').removeAttr('id');
            $("#relationTable").prepend(cloned_row);
        }

        /* Qualifications */

        function addQualification() {
            var uId = 'id' + (new Date()).getTime();
            var cloned_row = $("#qualificationTable").find('tr:first-child').clone();
            cloned_row.find('input[type=hidden]').attr('id', uId).val(uId);
            cloned_row.find('input[type=text]').val('').attr('placeholder', 'Start Writing').removeAttr('id');
            $("#qualificationTable").prepend(cloned_row);
        }

        /* Profession */

        function addProfession() {
            var uId = 'id' + (new Date()).getTime();
            var cloned_row = $("#professionTable").find('tr:first-child').clone();
            cloned_row.find('input[type=hidden]').attr('id', uId).val(uId);
            cloned_row.find('input[type=text]').val('').attr('placeholder', 'Start Writing').removeAttr('id');
            $("#professionTable").prepend(cloned_row);
        }

        /* Company Types*/

        function addCompanyType() {
            var uId = 'id' + (new Date()).getTime();
            var cloned_row = $("#companyTypeTable").find('tr:first-child').clone();
            cloned_row.find('input[type=hidden]').attr('id', uId).val(uId);
            cloned_row.find('input[type=text]').val('').attr('placeholder', 'Start Writing').removeAttr('id');
            $("#companyTypeTable").prepend(cloned_row);
        }

        /* Property Types*/

        function addPropertyType() {
            var uId = 'id' + (new Date()).getTime();
            var cloned_row = $("#propertyTypeTable").find('tr:first-child').clone();
            cloned_row.find('input[type=hidden]').attr('id', uId).val(uId);
            cloned_row.find('input[type=text]').val('').attr('placeholder', 'Start Writing').removeAttr('id');
            $("#propertyTypeTable").prepend(cloned_row);
        }

    </script>
@endsection

<div class="row">
    <div class="col-12 text-right p-3">
        <a class="btn btn-success btn-xs text-white cursor-pointer" onclick="add('postfix');">
            Add
        </a>
    </div>
</div>
<table class="table table-bordered" id="postfixTable">
    @php
        $postfix_settings = \App\Models\LrmSettings::whereParent('postfix')->orderby('id','desc')->get();
    @endphp
    @foreach($postfix_settings as $setting)
        <tr class="my-2 bg-white">
            <td>
                <input type="text" name="settings[postfix][]" class="form-control" value="{{$setting->name}}" placeholder="{{__('system.name')}}">
            </td>
        </tr>
    @endforeach
</table>

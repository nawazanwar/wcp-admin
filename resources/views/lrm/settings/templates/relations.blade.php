<div class="row">
    <div class="col-12 text-right p-3">
        <a class="btn btn-success btn-xs text-white cursor-pointer" onclick="add('relation');">
            Add
        </a>
    </div>
</div>
<table class="table table-bordered" id="relationTable">
    @php
        $relation_settings = \App\Models\LrmSettings::whereParent('relation')->orderby('id','desc')->get();
    @endphp
    @foreach($relation_settings as $setting)
        <tr class="my-2 bg-white">
            <td>
                <input type="text" name="settings[relation][]" class="form-control" value="{{$setting->name}}" placeholder="{{__('system.name')}}">
            </td>
        </tr>
    @endforeach
</table>

<div class="row">
    <div class="col-12 text-right p-3">
        <a class="btn btn-success btn-xs text-white cursor-pointer" onclick="add('qualification');">
            Add
        </a>
    </div>
</div>
<table class="table table-bordered" id="qualificationTable">
    @php
        $qualification_settings = \App\Models\LrmSettings::whereParent('qualification')->orderby('id','desc')->get();
    @endphp
    @foreach($qualification_settings as $setting)
        <tr class="my-2 bg-white">
            <td>
                <input type="text" name="settings[qualification][]" class="form-control" value="{{$setting->name}}" placeholder="{{__('system.name')}}">
            </td>
        </tr>
    @endforeach
</table>

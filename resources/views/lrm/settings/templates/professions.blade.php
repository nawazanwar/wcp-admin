<div class="row">
    <div class="col-12 text-right p-3">
        <a class="btn btn-success btn-xs text-white cursor-pointer" onclick="add('profession');">
            Add
        </a>
    </div>
</div>
<table class="table table-bordered" id="professionTable">
    @php
        $profession_settings = \App\Models\LrmSettings::whereParent('profession')->orderby('id','desc')->get();
    @endphp
    @foreach($profession_settings as $setting)
        <tr class="my-2 bg-white">
            <td>
                <input type="text" name="settings[profession][]" class="form-control" value="{{$setting->name}}" placeholder="{{__('system.name')}}">
            </td>
        </tr>
    @endforeach
</table>

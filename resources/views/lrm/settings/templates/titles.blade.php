<div class="row">
    <div class="col-12 text-right p-3">
        <a class="btn btn-success btn-xs text-white cursor-pointer" onclick="add('title');">
            Add
        </a>
    </div>
</div>
<table class="table table-bordered" id="titleTable">
    @php
        $title_settings = \App\Models\LrmSettings::whereParent('title')->orderby('id','desc')->get();
    @endphp
    @foreach($title_settings as $setting)
        <tr class="my-2 bg-white">
            <td>
                <input type="text" name="settings[title][]" class="form-control" value="{{$setting->name}}" placeholder="{{__('system.name')}}">
            </td>
        </tr>
    @endforeach
</table>

<div class="row">
    <div class="col-12 text-right p-3">
        <a class="btn btn-success btn-xs text-white cursor-pointer" onclick="add('nationality');">
            Add
        </a>
    </div>
</div>
<table class="table table-bordered" id="nationalityTable">
    @php
        $nationality_settings = \App\Models\LrmSettings::whereParent('nationality')->orderby('id','desc')->get();
    @endphp
    @foreach($nationality_settings as $setting)
        <tr class="my-2 bg-white">
            <td>
                <input type="text" name="settings[nationality][]" class="form-control" value="{{$setting->name}}" placeholder="{{__('system.name')}}">
            </td>
        </tr>
    @endforeach
</table>

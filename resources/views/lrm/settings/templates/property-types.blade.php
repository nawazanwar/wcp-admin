<div class="row">
    <div class="col-12 text-right p-3">
        <a class="btn btn-success btn-xs text-white cursor-pointer" onclick="add('property_type');">
            Add
        </a>
    </div>
</div>
<table class="table table-bordered" id="propertyTypeTable">
    @php
        $property_types_settings = \App\Models\LrmSettings::whereParent('property_type')->orderby('id','desc')->get();
    @endphp
    @foreach($property_types_settings as $setting)
        <tr class="my-2 bg-white">
            <td>
                <input type="text" name="settings[property_type][]" class="form-control" value="{{$setting->name}}"
                       placeholder="{{__('system.name')}}">
            </td>
        </tr>
    @endforeach
</table>

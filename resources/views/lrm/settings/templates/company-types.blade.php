<div class="row">
    <div class="col-12 text-right p-3">
        <a class="btn btn-success btn-xs text-white cursor-pointer" onclick="add('company_type');">
            Add
        </a>
    </div>
</div>
<table class="table table-bordered" id="companyTypeTable">
    @php
        $company_types_settings = \App\Models\LrmSettings::whereParent('company_type')->orderby('id','desc')->get();
    @endphp
    @foreach($company_types_settings as $setting)
        <tr class="my-2 bg-white">
            <td>
                <input type="text" name="settings[company_type][]" class="form-control" value="{{$setting->name}}"
                       placeholder="{{__('system.name')}}">
            </td>
        </tr>
    @endforeach
</table>

<div class="row">

    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">Primary Contact (Owner Only)</h6>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="contact_details['primary_contact']['cell_priority_1']">Cell (Priority 1)</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="contact_details['primary_contact']['cell_priority_1']"
                   name="contact_details['primary_contact']['cell_priority_1']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="contact_details['primary_contact']['cell_priority_2']">Cell (Priority 2)</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="contact_details['primary_contact']['cell_priority_2']"
                   name="contact_details['primary_contact']['cell_priority_2']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="contact_details['primary_contact']['cell_whatsApp']">Cell (WhatsApp)</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="contact_details['primary_contact']['cell_whatsApp']"
                   name="contact_details['primary_contact']['cell_whatsApp']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="contact_details['primary_contact']['land_line_no']">Landline No.</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="contact_details['primary_contact']['land_line_no']"
                   name="contact_details['primary_contact']['land_line_no']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="contact_details['primary_contact']['email']">Email</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="contact_details['primary_contact']['email']"
                   name="contact_details['primary_contact']['email']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="contact_details['primary_contact']['facebook']">Facebook</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="contact_details['primary_contact']['facebook']"
                   name="contact_details['primary_contact']['facebook']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="contact_details['primary_contact']['notes']">Notes</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="contact_details['primary_contact']['notes']"
                   name="contact_details['primary_contact']['notes']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="contact_details['primary_contact']['remarks']">Remarks</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="contact_details['primary_contact']['remarks']"
                   name="contact_details['primary_contact']['remarks']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="contact_details['primary_contact']['remarks']">Remarks</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="contact_details['primary_contact']['remarks']"
                   name="contact_details['primary_contact']['remarks']" type="text">
        </div>
    </div>

</div>

<div class="row">

    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">Secondary Contact</h6>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="contact_details['secondary_contact']['title']">Title</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="contact_details_secondary_contact_title"
                   id="contact_details['secondary_contact']['title']"
                   name="contact_details['secondary_contact']['title']" type="text">
            <datalist id="contact_details_secondary_contact_title">
                @php $titles = \App\Models\LrmSettings::whereParent('title')->orderby('id','desc')->get()  @endphp
                @foreach($titles as $title)
                    <option value="{{ $title->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('contact_details_secondary_contact_title','title');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="contact_details['secondary_contact']['prefix']">Prefix</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="contact_details_secondary_contact_prefix"
                   id="contact_details['secondary_contact']['prefix']"
                   name="contact_details['secondary_contact']['prefix']" type="text">
            <datalist id="contact_details_secondary_contact_prefix">
                @php $titles = \App\Models\LrmSettings::whereParent('title')->orderby('id','desc')->get()  @endphp
                @foreach($titles as $title)
                    <option value="{{ $title->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('contact_details_secondary_contact_prefix','Prefix');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="contact_details['secondary_contact']['first_name']">First Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="contact_details['secondary_contact']['first_name']"
                   name="contact_details['secondary_contact']['first_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="contact_details['secondary_contact']['middle_name']">Middle Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="contact_details['secondary_contact']['middle_name']"
                   name="contact_details['secondary_contact']['middle_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="contact_details['secondary_contact']['last_name']">Last Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="contact_details['secondary_contact']['last_name']"
                   name="contact_details['secondary_contact']['last_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="contact_details['secondary_contact']['postfix']">Postfix</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="contact_details_secondary_contact_postfix"
                   id="contact_details['secondary_contact']['postfix']"
                   name="contact_details['secondary_contact']['postfix']" type="text">
            <datalist id="contact_details_secondary_contact_postfix">
                @php $postfixes = \App\Models\LrmSettings::whereParent('postfix')->orderby('id','desc')->get()  @endphp
                @foreach($postfixes as $postfix)
                    <option value="{{ $postfix->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('contact_details_secondary_contact_postfix','postfix');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="contact_details['secondary_contact']['relation']">Relation</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="contact_details_secondary_contact_relation"
                   id="contact_details['secondary_contact']['relation']"
                   name="contact_details['secondary_contact']['relation']" type="text">
            <datalist id="contact_details_secondary_contact_relation">
                @php $relations = \App\Models\LrmSettings::whereParent('relation')->orderby('id','desc')->get()  @endphp
                @foreach($relations as $relation)
                    <option value="{{ $relation->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('contact_details_secondary_contact_relation','relation');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="contact_details['secondary_contact']['cell_priority_1']">Cell (Priority 1)</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="contact_details['secondary_contact']['cell_priority_1']"
                   name="contact_details['secondary_contact']['cell_priority_1']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="contact_details['secondary_contact']['cell_priority_2']">Cell (Priority 2)</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="contact_details['secondary_contact']['cell_priority_2']"
                   name="contact_details['secondary_contact']['cell_priority_2']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="contact_details['secondary_contact']['cell_whatsApp']">Cell (WhatsApp)</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="contact_details['secondary_contact']['cell_whatsApp']"
                   name="contact_details['secondary_contact']['cell_whatsApp']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="contact_details['secondary_contact']['land_line_no']">Landline No.</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="contact_details['secondary_contact']['land_line_no']"
                   name="contact_details['secondary_contact']['land_line_no']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="contact_details['secondary_contact']['email']">Email</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="contact_details['secondary_contact']['email']"
                   name="contact_details['secondary_contact']['email']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="contact_details['secondary_contact']['facebook']">Facebook</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="contact_details['secondary_contact']['facebook']"
                   name="contact_details['secondary_contact']['facebook']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="contact_details['secondary_contact']['notes']">Notes</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="contact_details['secondary_contact']['notes']"
                   name="contact_details['secondary_contact']['notes']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="contact_details['secondary_contact']['remarks']">Remarks</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="contact_details['secondary_contact']['remarks']"
                   name="contact_details['secondary_contact']['remarks']" type="text">
        </div>
    </div>

</div>

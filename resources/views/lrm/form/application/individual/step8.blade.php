{{-- Applicant--}}
<div class="row">
    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">Name</h6>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="nominees['name']['title']">Title</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="nominees_name_title_lists"
                   id="nominees['name']['title']"
                   name="nominees['name']['title']" type="text">
            <datalist id="nominees_name_title_lists">
                @php $titles = \App\Models\LrmSettings::whereParent('title')->orderby('id','desc')->get()  @endphp
                @foreach($titles as $title)
                    <option value="{{ $title->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('nominees_name_lists','title');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="nominees['name']['prefix']">Prefix</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="application_prefix_lists"
                   id="nominees['name']['prefix']"
                   name="nominees['name']['prefix']" type="text">
            <datalist id="nominees_name_prefix_lists">
                @php $prefixes = \App\Models\LrmSettings::whereParent('prefix')->orderby('id','desc')->get()  @endphp
                @foreach($prefixes as $prefix)
                    <option value="{{ $prefix->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('nominees_name_prefix_lists','prefix');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="nominees['name']['first_name']">First Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="nominees['name']['first_name']"
                   name="nominees['name']['first_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="nominees['name']['middle_name']">Middle Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="nominees['name']['middle_name']"
                   name="nominees['name']['middle_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="nominees['name']['last_name']">Last Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="nominees['name']['last_name']"
                   name="nominees['name']['last_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="nominees['name']['postfix']">Postfix</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="nominees_name_postfix_lists"
                   id="nominees['name']['postfix']"
                   name="nominees['name']['postfix']" type="text">
            <datalist id="nominees_name_postfix_lists">
                @php $postfixes = \App\Models\LrmSettings::whereParent('postfix')->orderby('id','desc')->get()  @endphp
                @foreach($postfixes as $postfix)
                    <option value="{{ $postfix->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('nominees_name_postfix_lists','postfix');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
</div>
{{-- Applicant Father/Husband --}}
<div class="row">
    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">Father/Husband Name</h6>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="nominees['father_husband']['title']">Title</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="nominees_father_husband_title_lists"
                   id="nominees['father_husband']['title']"
                   name="nominees['father_husband']['title']" type="text">
            <datalist id="nominees_father_husband_title_lists">
                @php $titles = \App\Models\LrmSettings::whereParent('title')->orderby('id','desc')->get()  @endphp
                @foreach($titles as $title)
                    <option value="{{ $title->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('nominees_father_husband_title_lists','title');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="nominees['father_husband_']['prefix']">Prefix</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="nominees_father_husband_title_prefix"
                   id="nominees['father_husband_']['prefix']"
                   name="nominees['father_husband_']['prefix']" type="text">
            <datalist id="nominees_father_husband_title_prefix">
                @php $prefixes = \App\Models\LrmSettings::whereParent('prefix')->orderby('id','desc')->get()  @endphp
                @foreach($prefixes as $prefix)
                    <option value="{{ $prefix->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('nominees_father_husband_title_prefix','prefix');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="nominees['father_husband']['first_name']">First Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="nominees['father_husband']['first_name']"
                   name="nominees['father_husband']['first_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="nominees['father_husband']['middle_name']">Middle Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="nominees['father_husband']['middle_name']"
                   name="nominees['father_husband']['middle_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="nominees['father_husband']['last_name']">Last Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="nominees['father_husband']['last_name']"
                   name="nominees['father_husband']['last_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="nominees['father_husband']['postfix']">Postfix</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="nominees_father_husband_postfix_lists"
                   id="nominees['father_husband']['postfix']"
                   name="nominees['father_husband']['postfix']" type="text">
            <datalist id="nominees_father_husband_postfix_lists">
                @php $postfixes = \App\Models\LrmSettings::whereParent('postfix')->orderby('id','desc')->get()  @endphp
                @foreach($postfixes as $postfix)
                    <option value="{{ $postfix->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('nominees_father_husband_postfix_lists','postfix');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
</div>
{{-- Appliocant Other--}}
<div class="row">
    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">Others</h6>
    </div>
    <div class="form-group col-md-4 mb-1">
        <label
            for="nominees['others']['date_of_birth']">Date of Birth</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="nominees['others']['date_of_birth']"
                   name="nominees['others']['date_of_birth']" type="date">
        </div>
    </div>
    <div class="form-group col-md-4 mb-1">
        <label
            for="nominees['others']['cnic']">CNIC</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="nominees['others']['cnic']"
                   onkeyup="Custom.validateCNIC(event);"
                   name="nominees['others']['cnic']" type="text" placeholder="xxxxx-xxxxxxx-x">
        </div>
    </div>
    <div class="form-group col-md-4 mb-1">
        <label
            for="nominees['others']['nationality']">Nationalities</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="nominees_other_nationality_lists"
                   id="nominees['others']['nationality']"
                   name="nominees['others']['nationality']" type="text">
            <datalist id="nominees_other_nationality_lists">
                @php $nationalities = \App\Models\LrmSettings::whereParent('nationality')->orderby('id','desc')->get()  @endphp
                @foreach($nationalities as $nationality)
                    <option value="{{ $nationality->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('nominees_other_nationality_lists','nationality');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-4 mb-1">
        <label
            for="nominees['others']['qualification']">Qualifications</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="nominees_other_qualification_lists"
                   id="nominees['others']['qualification']"
                   name="nominees['others']['qualification']" type="text">
            <datalist id="nominees_other_qualification_lists">
                @php $qualification = \App\Models\LrmSettings::whereParent('qualification')->orderby('id','desc')->get()  @endphp
                @foreach($qualification as $q)
                    <option value="{{ $q->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('nominees_other_qualification_lists','qualification');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-8 mb-1 mx-auto">
        <label for="nominees['others']['gender']">Gender</label>
        <div class="input-group input-group-sm">
            <div class="icheck-success d-inline">
                <input type="radio" name="nominees['others']['male']" id="nominees['others']['male']" value="male"
                       checked>
                <label for="nominees['others']['male']">Male</label>
            </div>
            <div class="icheck-success d-inline">
                <input type="radio" name="nominees['others']['female']" id="nominees['others']['female']" value="female"
                >
                <label for="nominees['others']['female']">Female</label>
            </div>
            <div class="icheck-success d-inline">
                <input type="radio" name="nominees['others']['other']" id="nominees['others']['other']" value="other"
                >
                <label for="nominees['others']['other']">Female</label>
            </div>
        </div>
    </div>
    <div class="form-group col-md-4 mb-1">
        <label
            for="nominees['others']['mailing_address']">Mailing Address</label>
        <div class="input-group input-group-sm">
            <textarea class="form-control form-control-sm" id="nominees['others']['mailing_address']"
                      name="nominees['others']['mailing_address']"></textarea>
        </div>
    </div>
    <div class="form-group col-md-4 mb-1">
        <label
            for="nominees['others']['permanent_address']">Permanent Address</label>
        <div class="input-group input-group-sm">
            <textarea class="form-control form-control-sm" id="nominees['others']['permanent_address']"
                      name="nominees['others']['permanent_address']"></textarea>
        </div>
    </div>
    <div class="form-group col-md-4 mb-1">
        <label
            for="nominees['others']['permanent_remarks']">Remarks (Applicant's Remarks)</label>
        <div class="input-group input-group-sm">
            <textarea class="form-control form-control-sm" id="nominees['others']['permanent_remarks']"
                      name="nominees['others']['permanent_remarks']"></textarea>
        </div>
    </div>
</div>

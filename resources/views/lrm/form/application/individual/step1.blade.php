<div class="row">
    <div class="form-group col-md-3">
        <label for="application_basic">{{__('lrm.form.individual.application_basic')}}</label>
        <input class="form-control form-control-sm" list="application_basic_lists" id="application_basic"
               name="application_basic"
               type="text">
        <datalist id="application_basic_lists">
            <option value="New Application">
            <option value="Nominee Invoked">
        </datalist>
    </div>
    <div class="form-group col-md-3">
        <label for="application_number">{{__('lrm.form.individual.application_number')}}</label>
        <input class="form-control form-control-sm" id="application_number" name="application_number" type="text">
    </div>
    <div class="form-group col-md-3">
        <label for="application_date">{{__('lrm.form.individual.application_date')}}</label>
        <input class="form-control form-control-sm" id="application_date" data-date-format="DD MMMM YYYY"
               name="application_date" type="date" value="{{ date('Y-m-d') }}">
    </div>
    <div class="form-group col-md-3">
        <label for="applied_for">{{__('lrm.form.individual.applied_for')}}</label>
        <input class="form-control form-control-sm" list="browsers" id="applied_for" name="applied_for" type="text">
        <datalist id="browsers">
            <option value="Commercial">
            <option value="Residence">
            <option value="Plot">
            <option value="Shop">
        </datalist>
    </div>
    <div class="form-group col-md-3">
        <label for="submission_mode">{{__('lrm.form.individual.submission_mode')}}</label>
        <input class="form-control form-control-sm" list="submission_modes" id="submission_mode" name="submission_mode"
               type="text">
        <datalist id="submission_modes">
            <option value="By Hand">
            <option value="By Email">
        </datalist>
    </div>
    <div class="form-group col-md-3">
        <label for="submission_by">{{__('lrm.form.individual.submission_by')}}</label>
        <select class="form-control form-control-sm" id="submission_by" name="submission_by">
            <option></option>
        </select>
    </div>
    <div class="form-group col-md-3">
        <label for="direction_by">{{__('lrm.form.individual.direction_by')}}</label>
        <select class="form-control form-control-sm" id="direction_by" name="direction_by">
            <option></option>
        </select>
    </div>
    <div class="form-group col-md-3">
        <label for="received_by">{{__('lrm.form.individual.received_by')}}</label>
        <select class="form-control form-control-sm" id="received_by" name="received_by">
            <option></option>
        </select>
    </div>
    <div class="form-group col-md-3">
        <label for="received_notes">{{__('lrm.form.individual.received_notes')}}</label>
        <textarea class="form-control form-control-sm" name="received_notes" id="received_notes" rows="1"></textarea>
    </div>
    <div class="form-group col-md-3">
        <label for="recommended_by">{{__('lrm.form.individual.recommended_by')}}</label>
        <select class="form-control form-control-sm" id="recommended_by" name="recommended_by">
            <option></option>
        </select>
    </div>
    <div class="form-group col-md-3">
        <label for="recommender_comments">{{__('lrm.form.individual.recommender_comments')}}</label>
        <textarea class="form-control form-control-sm" name="recommender_comments" id="recommender_comments"
                  rows="1"></textarea>
    </div>
    <div class="form-group col-md-3">
        <label for="remarks_overall">{{__('lrm.form.individual.remarks_overall')}}</label>
        <textarea class="form-control form-control-sm" name="remarks_overall" id="remarks_overall" rows="1"></textarea>
    </div>
</div>

{{-- Applicant--}}
<div class="row">
    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">Name</h6>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="co_applicant['name']['title']">Title</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="co_applicant_name_title_lists"
                   id="co_applicant['name']['title']"
                   name="co_applicant['name']['title']" type="text">
            <datalist id="co_applicant_name_title_lists">
                @php $titles = \App\Models\LrmSettings::whereParent('title')->orderby('id','desc')->get()  @endphp
                @foreach($titles as $title)
                    <option value="{{ $title->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('co_applicant_name_lists','title');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="co_applicant['name']['prefix']">Prefix</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="application_prefix_lists"
                   id="co_applicant['name']['prefix']"
                   name="co_applicant['name']['prefix']" type="text">
            <datalist id="co_applicant_name_prefix_lists">
                @php $prefixes = \App\Models\LrmSettings::whereParent('prefix')->orderby('id','desc')->get()  @endphp
                @foreach($prefixes as $prefix)
                    <option value="{{ $prefix->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('co_applicant_name_prefix_lists','prefix');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="co_applicant['name']['first_name']">First Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="co_applicant['name']['first_name']"
                   name="co_applicant['name']['first_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="co_applicant['name']['middle_name']">Middle Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="co_applicant['name']['middle_name']"
                   name="co_applicant['name']['middle_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="co_applicant['name']['last_name']">Last Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="co_applicant['name']['last_name']"
                   name="co_applicant['name']['last_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="co_applicant['name']['postfix']">Postfix</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="co_applicant_name_postfix_lists"
                   id="co_applicant['name']['postfix']"
                   name="co_applicant['name']['postfix']" type="text">
            <datalist id="co_applicant_name_postfix_lists">
                @php $postfixes = \App\Models\LrmSettings::whereParent('postfix')->orderby('id','desc')->get()  @endphp
                @foreach($postfixes as $postfix)
                    <option value="{{ $postfix->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('co_applicant_name_postfix_lists','postfix');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
</div>
{{-- Applicant Father/Husband --}}
<div class="row">
    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">Father/Husband Name</h6>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="co_applicant['father_husband']['title']">Title</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="co_applicant_father_husband_title_lists"
                   id="co_applicant['father_husband']['title']"
                   name="co_applicant['father_husband']['title']" type="text">
            <datalist id="co_applicant_father_husband_title_lists">
                @php $titles = \App\Models\LrmSettings::whereParent('title')->orderby('id','desc')->get()  @endphp
                @foreach($titles as $title)
                    <option value="{{ $title->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('co_applicant_father_husband_title_lists','title');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="co_applicant['father_husband_']['prefix']">Prefix</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="co_applicant_father_husband_title_prefix"
                   id="co_applicant['father_husband_']['prefix']"
                   name="co_applicant['father_husband_']['prefix']" type="text">
            <datalist id="co_applicant_father_husband_title_prefix">
                @php $prefixes = \App\Models\LrmSettings::whereParent('prefix')->orderby('id','desc')->get()  @endphp
                @foreach($prefixes as $prefix)
                    <option value="{{ $prefix->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('co_applicant_father_husband_title_prefix','prefix');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="co_applicant['father_husband']['first_name']">First Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="co_applicant['father_husband']['first_name']"
                   name="co_applicant['father_husband']['first_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="co_applicant['father_husband']['middle_name']">Middle Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="co_applicant['father_husband']['middle_name']"
                   name="co_applicant['father_husband']['middle_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="co_applicant['father_husband']['last_name']">Last Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="co_applicant['father_husband']['last_name']"
                   name="co_applicant['father_husband']['last_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="co_applicant['father_husband']['postfix']">Postfix</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="co_applicant_father_husband_postfix_lists"
                   id="co_applicant['father_husband']['postfix']"
                   name="co_applicant['father_husband']['postfix']" type="text">
            <datalist id="co_applicant_father_husband_postfix_lists">
                @php $postfixes = \App\Models\LrmSettings::whereParent('postfix')->orderby('id','desc')->get()  @endphp
                @foreach($postfixes as $postfix)
                    <option value="{{ $postfix->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('co_applicant_father_husband_postfix_lists','postfix');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
</div>
{{-- Appliocant Other--}}
<div class="row">
    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">Others</h6>
    </div>
    <div class="form-group col-md-4 mb-1">
        <label
            for="co_applicant['others']['date_of_birth']">Date of Birth</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="co_applicant['others']['date_of_birth']"
                   name="co_applicant['others']['date_of_birth']" type="date">
        </div>
    </div>
    <div class="form-group col-md-4 mb-1">
        <label
            for="co_applicant['others']['cnic']">CNIC</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="co_applicant['others']['cnic']"
                   onkeyup="Custom.validateCNIC(event);"
                   name="co_applicant['others']['cnic']" type="text" placeholder="xxxxx-xxxxxxx-x">
        </div>
    </div>
    <div class="form-group col-md-4 mb-1">
        <label
            for="co_applicant['others']['nationality']">Nationalities</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="co_applicant_other_nationality_lists"
                   id="co_applicant['others']['nationality']"
                   name="co_applicant['others']['nationality']" type="text">
            <datalist id="co_applicant_other_nationality_lists">
                @php $nationalities = \App\Models\LrmSettings::whereParent('nationality')->orderby('id','desc')->get()  @endphp
                @foreach($nationalities as $nationality)
                    <option value="{{ $nationality->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('co_applicant_other_nationality_lists','nationality');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-4 mb-1">
        <label
            for="co_applicant['others']['qualification']">Qualifications</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="co_applicant_other_qualification_lists"
                   id="co_applicant['others']['qualification']"
                   name="co_applicant['others']['qualification']" type="text">
            <datalist id="co_applicant_other_qualification_lists">
                @php $qualification = \App\Models\LrmSettings::whereParent('qualification')->orderby('id','desc')->get()  @endphp
                @foreach($qualification as $q)
                    <option value="{{ $q->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('co_applicant_other_qualification_lists','qualification');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-8 mb-1 mx-auto">
        <label for="co_applicant['others']['gender']">Gender</label>
        <div class="input-group input-group-sm">
            <div class="icheck-success d-inline">
                <input type="radio" name="co_applicant['others']['male']" id="co_applicant['others']['male']" value="male"
                       checked>
                <label for="co_applicant['others']['male']">Male</label>
            </div>
            <div class="icheck-success d-inline">
                <input type="radio" name="co_applicant['others']['female']" id="co_applicant['others']['female']" value="female"
                >
                <label for="co_applicant['others']['female']">Female</label>
            </div>
            <div class="icheck-success d-inline">
                <input type="radio" name="co_applicant['others']['other']" id="co_applicant['others']['other']" value="other"
                >
                <label for="co_applicant['others']['other']">Female</label>
            </div>
        </div>
    </div>
    <div class="form-group col-md-4 mb-1">
        <label
            for="co_applicant['others']['mailing_address']">Mailing Address</label>
        <div class="input-group input-group-sm">
            <textarea class="form-control form-control-sm" id="co_applicant['others']['mailing_address']"
                      name="co_applicant['others']['mailing_address']"></textarea>
        </div>
    </div>
    <div class="form-group col-md-4 mb-1">
        <label
            for="co_applicant['others']['permanent_address']">Permanent Address</label>
        <div class="input-group input-group-sm">
            <textarea class="form-control form-control-sm" id="co_applicant['others']['permanent_address']"
                      name="co_applicant['others']['permanent_address']"></textarea>
        </div>
    </div>
    <div class="form-group col-md-4 mb-1">
        <label
            for="co_applicant['others']['permanent_remarks']">Remarks (Applicant's Remarks)</label>
        <div class="input-group input-group-sm">
            <textarea class="form-control form-control-sm" id="co_applicant['others']['permanent_remarks']"
                      name="co_applicant['others']['permanent_remarks']"></textarea>
        </div>
    </div>
</div>

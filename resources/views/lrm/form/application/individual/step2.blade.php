{{-- Applicant--}}
<div class="row">
    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">{{__('lrm.form.individual.applicant_info.applicant')}}</h6>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="applicant_father_husband['title']">{{__('lrm.form.individual.applicant_info.title')}}</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="application_titles_lists"
                   id="applicant_father_husband['title']"
                   name="applicant_father_husband['title']" type="text">
            <datalist id="application_titles_lists">
                @php $titles = \App\Models\LrmSettings::whereParent('title')->orderby('id','desc')->get()  @endphp
                @foreach($titles as $title)
                    <option value="{{ $title->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('application_titles_lists','title');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="applicant_father_husband['prefix']">{{__('lrm.form.individual.applicant_info.prefix')}}</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="application_prefix_lists"
                   id="applicant_father_husband['prefix']"
                   name="applicant_father_husband['prefix']" type="text">
            <datalist id="application_prefix_lists">
                @php $prefixes = \App\Models\LrmSettings::whereParent('prefix')->orderby('id','desc')->get()  @endphp
                @foreach($prefixes as $prefix)
                    <option value="{{ $prefix->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('application_prefix_lists','prefix');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="applicant_father_husband['first_name']">{{__('lrm.form.individual.applicant_info.first_name')}}</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="applicant_father_husband['first_name']"
                   name="applicant_father_husband['first_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="applicant_father_husband['middle_name']">{{__('lrm.form.individual.applicant_info.middle_name')}}</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="applicant_father_husband['middle_name']"
                   name="applicant_father_husband['middle_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="applicant_father_husband['last_name']">{{__('lrm.form.individual.applicant_info.last_name')}}</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="applicant_father_husband['last_name']"
                   name="applicant_father_husband['last_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="applicant_father_husband['postfix']">{{__('lrm.form.individual.applicant_info.postfix')}}</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="application_postfix_lists"
                   id="applicant_father_husband['postfix']"
                   name="applicant_father_husband['postfix']" type="text">
            <datalist id="application_postfix_lists">
                @php $postfixes = \App\Models\LrmSettings::whereParent('postfix')->orderby('id','desc')->get()  @endphp
                @foreach($postfixes as $postfix)
                    <option value="{{ $postfix->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('application_postfix_lists','postfix');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
</div>
{{-- Applicant Father/Husband --}}
<div class="row">
    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">{{__('lrm.form.individual.applicant_info.father_husband')}}</h6>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="applicant_father_husband['relation']">{{__('lrm.form.individual.applicant_info.relation')}}</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="applicant_father_husband_relation_list"
                   id="applicant_father_husband['relation']"
                   name="applicant_father_husband['relation']" type="text">
            <datalist id="applicant_father_husband_relation_list">
                @php $relations = \App\Models\LrmSettings::whereParent('relation')->orderby('id','desc')->get()  @endphp
                @foreach($relations as $relation)
                    <option value="{{ $relation->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('applicant_father_husband_relation_list','relation');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="applicant_father_husband['prefix']">{{__('lrm.form.individual.applicant_info.prefix')}}</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="applicant_father_husband_prefix_list"
                   id="applicant_father_husband['prefix']"
                   name="applicant_father_husband['prefix']" type="text">
            <datalist id="applicant_father_husband_prefix_list">
                @php $prefixes = \App\Models\LrmSettings::whereParent('prefix')->orderby('id','desc')->get()  @endphp
                @foreach($prefixes as $prefix)
                    <option value="{{ $prefix->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('applicant_father_husband_prefix_list','prefix');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="applicant_father_husband['first_name']">{{__('lrm.form.individual.applicant_info.first_name')}}</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="applicant_father_husband['first_name']"
                   name="applicant_father_husband['first_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="applicant_father_husband['middle_name']">{{__('lrm.form.individual.applicant_info.middle_name')}}</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="applicant_father_husband['middle_name']"
                   name="applicant_father_husband['middle_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="applicant_father_husband['last_name']">{{__('lrm.form.individual.applicant_info.last_name')}}</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="applicant_father_husband['last_name']"
                   name="applicant_father_husband['last_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="applicant_father_husband['postfix']">{{__('lrm.form.individual.applicant_info.postfix')}}</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="applicant_father_husband_postfix_list"
                   id="applicant_father_husband['postfix']"
                   name="applicant_father_husband['postfix']" type="text">
            <datalist id="applicant_father_husband_postfix_list">
                @php $postfixes = \App\Models\LrmSettings::whereParent('postfix')->orderby('id','desc')->get()  @endphp
                @foreach($postfixes as $postfix)
                    <option value="{{ $postfix->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('applicant_father_husband_postfix_list','postfix');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
</div>
{{-- Appliocant Other--}}
<div class="row">
    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">{{__('lrm.form.individual.applicant_info.others')}}</h6>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="applicant_others['date_of_birth']">{{__('lrm.form.individual.applicant_info.date_of_birth')}}</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="applicant_others['date_of_birth']"
                   name="applicant_others['date_of_birth']" type="date">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1 text-center">
        <label for="applicant_others['date_of_birth']">{{__('lrm.form.individual.applicant_info.gender')}}</label>
        <div class="input-group input-group-sm">
            <div class="icheck-success d-inline">
                <input type="radio" name="applicant_others['gender']" id="applicant_others['male']" value="male"
                       checked>
                <label for="applicant_others['male']">{{__('lrm.form.individual.applicant_info.male')}}</label>
            </div>
            <div class="icheck-success d-inline ml-2">
                <input type="radio" name="applicant_others['gender']" id="applicant_others['female']" value="female">
                <label for="applicant_others['female']">{{__('lrm.form.individual.applicant_info.female')}}</label>
            </div>
            <div class="icheck-success d-inline ml-2">
                <input type="radio" name="applicant_others['gender']" id="applicant_others['other']" value="other">
                <label for="applicant_others['other']">{{__('lrm.form.individual.applicant_info.other')}}</label>
            </div>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="applicant_others['cnic']">{{__('lrm.form.individual.applicant_info.cnic')}}</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="applicant_others['cnic']"
                   onkeyup="Custom.validateCNIC(event);"
                   name="applicant_others['cnic']" type="text" placeholder="xxxxx-xxxxxxx-x">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="applicant_father_husband['nationality']">{{__('lrm.form.individual.applicant_info.nationality')}}</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="applicant_father_husband_nationality_lists"
                   id="applicant_father_husband['nationality']"
                   name="applicant_father_husband['nationality']" type="text">
            <datalist id="applicant_father_husband_nationality_lists">
                @php $nationalities = \App\Models\LrmSettings::whereParent('nationality')->orderby('id','desc')->get()  @endphp
                @foreach($nationalities as $nationality)
                    <option value="{{ $nationality->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('applicant_father_husband_nationality_lists','nationality');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
</div>

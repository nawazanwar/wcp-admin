{{-- Government--}}
<div class="row">
    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">Government</h6>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="employment['government']['fed_prov']">Federal/Provincial</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="employment['government']['fed_prov']"
                   name="employment['government']['fed_prov']" type="text">
        </div>
    </div>
    {{-- Ministry--}}
    <div class="form-group col-md-3 mb-1">
        <label for="employment['government']['ministry']">Ministry</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="employment_government_ministry_list"
                   id="employment['government']['ministry']" name="employment['government']['ministry']" type="text">
            <datalist id="employment_government_ministry_list">
                @php $govt_ministries = \App\Models\LRMMinistry::orderby('id','desc')->get()  @endphp
                @foreach($govt_ministries as $ministry)
                    <option value="{{ $ministry->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_ministry('employment_government_ministry_list','ministry');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{--Organization--}}
    <div class="form-group col-md-3 mb-1">
        <label for="employment['government']['organization']">Organization</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="employment_government_organization_list"
                   id="employment['government']['organization']" name="employment['government']['organization']"
                   type="text">
            <datalist id="employment_government_organization_list">
                @php
                    $govt_organizations = \App\Models\LRMOrganization::whereHas('types',function($q){
                           $q->where('name', '=', 'Government');
                    })->orderby('id','desc')->get();
                @endphp
                @foreach($govt_organizations as $go)
                    <option value="{{ $go->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_organization('employment_government_organization_list','organization','Government');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{--Wing--}}
    <div class="form-group col-md-3 mb-1">
        <label for="employment['government']['wing']">Wing</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="employment_government_wing_list"
                   id="employment['government']['wing']" name="employment['government']['wing']"
                   type="text">
            <datalist id="employment_government_wing_list">
                @php
                    $wings = \App\Models\LRMWing::whereHas('types',function($q){
                           $q->where('name', '=', 'Government');
                    })->orderby('id','desc')->get();
                @endphp
                @foreach($wings as $w)
                    <option value="{{ $w->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_wing('employment_government_wing_list','wing','Government');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{--Department--}}
    <div class="form-group col-md-3 mb-1">
        <label for="employment['government']['department']">Department</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="employment_government_department_list"
                   id="employment['government']['department']" name="employment['government']['department']"
                   type="text">
            <datalist id="employment_government_department_list">
                @php
                    $govt_departments = \App\Models\LRMDepartment::whereHas('types',function($q){
                           $q->where('name', '=', 'Government');
                    })->orderby('id','desc')->get();
                @endphp
                @foreach($govt_departments as $d)
                    <option value="{{ $d->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_department('employment_government_department_list','department','Government');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- Current Last Served--}}
    <div class="form-group col-md-3 mb-1">
        <label for="employment['government']['current_last_served']">Current/Last Served</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="employment['government']['current_last_served']"
                   name="employment['government']['current_last_served']" type="text">
        </div>
    </div>
    {{--Rank--}}
    <div class="form-group col-md-3 mb-1">
        <label for="employment['government']['rank']">Rank</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="employment_government_rank_list"
                   id="employment['government']['rank']" name="employment['government']['rank']"
                   type="text">
            <datalist id="employment_government_rank_list">
                @php
                    $govt_ranks = \App\Models\LRMRank::whereHas('types',function($q){
                           $q->where('types.name', '=', 'Government');
                    })->orderby('id','desc')->get();
                @endphp
                @foreach($govt_ranks as $r)
                    <option value="{{ $r->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_rank('employment_government_rank_list','rank','Government');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{--Grade--}}
    <div class="form-group col-md-3 mb-1">
        <label for="employment['government']['grade']">Grades</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="employment_government_grade_list"
                   id="employment['government']['grade']" name="employment['grade']['rank']"
                   type="text">
            <datalist id="employment_government_grade_list">
                @php
                    $gov_grades = \App\Models\LRMGrade::whereHas('types',function($q){
                           $q->where('name', '=', 'Government');
                    })->orderby('id','desc')->get();
                @endphp
                @foreach($gov_grades as $g)
                    <option value="{{ $g->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_grade('employment_government_grade_list','grade','Government');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{--Qualification--}}
    <div class="form-group col-md-3 mb-1">
        <label
            for="employment['government']['qualification']">Qualification</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="employment_gov_qualification_lists"
                   id="employment['government']['qualification']"
                   name="employment['government']['qualification']" type="text">
            <datalist id="employment_gov_qualification_lists">
                @php $gov_qualifications = \App\Models\LrmSettings::whereParent('qualification')->orderby('id','desc')->get()  @endphp
                @foreach($gov_qualifications as $q)
                    <option value="{{ $q->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('employment_gov_qualification_lists','qualification');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{--Professions--}}
    <div class="form-group col-md-3 mb-1">
        <label
            for="employment['government']['profession']">Professions</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="employment_gov_profession_lists"
                   id="employment['government']['profession']"
                   name="employment['government']['profession']" type="text">
            <datalist id="employment_gov_profession_lists">
                @php $gov_professions = \App\Models\LrmSettings::whereParent('profession')->orderby('id','desc')->get()  @endphp
                @foreach($gov_professions as $p)
                    <option value="{{ $p->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('employment_gov_profession_lists','profession');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- Specialization --}}
    <div class="form-group col-md-3 mb-1">
        <label for="employment['government']['specialization']">Specialization</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="employment['government']['specialization']"
                   name="employment['government']['specialization']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="employment['government']['serving_retired']">Serving/Retired</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="employment['government']['serving_retired']"
                   name="employment['government']['serving_retired']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="employment['government']['date_joining']">Date of Joining</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm datetime" id="employment['government']['date_joining']"
                   name="employment['government']['date_joining']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="employment['government']['date_retired']">Date of Retired</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm datetime" id="employment['government']['date_retired']"
                   name="employment['government']['date_retired']" type="text">
        </div>
    </div>
</div>
{{-- Private Establishment --}}
<div class="row">
    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">Private Establishment</h6>
    </div>
    {{--Organization--}}
    <div class="form-group col-md-3 mb-1">
        <label for="employment['private_establishment']['organization']">Organization</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="employment_private_establishment_organization_list"
                   id="employment['private_establishment']['organization']"
                   name="employment['private_establishment']['organization']"
                   type="text">
            <datalist id="employment_private_establishment_organization_list">
                @php
                    $private_organization = \App\Models\LRMOrganization::whereHas('types',function($q){
                           $q->where('name', '=', 'Private Establishment');
                    })->orderby('id','desc')->get();
                @endphp
                @foreach($private_organization as $o)
                    <option value="{{ $o->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_organization('employment_private_establishment_organization_list','organization','Private Establishment');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{--Department--}}
    <div class="form-group col-md-3 mb-1">
        <label for="employment['private_establishment']['department']">Department</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="employment_private_establishment_department_list"
                   id="employment['private_establishment']['department']"
                   name="employment['private_establishment']['department']"
                   type="text">
            <datalist id="employment_private_establishment_department_list">
                @php
                    $private_departments = \App\Models\LRMDepartment::whereHas('types',function($q){
                           $q->where('name', '=', 'Private Establishment');
                    })->orderby('id','desc')->get();
                @endphp
                @foreach($private_departments as $d)
                    <option value="{{ $d->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_department('employment_private_establishment_department_list','department','private_establishment');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- Current Last Served--}}
    <div class="form-group col-md-3 mb-1">
        <label for="employment['private_establishment']['current_last_served']">Current/Last Served</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="employment['private_establishment']['current_last_served']"
                   name="employment['private_establishment']['current_last_served']" type="text">
        </div>
    </div>
    {{--Rank--}}
    <div class="form-group col-md-3 mb-1">
        <label for="employment['private_establishment']['rank']">Rank</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="employment_private_establishment_rank_list"
                   id="employment['private_establishment']['rank']" name="employment['private_establishment']['rank']"
                   type="text">
            <datalist id="employment_private_establishment_rank_list">
                @php
                    $private_ranks = \App\Models\LRMRank::whereHas('types',function($q){
                           $q->where('types.name', '=', 'Private Establishment');
                    })->orderby('id','desc')->get();
                @endphp
                @foreach($private_ranks as $r)
                    <option value="{{ $r->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_rank('employment_private_establishment_rank_list','rank','Private Establishment');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{--Grade--}}
    <div class="form-group col-md-3 mb-1">
        <label for="employment['private_establishment']['grade']">Grades</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="employment_private_establishment_grade_list"
                   id="employment['private_establishment']['grade']" name="employment['grade']['rank']"
                   type="text">
            <datalist id="employment_private_establishment_grade_list">
                @php
                    $private_grades = \App\Models\LRMGrade::whereHas('types',function($q){
                           $q->where('name', '=', 'Private Establishment');
                    })->orderby('id','desc')->get();
                @endphp
                @foreach($private_grades as $g)
                    <option value="{{ $g->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_grade('employment_private_establishment_grade_list','grade','Private Establishment');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{--Qualification--}}
    <div class="form-group col-md-3 mb-1">
        <label
            for="employment['private_establishment']['qualification']">Qualification</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="employment_gov_qualification_lists"
                   id="employment['private_establishment']['qualification']"
                   name="employment['private_establishment']['qualification']" type="text">
            <datalist id="employment_gov_qualification_lists">
                @php $gov_qualifications = \App\Models\LrmSettings::whereParent('qualification')->orderby('id','desc')->get()  @endphp
                @foreach($gov_qualifications as $q)
                    <option value="{{ $q->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('employment_gov_qualification_lists','qualification');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{--Professions--}}
    <div class="form-group col-md-3 mb-1">
        <label
            for="employment['private_establishment']['profession']">Professions</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="employment_gov_profession_lists"
                   id="employment['private_establishment']['profession']"
                   name="employment['private_establishment']['profession']" type="text">
            <datalist id="employment_gov_profession_lists">
                @php $gov_professions = \App\Models\LrmSettings::whereParent('profession')->orderby('id','desc')->get()  @endphp
                @foreach($gov_professions as $p)
                    <option value="{{ $p->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('employment_gov_profession_lists','profession');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- Specialization --}}
    <div class="form-group col-md-3 mb-1">
        <label for="employment['private_establishment']['specialization']">Specialization</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="employment['private_establishment']['specialization']"
                   name="employment['private_establishment']['specialization']" type="text">
        </div>
    </div>
    {{-- Serving Retired--}}
    <div class="form-group col-md-3 mb-1">
        <label for="employment['private_establishment']['serving_retired']">Serving/Retired</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="employment['private_establishment']['serving_retired']"
                   name="employment['private_establishment']['serving_retired']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="employment['private_establishment']['date_joining']">Date of Joining</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm datetime"
                   id="employment['private_establishment']['date_joining']"
                   name="employment['private_establishment']['date_joining']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="employment['private_establishment']['date_retired']">Date of Retired</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm datetime"
                   id="employment['private_establishment']['date_retired']"
                   name="employment['private_establishment']['date_retired']" type="text">
        </div>
    </div>
</div>
{{-- Own Business --}}

<div class="row">
    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">Own Business</h6>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="employment['own_business']['business_title']">Business Title</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm"
                   id="employment['own_business']['business_title']"
                   name="employment['own_business']['business_title']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="employment['own_business']['owner_partner']">Owner/Partner</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm"
                   id="employment['own_business']['owner_partner']"
                   name="employment['own_business']['owner_partner']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="employment['own_business']['business_type']">Business Type</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm"
                   id="employment['own_business']['business_type']"
                   name="employment['own_business']['business_type']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="employment['own_business']['business_sub_type']">Business Sub Type</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm"
                   id="employment['own_business']['business_sub_type']"
                   name="employment['own_business']['business_sub_type']" type="text">
        </div>
    </div>
</div>


{{-- General --}}

<div class="row">
    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">General</h6>
    </div>
    <div class="form-group col-md-6 mb-1">
        <label for="employment['general']['address_line_1']">Address Line 1</label>
        <div class="input-group input-group-sm">
            <textarea class="form-control form-control-sm"
                      id="employment['general']['address_line_1']"
                      name="employment['general']['address_line_1']"></textarea>
        </div>
    </div>
    <div class="form-group col-md-6 mb-1">
        <label for="employment['general']['address_line_2']">Address Line 2</label>
        <div class="input-group input-group-sm">
            <textarea class="form-control form-control-sm"
                      id="employment['general']['address_line_2']"
                      name="employment['general']['address_line_2']"></textarea>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="employment['general']['country']">Country</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="employment_general_country_list"
                   id="employment['general']['country']" name="employment['general']['country']"
                   type="text">
            <datalist id="employment_general_country_list" style="max-height: 100px;overflow-y: auto;">
                @php
                    $general_countries = \App\Location\Country::orderby('id','desc')->get();
                @endphp
                @foreach($general_countries as $country)
                    <option value="{{ $country->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_rank('employment_general_country_list','country');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="employment['general']['city']">City</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="employment_general_city_list"
                   id="employment['general']['city']" name="employment['general']['city']"
                   type="text">
            <datalist id="employment_general_city_list" style="max-height: 100px;overflow-y: auto;">
                @php
                    $general_cities = \App\Location\City::orderby('id','desc')->get();
                @endphp
                @foreach($general_cities as $cities)
                    <option value="{{ $cities->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_city('employment_general_city_list','city');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
</div>

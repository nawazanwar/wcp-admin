{{-- Images--}}
<div class="row">
    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">{{__('lrm.form.individual.applicant_info.images')}}</h6>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="image['photograph']">{{__('lrm.form.individual.applicant_info.photograph')}}</label>
            <div class="input-group input-group-sm">
                <div class="custom-file">
                    <input type="file" name="image['photograph']" class="custom-file-input form-control-sm"
                           id="image['photograph']" onchange="Custom.previewImage(this,'picture_first')">
                    <label class="custom-file-label btn-sm" for="picture['back']">Choose File</label>
                </div>
            </div>
        </div>
        <div class="picture_first text-center my-3">
            <img src="http://wcp.local/img/defaults/user-picture.png" class="img-thumbnail h-150">
            <div class="input-group input-group-sm mt-2">
                <input class="form-control form-control-sm" id="image['photograph_remarks']"
                       name="image['photograph_remarks']" type="text"
                       placeholder="{{__('lrm.form.individual.applicant_info.remarks')}}">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="image['photograph']">{{__('lrm.form.individual.applicant_info.cnic_front')}}</label>
            <div class="input-group input-group-sm">
                <div class="custom-file">
                    <input type="file" name="image['cnic_front']" class="custom-file-input form-control-sm"
                           id="image['cnic_front']" onchange="Custom.previewImage(this,'cnic_front_image_holder')">
                    <label class="custom-file-label btn-sm" for="picture['cnic_front']">Choose File</label>
                </div>
            </div>
        </div>
        <div class="cnic_front_image_holder text-center my-3">
            <img src="{{ asset('img/defaults/cnic/front.jpeg') }}" class="img-thumbnail h-150">
            <div class="input-group input-group-sm mt-2">
                <input class="form-control form-control-sm" id="image['cnic_front_image_remarks']"
                       name="image['cnic_front_image_remarks']" type="text"
                       placeholder="{{__('lrm.form.individual.applicant_info.remarks')}}">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="image['photograph']">{{__('lrm.form.individual.applicant_info.cnic_back')}}</label>
            <div class="input-group input-group-sm">
                <div class="custom-file">
                    <input type="file" name="image['cnic_back']" class="custom-file-input form-control-sm"
                           id="image['cnic_back']" onchange="Custom.previewImage(this,'cnic_back_image_holder')">
                    <label class="custom-file-label btn-sm" for="picture['cnic_front']">Choose File</label>
                </div>
            </div>
        </div>
        <div class="cnic_back_image_holder text-center my-3">
            <img src="{{ asset('img/defaults/cnic/back.jpeg') }}" class="img-thumbnail h-150">
            <div class="input-group input-group-sm mt-2">
                <input class="form-control form-control-sm" id="image['cnic_back_image_remarks']"
                       name="image['cnic_back_image_remarks']" type="text"
                       placeholder="{{__('lrm.form.individual.applicant_info.remarks')}}">
            </div>
        </div>
    </div>
</div>

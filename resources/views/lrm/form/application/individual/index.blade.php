<div class="arrowwizard">
    <ul>
        <li><a href="#application-form-step-1">Step 1<br/><small>Application Details</small></a>
        </li>
        <li><a href="#step-2">Step 2<br/><small>Applicant Details</small></a></li>
        <li><a href="#step-3">Step 3<br/><small>Images</small></a></li>
        <li><a href="#step-4">Step 4<br/><small>Employment</small></a></li>
        <li><a href="#step-5">Step 5<br/><small>Address</small></a></li>
        <li><a href="#step-6">Step 6<br/><small>Contact Details</small></a></li>
        <li><a href="#step-7">Step 7<br/><small>Co-Applicant Details</small></a></li>
        <li><a href="#step-8">Step 8<br/><small>Nominee Details</small></a></li>
    </ul>
    <div class="mt-4">
        <div id="application-form-step-1">
            @include('lrm.form.application.individual.step1')
        </div>
        <div id="step-2">
            @include('lrm.form.application.individual.step2')
        </div>
        <div id="step-3">
            @include('lrm.form.application.individual.step3')
        </div>
        <div id="step-4">
            @include('lrm.form.application.individual.step4')
        </div>
        <div id="step-5">
            @include('lrm.form.application.individual.step5')
        </div>
        <div id="step-6">
            @include('lrm.form.application.individual.step6')
        </div>
        <div id="step-7">
            @include('lrm.form.application.individual.step7')
        </div>
        <div id="step-8">
            @include('lrm.form.application.individual.step8')
        </div>
    </div>
</div>

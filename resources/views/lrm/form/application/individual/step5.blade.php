<div class="row">

    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">Address</h6>
    </div>
    {{-- Countires --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['permanent']['country']">Country</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="address_permanent_country_lists"
                   id="address['permanent']['country']"
                   name="address['permanent']['country']" type="text">
            <datalist id="address_permanent_country_lists">
                @php $countries = \App\Location\Country::orderby('id','desc')->get()  @endphp
                @foreach($countries as $c)
                    <option value="{{ $c->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('address_permanent_country_lists','country','countries');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- Provinces/States --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['permanent']['province']">Province</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="address_permanent_province_lists"
                   id="address['permanent']['province']"
                   name="address['permanent']['province']" type="text">
            <datalist id="address_permanent_province_lists">
                @php $states = \App\Location\State::orderby('id','desc')->get()  @endphp
                @foreach($states as $state)
                    <option value="{{ $state->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('address_permanent_province_lists','province','states');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- District  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['permanent']['district']">District</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="address_permanent_district_lists"
                   id="address['permanent']['district']"
                   name="address['permanent']['district']" type="text">
            <datalist id="address_permanent_district_lists">
                @php $districts = \App\Location\District::orderby('id','desc')->get()  @endphp
                @foreach($districts as $d)
                    <option value="{{ $d->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('address_permanent_district_lists','district','districts');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- City  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['permanent']['city']">City</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="address_permanent_city_lists"
                   id="address['permanent']['city']"
                   name="address['permanent']['city']" type="text">
            <datalist id="address_permanent_city_lists">
                @php $cities = \App\Location\City::orderby('id','desc')->get()  @endphp
                @foreach($cities as $c)
                    <option value="{{ $c->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('address_permanent_city_lists','city','cities');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- Tehsil  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['permanent']['tehsil']">Tehsil</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="address_permanent_tehsil_lists"
                   id="address['permanent']['tehsil']"
                   name="address['permanent']['tehsil']" type="text">
            <datalist id="address_permanent_tehsil_lists">
                @php $tehsils = \App\Location\Tehsil::orderby('id','desc')->get()  @endphp
                @foreach($tehsils as $t)
                    <option value="{{ $t->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('address_permanent_tehsil_lists','tehsil','tehsils');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- Colony/Village  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['permanent']['colony']">Colony/Village</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="address_permanent_colony_lists"
                   id="address['permanent']['colony']"
                   name="address['permanent']['colony']" type="text">
            <datalist id="address_permanent_colony_lists">
                @php $colonies = \App\Location\Colony::orderby('id','desc')->get()  @endphp
                @foreach($colonies as $c)
                    <option value="{{ $c->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('address_permanent_colony_lists','colony','colonies');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{--Block--}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['permanent']['block']">Block</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="address['permanent']['block']"
                   name="address['permanent']['block']" type="text">
        </div>
    </div>
    {{--building--}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['permanent']['building']">Building</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="address['permanent']['building']"
                   name="address['permanent']['building']" type="text">
        </div>
    </div>
    {{-- Street No. --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['permanent']['street_no']">Street No.</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="address['permanent']['street_no']"
                   name="address['permanent']['street_no']" type="text">
        </div>
    </div>
    {{-- Floor No. --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['permanent']['floor_no']">Floor No.</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="address['permanent']['floor_no']"
                   name="address['permanent']['floor_no']" type="text">
        </div>
    </div>
    {{-- House No. --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['permanent']['house_no']">House No.</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="address['permanent']['house_no']"
                   name="address['permanent']['house_no']" type="text">
        </div>
    </div>
    {{-- Room No. --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['permanent']['room_no']">Room No.</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="address['permanent']['room_no']"
                   name="address['permanent']['room_no']" type="text">
        </div>
    </div>
    {{-- Post Office --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['permanent']['post_office']">Post Office</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="address['permanent']['post_office']"
                   name="address['permanent']['post_office']" type="text">
        </div>
    </div>
    {{-- Near By/Opposite etc.  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['permanent']['near_by']">Near By/Opposite</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="address['permanent']['near_by']"
                   name="address['permanent']['near_by']" type="text">
        </div>
    </div>
    {{-- Postal Code  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['permanent']['postal_code']">Postal Code</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="address['permanent']['postal_code']"
                   name="address['permanent']['postal_code']" type="text">
        </div>
    </div>
</div>

<div class="row">

    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">Postal Address</h6>
    </div>
    {{-- Countires --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['postal_address']['country']">Country</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="address_postal_address_country_lists"
                   id="address['postal_address']['country']"
                   name="address['postal_address']['country']" type="text">
            <datalist id="address_postal_address_country_lists">
                @php $countries = \App\Location\Country::orderby('id','desc')->get()  @endphp
                @foreach($countries as $c)
                    <option value="{{ $c->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('address_postal_address_country_lists','country','countries');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- Provinces/States --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['postal_address']['province']">Province</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="address_postal_address_province_lists"
                   id="address['postal_address']['province']"
                   name="address['postal_address']['province']" type="text">
            <datalist id="address_postal_address_province_lists">
                @php $states = \App\Location\State::orderby('id','desc')->get()  @endphp
                @foreach($states as $state)
                    <option value="{{ $state->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('address_postal_address_province_lists','province','states');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- Districts --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['postal_address']['district']">District</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="address_postal_address_district_lists"
                   id="address['postal_address']['district']"
                   name="address['postal_address']['district']" type="text">
            <datalist id="address_postal_address_district_lists">
                @php $cities = \App\Location\City::orderby('id','desc')->get()  @endphp
                @foreach($cities as $c)
                    <option value="{{ $c->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('address_postal_address_city_lists','district','districts');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- City  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['postal_address']['city']">City</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="address_postal_address_city_lists"
                   id="address['postal_address']['city']"
                   name="address['postal_address']['city']" type="text">
            <datalist id="address_postal_address_city_lists">
                @php $cities = \App\Location\City::orderby('id','desc')->get()  @endphp
                @foreach($cities as $c)
                    <option value="{{ $c->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_city('address_postal_address_city_lists','city');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- Tehsil  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['postal_address']['tehsil']">Tehsil</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="address_postal_address_tehsil_lists"
                   id="address['postal_address']['tehsil']"
                   name="address['postal_address']['tehsil']" type="text">
            <datalist id="address_postal_address_tehsil_lists">
                @php $tehsils = \App\Location\Tehsil::orderby('id','desc')->get()  @endphp
                @foreach($tehsils as $t)
                    <option value="{{ $t->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('address_postal_address_tehsil_lists','tehsil','tehsils');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- Colony/Village  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['postal_address']['colony']">Colony/Village</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="address_postal_address_colony_lists"
                   id="address['postal_address']['colony']"
                   name="address['postal_address']['colony']" type="text">
            <datalist id="address_postal_address_colony_lists">
                @php $colonies = \App\Location\Colony::orderby('id','desc')->get()  @endphp
                @foreach($colonies as $c)
                    <option value="{{ $c->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('address_postal_address_colony_lists','colony','colonies');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{--Block--}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['postal_address']['block']">Block</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="address['postal_address']['block']"
                   name="address['postal_address']['block']" type="text">
        </div>
    </div>
    {{--building--}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['postal_address']['building']">Building</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="address['postal_address']['building']"
                   name="address['postal_address']['building']" type="text">
        </div>
    </div>
    {{-- Street No. --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['postal_address']['street_no']">Street No.</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="address['postal_address']['street_no']"
                   name="address['postal_address']['street_no']" type="text">
        </div>
    </div>
    {{-- Floor No. --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['postal_address']['floor_no']">Floor No.</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="address['postal_address']['floor_no']"
                   name="address['postal_address']['floor_no']" type="text">
        </div>
    </div>
    {{-- House No. --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['postal_address']['house_no']">House No.</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="address['postal_address']['house_no']"
                   name="address['postal_address']['house_no']" type="text">
        </div>
    </div>
    {{-- Room No. --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['postal_address']['room_no']">Room No.</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="address['postal_address']['room_no']"
                   name="address['postal_address']['room_no']" type="text">
        </div>
    </div>
    {{-- Post Office --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['postal_address']['post_office']">Post Office</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="address['postal_address']['post_office']"
                   name="address['postal_address']['post_office']" type="text">
        </div>
    </div>
    {{-- Near By/Opposite etc.  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['postal_address']['near_by']">Near By/Opposite</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="address['postal_address']['near_by']"
                   name="address['postal_address']['near_by']" type="text">
        </div>
    </div>
    {{-- Postal Code  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="address['postal_address']['postal_code']">Postal Code</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="address['postal_address']['postal_code']"
                   name="address['postal_address']['postal_code']" type="text">
        </div>
    </div>
</div>

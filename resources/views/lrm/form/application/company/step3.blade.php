<div class="row">

    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">Head Office</h6>
    </div>
    {{-- Provinces/States --}}
    <div class="form-group col-md-3 mb-1">
        <label for="head_office['province']">Province</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="head_office_province_lists"
                   id="head_office['province']"
                   name="head_office['province']" type="text">
            <datalist id="head_office_province_lists">
                @php $states = \App\Location\State::orderby('id','desc')->get()  @endphp
                @foreach($states as $state)
                    <option value="{{ $state->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('head_office_province_lists','province','states');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- District  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="head_office['district']">District</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="head_office_district_lists"
                   id="head_office['district']"
                   name="head_office['district']" type="text">
            <datalist id="head_office_district_lists">
                @php $districts = \App\Location\District::orderby('id','desc')->get()  @endphp
                @foreach($districts as $d)
                    <option value="{{ $d->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('head_office_district_lists','district','districts');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- City  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="head_office['city']">City</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="head_office_city_lists"
                   id="head_office['city']"
                   name="head_office['city']" type="text">
            <datalist id="head_office_city_lists">
                @php $cities = \App\Location\City::orderby('id','desc')->get()  @endphp
                @foreach($cities as $c)
                    <option value="{{ $c->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('head_office_city_lists','city','cities');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- Tehsil  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="head_office['tehsil']">Tehsil</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="head_office_tehsil_lists"
                   id="head_office['tehsil']"
                   name="head_office['tehsil']" type="text">
            <datalist id="head_office_tehsil_lists">
                @php $tehsils = \App\Location\Tehsil::orderby('id','desc')->get()  @endphp
                @foreach($tehsils as $t)
                    <option value="{{ $t->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('head_office_tehsil_lists','tehsil','tehsils');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- Colony/Village  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="head_office['colony']">Colony/Village</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="head_office_colony_lists"
                   id="head_office['colony']"
                   name="head_office['colony']" type="text">
            <datalist id="head_office_colony_lists">
                @php $colonies = \App\Location\Colony::orderby('id','desc')->get()  @endphp
                @foreach($colonies as $c)
                    <option value="{{ $c->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('head_office_colony_lists','colony','colonies');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{--Block--}}
    <div class="form-group col-md-3 mb-1">
        <label for="head_office['block']">Block</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="head_office['block']"
                   name="head_office['block']" type="text">
        </div>
    </div>
    {{--building--}}
    <div class="form-group col-md-3 mb-1">
        <label for="head_office['building']">Building</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="head_office['building']"
                   name="head_office['building']" type="text">
        </div>
    </div>
    {{-- Street No. --}}
    <div class="form-group col-md-3 mb-1">
        <label for="head_office['street_no']">Street No.</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="head_office['street_no']"
                   name="head_office['street_no']" type="text">
        </div>
    </div>
    {{-- Floor No. --}}
    <div class="form-group col-md-3 mb-1">
        <label for="head_office['floor_no']">Floor No.</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="head_office['floor_no']"
                   name="head_office['floor_no']" type="text">
        </div>
    </div>
    {{-- House No. --}}
    <div class="form-group col-md-3 mb-1">
        <label for="head_office['house_no']">House No.</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="head_office['house_no']"
                   name="head_office['house_no']" type="text">
        </div>
    </div>
    {{-- Room No. --}}
    <div class="form-group col-md-3 mb-1">
        <label for="head_office['room_no']">Room No.</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="head_office['room_no']"
                   name="head_office['room_no']" type="text">
        </div>
    </div>
    {{-- Post Office --}}
    <div class="form-group col-md-3 mb-1">
        <label for="head_office['post_office']">Post Office</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="head_office['post_office']"
                   name="head_office['post_office']" type="text">
        </div>
    </div>
    {{-- Near By/Opposite etc.  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="head_office['near_by']">Near By/Opposite</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="head_office['near_by']"
                   name="head_office['near_by']" type="text">
        </div>
    </div>
    {{-- Postal Code  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="head_office['postal_code']">Postal Code</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="head_office['postal_code']"
                   name="head_office['postal_code']" type="text">
        </div>
    </div>
</div>
<div class="row">

    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">Factory/Sub Office</h6>
    </div>
    {{-- Provinces/States --}}
    <div class="form-group col-md-3 mb-1">
        <label for="factory_or_sub_office['province']">Province</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="factory_or_sub_office_province_lists"
                   id="factory_or_sub_office['province']"
                   name="factory_or_sub_office['province']" type="text">
            <datalist id="factory_or_sub_office_province_lists">
                @php $states = \App\Location\State::orderby('id','desc')->get()  @endphp
                @foreach($states as $state)
                    <option value="{{ $state->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('factory_or_sub_office_province_lists','province','states');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- District  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="factory_or_sub_office['district']">District</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="factory_or_sub_office_district_lists"
                   id="factory_or_sub_office['district']"
                   name="factory_or_sub_office['district']" type="text">
            <datalist id="factory_or_sub_office_district_lists">
                @php $districts = \App\Location\District::orderby('id','desc')->get()  @endphp
                @foreach($districts as $d)
                    <option value="{{ $d->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('factory_or_sub_office_district_lists','district','districts');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- City  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="factory_or_sub_office['city']">City</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="factory_or_sub_office_city_lists"
                   id="factory_or_sub_office['city']"
                   name="factory_or_sub_office['city']" type="text">
            <datalist id="factory_or_sub_office_city_lists">
                @php $cities = \App\Location\City::orderby('id','desc')->get()  @endphp
                @foreach($cities as $c)
                    <option value="{{ $c->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('factory_or_sub_office_city_lists','city','cities');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- Tehsil  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="factory_or_sub_office['tehsil']">Tehsil</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="factory_or_sub_office_tehsil_lists"
                   id="factory_or_sub_office['tehsil']"
                   name="factory_or_sub_office['tehsil']" type="text">
            <datalist id="factory_or_sub_office_tehsil_lists">
                @php $tehsils = \App\Location\Tehsil::orderby('id','desc')->get()  @endphp
                @foreach($tehsils as $t)
                    <option value="{{ $t->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('factory_or_sub_office_tehsil_lists','tehsil','tehsils');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{-- Colony/Village  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="factory_or_sub_office['colony']">Colony/Village</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="factory_or_sub_office_colony_lists"
                   id="factory_or_sub_office['colony']"
                   name="factory_or_sub_office['colony']" type="text">
            <datalist id="factory_or_sub_office_colony_lists">
                @php $colonies = \App\Location\Colony::orderby('id','desc')->get()  @endphp
                @foreach($colonies as $c)
                    <option value="{{ $c->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_location('factory_or_sub_office_colony_lists','colony','colonies');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    {{--Block--}}
    <div class="form-group col-md-3 mb-1">
        <label for="factory_or_sub_office['block']">Block</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="factory_or_sub_office['block']"
                   name="factory_or_sub_office['block']" type="text">
        </div>
    </div>
    {{--building--}}
    <div class="form-group col-md-3 mb-1">
        <label for="factory_or_sub_office['building']">Building</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="factory_or_sub_office['building']"
                   name="factory_or_sub_office['building']" type="text">
        </div>
    </div>
    {{-- Street No. --}}
    <div class="form-group col-md-3 mb-1">
        <label for="factory_or_sub_office['street_no']">Street No.</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="factory_or_sub_office['street_no']"
                   name="factory_or_sub_office['street_no']" type="text">
        </div>
    </div>
    {{-- Floor No. --}}
    <div class="form-group col-md-3 mb-1">
        <label for="factory_or_sub_office['floor_no']">Floor No.</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="factory_or_sub_office['floor_no']"
                   name="factory_or_sub_office['floor_no']" type="text">
        </div>
    </div>
    {{-- House No. --}}
    <div class="form-group col-md-3 mb-1">
        <label for="factory_or_sub_office['house_no']">House No.</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="factory_or_sub_office['house_no']"
                   name="factory_or_sub_office['house_no']" type="text">
        </div>
    </div>
    {{-- Room No. --}}
    <div class="form-group col-md-3 mb-1">
        <label for="factory_or_sub_office['room_no']">Room No.</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="factory_or_sub_office['room_no']"
                   name="factory_or_sub_office['room_no']" type="text">
        </div>
    </div>
    {{-- Post Office --}}
    <div class="form-group col-md-3 mb-1">
        <label for="factory_or_sub_office['post_office']">Post Office</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="factory_or_sub_office['post_office']"
                   name="factory_or_sub_office['post_office']" type="text">
        </div>
    </div>
    {{-- Near By/Opposite etc.  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="factory_or_sub_office['near_by']">Near By/Opposite</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="factory_or_sub_office['near_by']"
                   name="factory_or_sub_office['near_by']" type="text">
        </div>
    </div>
    {{-- Postal Code  --}}
    <div class="form-group col-md-3 mb-1">
        <label for="factory_or_sub_office['postal_code']">Postal Code</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="factory_or_sub_office['postal_code']"
                   name="factory_or_sub_office['postal_code']" type="text">
        </div>
    </div>
</div>

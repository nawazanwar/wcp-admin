<div class="row">
    <div class="form-group col-md-3">
        <label for="applicant_detail['title']">Title</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="applicant_detail_titles_lists"
                   id="applicant_detail['title']"
                   name="applicant_detail['title']" type="text">
            <datalist id="applicant_detail_titles_lists">
                @php $titles = \App\Models\LrmSettings::whereParent('title')->orderby('id','desc')->get()  @endphp
                @foreach($titles as $title)
                    <option value="{{ $title->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('applicant_detail_titles_lists','title');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3">
        <label for="applicant_detail['company_type']">Company Type</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="applicant_detail_company_type_lists"
                   id="applicant_detail['company_type']"
                   name="applicant_detail['company_type']" type="text">
            <datalist id="applicant_detail_company_type_lists">
                @php $company_types = \App\Models\LrmSettings::whereParent('company_type')->orderby('id','desc')->get()  @endphp
                @foreach($company_types as $company_type)
                    <option value="{{ $company_type->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('applicant_detail_company_type_lists','company_type');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3">
        <label for="applicant_detail['phase']">Phase</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="applicant_detail_phase_lists"
                   id="applicant_detail['phase']"
                   name="applicant_detail['phase']" type="text">
            <datalist id="applicant_detail_phase_lists">
                @php $phases = \App\Models\Phase::orderby('id','desc')->get()  @endphp
                @foreach($phases as $phase)
                    <option value="{{ $phase->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_phase('applicant_detail_phase_lists','Phase');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3">
        <label for="applicant_detail['application_number']">Application Number</label>
        <input class="form-control form-control-sm" id="applicant_detail['application_number']"
               name="applicant_detail['application_number']" type="text">
    </div>
    <div class="form-group col-md-3">
        <label for="applicant_detail['application_date']">Application Date</label>
        <input class="form-control form-control-sm" id="applicant_detail['application_date']"
               data-date-format="DD MMMM YYYY"
               name="applicant_detail['application_date']" type="date" value="{{ date('Y-m-d') }}">
    </div>
    <div class="form-group col-md-3">
        <label for="applicant_detail['property_applied_for']">Property Applied For</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="applicant_property_applied_for_lists"
                   id="applicant_detail['property_applied_for']"
                   name="applicant_detail['property_applied_for']" type="text">
            <datalist id="applicant_property_applied_for_lists">
                @php $property_types = \App\Models\LrmSettings::whereParent('property_type')->orderby('id','desc')->get()  @endphp
                @foreach($property_types as $property_type)
                    <option value="{{ $property_type->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('applicant_property_applied_for_lists','property_type');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3">
        <label for="applicant_detail['submission_mode']">Application Submission Mode</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="applicant_submission_mode_lists"
                   id="applicant_detail['submission_mode']"
                   name="applicant_detail['submission_mode']" type="text">
            <datalist id="applicant_submission_mode_lists">
                <option>Hard Coded</option>
                <option>Mail</option>
            </datalist>
        </div>
    </div>
    <div class="form-group col-md-3">
        <label for="applicant_detail['submitted_by']">Submitted By</label>
        <input class="form-control form-control-sm" id="applicant_detail['submitted_by']"
               name="applicant_detail['submitted_by']" type="text">
    </div>
    <div class="form-group col-md-3">
        <label for="applicant_detail[direction_by]">Direction By</label>
        <select class="form-control form-control-sm" id="applicant_detail[direction_by]"
                name="applicant_detail[direction_by]">
            <option></option>
        </select>
    </div>
    <div class="form-group col-md-3">
        <label for="applicant_detail[received_by]">Received By</label>
        <select class="form-control form-control-sm" id="applicant_detail[received_by]"
                name="applicant_detail[received_by]">
            <option></option>
        </select>
    </div>
    <div class="form-group col-md-3">
        <label for="applicant_detail[received_notes]">Received Note</label>
        <textarea class="form-control form-control-sm" name="applicant_detail[received_notes]"
                  id="applicant_detail[received_notes]" rows="1"></textarea>
    </div>
    <div class="form-group col-md-3">
        <label for="applicant_detail[recommended_by]">Recommended By</label>
        <select class="form-control form-control-sm" id="applicant_detail[recommended_by]"
                name="applicant_detail[recommended_by]">
            <option></option>
        </select>
    </div>
    <div class="form-group col-md-3">
        <label for="applicant_detail[recommender_comments]">Recommender's Comments</label>
        <textarea class="form-control form-control-sm" name="applicant_detail[recommender_comments]"
                  id="applicant_detail[recommender_comments]"
                  rows="1"></textarea>
    </div>
    <div class="form-group col-md-3">
        <label for="applicant_detail[remarks_overall]">Remarks (Overall)</label>
        <textarea class="form-control form-control-sm" name="applicant_detail[remarks_overall]"
                  id="applicant_detail[remarks_overall]" rows="1"></textarea>
    </div>
</div>

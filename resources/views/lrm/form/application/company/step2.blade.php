<div class="row">
    <div class="form-group col-md-3">
        <label for="company_rep_details[representative_or_authority]">Representative/Authority</label>
        <input class="form-control form-control-sm" list="company_rep_details_representative_or_authority_lists"
               id="company_rep_details[representative_or_authority]"
               name="company_rep_details[representative_or_authority]"
               type="text">
        <datalist id="company_rep_details_representative_or_authority_lists">
            <option value="Representative ">
            <option value="Authority">
        </datalist>
    </div>
    <div class="form-group col-md-3">
        <label for="company_rep_details[appointment_memo_number]">Appointment Memo No.</label>
        <input class="form-control form-control-sm" id="company_rep_details[appointment_memo_number]"
               name="company_rep_details[appointment_memo_number]"
               type="text">
    </div>
    <div class="form-group col-md-2 mb-1">
        <label for="company_rep_details['appointment_date']">Appointment Date</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm datetime" id="company_rep_details['appointment_date']"
                   name="company_rep_details['appointment_date']" type="text">
        </div>
    </div>
    <div class="form-group col-md-2 mb-1">
        <label for="company_rep_details['dairy_number']">Diary No.</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="company_rep_details['dairy_number']"
                   name="company_rep_details['dairy_number']" type="text">
        </div>
    </div>
    <div class="form-group col-md-2 mb-1">
        <label for="company_rep_details['dairy_date']">Diary Date.</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm datetime" id="company_rep_details['dairy_date']"
                   name="company_rep_details['dairy_date']" type="text">
        </div>
    </div>
</div>
{{-- Name--}}
<div class="row">
    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">Name</h6>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="company_rep_details_name['title']">Title</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="application_titles_lists"
                   id="company_rep_details_name['title']"
                   name="company_rep_details_name['title']" type="text">
            <datalist id="company_rep_details_name_titles_lists">
                @php $titles = \App\Models\LrmSettings::whereParent('title')->orderby('id','desc')->get()  @endphp
                @foreach($titles as $title)
                    <option value="{{ $title->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('company_rep_details_name_titles_lists','title');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="company_rep_details_name['prefix']">Prefix</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="company_rep_details_name_lists"
                   id="company_rep_details_name['prefix']"
                   name="company_rep_details_name['prefix']" type="text">
            <datalist id="company_rep_details_name_lists">
                @php $prefixes = \App\Models\LrmSettings::whereParent('prefix')->orderby('id','desc')->get()  @endphp
                @foreach($prefixes as $prefix)
                    <option value="{{ $prefix->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('company_rep_details_name_lists','prefix');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="company_rep_details_name['first_name']">First Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="company_rep_details_name['first_name']"
                   name="company_rep_details_name['first_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="company_rep_details_name['middle_name']">Middle Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="company_rep_details_name['middle_name']"
                   name="company_rep_details_name['middle_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="company_rep_details_name['last_name']">Last Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="company_rep_details_name['last_name']"
                   name="company_rep_details_name['last_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="company_rep_details_name['postfix']">Postfix</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="company_rep_details_name_postfix_lists"
                   id="company_rep_details_name['postfix']"
                   name="company_rep_details_name['postfix']" type="text">
            <datalist id="company_rep_details_name_postfix_lists">
                @php $postfixes = \App\Models\LrmSettings::whereParent('postfix')->orderby('id','desc')->get()  @endphp
                @foreach($postfixes as $postfix)
                    <option value="{{ $postfix->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('company_rep_details_name_postfix_lists','postfix');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
</div>
{{-- Applicant Father/Husband --}}
<div class="row">
    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">Father/Husband Name</h6>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="company_rep_details_father_husband['relation']">Relation</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="company_rep_details_father_husband_relation_list"
                   id="company_rep_details_father_husband['relation']"
                   name="company_rep_details_father_husband['relation']" type="text">
            <datalist id="company_rep_details_father_husband_relation_list">
                @php $relations = \App\Models\LrmSettings::whereParent('relation')->orderby('id','desc')->get()  @endphp
                @foreach($relations as $relation)
                    <option value="{{ $relation->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('company_rep_details_father_husband_relation_list','relation');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="company_rep_details_father_husband['prefix']">Prefix</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="company_rep_details_father_husband_prefix_list"
                   id="company_rep_details_father_husband['prefix']"
                   name="company_rep_details_father_husband['prefix']" type="text">
            <datalist id="company_rep_details_father_husband_prefix_list">
                @php $prefixes = \App\Models\LrmSettings::whereParent('prefix')->orderby('id','desc')->get()  @endphp
                @foreach($prefixes as $prefix)
                    <option value="{{ $prefix->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('company_rep_details_father_husband_prefix_list','prefix');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="company_rep_details_father_husband['first_name']">First Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="company_rep_details_father_husband['first_name']"
                   name="company_rep_details_father_husband['first_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="company_rep_details_father_husband['middle_name']">Middle Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="company_rep_details_father_husband['middle_name']"
                   name="company_rep_details_father_husband['middle_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label
            for="company_rep_details_father_husband['last_name']">Last Name</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="company_rep_details_father_husband['last_name']"
                   name="company_rep_details_father_husband['last_name']" type="text">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="company_rep_details_father_husband['postfix']">Postfix</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" list="company_rep_details_father_husband_postfix_list"
                   id="company_rep_details_father_husband['postfix']"
                   name="company_rep_details_father_husband['postfix']" type="text">
            <datalist id="company_rep_details_father_husband_postfix_list">
                @php $postfixes = \App\Models\LrmSettings::whereParent('postfix')->orderby('id','desc')->get()  @endphp
                @foreach($postfixes as $postfix)
                    <option value="{{ $postfix->name }}">
                @endforeach
            </datalist>
            <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"
                        onclick="Custom.generate_lrm_setting('company_rep_details_father_husband_postfix_list','postfix');">
                    <i class="fa fa-plus"></i>
                </button>
            </span>
        </div>
    </div>
</div>
{{-- Appliocant Other--}}
<div class="row">
    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">Others</h6>
    </div>
    <div class="form-group col-md-3 mb-1 text-center">
        <label for="company_rep_details_others['gender']">Gender</label>
        <div class="input-group input-group-sm">
            <div class="icheck-success d-inline">
                <input type="radio" name="company_rep_details_others['gender']" id="company_rep_details_others['male']"
                       value="male" checked="">
                <label for="company_rep_details_others['male']">Male</label>
            </div>
            <div class="icheck-success d-inline ml-2">
                <input type="radio" name="company_rep_details_others['gender']"
                       id="company_rep_details_others['female']" value="female">
                <label for="company_rep_details_others['female']">Female</label>
            </div>
            <div class="icheck-success d-inline ml-2">
                <input type="radio" name="company_rep_details_others['gender']" id="company_rep_details_others['other']"
                       value="other">
                <label for="company_rep_details_others['other']">Other</label>
            </div>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="company_rep_details_others['cnic']">CNIC</label>
        <div class="input-group input-group-sm">
            <input class="form-control form-control-sm" id="company_rep_details_others['cnic']"
                   onkeyup="Custom.validateCNIC(event);" name="company_rep_details_others['cnic']" type="text"
                   placeholder="xxxxx-xxxxxxx-x">
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="company_rep_details_others['mailing_address']">Mailing Address</label>
        <div class="input-group input-group-sm">
            <textarea class="form-control form-control-sm" id="company_rep_details_others['mailing_address']"
                      name="company_rep_details_others['mailing_address']" rows="1"></textarea>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="company_rep_details_others['permanent_address']">Permanent Address</label>
        <div class="input-group input-group-sm">
            <textarea class="form-control form-control-sm" id="company_rep_details_others['permanent_address']"
                      name="company_rep_details_others['permanent_address']" rows="1"></textarea>
        </div>
    </div>
    <div class="form-group col-md-3 mb-1">
        <label for="company_rep_details_others['remarks']">Remarks (Applicant's Remarks)</label>
        <div class="input-group input-group-sm">
            <textarea class="form-control form-control-sm" id="company_rep_details_others['remarks']"
                      name="company_rep_details_others['remarks']" rows="1"></textarea>
        </div>
    </div>
</div>

{{--Documents--}}
<div class="row">
    <div class="col-12 info-heading-parent">
        <h6 class="info-heading">Images</h6>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="company_rep_details_docs[supporting_docs]">Supporting Doc.</label>
            <div class="input-group input-group-sm">
                <div class="custom-file">
                    <input type="file" name="company_rep_details_docs[supporting_docs]"
                           class="custom-file-input form-control-sm" id="company_rep_details_docs[supporting_docs]"
                           onchange="Custom.previewImage(this,'company_rep_details_docs_supporting_docs')">
                    <label class="custom-file-label btn-sm" for="company_rep_details_docs[supporting_docs]">Choose
                        File</label>
                </div>
            </div>
        </div>
        <div class="company_rep_details_docs_supporting_docs text-center my-3">
            <img src="{{ asset('img/defaults/document.png') }}" class="img-thumbnail h-150">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="company_rep_details_docs[cnic_front]">CNIC Front</label>
            <div class="input-group input-group-sm">
                <div class="custom-file">
                    <input type="file" name="company_rep_details_docs[cnic_front]"
                           class="custom-file-input form-control-sm" id="company_rep_details_docs[cnic_front]"
                           onchange="Custom.previewImage(this,'company_rep_details_docs_cnic_front_image_holder')">
                    <label class="custom-file-label btn-sm" for="company_rep_details_docs[cnic_front]">Choose
                        File</label>
                </div>
            </div>
        </div>
        <div class="company_rep_details_docs_cnic_front_image_holder text-center my-3">
            <img src="http://wcp.local/img/defaults/cnic/front.jpeg" class="img-thumbnail h-150">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="company_rep_details_docs[cnic_back]">CNIC Back</label>
            <div class="input-group input-group-sm">
                <div class="custom-file">
                    <input type="file" name="company_rep_details_docs[cnic_back]"
                           class="custom-file-input form-control-sm" id="company_rep_details_docs[cnic_back]"
                           onchange="Custom.previewImage(this,'company_rep_details_docs_cnic_back_image_holder')">
                    <label class="custom-file-label btn-sm" for="company_rep_details_docs[cnic_back]">Choose
                        File</label>
                </div>
            </div>
        </div>
        <div class="company_rep_details_docs_cnic_back_image_holder text-center my-3">
            <img src="http://wcp.local/img/defaults/cnic/back.jpeg" class="img-thumbnail h-150">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="company_rep_details_docs[photograph]">Photograph</label>
            <div class="input-group input-group-sm">
                <div class="custom-file">
                    <input type="file" name="company_rep_details_docs[photograph]"
                           class="custom-file-input form-control-sm" id="company_rep_details_docs[photograph]"
                           onchange="Custom.previewImage(this,'company_rep_details_docs_photograph_image_holder')">
                    <label class="custom-file-label btn-sm" for="company_rep_details_docs[photograph]">Choose
                        File</label>
                </div>
            </div>
        </div>
        <div class="company_rep_details_docs_photograph_image_holder text-center my-3">
            <img src="{{ asset('img/defaults/user-picture.png') }}" class="img-thumbnail h-150">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="company_rep_details_docs[thumb]">Thumb</label>
            <div class="input-group input-group-sm">
                <div class="custom-file">
                    <input type="file" name="company_rep_details_docs[thumb]"
                           class="custom-file-input form-control-sm" id="company_rep_details_docs[thumb]"
                           onchange="Custom.previewImage(this,'company_rep_details_docs_thumb_image_holder')">
                    <label class="custom-file-label btn-sm" for="company_rep_details_docs[thumb]">Choose File</label>
                </div>
            </div>
        </div>
        <div class="company_rep_details_docs_thumb_image_holder text-center my-3">
            <img src="{{ asset('img/defaults/thumb.jpg') }}" class="img-thumbnail h-150">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="company_rep_details_docs[signature]">Signature</label>
            <div class="input-group input-group-sm">
                <div class="custom-file">
                    <input type="file" name="company_rep_details_docs[signature]"
                           class="custom-file-input form-control-sm" id="company_rep_details_docs[signature]"
                           onchange="Custom.previewImage(this,'company_rep_details_docs_signature_image_holder')">
                    <label class="custom-file-label btn-sm" for="company_rep_details_docs[signature]">Choose File</label>
                </div>
            </div>
        </div>
        <div class="company_rep_details_docs_signature_image_holder text-center my-3">
            <img src="{{ asset('img/defaults/signature.png') }}" class="img-thumbnail h-150">
        </div>
    </div>
</div>

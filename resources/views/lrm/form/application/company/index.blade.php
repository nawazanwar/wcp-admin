<div class="arrowwizard">
    <ul>
        <li>
            <a href="#application-company-step-1">Step 1<br/><small>Application & Applicant's Details</small></a>
        </li>
        <li>
            <a href="#application-company-step-2">Step 2<br/><small>Company Representative Details</small></a>
        </li>
        <li>
            <a href="#application-company-step-3">Step 3<br/><small>Address</small></a>
        </li>
        <li>
            <a href="#application-company-step-4">Step 4<br/><small>Contact Details</small></a>
        </li>
    </ul>
    <div class="mt-4">
        <div id="application-company-step-1">
            @include('lrm.form.application.company.step1')
        </div>
        <div id="application-company-step-2">
            @include('lrm.form.application.company.step2')
        </div>
        <div id="application-company-step-3">
            @include('lrm.form.application.company.step3')
        </div>
        <div id="application-company-step-4">
            @include('lrm.form.application.company.step4')
        </div>
    </div>
</div>

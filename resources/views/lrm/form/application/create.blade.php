@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/multiform/css/style.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/multiform/css/arrows.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/multiform/css/dots.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/promptbox/css/jq-prompt.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datetime/css/style.css') }}">
@endsection
@section('content')
    <div class="card card-primary card-outline card-tabs">
        <div class="card-header p-0 border-bottom-0 my-2 row">
            <div class="col-md-6 mx-auto custom_tab_holder">
                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill"
                           href="#custom-tabs-for-individual" role="tab" aria-controls="custom-tabs-for-individual"
                           aria-selected="true">Application For Individual</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-three-home-tab" data-toggle="pill"
                           href="#custom-tabs-for-company" role="tab" aria-controls="custom-tabs-for-individual"
                           aria-selected="true">Application For Company</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card-body border-top">
            <div class="tab-content" id="custom-tabs-three-tabContent">
                <div class="tab-pane fade show active" id="custom-tabs-for-individual" role="tabpanel"
                     aria-labelledby="custom-tabs-three-home-tab">
                    @include('lrm.form.application.individual.index')
                </div>
                <div class="tab-pane fade" id="custom-tabs-for-company" role="tabpanel"
                     aria-labelledby="custom-tabs-three-profile-tab">
                    @include('lrm.form.application.company.index')
                </div>
            </div>
        </div>
        <!-- /.card -->
    </div>
@endsection
@section('scriptInnerFiles')
    <script src="{{ asset('plugins/multiform/js/style.min.js') }}"></script>
    <script src="{{ asset('plugins/promptbox/js/jq-prompt.min.js') }}"></script>
    <script src="{{ asset('plugins/datetime/js/style.js') }}"></script>
@endsection

@section('pageScript')
    <script>
        $(document).ready(function () {
            $(".arrowwizard").smartWizard({
                selected: 0,
                theme: 'arrows',
                autoAdjustHeight: true,
                transitionEffect: 'fade',
                showStepURLhash: false
            });
        });
    </script>
@endsection


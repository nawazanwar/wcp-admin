<div class="row justify-content-center">
    <div class="col-md-4">
        <div class="card card-widget widget-user">
            <div class="widget-user-header bg-gradient-danger">
                <h4 class="widget-user-username  fs-18">{{__('lrm.application.long_title')}}</h4>
            </div>
            <div class="widget-user-image">
                <img class="img-circle elevation-2" src="{{ asset('img/lrm/application.jpg') }}" alt="HRM">
            </div>
            <div class="card-footer my-2">
                <div class="row ">
                    <div class="col-12 text-right">
                        @can('create',\App\Models\LRM::class)
                            <a href="{{ route('lrm-application.create') }}"
                               class="btn bg-gradient-danger btn-sm">{{ __('lrm.application.short_title') }} <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card card-widget widget-user">
            <div class="widget-user-header bg-gradient-info">
                <h4 class="widget-user-username  fs-18 ">{{__('lrm.membership.long_title')}}</h4>
            </div>
            <div class="widget-user-image">
                <img class="img-circle elevation-2" src="{{ asset('img/lrm/membership.jpg') }}" alt="HRM">
            </div>
            <div class="card-footer my-2">
                <div class="row ">
                    <div class="col-12 text-right">
                        @can('read',\App\Models\LRM::class)
                            <a href="{{ route('hrm.index') }}"
                               class="btn bg-gradient-info btn-sm">{{ __('lrm.membership.short_title') }} <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card card-widget widget-user">
            <div class="widget-user-header bg-gradient-success">
                <h4 class="widget-user-username  fs-18 ">{{__('lrm.plot.long_title')}}</h4>
            </div>
            <div class="widget-user-image">
                <img class="img-circle elevation-2" src="{{ asset('img/lrm/plots.jpg') }}" alt="HRM">
            </div>
            <div class="card-footer my-2">
                <div class="row ">
                    <div class="col-12 text-right">
                        @can('read',\App\Models\LRM::class)
                            <a href="{{ route('hrm.index') }}"
                               class="btn bg-gradient-success btn-sm">{{ __('lrm.plot.short_title') }} <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Title transer Box--}}
    <div class="col-md-4">
        <div class="card card-widget widget-user">
            <div class="widget-user-header bg-gradient-brown">
                <h4 class="widget-user-username  fs-18 text-white ">{{__('lrm.title_transfer.long_title')}}</h4>
            </div>
            <div class="widget-user-image">
                <img class="img-circle elevation-2" src="{{ asset('img/lrm/titles.jpg') }}" alt="HRM">
            </div>
            <div class="card-footer my-2">
                <div class="row ">
                    <div class="col-12 text-right">
                        @can('read',\App\Models\LRM::class)
                            <a href="{{ route('hrm.index') }}"
                               class="btn bg-gradient-brown btn-sm text-white">{{ __('lrm.title_transfer.short_title') }} <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Tanet management--}}
    <div class="col-md-4">
        <div class="card card-widget widget-user">
            <div class="widget-user-header bg-gradient-danger">
                <h4 class="widget-user-username  fs-18 ">{{__('lrm.tenat.long_title')}}</h4>
            </div>
            <div class="widget-user-image">
                <img class="img-circle elevation-2" src="{{ asset('img/lrm/tanet.jpg') }}" alt="HRM">
            </div>
            <div class="card-footer my-2">
                <div class="row ">
                    <div class="col-12 text-right">
                        @can('read',\App\Models\LRM::class)
                            <a href="{{ route('hrm.index') }}"
                               class="btn bg-gradient-danger btn-sm">{{ __('lrm.tenat.short_title') }} <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

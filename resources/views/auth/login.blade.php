@extends('layouts.auth')
@section('pageTitle', $pageTitle)
@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('content')
    <div class="card" style="max-width: 363px;">
        <div class="card-body login-card-body">
            <h5 class="login-box-msg">{{__('auth.login_heading')}}</h5>
            <form method="POST" action="{{ route('customLogin') }}">
                @csrf
                <div class="input-group mb-3">
                    <input id="email" type="text" class="form-control @error('unique_id') is-invalid @enderror"
                           name="unique_id" value="{{ old('unique_id') }}"
                           autocomplete="off"
                           placeholder="{{ __('auth.enter_unique_id') }}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fa fa-id-card"></span>
                        </div>
                    </div>
                    @error('unique_id')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>
                <div class="input-group mb-3">
                    <input id="password" type="password"
                           class="form-control  @error('password') is-invalid @enderror"
                           name="password"
                           placeholder="{{ __('auth.enter_password')}}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fa fa-lock"></span>
                        </div>
                    </div>
                    @error('password')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>
                @php
                    $MAC = exec('getmac');
                    $MAC = strtok($MAC, ' ');
                @endphp
                <input type="hidden" name="mac_address" value="{{ $MAC }}">
                <input type="hidden" name="ip_address" value="{{ $_SERVER['REMOTE_ADDR'] }}">
                <div class="form-group text-center">
                    <button type="submit" class="btn bg-gradient-primary btn-block">{{__('auth.login')}}</button>
                </div>
            </form>
        </div>
    </div>
@endsection

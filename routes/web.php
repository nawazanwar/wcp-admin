<?php

use App\Models\Role;
use App\Models\Permission;
use App\Models\User;
use App\Models\Setting;
use App\Models\Log;
use App\Models\Vehicle;
use App\Models\House;
use App\Models\Type;
use App\Models\Family;
use App\Models\FamilyPerson;
use App\Models\Camera;
use App\Models\VCompany;
use App\Models\VModel;

Auth::routes();

//auto redirect tp dashboard.....

Route::get('/', function () {

    return redirect('/dashboard/');

})->middleware('maintenance');

Route::group(['prefix' => '/dashboard', 'middleware' => ['maintenance']], function () {


    /* Country state city village dependent Drop Downs*/

    Route::resource('location', 'Location\HomeController');
    Route::resource('location-countries', 'Location\CountryController');
    Route::resource('location-states', 'Location\StateController');
    Route::resource('location-districts', 'Location\DistrictController');
    Route::resource('location-cities', 'Location\CityController');
    Route::resource('location-tehsils', 'Location\TehsilController');
    Route::resource('location-colonies', 'Location\ColonyController');

    /*Message Modules*/

    Route::resource('messages', 'MessageController');

    /*LRM (Land Record Management System*/

    Route::resource('lrm', 'LRM\HomeController');
    Route::resource('lrm-ministry', 'LRM\MinistryController');
    Route::resource('lrm-organization', 'LRM\OrganizationController');
    Route::resource('lrm-department', 'LRM\DepartmentController');
    Route::resource('lrm-rank', 'LRM\RankController');
    Route::resource('lrm-grade', 'LRM\GradeController');
    Route::resource('lrm-qualification', 'LRM\QualificationController');
    Route::resource('lrm-profession', 'LRM\ProfessionController');
    Route::resource('lrm-wing', 'LRM\WingController');
    Route::resource('lrm-settings', 'LRM\SettingController');
    /* Now ready to create Forms*/
    Route::resource('lrm-application', 'LRM\Form\ApplicationController');

    /*Device */

    Route::resource('device', 'Device\HomeController');
    Route::resource('device-class', 'Device\ClassController');
    Route::resource('device-operatingsystem', 'Device\DOperatingSystemController');
    Route::resource('device-make', 'Device\MakeController');
    Route::resource('device-model', 'Device\ModelController');
    Route::resource('device-type', 'Device\TypeController');

    /*VRS*/

    Route::resource('vrs', 'VRS\HomeController');
    Route::resource('vrs-vehicles', 'VRS\VehicleController');
    Route::resource('vrs-vehicle-types', 'VRS\VehicleTypeController');
    Route::resource('vrs-vehicle-companies', 'VRS\VehicleCompanyController');
    Route::resource('vrs-vehicle-models', 'VRS\VehicleModelController');
    Route::resource('vrs-vehicle-nature', 'VRS\VehicleNatureController');

    /*HRM*/

    Route::resource('hrm', 'HRM\HomeController');
    Route::resource('hrm-departments', 'HRM\DepartmentController');
    Route::resource('hrm-designations', 'HRM\DesignationController');
    Route::resource('hrm-employers', 'HRM\EmployerController');
    Route::resource('hrm-emp-types', 'HRM\EmployeeTypeController');
    Route::resource('hrm-card-types', 'HRM\CardTypeController');
    Route::resource('hrm-relations', 'HRM\RelationController');

    /*....Society Phases and Gates....*/

    Route::resource('society-gates', 'Society\GateController');
    Route::resource('society-booths', 'Society\BoothController');
    Route::resource('society-phase-gate-locations', 'Society\LocationController');


    /*visitor companies*/

    Route::resource('vcompanies', 'Vehicle\CompanyController');

    /*visitor models*/

    Route::resource('vmodels', 'Vehicle\ModelController');

    Route::get('', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);

    Route::get('system/apply/status/{type}/{id}/{value}', ['as' => 'system.apply.status', 'uses' => 'System\AjaxController@apply_status']);
    //Load more routes
    Route::get('system/load/more/{type}', ['as' => 'system.load.more', 'uses' => 'System\AjaxController@load_more']);
    //System Assigning routes
    Route::post('system/assign', ['as' => 'system.assign', 'uses' => 'System\AjaxController@assign']);

    Route::get('till', ['as' => 'till', 'uses' => 'TillController@index',
        'permissions' => User::modulePermissions(true, 'read')]);
    Route::get('tills/logs', ['as' => 'tills.logs', 'uses' => 'TillController@logs']);
    Route::get('till/manage', ['as' => 'till.manage', 'uses' => 'TillController@index',
        'permissions' => \App\Models\Booth::modulePermissions(true, 'read')]);

    //Settings

    Route::get('settings', ['as' => 'settings', 'uses' => 'SettingController@index',
        'permissions' => Setting::modulePermissions(true, 'read')]);
    Route::put('settings/update/{id}', ['as' => 'settings.update', 'uses' => 'SettingController@update',
        'permissions' => Setting::modulePermissions(true, 'edit')]);

    //Logs

    Route::get('logs/{type?}', ['as' => 'logs', 'uses' => 'LogController@index',
        'permissions' => Log::modulePermissions(true, 'read')]);

    //Applying status
    Route::get('system/apply/status/{type}/{id}/{value}', ['as' => 'system.apply.status', 'uses' => 'System\AjaxController@apply_status']);
    //Load more routes
    Route::get('system/load/more/{type}', ['as' => 'system.load.more', 'uses' => 'System\AjaxController@load_more']);
    //System Assigning routes
    Route::post('system/assign', ['as' => 'system.assign', 'uses' => 'System\AjaxController@assign']);


    // List of all Notifications

    Route::get('booth-notifications-count', 'BoothController@countNotifications')->name('booth-notifications-count');
    Route::get('booth-notifications/{for}/{type}/{boothId}', 'BoothController@getNotifications')->name('booth-notifications');
    Route::post('booth-read-notifications', 'BoothController@markAsReadNotifications')->name('booth-read-notifications');
    Route::get('booth-visitor-logs/{boothId}', 'BoothController@visitorLog')->name('booth-visitor-logs');
    Route::get('booth-cameras/{boothId}', 'BoothController@boothCameras')->name('booth-cameras');
    Route::get('booth-images/{boothId}', 'BoothController@boothImages')->name('booth-images');
    // Manage Visitor

    Route::resource('visitors', 'VisitorController');

});


Route::get('/logout', ['as' => 'auth.logout', 'uses' => 'Auth\LoginController@logout']);
Route::get('lang/{locale}', 'SwitcherController@lang')->name('lang.switcher')->middleware('maintenance');
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login')->middleware('maintenance');
Route::post('customLogin', 'Auth\LoginController@customLogin')->name('customLogin')->middleware('maintenance');
Route::post('customBoothLogin', 'Auth\BoothController@customBoothLogin')->name('customBoothLogin');
Route::get('boothLogin', 'Auth\BoothController@login')->name('booth.login');
Route::get('/till/filter', 'TillController@filter')->name('till.filter');
Route::get('/logs/visitor/set', 'LogController@save_visitor_logs')->name('till.filter');
Route::post('till/visitor/save', 'TillController@save_visitor')->name('till.visitor.save');
Route::post('/till/visitor/logs', 'VisitorController@save_logs')->name('till.visitor.save');
Route::post('/dependent/dropdown', 'DropDownController@filter');
Route::get('booth', 'BoothController@manage')->name('booth.manage')->middleware('boothLogin');


Route::post('booth/logout', 'Auth\BoothController@logout')->name('booth.logout');

Route::post('booth/visitors/enter', 'BoothController@enter')->name('booth-enter-visitor');
Route::post('booth/visitors/exit', 'BoothController@exit')->name('booth-exit-visitor');
Route::get('booth/visitor-already-enter-not-exit', 'BoothController@alreadyEnterNotExit')->name('visitor-already-enter-not-exit');
Route::post('booth/visitor/check/exit', 'BoothController@checkVisitorAtExit')->name('booth-check-visitor-at-exit');
Route::post('booth/visitor/check/enter', 'BoothController@checkVisitorAtEnter')->name('booth-check-visitor-at-enter');
Route::post('gen/veh/types', 'GenerationController@vehicleTypes')->name('gen-vehicle-types');
Route::post('gen/veh/companies', 'GenerationController@vehicleCompanies')->name('gen-vehicle-companies');
Route::post('gen/veh/models', 'GenerationController@vehicleModels')->name('gen-vehicle-models');
Route::post('gen/veh/nature/material', 'GenerationController@vehicleMaterialNature')->name('gen-vehicle-nature-of-materials');
Route::post('booth/temp/pic', 'BoothController@getTempPics')->name('booth.temp.pic');
Route::post('booth/temp/pic/all', 'BoothController@getAllTempPics')->name('booth.temp.pic.all');


Route::post('generate_hrm_employee_type_with_ajax', 'HRM\EmployeeTypeController@generate_with_ajax');
Route::post('generate_hrm_employer_with_ajax', 'HRM\EmployerController@generate_with_ajax');
Route::post('generate_hrm_department_with_ajax', 'HRM\DepartmentController@generate_with_ajax');
Route::post('generate_hrm_designation_with_ajax', 'HRM\DesignationController@generate_with_ajax');
Route::post('generate_hrm_card_type_with_ajax', 'HRM\CardTypeController@generate_with_ajax');

Route::post('generate_relation_in_english_with_ajax', 'HRM\RelationController@generate_relation_in_english_with_ajax');
Route::post('generate_relation_in_urdu_with_ajax', 'HRM\RelationController@generate_relation_in_urdu_with_ajax');

var Ajax = new function () {
    this.call = function (url, data, method = 'GET', callback) {
        Ajax.setAjaxHeader();
        $.ajax({
            type: method,
            global: false,
            async: true,
            url: url,
            data: data,
            success: function (response) {
                callback(response);
            },
            error: function () {
                console.log("Error Occurred");
            }
        });
    };
    this.setAjaxHeader = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            }
        });
    };
};

var Custom = new function () {

    this.generate_relation_in_english_with_ajax = function (id) {

        $.xPrompt({
            header: "Create new Relation in English",
            placeholder: 'Please Enter the Name'
        }, function (i) {
            Ajax.setAjaxHeader();
            Ajax.call('/generate_relation_in_english_with_ajax', {
                'name': i
            }, 'POST', function (response) {
                console.log(response);
                if (response.status == true) {
                    $('#' + id).prepend('<option id=' + response.model.id + '>' + response.model.name + '</option>');
                    $('#' + id).val(i).change();
                    $("#pt-Cancel").click();
                }
            });
        });

    }
    this.generate_relation_in_urdu_with_ajax = function (id) {

        $.xPrompt({
            header: "Create new Location in Urdu",
            placeholder: 'اردو میں تعلق'
        }, function (i) {
            Ajax.setAjaxHeader();
            Ajax.call('/generate_relation_in_urdu_with_ajax', {
                'name': i
            }, 'POST', function (response) {
                if (response.status == true) {
                    $('#' + id).prepend('<option id=' + response.model.id + '>' + response.model.name_ur + '</option>');
                    $('#' + id).val(i).change();
                    $("#pt-Cancel").click();
                }
            });
        });
    }

    this.generate_hrm_card_type = function (id) {
        $.xPrompt({
            header: "Create new Card Type",
            placeholder: 'Please Enter the Name'
        }, function (i) {
            Ajax.setAjaxHeader();
            Ajax.call('/generate_hrm_card_type_with_ajax', {
                'name': i
            }, 'POST', function (response) {
                if (response.status == true) {
                    $('#' + id).prepend('<option id=' + response.model.id + '>' + response.model.name + '</option>');
                    $('#' + id).val(i).change();
                    $("#pt-Cancel").click();
                }
            });
        });
    }

    this.generate_hrm_employee_type = function (id) {

        $.xPrompt({
            header: "Create new Employee Type",
            placeholder: 'Please Enter the Name'
        }, function (i) {
            Ajax.setAjaxHeader();
            Ajax.call('/generate_hrm_employee_type_with_ajax', {
                'name': i
            }, 'POST', function (response) {
                if (response.status == true) {
                    $('#' + id).prepend('<option id=' + response.model.id + '>' + response.model.name + '</option>');
                    $('#' + id).val(i).change();
                    $("#pt-Cancel").click();
                }
            });
        });
    }
    this.generate_hrm_employer = function (id) {
        $.xPrompt({
            header: "Create new Employer",
            placeholder: 'Please Enter the Name'
        }, function (i) {
            Ajax.setAjaxHeader();
            Ajax.call('/generate_hrm_employer_with_ajax', {
                'name': i
            }, 'POST', function (response) {
                if (response.status == true) {
                    $('#' + id).prepend('<option id=' + response.model.id + '>' + response.model.name + '</option>');
                    $('#' + id).val(i).change();
                    $("#pt-Cancel").click();
                }
            });
        });
    }
    this.generate_hrm_department = function (id) {
        $.xPrompt({
            header: "Create new Department",
            placeholder: 'Please Enter the Name'
        }, function (i) {
            Ajax.setAjaxHeader();
            Ajax.call('/generate_hrm_department_with_ajax', {
                'name': i
            }, 'POST', function (response) {
                if (response.status == true) {
                    $('#' + id).prepend('<option id=' + response.model.id + '>' + response.model.name + '</option>');
                    $('#' + id).val(i).change();
                    $("#pt-Cancel").click();
                }
            });
        });
    }
    this.generate_phase = function () {
        $.xPrompt({
            header: "Create new " + parent[0].toUpperCase() + parent.slice(1),
            placeholder: 'Please Enter the Name'
        }, function (i) {

            $('#' + id).prepend('<option>' + i + '</option>');
            $('#' + id).prev('input').val(i);
            Ajax.setAjaxHeader();
        });
    };

    this.checkBoxStatus = function (cElement) {

        if ($(cElement).prop("checked") == true) {
            $(cElement).val(1);
        } else if ($(cElement).prop("checked") == false) {
            $(cElement).val(0);
        }
    };
    this.startClock = function () {
        var date = new Date();
        var h = date.getHours(); // 0 - 23
        var m = date.getMinutes(); // 0 - 59
        var s = date.getSeconds(); // 0 - 59
        var session = "AM";
        if (h == 0) {
            h = 12;
        }
        if (h > 12) {
            h = h - 12;
            session = "PM";
        }
        h = (h < 10) ? "0" + h : h;
        m = (m < 10) ? "0" + m : m;
        s = (s < 10) ? "0" + s : s;
        var time = h + ":" + m + ":" + s + " " + session;
        $("#till_clock").empty().text(time);
        setTimeout(Custom.startClock, 1000);
    },
        //Delete all Records
        this.deleteAll = function (route = null) {
            var allVals = [];
            $(".sub_chk:checked").each(function () {
                if ($(this).closest('tr').attr('data-deleted') == 'no') {
                    allVals.push($(this).attr('data-id'));
                }
            });
            var confirm = window.confirm("Are you sure to delete the record");
            if (confirm == true) {
                var data = 'ids=' + allVals.join(",");
                Ajax.call(route + "?for=multiple", data, 'DELETE', function (data) {
                    if (data.status == 'success') {
                        $(".sub_chk:checkbox").each(function () {
                            $(this).parents('tr').addClass('deleted-row');
                        });
                        if ($('.sub_chk').length < 1) {
                            $(".delete_all").remove();
                            $("#master").closest('th').remove();
                        }
                    } else if (data.status == 'no_permission') {
                        toastr.error("No Permission to perform this action", 'Error');
                    }
                });
            }
        };

    //Delete Single Record

    this.deleteMe = function (cElement) {
        var route = $(cElement).attr('href');
        var confirm = window.confirm("Are you sure to delete the record");
        if (confirm == true) {
            Ajax.call(route, null, 'DELETE', function (data) {
                if (data.status == 'no_permission') {
                    toastr.error("No Permission to perform this action", 'Error');
                } else if (data.status == 'success') {
                    if (!$(cElement).parents('tr').hasClass('deleted-row')) {
                        $(cElement).parents('tr').addClass('deleted-row');
                        $(cElement).parents('tr').find('.active_holder').empty().html('<i class="fa fa-times"></i>');
                    }
                }
            })
        }
    };

    //restore record

    this.restoreMe = function (cElement) {
        var route = $(cElement).attr('href');
        var confirm = window.confirm("Are you sure to restore the record");
        if (confirm == true) {
            Ajax.call(route, null, 'GET', function (data) {
                if (data.status == 'no_permission') {
                    toastr.error("No Permission to perform this action", 'Error');
                } else if (data.status == 'success') {
                    if ($(cElement).parents('tr').hasClass('deleted-row')) {
                        $(cElement).parents('tr').removeClass('deleted-row');
                        $(cElement).parents('tr').find('.active_holder').empty().html('<i class="fa fa-check text-success"></i>');
                    }
                }
            })
        }
    };

    this.printMe = function (id) {
        var divToPrint = document.getElementById(id);
        $(divToPrint).css({
            'width': '100%'
        }).css({
            'border': '1px solid #ddd',
            'border-collapse': 'collapse'
        });
        $(divToPrint).find('th').css('border', '1px solid #ddd');
        $(divToPrint).find('td').css('border', '1px solid #ddd');
        $(divToPrint).find('.action').hide();
        $(divToPrint).find('.avatar').css({
            "width": "40px",
            "height": "40px"
        });
        var newWin = window.open("");
        newWin.document.write(divToPrint.outerHTML);
        newWin.print();
        newWin.close();
    };

    this.validateCNIC = function (event) {
        if (event.keyCode == 8 || event.keyCode == 9 ||
            event.keyCode == 27 || event.keyCode == 13 ||
            (event.keyCode == 65 && event.ctrlKey === true))
            return;
        if ((event.keyCode < 48 || event.keyCode > 57))
            event.preventDefault();

        var length = $(event.target).val().length;

        if (length == 5 || length == 13)
            $(event.target).val($(event.target).val() + '-');
    };
    this.validatePhone = function (event) {
        if (event.keyCode == 8 || event.keyCode == 9 ||
            event.keyCode == 27 || event.keyCode == 13 ||
            (event.keyCode == 65 && event.ctrlKey === true))
            return;
        if ((event.keyCode < 48 || event.keyCode > 57))
            event.preventDefault();

        if ($(event.target).val().length == 4) {
            $(event.target).val($(event.target).val() + '-');
        }
    };
    this.replaceSpaceWithHypen = function (cElement) {
        $(cElement).val($(cElement).val().replace(/\s+/g, '-').toLowerCase());
    };
    this.replaceSpaceWithUnderScore = function (cElement) {
        $(cElement).val($(cElement).val().replace(/\s+/g, '_').toLowerCase());
    };
    this.upperCaseEveryLetter = function (cElement) {
        var splitStr = $(cElement).val().toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        $(cElement).val(splitStr.join(' '));
    };
    this.previewAvatar = function (input) {
        var reader = new FileReader();
        reader.onload = function () {
            $(input).parents('.avatar_preview_holder').find('img').attr('src', reader.result);
        };
        reader.readAsDataURL(input.files[0]);
    };
    this.previewImage = function (cElement, parent_holder) {
        var reader = new FileReader();
        reader.onload = function () {
            $("." + parent_holder).find('img').attr('src', reader.result);
        };
        reader.readAsDataURL(cElement.files[0]);
    };
    this.prev = function (event, id) {
        var reader = new FileReader();
        reader.onload = function () {
            $("#" + id).attr('src', reader.result);
        };
        reader.readAsDataURL(event.target.files[0]);
    };
    this.prevMultipleImages = function (input, holder) {
        if (input.files) {
            var filesAmount = input.files.length;
            $("." + holder).empty();
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    var html = '<div class="col-xl-2 col-lg-2 col-md-2">';
                    html += '<img class="img-thumbnail" src="' + event.target.result + '">';
                    html += "</div>";
                    $("." + holder).append(html);
                };
                reader.readAsDataURL(input.files[i]);
            }
        }
    };
    this.searchDiv = function (cElement, id) {
        var filter = $(cElement).val().toLowerCase();
        var nodes = document.getElementById(id).childNodes;
        for (i = 0; i < nodes.length; i++) {
            if (nodes[i].nodeName.toLowerCase() == 'div') {
                if (nodes[i].innerText.toLowerCase().includes(filter)) {
                    nodes[i].style.display = "block";
                } else {
                    nodes[i].style.display = "none";
                }
            }
        }
    };

    this.showHideDeleteAll = function (cElement) {
        if ($(cElement).hasClass('sub_chk')) {
            if ($(cElement).is(':checked', true)) {
                $(".delete_all").show();
            } else {
                $(".delete_all").hide();
            }
            if ($(".sub_chk:checked").length > 0) {
                $(".delete_all").show();
            } else {
                $(".delete_all").hide();
            }
        } else {
            if ($(cElement).is(':checked', true)) {
                $(".sub_chk").prop('checked', true);
                $(".delete_all").show();
            } else {
                $(".sub_chk").prop('checked', false);
                $(".delete_all").hide();
            }
        }
    };
    /*Settings*/
    this.generate_lrm_setting = function (id, parent) {

        var messageText = parent[0].toUpperCase() + parent.slice(1);
        messageText = messageText.replace('_', ' ');
        $.xPrompt({
            header: "Create new " + messageText,
            placeholder: 'Please Enter the Name'
        }, function (i) {

            $('#' + id).prepend('<option>' + i + '</option>');
            $('#' + id).prev('input').val(i);
            Ajax.setAjaxHeader();
            Ajax.call('/dashboard/lrm-settings', {
                'name': i,
                'parent': parent,
                'is_ajax': true
            }, 'POST', function (response) {

                if (response.status == true) {

                    $("#pt-Cancel").click();

                }

            });

        });

    }
    /*Ministry*/
    this.generate_ministry = function (id, type) {

        $.xPrompt({
            header: "Create new " + type[0].toUpperCase() + type.slice(1),
            placeholder: 'Please Enter the Name'
        }, function (i) {

            $('#' + id).prepend('<option>' + i + '</option>');
            $('#' + id).prev('input').val(i);
            Ajax.setAjaxHeader();
            Ajax.call('/dashboard/lrm-ministry', {
                'name': i,
                'is_ajax': true
            }, 'POST', function (response) {

                if (response.status == true) {

                    $("#pt-Cancel").click();

                }
            });
        });
    }
    /*Organization*/
    this.generate_organization = function (id, title, type) {

        $.xPrompt({
            header: "Create new " + title[0].toUpperCase() + title.slice(1),
            placeholder: 'Please Enter the Name'
        }, function (i) {

            $('#' + id).prepend('<option>' + i + '</option>');
            $('#' + id).prev('input').val(i);
            Ajax.setAjaxHeader();
            Ajax.call('/dashboard/lrm-organization', {
                'name': i,
                'is_ajax': true,
                'type': type
            }, 'POST', function (response) {

                if (response.status == true) {

                    $("#pt-Cancel").click();

                }
            });
        });
    }
    /*Wings*/
    this.generate_wing = function (id, title, type) {

        $.xPrompt({
            header: "Create new " + title[0].toUpperCase() + title.slice(1),
            placeholder: 'Please Enter the Name'
        }, function (i) {

            $('#' + id).prepend('<option>' + i + '</option>');
            $('#' + id).prev('input').val(i);
            Ajax.setAjaxHeader();
            Ajax.call('/dashboard/lrm-wings', {
                'name': i,
                'is_ajax': true,
                'type': type
            }, 'POST', function (response) {

                if (response.status == true) {

                    $("#pt-Cancel").click();

                }
            });
        });
    }
    /*Generate Department*/
    this.generate_department = function (id, title, type) {

        $.xPrompt({
            header: "Create new " + title[0].toUpperCase() + title.slice(1),
            placeholder: 'Please Enter the Name'
        }, function (i) {

            $('#' + id).prepend('<option>' + i + '</option>');
            $('#' + id).prev('input').val(i);
            Ajax.setAjaxHeader();
            Ajax.call('/dashboard/lrm-departments', {
                'name': i,
                'is_ajax': true,
                'type': type
            }, 'POST', function (response) {

                if (response.status == true) {

                    $("#pt-Cancel").click();

                }
            });
        });
    }
    /*Generate Rank*/
    this.generate_rank = function (id, title, type) {

        $.xPrompt({
            header: "Create new " + title[0].toUpperCase() + title.slice(1),
            placeholder: 'Please Enter the Name'
        }, function (i) {

            $('#' + id).prepend('<option>' + i + '</option>');
            $('#' + id).prev('input').val(i);
            Ajax.setAjaxHeader();
            Ajax.call('/dashboard/lrm-ranks', {
                'name': i,
                'is_ajax': true,
                'type': type
            }, 'POST', function (response) {

                if (response.status == true) {

                    $("#pt-Cancel").click();

                }
            });
        });
    };

    this.generate_location = function (id, title, type) {

        $.xPrompt({
            header: "Create new " + title[0].toUpperCase() + title.slice(1),
            placeholder: 'Please Enter the Name'
        }, function (i) {

            $('#' + id).prepend('<option>' + i + '</option>');
            $('#' + id).prev('input').val(i);
            Ajax.setAjaxHeader();
            Ajax.call('/dashboard/location-' + type, {

                'name': i,
                'is_ajax': true

            }, 'POST', function (response) {

                if (response.status == true) {

                    $("#pt-Cancel").click();

                }
            });
        });
    }
};


$(function () {

    $('input[type="checkbox"]').click(function () {
        if ($(this).prop("checked") == true) {
            $(this).val(1);
        } else {
            $(this).val(0);
        }
    });

    setTimeout(function () {
        if ($("#snackbar").is(":visible")) {
            $("#snackbar").fadeOut("fast");
        }
    }, 3000);


});
